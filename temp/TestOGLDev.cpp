#include <stdio.h>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <GL/glew.h>
#include <GL/freeglut.h>
#include "math_3d.h"

using namespace std;

GLuint VAO;
GLuint VBO;
GLuint programId;

static void RenderSceneCB() {
    glClear(GL_COLOR_BUFFER_BIT);
//    
//    float projectionModelviewMatrix[16];
//
//    //Just set it to identity matrix
//    memset(projectionModelviewMatrix, 0, sizeof (float)*16);
//    projectionModelviewMatrix[0] = 1.0;
//    projectionModelviewMatrix[5] = 1.0;
//    projectionModelviewMatrix[10] = 1.0;
//    projectionModelviewMatrix[15] = 1.0;
//
////    glEnableVertexAttribArray(0);
////    glBindBuffer(GL_ARRAY_BUFFER, VBO);
////    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
//    
////    glUseProgram(programId);
////    glUniformMatrix4fv(glGetUniformLocation(programId, "ProjectionModelviewMatrix"),
////            1, FALSE, projectionModelviewMatrix);
//    
//    glUseProgram(0);
    
    glBindVertexArray(VAO);
    //glDrawArrays(GL_TRIANGLES, 0, 3);
    glDrawRangeElements(GL_TRIANGLES, 0, 3, 3, GL_UNSIGNED_SHORT, NULL);
    glBindVertexArray(0);
    // glDisableVertexAttribArray(0);

   // glUseProgram(0);
    glutSwapBuffers();
}

static void InitializeGlutCallbacks() {
    glutDisplayFunc(RenderSceneCB);
}

static void CreateVertexBuffer() {
    Vector3f Vertices[3];
    Vertices[0] = Vector3f(-1.0f, -1.0f, 0.0f);
    Vertices[1] = Vector3f(1.0f, -1.0f, 0.0f);
    Vertices[2] = Vector3f(0.0f, 1.0f, 0.0f);
    
    GLuint IBOID;
    glGenBuffers(1, &IBOID);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBOID);
    GLushort indices[] = { 0, 1, 2 };
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, 3 * sizeof(GLushort), indices, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    glGenVertexArrays(1, &VAO);
    glBindVertexArray(VAO);

    glGenBuffers(1, &VBO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof (Vertices), Vertices, GL_STATIC_DRAW);
    
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vector3f), 0);
    glEnableVertexAttribArray(0);
    
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBOID);

    glBindVertexArray(0);
}

// loadFile - loads text file into char* fname
// allocates memory - so need to delete after use
// size of file returned in fSize

std::string loadFile(const char *fname) {
    std::ifstream file(fname);
    if (!file.is_open()) {
        cout << "Unable to open file " << fname << endl;
        exit(1);
    }

    std::stringstream fileData;
    fileData << file.rdbuf();
    file.close();

    return fileData.str();
}


// printShaderInfoLog
// From OpenGL Shading Language 3rd Edition, p215-216
// Display (hopefully) useful error messages if shader fails to compile

void printShaderInfoLog(GLint shader) {
    int infoLogLen = 0;
    int charsWritten = 0;
    GLchar *infoLog;

    glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLogLen);

    if (infoLogLen > 0) {
        infoLog = new GLchar[infoLogLen];
        // error check for fail to allocate memory omitted
        glGetShaderInfoLog(shader, infoLogLen, &charsWritten, infoLog);
        cout << "InfoLog : " << endl << infoLog << endl;
        delete [] infoLog;
    }
}

int LoadShader(const char *pfilePath_vs, const char *pfilePath_fs, bool bindTexCoord0, bool bindNormal, bool bindColor, GLuint &shaderProgram, GLuint &vertexShader, GLuint &fragmentShader) {
    shaderProgram = 0;
    vertexShader = 0;
    fragmentShader = 0;

    // load shaders & get length of each
    int vlen;
    int flen;
    std::string vertexShaderString = loadFile(pfilePath_vs);
    std::string fragmentShaderString = loadFile(pfilePath_fs);
    vlen = vertexShaderString.length();
    flen = fragmentShaderString.length();

    if (vertexShaderString.empty()) {
        return -1;
    }

    if (fragmentShaderString.empty()) {
        return -1;
    }

    vertexShader = glCreateShader(GL_VERTEX_SHADER);
    fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);

    const char *vertexShaderCStr = vertexShaderString.c_str();
    const char *fragmentShaderCStr = fragmentShaderString.c_str();
    glShaderSource(vertexShader, 1, (const GLchar **) &vertexShaderCStr, &vlen);
    glShaderSource(fragmentShader, 1, (const GLchar **) &fragmentShaderCStr, &flen);

    GLint compiled;

    glCompileShader(vertexShader);
    glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &compiled);
    if (compiled == FALSE) {
        cout << "Vertex shader not compiled." << endl;
        printShaderInfoLog(vertexShader);

        glDeleteShader(vertexShader);
        vertexShader = 0;
        glDeleteShader(fragmentShader);
        fragmentShader = 0;

        return -1;
    }

    glCompileShader(fragmentShader);
    glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &compiled);
    if (compiled == FALSE) {
        cout << "Fragment shader not compiled." << endl;
        printShaderInfoLog(fragmentShader);

        glDeleteShader(vertexShader);
        vertexShader = 0;
        glDeleteShader(fragmentShader);
        fragmentShader = 0;

        return -1;
    }

    shaderProgram = glCreateProgram();

    glAttachShader(shaderProgram, vertexShader);
    glAttachShader(shaderProgram, fragmentShader);

    glBindAttribLocation(shaderProgram, 0, "InVertex");

    if (bindTexCoord0)
        glBindAttribLocation(shaderProgram, 1, "InTexCoord0");

    if (bindNormal)
        glBindAttribLocation(shaderProgram, 2, "InNormal");

    if (bindColor)
        glBindAttribLocation(shaderProgram, 3, "InColor");

    glLinkProgram(shaderProgram);

    GLint IsLinked;
    glGetProgramiv(shaderProgram, GL_LINK_STATUS, (GLint *) & IsLinked);
    if (IsLinked == FALSE) {
        cout << "Failed to link shader." << endl;

        GLint maxLength;
        glGetProgramiv(shaderProgram, GL_INFO_LOG_LENGTH, &maxLength);
        if (maxLength > 0) {
            char *pLinkInfoLog = new char[maxLength];
            glGetProgramInfoLog(shaderProgram, maxLength, &maxLength, pLinkInfoLog);
            cout << pLinkInfoLog << endl;
            delete [] pLinkInfoLog;
        }

        glDetachShader(shaderProgram, vertexShader);
        glDetachShader(shaderProgram, fragmentShader);
        glDeleteShader(vertexShader);
        vertexShader = 0;
        glDeleteShader(fragmentShader);
        fragmentShader = 0;
        glDeleteProgram(shaderProgram);
        shaderProgram = 0;

        return -1;
    }

    return 1; //Success
}

int main(int argc, char** argv) {
    glutInit(&argc, argv);

    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);
    glutInitWindowSize(1024, 768);
    glutInitWindowPosition(100, 100);
    
    glutInitContextVersion(3, 3);
    glutInitContextFlags(GLUT_COMPATIBILITY_PROFILE);
    
    glutCreateWindow("Tutorial 03");
    // Must be done after glut is initialized!
    glewExperimental = TRUE;
    GLenum res = glewInit();
    if (res != GLEW_OK) {
        fprintf(stderr, "Error: '%s'\n", glewGetErrorString(res));
        return 1;
    }
    
//    glutInit(&argc, argv);
//    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH | GLUT_STENCIL);
//    //We want to make a GL 3.3 context
//    glutInitContextVersion(2, 0);
//    glutInitContextFlags(GLUT_COMPATIBILITY_PROFILE);
//    glutInitWindowPosition(100, 50);
//    glutInitWindowSize(600, 600);
//    glutCreateWindow("GL 3.3 Test");
//
//    //Currently, GLEW uses glGetString(GL_EXTENSIONS) which is not legal code
//    //in GL 3.3, therefore GLEW would fail if we don't set this to TRUE.
//    //GLEW will avoid looking for extensions and will just get function pointers for all GL functions.
//    glewExperimental = TRUE;
//    GLenum err = glewInit();
//    if (err != GLEW_OK) {
//        //Problem: glewInit failed, something is seriously wrong.
//        cout << "glewInit failed, aborting." << endl;
//        exit(1);
//    }
 
    InitializeGlutCallbacks();

    int OpenGLVersion[2];
    cout << "OpenGL version = " << glGetString(GL_VERSION) << endl;

    //This is the new way for getting the GL version.
    //It returns integers. Much better than the old glGetString(GL_VERSION).
    glGetIntegerv(GL_MAJOR_VERSION, &OpenGLVersion[0]);
    glGetIntegerv(GL_MINOR_VERSION, &OpenGLVersion[1]);
    cout << "OpenGL major version = " << OpenGLVersion[0] << endl;
    cout << "OpenGL minor version = " << OpenGLVersion[1] << endl << endl;

    glClearColor(0.0f, 1.0f, 0.0f, 1.0f);
//    
//    GLuint vsId;
//    GLuint fsId;
//    if (LoadShader("Shader1.vert", "Shader1.frag", false, false, true, programId, vsId, fsId) == -1) {
//        exit(1);
//    }

    CreateVertexBuffer();

    glutMainLoop();

    return 0;
}


