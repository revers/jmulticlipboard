package jmulticlip.gui.graph.util;

import jmulticlip.gui.graph.tools.action.CanvasAction;
import java.util.Iterator;

/**
 *
 * @author Revers
 */
public class UndoableList<E> implements Iterable<E> {

    private static final int INIT_SIZE = 128;
    private Object[] painters = new Object[INIT_SIZE];
    protected int index = 0;
    private int size = 0;
    private int trueSize = 0;

    public void add(E painter) {
        if (index >= painters.length) {
            Object[] oldCanvasActions = painters;
            painters = new CanvasAction[painters.length * 2];
            System.arraycopy(oldCanvasActions, 0, painters, 0, oldCanvasActions.length);

            for (int i = 0; i < oldCanvasActions.length; i++) {
                oldCanvasActions[i] = null;
            }
        }
        painters[index++] = painter;
        size++;

        for (int i = size; i < trueSize; i++) {
            painters[i] = null;
        }

        trueSize = size;

    }

    public E get(int index) {
        return (E) painters[index];
    }

    public int getSize() {
        return size;
    }

    public boolean undoAdd() {
        if (canUndo()) {
            index--;
            size--;
            return true;
        }
        return false;
    }

    public boolean redoAdd() {
        if (canRedo()) {
            index++;
            size++;

            return true;
        }
        return false;
    }

    public boolean canUndo() {
        return index > 0;
    }

    public boolean canRedo() {
        return trueSize > size;
    }

    public E getLastElement() {
        if (size == 0) {
            return null;
        }
        return get(size - 1);
    }

    @Override
    public Iterator<E> iterator() {
        return new Iterator<E>() {

            private int itIndex = 0;

            @Override
            public boolean hasNext() {
                return itIndex < getSize();
            }

            @Override
            public E next() {
                return get(itIndex++);
            }

            @Override
            public void remove() {
            }
        };
    }
}
