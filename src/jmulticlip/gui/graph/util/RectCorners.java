package jmulticlip.gui.graph.util;

import java.awt.Point;
import java.awt.Rectangle;

/**
 *
 * @author Revers
 */
public class RectCorners {

    public Point upperLeft = new Point();
    public Point upperRight = new Point();
    public Point bottomLeft = new Point();
    public Point bottomRight = new Point();

    public void setCorners(Rectangle rect) {

        upperLeft.x = rect.x;
        upperLeft.y = rect.y;
        upperRight.x = rect.x + rect.width;
        upperRight.y = rect.y;
        bottomLeft.x = rect.x;
        bottomLeft.y = rect.y + rect.height;
        bottomRight.x = rect.x + rect.width;
        bottomRight.y = rect.y + rect.height;
    }

    public boolean isInside(Point p) {
        if (p.x > upperLeft.x && p.x < upperRight.x
                && p.y > upperLeft.y && p.y < bottomLeft.y) {
            return true;
        }

        return false;
    }
}
