package jmulticlip.gui.graph.util;

import java.awt.Cursor;

/**
 *
 * @author Revers
 */
public interface Cursors {

    public static final Cursor CURSOR_MOVE = new Cursor(Cursor.MOVE_CURSOR);
    public static final Cursor CURSOR_DEFAULT = Cursor.getDefaultCursor();
    public static final Cursor CURSOR_RESIZE_E = new Cursor(Cursor.E_RESIZE_CURSOR);
    public static final Cursor CURSOR_RESIZE_W = new Cursor(Cursor.W_RESIZE_CURSOR);
    public static final Cursor CURSOR_RESIZE_N = new Cursor(Cursor.N_RESIZE_CURSOR);
    public static final Cursor CURSOR_RESIZE_S = new Cursor(Cursor.S_RESIZE_CURSOR);
    public static final Cursor CURSOR_RESIZE_NE = new Cursor(Cursor.NE_RESIZE_CURSOR);
    public static final Cursor CURSOR_RESIZE_NW = new Cursor(Cursor.NW_RESIZE_CURSOR);
    public static final Cursor CURSOR_RESIZE_SE = new Cursor(Cursor.SE_RESIZE_CURSOR);
    public static final Cursor CURSOR_RESIZE_SW = new Cursor(Cursor.SW_RESIZE_CURSOR);
    public static final Cursor CURSOR_HAND = new Cursor(Cursor.HAND_CURSOR);
}
