package jmulticlip.gui.graph;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Rectangle2D;

import javax.swing.*;
import javax.swing.border.*;
import jmulticlip.util.GUIUtils;

public class ToolbarBorder implements Border {

    private static final int OFFSET = 4;
    private static final Insets HORIZONTAL_INSETS = new Insets(0, OFFSET, 0, 0);
    private static final Insets VERTICAL_INSETS = new Insets(OFFSET, 0, 0, 0);
    private boolean horizontal = true;

    public ToolbarBorder() {
    }

    public ToolbarBorder(boolean horizontal) {
        this.horizontal = horizontal;
    }

    @Override
    public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
        g.setColor(
                GUIUtils.PANEL_COLOR);
                //Color.black);
        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        
        int dots = 7;
        
        if (horizontal) {
            g.fillRect(x, y, OFFSET, height);
                        int separation = (height - dots * DIAMETER - 2*MARGIN) / (dots - 1) + DIAMETER;
            int xDot = 0;
            int yDot = MARGIN + RADIUS;

            for (int i = 0; i < dots; i++) {
                draw3dDot(g, xDot, yDot, RADIUS);
                yDot += separation;
            }
        } else {

            g.fillRect(x, y, width, OFFSET);
            
            int separation = (width - dots * DIAMETER - 2*MARGIN) / (dots - 1) + DIAMETER;
            int xDot = MARGIN + RADIUS;
            int yDot = 0;

            for (int i = 0; i < dots; i++) {
                draw3dDot(g, xDot, yDot, RADIUS);
                xDot += separation;
            }


        }
    }
    private static final int MARGIN = 3;
    private static final int RADIUS = 2;
    private static final int DIAMETER = RADIUS * 2;
    private static final Color DOT_COLOR = new Color(160, 160, 160);

    private void draw3dDot(Graphics g, int x, int y, int diameter) {

        g.setColor(Color.white);
        g.fillOval(x + diameter / 2, y + diameter / 2, diameter, diameter);

        g.setColor(DOT_COLOR);
        g.fillOval(x, y, diameter, diameter);


    }

    @Override
    public Insets getBorderInsets(Component c) {
        if (horizontal) {
            return HORIZONTAL_INSETS;
        } else {
            return VERTICAL_INSETS;
        }
    }

    @Override
    public boolean isBorderOpaque() {
        return true;
    }

    public boolean isHorizontal() {
        return horizontal;
    }

    public void setHorizontal(boolean horizontal) {
        this.horizontal = horizontal;
    }
}