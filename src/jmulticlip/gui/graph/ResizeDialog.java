/*
 * ResizeDialog.java
 *
 * Created on 2010-09-23, 14:24:48
 */
package jmulticlip.gui.graph;

import java.awt.*;
import java.awt.event.*;
import java.beans.PropertyChangeListener;
import java.util.Enumeration;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import pl.ugu.revers.swing.SwingUtils;
import jmulticlip.gui.graph.components.*;
import jmulticlip.gui.graph.tools.CanvasResizer;
import jmulticlip.util.GUIUtils;

/**
 *
 * @author Revers
 */
public class ResizeDialog extends javax.swing.JDialog {

    private JRadioButton pxSizeCB = new JRadioButton("Ustaw nowy rozmiar", true);
    private JRadioButton percentSizeCB = new JRadioButton("Ustaw nowy rozmiar procentowo", false);
    private JRadioButton predefinedSizeCB = new JRadioButton("Wybierz predefiniowany rozmiar", false);
    private CanvasPanel canvasPanel;
    private Dimension originalSize;
    private Dimension newSize;
    private boolean canChangeValue = true;
    private boolean canceled = true;

    public ResizeDialog(Window parent, CanvasPanel canvasPanel) {
        super(parent, ModalityType.TOOLKIT_MODAL);
        this.canvasPanel = canvasPanel;

        initComponents();
        setupPanelBorders();

        GUIUtils.useApplicationLAF(this);
        SwingUtils.setLocationOnCenter(parent, this);

        buttonGroup.clearSelection();

        if (canvasPanel != null) {
            Image image = canvasPanel.getCanvasImage();
            originalSize = new Dimension(image.getWidth(null), image.getHeight(null));
            //System.out.println(originalSize);
            newSize = new Dimension(originalSize.width, originalSize.height);

            pxWidthTF.setText(Integer.toString(newSize.width));
            pxHeightTF.setText(Integer.toString(newSize.height));

            showNewSize();
            originalSizeJL.setText(newSize.width + " x " + newSize.height);
        }

        addListeners();

        pxWidthTF.setAcceptOnlyNonNegative(true);
        pxHeightTF.setAcceptOnlyNonNegative(true);
        percentWidthTF.setAcceptOnlyNonNegative(true);
        percentHeightTF.setAcceptOnlyNonNegative(true);
    }

    private void addListeners() {
        predefinedSizeCB.addChangeListener(new ChangeListener() {

            public void stateChanged(ChangeEvent e) {
                if (predefinedSizeCB.isSelected() == false) {
                    buttonGroup.clearSelection();
                    preserveAspectRatioCB.setEnabled(true);
                } else {
                    preserveAspectRatioCB.setEnabled(false);
                }
            }
        });

        pxSizeCB.addChangeListener(new ChangeListener() {

            public void stateChanged(ChangeEvent e) {
                if (pxSizeCB.isSelected()) {
                    showPxSize();
                }
            }
        });

        percentSizeCB.addChangeListener(new ChangeListener() {

            public void stateChanged(ChangeEvent e) {
                if (percentSizeCB.isSelected()) {
                    showPercentSize();
                }
            }
        });

        ActionListener action = new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                AbstractButton b = (AbstractButton) e.getSource();
                if (b.isSelected() == false) {
                    return;
                }

                String text = b.getText();
                newSizeJL.setText(text);
                String[] dimText = text.split(" x ");
                newSize.width = Integer.parseInt(dimText[0]);
                newSize.height = Integer.parseInt(dimText[1]);
            }
        };
        Enumeration<AbstractButton> buttons = buttonGroup.getElements();
        while (buttons.hasMoreElements()) {
            AbstractButton b = buttons.nextElement();
            b.addActionListener(action);
        }


        pxWidthTF.getDocument().addDocumentListener(new DocumentListener() {

            public void insertUpdate(DocumentEvent e) {
                //   System.out.println("insert update pxWidth");
                if (canChangeValue) {
                    contentChanged(e);
                }
            }

            public void removeUpdate(DocumentEvent e) {
                //   System.out.println("remove update pxWidth");
                if (canChangeValue) {
                    contentChanged(e);
                }
            }

            public void changedUpdate(DocumentEvent e) {
                //      System.out.println("changed update pxWidth");
                if (canChangeValue) {
                    contentChanged(e);
                }
            }

            private void contentChanged(DocumentEvent e) {
                if (preserveAspectRatioCB.isSelected()) {
                    canChangeValue = false;
//                    SwingUtilities.invokeLater(new Runnable() {
//
//                        public void run() {



                    float sourceVal = 0.0f;
                    if (pxWidthTF.getText().equals("") == false
                            && (sourceVal = Float.parseFloat(pxWidthTF.getText())) != 0.0f) {
                        float origWidth = (float) originalSize.width;
                        float origHeight = (float) originalSize.height;

                        int x = Math.round((origHeight * sourceVal) / origWidth);
                        if (x == 0) {
                            x = 1;
                        }

                        pxHeightTF.setText(Integer.toString(x));
                    }

                    canChangeValue = true;
//                        }
//                    });
                }
                showPxSize();
            }
        });

        pxHeightTF.getDocument().addDocumentListener(new DocumentListener() {

            //private boolean canChangeValue = true;
            public void insertUpdate(DocumentEvent e) {
                //   System.out.println("insert update pxheight");
                if (canChangeValue) {
                    contentChanged(e);
                }
            }

            public void removeUpdate(DocumentEvent e) {
                //   System.out.println("remove update pxheight");
                if (canChangeValue) {
                    contentChanged(e);
                }
            }

            public void changedUpdate(DocumentEvent e) {
                // System.out.println("changed update pxheight");
                if (canChangeValue) {
                    contentChanged(e);
                }
            }

            private void contentChanged(DocumentEvent e) {
                if (preserveAspectRatioCB.isSelected()) {
                    canChangeValue = false;

                    float sourceVal = 0.0f;
                    if (pxHeightTF.getText().equals("") == false
                            && (sourceVal = Float.parseFloat(pxHeightTF.getText())) != 0.0f) {
                        float origWidth = (float) originalSize.width;
                        float origHeight = (float) originalSize.height;

                        int x = Math.round((origWidth * sourceVal) / origHeight);
                        if (x == 0) {
                            x = 1;
                        }

                        pxWidthTF.setText(Integer.toString(x));
                    }

                    canChangeValue = true;
                }
                showPxSize();
            }
        });


        percentWidthTF.getDocument().addDocumentListener(new DocumentListener() {

            public void insertUpdate(DocumentEvent e) {
                if (canChangeValue) {
                    contentChanged(e);
                }
            }

            public void removeUpdate(DocumentEvent e) {
                if (canChangeValue) {
                    contentChanged(e);
                }
            }

            public void changedUpdate(DocumentEvent e) {
                if (canChangeValue) {
                    contentChanged(e);
                }
            }

            private void contentChanged(DocumentEvent e) {
                if (preserveAspectRatioCB.isSelected()) {
                    canChangeValue = false;
                    percentHeightTF.setText(percentWidthTF.getText());
                    canChangeValue = true;
                }
                showPercentSize();
            }
        });


        percentHeightTF.getDocument().addDocumentListener(new DocumentListener() {

            public void insertUpdate(DocumentEvent e) {
                if (canChangeValue) {
                    contentChanged(e);
                }
            }

            public void removeUpdate(DocumentEvent e) {
                if (canChangeValue) {
                    contentChanged(e);
                }
            }

            public void changedUpdate(DocumentEvent e) {
                if (canChangeValue) {
                    contentChanged(e);
                }
            }

            private void contentChanged(DocumentEvent e) {
                if (preserveAspectRatioCB.isSelected()) {
                    canChangeValue = false;
                    percentWidthTF.setText(percentHeightTF.getText());
                    canChangeValue = true;
                }
                showPercentSize();
            }
        });
    }

    private void showPercentSize() {
        if (percentWidthTF.getText().equals("") == false) {
            int wPrc = Integer.parseInt(percentWidthTF.getText());
            newSize.width = Math.round((float) wPrc * (float) originalSize.width / 100.0f);
        }
        if (percentHeightTF.getText().equals("") == false) {
            int hPrc = Integer.parseInt(percentHeightTF.getText());
            newSize.height = Math.round((float) hPrc * (float) originalSize.height / 100.0f);
        }
        showNewSize();
    }

    private void showPxSize() {
        if (pxWidthTF.getText().equals("") == false) {
            newSize.width = Integer.parseInt(pxWidthTF.getText());
        }
        if (pxHeightTF.getText().equals("") == false) {
            newSize.height = Integer.parseInt(pxHeightTF.getText());
        }
        showNewSize();
    }

    private void showNewSize() {
        newSizeJL.setText(newSize.width + " x " + newSize.height);
    }

    private void setupPanelBorders() {
        setSubcomponentsEnabled(pxSizePanel, true);
        setSubcomponentsEnabled(percentSizePanel, false);
        setSubcomponentsEnabled(predefinedSizePanel, false);

        pxSizeCB.setFocusPainted(false);
        percentSizeCB.setFocusPainted(false);
        predefinedSizeCB.setFocusPainted(false);

        ComponentTitledBorder pxSizeBorder = new ComponentTitledBorder(
                pxSizeCB, pxSizePanel, BorderFactory.createEtchedBorder());
        pxSizePanel.setBorder(pxSizeBorder);

        ComponentTitledBorder percentSizeBorder = new ComponentTitledBorder(
                percentSizeCB, percentSizePanel, BorderFactory.createEtchedBorder());
        percentSizePanel.setBorder(percentSizeBorder);

        ComponentTitledBorder predefinedSizeBorder = new ComponentTitledBorder(
                predefinedSizeCB, predefinedSizePanel, BorderFactory.createEtchedBorder());
        predefinedSizePanel.setBorder(predefinedSizeBorder);

        pxSizeCB.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                if (pxSizeCB.isSelected() == false) {
                    pxSizeCB.setSelected(true);
                }

                setSubcomponentsEnabled(pxSizePanel, true);
                setSubcomponentsEnabled(percentSizePanel, false);
                setSubcomponentsEnabled(predefinedSizePanel, false);
                percentSizeCB.setSelected(false);
                predefinedSizeCB.setSelected(false);

                repaintBorderCBs();
            }
        });

        percentSizeCB.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {

                if (percentSizeCB.isSelected() == false) {
                    percentSizeCB.setSelected(true);
                }

                setSubcomponentsEnabled(pxSizePanel, false);
                setSubcomponentsEnabled(percentSizePanel, true);
                setSubcomponentsEnabled(predefinedSizePanel, false);
                pxSizeCB.setSelected(false);
                predefinedSizeCB.setSelected(false);

                repaintBorderCBs();
            }
        });

        predefinedSizeCB.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {

                if (predefinedSizeCB.isSelected() == false) {
                    predefinedSizeCB.setSelected(true);
                }

                setSubcomponentsEnabled(pxSizePanel, false);
                setSubcomponentsEnabled(percentSizePanel, false);
                setSubcomponentsEnabled(predefinedSizePanel, true);
                percentSizeCB.setSelected(false);
                pxSizeCB.setSelected(false);

                repaintBorderCBs();
            }
        });
    }

    private void setSubcomponentsEnabled(JPanel parent, boolean enabled) {
        Component comp[] = parent.getComponents();
        for (int i = 0; i < comp.length; i++) {
            comp[i].setEnabled(enabled);
        }
    }

    private void repaintBorderCBs() {
        pxSizePanel.repaint();
        percentSizePanel.repaint();
        predefinedSizePanel.repaint();

    }

    public boolean isCanceled() {
        return canceled;
    }
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        buttonGroup = new javax.swing.ButtonGroup();
        infoPanel = new javax.swing.JPanel();
        atucalSizeTxtJL = new javax.swing.JLabel();
        newSizeTxtJL = new javax.swing.JLabel();
        originalSizeJL = new javax.swing.JLabel();
        newSizeJL = new javax.swing.JLabel();
        preserveAspectRatioCB = new javax.swing.JCheckBox();
        pxSizePanel = new javax.swing.JPanel();
        pxWidthTxtJL = new javax.swing.JLabel();
        pxWidthTF = new jmulticlip.gui.graph.components.IntegerTextField();
        pxHeightTxtJL = new javax.swing.JLabel();
        pxHeightTF = new jmulticlip.gui.graph.components.IntegerTextField();
        percentSizePanel = new javax.swing.JPanel();
        percentWidthTxtJL = new javax.swing.JLabel();
        percentHeightTxtJL = new javax.swing.JLabel();
        percentWidthTF = new jmulticlip.gui.graph.components.IntegerTextField();
        prcJL1 = new javax.swing.JLabel();
        percentHeightTF = new jmulticlip.gui.graph.components.IntegerTextField();
        prcJL2 = new javax.swing.JLabel();
        predefinedSizePanel = new javax.swing.JPanel();
        doubleJB = new javax.swing.JButton();
        halfJB = new javax.swing.JButton();
        predefinedSizeJL6 = new javax.swing.JRadioButton();
        predefinedSizeJL5 = new javax.swing.JRadioButton();
        predefinedSizeJL4 = new javax.swing.JRadioButton();
        predefinedSizeJL3 = new javax.swing.JRadioButton();
        predefinedSizeJL2 = new javax.swing.JRadioButton();
        predefinedSizeJL1 = new javax.swing.JRadioButton();
        predefinedSizeJL7 = new javax.swing.JRadioButton();
        predefinedSizeJL9 = new javax.swing.JRadioButton();
        predefinedSizeJL11 = new javax.swing.JRadioButton();
        predefinedSizeJL10 = new javax.swing.JRadioButton();
        predefinedSizeJL8 = new javax.swing.JRadioButton();
        cancelJB = new javax.swing.JButton();
        okJB = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Zmień Rozmiar");
        setResizable(false);
        getContentPane().setLayout(new java.awt.GridBagLayout());

        infoPanel.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        infoPanel.setLayout(new java.awt.GridBagLayout());

        atucalSizeTxtJL.setText("Aktualny rozmiar:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(15, 10, 5, 5);
        infoPanel.add(atucalSizeTxtJL, gridBagConstraints);

        newSizeTxtJL.setText("Nowy rozmiar:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 10, 5, 5);
        infoPanel.add(newSizeTxtJL, gridBagConstraints);

        originalSizeJL.setText("1280 x 1024");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(15, 0, 5, 10);
        infoPanel.add(originalSizeJL, gridBagConstraints);

        newSizeJL.setText("800 x 600");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 5, 10);
        infoPanel.add(newSizeJL, gridBagConstraints);

        preserveAspectRatioCB.setSelected(true);
        preserveAspectRatioCB.setText("Zachowaj stosunek boków");
        preserveAspectRatioCB.setFocusPainted(false);
        preserveAspectRatioCB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                preserveAspectRatioCBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.PAGE_END;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 10, 10);
        infoPanel.add(preserveAspectRatioCB, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(10, 10, 0, 0);
        getContentPane().add(infoPanel, gridBagConstraints);

        pxSizePanel.setBorder(javax.swing.BorderFactory.createTitledBorder("temp2"));
        pxSizePanel.setLayout(new java.awt.GridBagLayout());

        pxWidthTxtJL.setText("Szerokość:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 10, 10, 5);
        pxSizePanel.add(pxWidthTxtJL, gridBagConstraints);

        pxWidthTF.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        pxWidthTF.setText("1280");
        pxWidthTF.setPreferredSize(new java.awt.Dimension(40, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 10, 31);
        pxSizePanel.add(pxWidthTF, gridBagConstraints);

        pxHeightTxtJL.setText("Wysokość:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 10, 5);
        pxSizePanel.add(pxHeightTxtJL, gridBagConstraints);

        pxHeightTF.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        pxHeightTF.setText("1024");
        pxHeightTF.setPreferredSize(new java.awt.Dimension(40, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 10, 26);
        pxSizePanel.add(pxHeightTF, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(10, 10, 0, 0);
        getContentPane().add(pxSizePanel, gridBagConstraints);

        percentSizePanel.setBorder(javax.swing.BorderFactory.createTitledBorder("temp1"));
        percentSizePanel.setLayout(new java.awt.GridBagLayout());

        percentWidthTxtJL.setText("Szerokość:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 10, 10, 5);
        percentSizePanel.add(percentWidthTxtJL, gridBagConstraints);

        percentHeightTxtJL.setText("Wysokość:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 10, 5);
        percentSizePanel.add(percentHeightTxtJL, gridBagConstraints);

        percentWidthTF.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        percentWidthTF.setText("100");
        percentWidthTF.setPreferredSize(new java.awt.Dimension(40, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 10, 5);
        percentSizePanel.add(percentWidthTF, gridBagConstraints);

        prcJL1.setText("%");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 10, 15);
        percentSizePanel.add(prcJL1, gridBagConstraints);

        percentHeightTF.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        percentHeightTF.setText("100");
        percentHeightTF.setPreferredSize(new java.awt.Dimension(40, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 10, 5);
        percentSizePanel.add(percentHeightTF, gridBagConstraints);

        prcJL2.setText("%");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 10, 10);
        percentSizePanel.add(prcJL2, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(10, 10, 0, 0);
        getContentPane().add(percentSizePanel, gridBagConstraints);

        predefinedSizePanel.setBorder(javax.swing.BorderFactory.createTitledBorder("temp3"));
        predefinedSizePanel.setLayout(new java.awt.GridBagLayout());

        doubleJB.setText("Podwójny");
        doubleJB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                doubleJBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.PAGE_END;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 10, 10, 5);
        predefinedSizePanel.add(doubleJB, gridBagConstraints);

        halfJB.setText("Połowa");
        halfJB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                halfJBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.PAGE_END;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 10, 10);
        predefinedSizePanel.add(halfJB, gridBagConstraints);

        buttonGroup.add(predefinedSizeJL6);
        predefinedSizeJL6.setText("1920 x 1200");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 5, 10);
        predefinedSizePanel.add(predefinedSizeJL6, gridBagConstraints);

        buttonGroup.add(predefinedSizeJL5);
        predefinedSizeJL5.setText("1920 x 1080");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 10, 5, 5);
        predefinedSizePanel.add(predefinedSizeJL5, gridBagConstraints);

        buttonGroup.add(predefinedSizeJL4);
        predefinedSizeJL4.setText("1280 x 800");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 5, 10);
        predefinedSizePanel.add(predefinedSizeJL4, gridBagConstraints);

        buttonGroup.add(predefinedSizeJL3);
        predefinedSizeJL3.setText("1024 x 768");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 10, 5, 5);
        predefinedSizePanel.add(predefinedSizeJL3, gridBagConstraints);

        buttonGroup.add(predefinedSizeJL2);
        predefinedSizeJL2.setText("800 x 600");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 5, 10);
        predefinedSizePanel.add(predefinedSizeJL2, gridBagConstraints);

        buttonGroup.add(predefinedSizeJL1);
        predefinedSizeJL1.setSelected(true);
        predefinedSizeJL1.setText("640 x 480");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 10, 5, 5);
        predefinedSizePanel.add(predefinedSizeJL1, gridBagConstraints);

        buttonGroup.add(predefinedSizeJL7);
        predefinedSizeJL7.setText("1280 x 960");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 10, 5, 5);
        predefinedSizePanel.add(predefinedSizeJL7, gridBagConstraints);

        buttonGroup.add(predefinedSizeJL9);
        predefinedSizeJL9.setText("1280 x 720");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 10, 5, 5);
        predefinedSizePanel.add(predefinedSizeJL9, gridBagConstraints);

        buttonGroup.add(predefinedSizeJL11);
        predefinedSizeJL11.setText("1600 x 1200");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 10, 5, 5);
        predefinedSizePanel.add(predefinedSizeJL11, gridBagConstraints);

        buttonGroup.add(predefinedSizeJL10);
        predefinedSizeJL10.setText("1680 x 1050");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 5, 10);
        predefinedSizePanel.add(predefinedSizeJL10, gridBagConstraints);

        buttonGroup.add(predefinedSizeJL8);
        predefinedSizeJL8.setText("2560 x 1600");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 5, 10);
        predefinedSizePanel.add(predefinedSizeJL8, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.gridheight = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(10, 10, 0, 10);
        getContentPane().add(predefinedSizePanel, gridBagConstraints);

        cancelJB.setText("Anuluj");
        cancelJB.setPreferredSize(new java.awt.Dimension(70, 23));
        cancelJB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelJBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(10, 0, 10, 10);
        getContentPane().add(cancelJB, gridBagConstraints);

        okJB.setText("OK");
        okJB.setPreferredSize(new java.awt.Dimension(70, 23));
        okJB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                okJBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(10, 10, 10, 5);
        getContentPane().add(okJB, gridBagConstraints);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void doubleJBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_doubleJBActionPerformed
        buttonGroup.clearSelection();
        newSize.width *= 2;
        newSize.height *= 2;
        showNewSize();
    }//GEN-LAST:event_doubleJBActionPerformed

    private void halfJBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_halfJBActionPerformed
        buttonGroup.clearSelection();
        newSize.width /= 2;
        newSize.height /= 2;
        showNewSize();
    }//GEN-LAST:event_halfJBActionPerformed

    private void cancelJBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelJBActionPerformed
        dispose();
    }//GEN-LAST:event_cancelJBActionPerformed

    private void okJBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_okJBActionPerformed
        if (newSize.width == 0 || newSize.height == 0) {
            JOptionPane.showMessageDialog(this, "Żaden z wymiarów nie może być równy 0!"
                    + "\nSzerokość: " + newSize.width
                    + " pikseli\nWysokość: " + newSize.height
                    + " pikseli", "Niedozwolony Wymiar!",
                    JOptionPane.WARNING_MESSAGE);
            return;
        }

        if (newSize.equals(originalSize)) {
            dispose();
        }
        CanvasResizer resizer = new CanvasResizer(canvasPanel);
        resizer.resize(newSize.width, newSize.height);
        canceled = false;
        dispose();
    }//GEN-LAST:event_okJBActionPerformed

    private void preserveAspectRatioCBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_preserveAspectRatioCBActionPerformed
    }//GEN-LAST:event_preserveAspectRatioCBActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                JFrame frame = new JFrame();
                GUIUtils.useApplicationLAF(frame);
                ResizeDialog dialog = new ResizeDialog(frame, null);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {

                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel atucalSizeTxtJL;
    private javax.swing.ButtonGroup buttonGroup;
    private javax.swing.JButton cancelJB;
    private javax.swing.JButton doubleJB;
    private javax.swing.JButton halfJB;
    private javax.swing.JPanel infoPanel;
    private javax.swing.JLabel newSizeJL;
    private javax.swing.JLabel newSizeTxtJL;
    private javax.swing.JButton okJB;
    private javax.swing.JLabel originalSizeJL;
    private jmulticlip.gui.graph.components.IntegerTextField percentHeightTF;
    private javax.swing.JLabel percentHeightTxtJL;
    private javax.swing.JPanel percentSizePanel;
    private jmulticlip.gui.graph.components.IntegerTextField percentWidthTF;
    private javax.swing.JLabel percentWidthTxtJL;
    private javax.swing.JLabel prcJL1;
    private javax.swing.JLabel prcJL2;
    private javax.swing.JRadioButton predefinedSizeJL1;
    private javax.swing.JRadioButton predefinedSizeJL10;
    private javax.swing.JRadioButton predefinedSizeJL11;
    private javax.swing.JRadioButton predefinedSizeJL2;
    private javax.swing.JRadioButton predefinedSizeJL3;
    private javax.swing.JRadioButton predefinedSizeJL4;
    private javax.swing.JRadioButton predefinedSizeJL5;
    private javax.swing.JRadioButton predefinedSizeJL6;
    private javax.swing.JRadioButton predefinedSizeJL7;
    private javax.swing.JRadioButton predefinedSizeJL8;
    private javax.swing.JRadioButton predefinedSizeJL9;
    private javax.swing.JPanel predefinedSizePanel;
    private javax.swing.JCheckBox preserveAspectRatioCB;
    private jmulticlip.gui.graph.components.IntegerTextField pxHeightTF;
    private javax.swing.JLabel pxHeightTxtJL;
    private javax.swing.JPanel pxSizePanel;
    private jmulticlip.gui.graph.components.IntegerTextField pxWidthTF;
    private javax.swing.JLabel pxWidthTxtJL;
    // End of variables declaration//GEN-END:variables
}
