/*
 * DrawingPanel.java
 *
 * Created on 2010-07-14, 17:43:43
 */
package jmulticlip.gui.graph;

import jmulticlip.gui.graph.components.ColorIcon;
import jmulticlip.gui.graph.CommonProperties.Type;
import jmulticlip.gui.graph.tools.painter.PencilPainter;
import jmulticlip.gui.graph.tools.painter.TextPainter;
import jmulticlip.gui.graph.tools.painter.OvalPainter;
import jmulticlip.gui.graph.tools.painter.AbstractPainter;
import jmulticlip.gui.graph.tools.painter.LinePainter;
import jmulticlip.gui.graph.tools.painter.ImagePainter;
import jmulticlip.gui.graph.tools.action.PainterAction;
import jmulticlip.gui.graph.tools.tool.SelectionPainterTool;
import jmulticlip.gui.graph.tools.tool.LinePainterTool;
import jmulticlip.gui.graph.tools.tool.AbstractPainterTool;
import jmulticlip.gui.graph.tools.tool.OvalPainterTool;
import jmulticlip.gui.graph.tools.tool.TextPainterTool;
import jmulticlip.gui.graph.tools.tool.PencilPainterTool;
import java.beans.PropertyChangeEvent;
import java.util.logging.Level;
import java.util.logging.Logger;
import jmulticlip.gui.ViewType;
import jmulticlip.gui.graph.tools.*;
import java.awt.*;
import java.awt.datatransfer.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeListener;
import java.io.*;
import java.util.Locale;
import java.util.prefs.Preferences;
import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import jmulticlip.clipboard.ClipboardManager;
import jmulticlip.config.Options;
import jmulticlip.core.ErrorManager;
import jmulticlip.core.InitializationManager;
import jmulticlip.gui.BottomLineBorder;
import jmulticlip.gui.navig.ClipDataRow;
import jmulticlip.gui.LoadingViewPanel;
import jmulticlip.gui.MainFrame;
import jmulticlip.ifaces.NotificationListener;
import jmulticlip.gui.View;
import jmulticlip.gui.graph.components.ColorPopupMenu;
import jmulticlip.gui.graph.components.ComponentMenuItem;
import jmulticlip.gui.graph.tools.painter.BracketPainter;
import jmulticlip.gui.graph.tools.painter.BubblePainter;
import jmulticlip.gui.graph.tools.painter.RectanglePainter;
import jmulticlip.gui.graph.tools.tool.BracketPainterTool;
import jmulticlip.gui.graph.tools.tool.BubblePainterTool;
import jmulticlip.gui.graph.tools.tool.RectanglePainterTool;
import jmulticlip.gui.navig.NavigTablePanel;
import jmulticlip.ifaces.Initializable;
import jmulticlip.util.WavePlayer;
import jmulticlip.util.GUIUtils;
import jmulticlip.util.ImageUploader;
import pl.ugu.revers.swing.ErrorDialog;

/**
 *
 * @author Revers
 */
public class GraphicsViewPanel extends View implements CanvasListener, ChangeListener {

    private static final int BUTTONS_COUNT = 10;
    private static final int BUTTON_WIDTH = 30;
    private static final int BUTTON_HEIGHT = 25;
    private static final int BUTTON_OFFSET = 5;
    private static final Dimension HORIZONTAL_TOOLBAR_DIMENSION =
            new Dimension(BUTTONS_COUNT * BUTTON_WIDTH + (BUTTONS_COUNT + 2) * BUTTON_OFFSET,
            BUTTON_HEIGHT + 2 * BUTTON_OFFSET);
    private static final Dimension VERTICAL_TOOLBAR_DIMENSION =
            new Dimension(BUTTON_WIDTH + 2 * BUTTON_OFFSET,
            BUTTONS_COUNT * BUTTON_HEIGHT + (BUTTONS_COUNT + 2) * BUTTON_OFFSET);
    //  private static Color selectedColor = Color.BLACK;
    private Image image;
    private AbstractPainterTool currentPainterTool;
    private ToolbarBorder toolbarBorder;
    private boolean lastSelected;
    private ColorPopupMenu colorPopup;
    private KeyStroke ctrlZ = KeyStroke.getKeyStroke(KeyEvent.VK_Z, KeyEvent.CTRL_MASK, false);
    private KeyStroke ctrlY = KeyStroke.getKeyStroke(KeyEvent.VK_Y, KeyEvent.CTRL_MASK, false);
    private KeyStroke ctrlV = KeyStroke.getKeyStroke(KeyEvent.VK_V, KeyEvent.CTRL_MASK, true);
    private KeyStroke ctrlC = KeyStroke.getKeyStroke(KeyEvent.VK_C, KeyEvent.CTRL_MASK, true);
    private KeyStroke ctrlX = KeyStroke.getKeyStroke(KeyEvent.VK_X, KeyEvent.CTRL_MASK, true);
    private KeyStroke ctrlR = KeyStroke.getKeyStroke(KeyEvent.VK_R, KeyEvent.CTRL_MASK, true);
    private KeyStroke delete = KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0, true);

    public GraphicsViewPanel() {
        this(new BufferedImage(80, 60, BufferedImage.TYPE_4BYTE_ABGR));
    }

    /** Creates new form DrawingPanel */
    public GraphicsViewPanel(Image image) {
        this.image = image;

        initComponents();
        colorPopup = new ColorPopupMenu(colorJB, Type.COLOR);

        registerKeyboardAction(undoAction, "undo",
                ctrlZ, JComponent.WHEN_IN_FOCUSED_WINDOW);
        registerKeyboardAction(redoAction, "redo",
                ctrlY, JComponent.WHEN_IN_FOCUSED_WINDOW);
        registerKeyboardAction(pasteAction, "paste",
                ctrlV, JComponent.WHEN_IN_FOCUSED_WINDOW);
        registerKeyboardAction(copyAction, "copy",
                ctrlC, JComponent.WHEN_IN_FOCUSED_WINDOW);
        registerKeyboardAction(cutAction, "cut",
                ctrlX, JComponent.WHEN_IN_FOCUSED_WINDOW);
        registerKeyboardAction(deleteAction, "delete",
                delete, JComponent.WHEN_IN_FOCUSED_WINDOW);
        registerKeyboardAction(resizeAction, "resize",
                ctrlR, JComponent.WHEN_IN_FOCUSED_WINDOW);

//        colorCB.addActionListener(new ActionListener() {
//
//            public void actionPerformed(ActionEvent e) {
//                canvas.requestFocusInWindow();
//            }
//        });

        selectionToolbar.getCopyButton().addActionListener(copyAction);
        selectionToolbar.getDeleteButton().addActionListener(deleteAction);
        selectionToolbar.getCutButton().addActionListener(cutAction);
        selectionToolbar.getPasteButton().addActionListener(pasteAction);

        setCanvas(canvas);

        GUIUtils.useApplicationLAF(this);
        GUIUtils.useApplicationLAF(painterToolbar);

        //    InitializationManager.getInstance().addInitializable(this);

        editCurrentCB.addChangeListener(this);

        initToolbar();
    }

    private void initToolbar() {
        toolBarPanel.setBackground(GUIUtils.PANEL_COLOR);
        painterToolbar.setBackground(GUIUtils.PANEL_COLOR);

        GUIUtils.makeButtonFlat(curlyBracketsTB);
        GUIUtils.makeButtonFlat(eraseJB);
        GUIUtils.makeButtonFlat(lineTB);
        GUIUtils.makeButtonFlat(ovalTB);
        GUIUtils.makeButtonFlat(pencilTB);
        GUIUtils.makeButtonFlat(rectangleTB);
        GUIUtils.makeButtonFlat(selectionTB);
        GUIUtils.makeButtonFlat(textTB);
        GUIUtils.makeButtonFlat(bubbleTB);
        GUIUtils.makeButtonFlat(colorJB);

        toolbarBorder = new ToolbarBorder(false);
        painterToolbar.setBorder(toolbarBorder);

        BottomLineBorder border = new BottomLineBorder(Color.white);
        mainToolbarPanel.setBorder(border);
    }

    @Override
    public void stateChanged(ChangeEvent e) {
        if (lastSelected == editCurrentCB.isSelected()) {
            return;
        }

        lastSelected = editCurrentCB.isSelected();

        if (lastSelected) {
            editCurrentCB.setFont(GUIUtils.SELECTED_CB_FONT);
        } else {
            editCurrentCB.setFont(GUIUtils.NORMAL_CB_FONT);
        }

        editCurrentCB.repaint();
    }

    public void setCanvas(CanvasPanel canvasPanel) {
        canvas.removeMouseListener(canvasMouseListener);
        canvas.removeMouseMotionListener(canvasMouseMotionListener);
        canvas.removeCanvasListener(this);
        canvas.removeComponentListener(canvasComponentListener);

        this.canvas = canvasPanel;

        canvas.addMouseListener(canvasMouseListener);
        canvas.addMouseMotionListener(canvasMouseMotionListener);
        canvas.addCanvasListener(this);
        canvas.addComponentListener(canvasComponentListener);
        statusPanel.getPositionPanel().setCanvasSize(canvas.getSize());

        canvasScrollPane.setViewportView(canvas);

        redoJB.setEnabled(canvas.canRedo());
        undoJB.setEnabled(canvas.canUndo());

        setProperTool(currentPainterTool);

        showZoomPercent();

        // canvas.requestFocusInWindow();

        //useDefaultTool();
    }

    private void useDefaultTool() {
        setPainterTool(new SelectionPainterTool(canvas, selectionToolbar, statusPanel.getPainterInfoPanel()));
        ((CardLayout) toolBarPanel.getLayout()).show(toolBarPanel, "selectionCard");
        selectionTB.setSelected(true);
        //  canvas.requestFocusInWindow();
    }

    private void clearPainterTool() {
        if (currentPainterTool != null) {
            currentPainterTool.finish();
            currentPainterTool = null;

            ((CardLayout) toolBarPanel.getLayout()).show(toolBarPanel, "defaultCard");
        }
    }

    private void setPainterTool(AbstractPainterTool painterTool) {
        clearPainterTool();

        this.currentPainterTool = painterTool;

        this.currentPainterTool.start();
        //   canvas.requestFocusInWindow();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        buttonGroup = new javax.swing.ButtonGroup();
        toolBarPanel = new javax.swing.JPanel();
        defaultToolbar = new jmulticlip.gui.graph.tools.toolbar.DefaultToolbar();
        pencilToobar = new jmulticlip.gui.graph.tools.toolbar.PencilToolbar();
        bubbleToolbar = new jmulticlip.gui.graph.tools.toolbar.BubbleToolbar();
        rectangleToolbar = new jmulticlip.gui.graph.tools.toolbar.RectangleToolbar();
        bracketToolbar = new jmulticlip.gui.graph.tools.toolbar.BracketToolbar();
        lineToolbar = new jmulticlip.gui.graph.tools.toolbar.LineToolbar();
        ovalToolbar = new jmulticlip.gui.graph.tools.toolbar.OvalToolbar();
        textToolbar = new jmulticlip.gui.graph.tools.toolbar.TextToolbar();
        selectionToolbar = new jmulticlip.gui.graph.tools.toolbar.SelectionToolbar();
        mainToolbarPanel = new javax.swing.JPanel();
        zoomInJB = new javax.swing.JButton();
        redoJB = new javax.swing.JButton();
        uploadJB = new javax.swing.JButton();
        resizeJB = new javax.swing.JButton();
        zoomOutJB = new javax.swing.JButton();
        undoJB = new javax.swing.JButton();
        separator1 = new javax.swing.JSeparator();
        zoomOriginalJB = new javax.swing.JButton();
        separator3 = new javax.swing.JSeparator();
        uploadServerCB = new javax.swing.JComboBox();
        statusPanel = new jmulticlip.gui.graph.StatusPanel();
        editCurrentCB = new javax.swing.JCheckBox();
        canvasPanel = new javax.swing.JPanel();
        canvasScrollPane = new javax.swing.JScrollPane();
        canvas = new CanvasPanel(image);
        painterToolbar = new javax.swing.JToolBar();
        toolbarPanel = new javax.swing.JPanel();
        selectionTB = new javax.swing.JToggleButton();
        eraseJB = new javax.swing.JToggleButton();
        pencilTB = new javax.swing.JToggleButton();
        lineTB = new javax.swing.JToggleButton();
        ovalTB = new javax.swing.JToggleButton();
        rectangleTB = new javax.swing.JToggleButton();
        curlyBracketsTB = new javax.swing.JToggleButton();
        bubbleTB = new javax.swing.JToggleButton();
        textTB = new javax.swing.JToggleButton();
        colorJB = new javax.swing.JButton();

        setLayout(new java.awt.GridBagLayout());

        toolBarPanel.setLayout(new java.awt.CardLayout());

        javax.swing.GroupLayout defaultToolbarLayout = new javax.swing.GroupLayout(defaultToolbar);
        defaultToolbar.setLayout(defaultToolbarLayout);
        defaultToolbarLayout.setHorizontalGroup(
            defaultToolbarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1049, Short.MAX_VALUE)
        );
        defaultToolbarLayout.setVerticalGroup(
            defaultToolbarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 22, Short.MAX_VALUE)
        );

        toolBarPanel.add(defaultToolbar, "defaultCard");
        toolBarPanel.add(pencilToobar, "pencilCard");
        toolBarPanel.add(bubbleToolbar, "bubbleCard");
        toolBarPanel.add(rectangleToolbar, "rectangleCard");
        toolBarPanel.add(bracketToolbar, "bracketCard");
        toolBarPanel.add(lineToolbar, "lineCard");
        toolBarPanel.add(ovalToolbar, "ovalCard");
        toolBarPanel.add(textToolbar, "textCard");
        toolBarPanel.add(selectionToolbar, "selectionCard");

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 10, 5, 10);
        add(toolBarPanel, gridBagConstraints);

        mainToolbarPanel.setPreferredSize(new java.awt.Dimension(406, 34));
        mainToolbarPanel.setLayout(new java.awt.GridBagLayout());

        zoomInJB.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/zoom_in.png"))); // NOI18N
        zoomInJB.setMinimumSize(new java.awt.Dimension(30, 25));
        zoomInJB.setPreferredSize(new java.awt.Dimension(30, 25));
        zoomInJB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                zoomInJBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 5);
        mainToolbarPanel.add(zoomInJB, gridBagConstraints);

        redoJB.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Redo.png"))); // NOI18N
        redoJB.setEnabled(false);
        redoJB.setMinimumSize(new java.awt.Dimension(30, 25));
        redoJB.setPreferredSize(new java.awt.Dimension(30, 25));
        redoJB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                redoJBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 9;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 5);
        mainToolbarPanel.add(redoJB, gridBagConstraints);

        uploadJB.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/upload.png"))); // NOI18N
        uploadJB.setMinimumSize(new java.awt.Dimension(30, 25));
        uploadJB.setPreferredSize(new java.awt.Dimension(30, 25));
        uploadJB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                uploadJBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 5);
        mainToolbarPanel.add(uploadJB, gridBagConstraints);

        resizeJB.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/icon_resize.gif"))); // NOI18N
        resizeJB.setMinimumSize(new java.awt.Dimension(30, 25));
        resizeJB.setPreferredSize(new java.awt.Dimension(30, 25));
        resizeJB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                resizeJBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 5);
        mainToolbarPanel.add(resizeJB, gridBagConstraints);

        zoomOutJB.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/zoom_out.png"))); // NOI18N
        zoomOutJB.setMinimumSize(new java.awt.Dimension(30, 25));
        zoomOutJB.setPreferredSize(new java.awt.Dimension(30, 25));
        zoomOutJB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                zoomOutJBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 5);
        mainToolbarPanel.add(zoomOutJB, gridBagConstraints);

        undoJB.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Undo.png"))); // NOI18N
        undoJB.setEnabled(false);
        undoJB.setMinimumSize(new java.awt.Dimension(30, 25));
        undoJB.setPreferredSize(new java.awt.Dimension(30, 25));
        undoJB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                undoJBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 5);
        mainToolbarPanel.add(undoJB, gridBagConstraints);

        separator1.setOrientation(javax.swing.SwingConstants.VERTICAL);
        separator1.setMinimumSize(new java.awt.Dimension(2, 25));
        separator1.setPreferredSize(new java.awt.Dimension(2, 25));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 5);
        mainToolbarPanel.add(separator1, gridBagConstraints);

        zoomOriginalJB.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/zoom_original.png"))); // NOI18N
        zoomOriginalJB.setEnabled(false);
        zoomOriginalJB.setMinimumSize(new java.awt.Dimension(30, 25));
        zoomOriginalJB.setPreferredSize(new java.awt.Dimension(30, 25));
        zoomOriginalJB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                zoomOriginalJBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 5);
        mainToolbarPanel.add(zoomOriginalJB, gridBagConstraints);

        separator3.setOrientation(javax.swing.SwingConstants.VERTICAL);
        separator3.setMinimumSize(new java.awt.Dimension(2, 25));
        separator3.setPreferredSize(new java.awt.Dimension(2, 25));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 7;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 5);
        mainToolbarPanel.add(separator3, gridBagConstraints);

        uploadServerCB.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "BankFotek", "Imageshack" }));
        uploadServerCB.setMinimumSize(new java.awt.Dimension(77, 25));
        uploadServerCB.setPreferredSize(new java.awt.Dimension(82, 25));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 5);
        mainToolbarPanel.add(uploadServerCB, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(10, 10, 0, 10);
        add(mainToolbarPanel, gridBagConstraints);

        statusPanel.setMinimumSize(new java.awt.Dimension(175, 25));
        statusPanel.setPreferredSize(new java.awt.Dimension(328, 22));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(10, 10, 10, 10);
        add(statusPanel, gridBagConstraints);

        editCurrentCB.setFont(new java.awt.Font("Tahoma", 1, 11));
        editCurrentCB.setSelected(true);
        editCurrentCB.setText("Edytuj bieżący obiekt");
        editCurrentCB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editCurrentCBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.insets = new java.awt.Insets(10, 10, 10, 0);
        add(editCurrentCB, gridBagConstraints);

        canvasPanel.setPreferredSize(new java.awt.Dimension(100, 100));
        canvasPanel.setLayout(new java.awt.BorderLayout());

        canvasScrollPane.setMinimumSize(new java.awt.Dimension(20, 20));
        canvasScrollPane.setPreferredSize(new java.awt.Dimension(100, 100));

        javax.swing.GroupLayout canvasLayout = new javax.swing.GroupLayout(canvas);
        canvas.setLayout(canvasLayout);
        canvasLayout.setHorizontalGroup(
            canvasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1, Short.MAX_VALUE)
        );
        canvasLayout.setVerticalGroup(
            canvasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1, Short.MAX_VALUE)
        );

        canvasScrollPane.setViewportView(canvas);

        canvasPanel.add(canvasScrollPane, java.awt.BorderLayout.CENTER);

        painterToolbar.setBorder(null);
        painterToolbar.setOrientation(1);
        painterToolbar.setRollover(true);
        painterToolbar.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                painterToolbarPropertyChange(evt);
            }
        });

        toolbarPanel.setPreferredSize(new java.awt.Dimension(40, 280));
        toolbarPanel.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEADING));

        buttonGroup.add(selectionTB);
        selectionTB.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/selectionTool.png"))); // NOI18N
        selectionTB.setFocusPainted(false);
        selectionTB.setFocusable(false);
        selectionTB.setMaximumSize(new java.awt.Dimension(30, 25));
        selectionTB.setMinimumSize(new java.awt.Dimension(30, 25));
        selectionTB.setPreferredSize(new java.awt.Dimension(30, 25));
        selectionTB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                selectionTBActionPerformed(evt);
            }
        });
        toolbarPanel.add(selectionTB);

        buttonGroup.add(eraseJB);
        eraseJB.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/eraser.png"))); // NOI18N
        eraseJB.setFocusPainted(false);
        eraseJB.setFocusable(false);
        eraseJB.setMaximumSize(new java.awt.Dimension(30, 25));
        eraseJB.setMinimumSize(new java.awt.Dimension(30, 25));
        eraseJB.setPreferredSize(new java.awt.Dimension(30, 25));
        toolbarPanel.add(eraseJB);

        buttonGroup.add(pencilTB);
        pencilTB.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/pencil.png"))); // NOI18N
        pencilTB.setFocusPainted(false);
        pencilTB.setFocusable(false);
        pencilTB.setMaximumSize(new java.awt.Dimension(30, 25));
        pencilTB.setMinimumSize(new java.awt.Dimension(30, 25));
        pencilTB.setPreferredSize(new java.awt.Dimension(30, 25));
        pencilTB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pencilTBActionPerformed(evt);
            }
        });
        toolbarPanel.add(pencilTB);

        buttonGroup.add(lineTB);
        lineTB.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/arrow16-2.png"))); // NOI18N
        lineTB.setFocusPainted(false);
        lineTB.setFocusable(false);
        lineTB.setMaximumSize(new java.awt.Dimension(30, 25));
        lineTB.setMinimumSize(new java.awt.Dimension(30, 25));
        lineTB.setPreferredSize(new java.awt.Dimension(30, 25));
        lineTB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                lineTBActionPerformed(evt);
            }
        });
        toolbarPanel.add(lineTB);

        buttonGroup.add(ovalTB);
        ovalTB.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/ellipse16.png"))); // NOI18N
        ovalTB.setFocusPainted(false);
        ovalTB.setFocusable(false);
        ovalTB.setMaximumSize(new java.awt.Dimension(30, 25));
        ovalTB.setMinimumSize(new java.awt.Dimension(30, 25));
        ovalTB.setPreferredSize(new java.awt.Dimension(30, 25));
        ovalTB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ovalTBActionPerformed(evt);
            }
        });
        toolbarPanel.add(ovalTB);

        buttonGroup.add(rectangleTB);
        rectangleTB.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/shape_square_gray.png"))); // NOI18N
        rectangleTB.setFocusPainted(false);
        rectangleTB.setFocusable(false);
        rectangleTB.setMaximumSize(new java.awt.Dimension(30, 25));
        rectangleTB.setMinimumSize(new java.awt.Dimension(30, 25));
        rectangleTB.setPreferredSize(new java.awt.Dimension(30, 25));
        rectangleTB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rectangleTBActionPerformed(evt);
            }
        });
        toolbarPanel.add(rectangleTB);

        buttonGroup.add(curlyBracketsTB);
        curlyBracketsTB.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/curly bracket16.png"))); // NOI18N
        curlyBracketsTB.setFocusPainted(false);
        curlyBracketsTB.setFocusable(false);
        curlyBracketsTB.setMaximumSize(new java.awt.Dimension(30, 25));
        curlyBracketsTB.setMinimumSize(new java.awt.Dimension(30, 25));
        curlyBracketsTB.setPreferredSize(new java.awt.Dimension(30, 25));
        curlyBracketsTB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                curlyBracketsTBActionPerformed(evt);
            }
        });
        toolbarPanel.add(curlyBracketsTB);

        buttonGroup.add(bubbleTB);
        bubbleTB.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/speech_bubble2.gif"))); // NOI18N
        bubbleTB.setFocusable(false);
        bubbleTB.setMinimumSize(new java.awt.Dimension(30, 25));
        bubbleTB.setPreferredSize(new java.awt.Dimension(30, 25));
        bubbleTB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bubbleTBActionPerformed(evt);
            }
        });
        toolbarPanel.add(bubbleTB);

        buttonGroup.add(textTB);
        textTB.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/text_font.png"))); // NOI18N
        textTB.setFocusPainted(false);
        textTB.setFocusable(false);
        textTB.setMaximumSize(new java.awt.Dimension(30, 25));
        textTB.setMinimumSize(new java.awt.Dimension(30, 25));
        textTB.setPreferredSize(new java.awt.Dimension(30, 25));
        textTB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                textTBActionPerformed(evt);
            }
        });
        toolbarPanel.add(textTB);

        colorJB.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/blackSquare16.png"))); // NOI18N
        colorJB.setFocusPainted(false);
        colorJB.setFocusable(false);
        colorJB.setMaximumSize(new java.awt.Dimension(30, 25));
        colorJB.setMinimumSize(new java.awt.Dimension(30, 25));
        colorJB.setPreferredSize(new java.awt.Dimension(30, 25));
        colorJB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                colorJBActionPerformed(evt);
            }
        });
        toolbarPanel.add(colorJB);

        painterToolbar.add(toolbarPanel);

        canvasPanel.add(painterToolbar, java.awt.BorderLayout.WEST);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 10);
        add(canvasPanel, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents
    private void selectionTBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_selectionTBActionPerformed
        setPainterTool(new SelectionPainterTool(canvas, selectionToolbar, statusPanel.getPainterInfoPanel()));

        ((CardLayout) toolBarPanel.getLayout()).show(toolBarPanel, "selectionCard");
        canvas.requestFocusInWindow();
    }//GEN-LAST:event_selectionTBActionPerformed
    private void pencilTBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pencilTBActionPerformed
        //  canvas.setMode(CanvasPanel.Mode.EDIT);
        setPainterTool(new PencilPainterTool(canvas, pencilToobar,
                statusPanel.getPainterInfoPanel()));

        ((CardLayout) toolBarPanel.getLayout()).show(toolBarPanel, "pencilCard");
        canvas.requestFocusInWindow();
    }//GEN-LAST:event_pencilTBActionPerformed
    private void lineTBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_lineTBActionPerformed
        // canvas.setMode(CanvasPanel.Mode.EDIT);
        setPainterTool(new LinePainterTool(canvas, lineToolbar,
                statusPanel.getPainterInfoPanel()));
        ((CardLayout) toolBarPanel.getLayout()).show(toolBarPanel, "lineCard");
        canvas.requestFocusInWindow();
    }//GEN-LAST:event_lineTBActionPerformed
    private void ovalTBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ovalTBActionPerformed
        //  canvas.setMode(CanvasPanel.Mode.EDIT);
        setPainterTool(new OvalPainterTool(canvas, ovalToolbar,
                statusPanel.getPainterInfoPanel()));

        ((CardLayout) toolBarPanel.getLayout()).show(toolBarPanel, "ovalCard");
        canvas.requestFocusInWindow();
    }//GEN-LAST:event_ovalTBActionPerformed
    private void textTBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_textTBActionPerformed
        //canvas.setMode(CanvasPanel.Mode.EDIT);
        setPainterTool(new TextPainterTool(canvas, textToolbar,
                statusPanel.getPainterInfoPanel(), editCurrentCB));

        ((CardLayout) toolBarPanel.getLayout()).show(toolBarPanel, "textCard");
        canvas.requestFocusInWindow();
    }//GEN-LAST:event_textTBActionPerformed

    private void undoJBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_undoJBActionPerformed

        if (currentPainterTool != null) {
            currentPainterTool.finish();
        }
        if (canvas.undo()) {
            if (editCurrentCB.isSelected()) {
                setProperTool(canvas.getCurrentPainter());
            }
            redoJB.setEnabled(canvas.canRedo());
            undoJB.setEnabled(canvas.canUndo());
            canvas.requestFocusInWindow();
        }

        if (currentPainterTool != null && currentPainterTool.isStarted() == false) {
            currentPainterTool.start();
        }
        if (editCurrentCB.isSelected() == false) {
            statusPanel.getPainterInfoPanel().setVisible(false);
        }
        canvas.requestFocusInWindow();
    }//GEN-LAST:event_undoJBActionPerformed

    private void redoJBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_redoJBActionPerformed
        if (currentPainterTool != null) {
            currentPainterTool.finish();
        }
        if (canvas.redo()) {
            if (editCurrentCB.isSelected()) {
                setProperTool(canvas.getCurrentPainter());
            }
            redoJB.setEnabled(canvas.canRedo());
            undoJB.setEnabled(canvas.canUndo());
            canvas.requestFocusInWindow();
        }

        if (currentPainterTool != null && currentPainterTool.isStarted() == false) {
            currentPainterTool.start();
        }
        if (editCurrentCB.isSelected() == false) {
            statusPanel.getPainterInfoPanel().setVisible(false);
        }
        canvas.requestFocusInWindow();
    }//GEN-LAST:event_redoJBActionPerformed
    private void colorCBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_colorCBActionPerformed
    }//GEN-LAST:event_colorCBActionPerformed

    private void resizeJBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_resizeJBActionPerformed
        clearPainterTool();
        Component parent = getParent();
        Window window = null;


        while (parent != null && window == null) {
            try {
                window = (Window) parent;
            } catch (Exception e) {
            }
            parent = parent.getParent();
        }

        ResizeDialog dialog = new ResizeDialog(window, canvas);

        dialog.setVisible(true);
        if (dialog.isCanceled() == false) {
            setCanvas(canvas);
        }
//        statusPanel.getPositionPanel().setZoom(canvas.getZoom());
//        redoJB.setEnabled(canvas.canRedo());
//        undoJB.setEnabled(canvas.canUndo());
//        canvas.requestFocusInWindow();
    }//GEN-LAST:event_resizeJBActionPerformed
    private void showZoomPercent() {
        statusPanel.getPositionPanel().setZoom(canvas.getZoom());

        if (canvas.getZoom() == 1.0f) {
            zoomOriginalJB.setEnabled(false);
        } else {
            zoomOriginalJB.setEnabled(true);
        }
    }
    private void zoomInJBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_zoomInJBActionPerformed
        //  cleanPainterTool();
        canvas.setZoom(canvas.getZoom() * 2.0f);
        showZoomPercent();
        canvas.requestFocusInWindow();
    }//GEN-LAST:event_zoomInJBActionPerformed

    private void zoomOutJBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_zoomOutJBActionPerformed
        //   cleanPainterTool();
        canvas.setZoom(canvas.getZoom() / 2.0f);
        showZoomPercent();
        canvas.requestFocusInWindow();

    }//GEN-LAST:event_zoomOutJBActionPerformed

    private void editCurrentCBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editCurrentCBActionPerformed

        canvas.setEditCurrent(editCurrentCB.isSelected());
        if (canvas.getCurrentPainter() != null) {
            setProperTool(canvas.getCurrentPainter());
        }

        if (editCurrentCB.isSelected() == false) {
            statusPanel.getPainterInfoPanel().setVisible(false);
        }
        canvas.requestFocusInWindow();
    }//GEN-LAST:event_editCurrentCBActionPerformed

    public BufferedImage getImage() {
        clearPainterTool();
        return canvas.getCanvasImage();
    }

    public void saveImage() {
        BufferedImage bi = getImage();
        if (bi == null) {
            return;
        }

        SaveUtil.saveImage(bi, this);
    }

    private void uploadJBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_uploadJBActionPerformed
        clearPainterTool();

        final BufferedImage img = canvas.getCanvasImage();
        final ImageUploader imageUploader = new ImageUploader();

        final NavigTablePanel navigTablePanel = MainFrame.getInstance().getNavigationPanel().getNavigTablePanel();

        final ClipDataRow clipData = navigTablePanel.getCurrentClipDataRow();
        final UploadAction action = new UploadAction(clipData, imageUploader);

        new Thread() {

            @Override
            public void run() {
                try {
                    ImageUploader.ImageLinks links = imageUploader.upload(img, action);

                    if (links != null) {
                        navigTablePanel.addTextData(links.getHtmlLink());
                        navigTablePanel.addTextData(links.getForumLink());

                        ClipboardManager.getInstance().setClipbardText(links.getLink());

                        if (Options.getInstance().getSoundAfterUpload()) {
                            WavePlayer.beep();
                        }
                    }
                    action.restoreView();
                } catch (IOException ex) {

                    action.restoreView();
                    ErrorManager.errorNormal(GraphicsViewPanel.class, ex, "Cannot upload image to BankFotek.pl!!");

                }
            }
        }.start();
        canvas.requestFocusInWindow();
    }//GEN-LAST:event_uploadJBActionPerformed

private void painterToolbarPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_painterToolbarPropertyChange
    String name = evt.getPropertyName();

    if (name.equals("ancestor") || name.equals("orientation")) {

        // "hack" for floating toolbar:
        boolean hor = false;
        if (painterToolbar.getParent() != null
                && painterToolbar.getParent().getName() != null
                && painterToolbar.getParent().getName().startsWith("null.content")) {
            hor = true;
        }

        int orientation = painterToolbar.getOrientation();
        if (orientation == 0 || hor) {
            toolbarBorder.setHorizontal(true);
            toolbarPanel.setPreferredSize(HORIZONTAL_TOOLBAR_DIMENSION);
            toolbarPanel.repaint();

        } else if (orientation == 1) {
            toolbarBorder.setHorizontal(false);
            toolbarPanel.setPreferredSize(VERTICAL_TOOLBAR_DIMENSION);
        }
    }
}//GEN-LAST:event_painterToolbarPropertyChange

private void zoomOriginalJBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_zoomOriginalJBActionPerformed
    canvas.setZoom(1.0f);
    showZoomPercent();
    canvas.requestFocusInWindow();
}//GEN-LAST:event_zoomOriginalJBActionPerformed

private void colorJBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_colorJBActionPerformed
    SwingUtilities.updateComponentTreeUI(colorPopup);
    colorPopup.show(painterToolbar, colorJB.getX() + 28, colorJB.getY() + 5);
    canvas.requestFocusInWindow();
}//GEN-LAST:event_colorJBActionPerformed

private void rectangleTBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rectangleTBActionPerformed
    //  canvas.setMode(CanvasPanel.Mode.EDIT);
    setPainterTool(new RectanglePainterTool(canvas, rectangleToolbar,
            statusPanel.getPainterInfoPanel()));

    ((CardLayout) toolBarPanel.getLayout()).show(toolBarPanel, "rectangleCard");
    canvas.requestFocusInWindow();
}//GEN-LAST:event_rectangleTBActionPerformed

private void curlyBracketsTBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_curlyBracketsTBActionPerformed
    setPainterTool(new BracketPainterTool(canvas, bracketToolbar,
            statusPanel.getPainterInfoPanel()));

    ((CardLayout) toolBarPanel.getLayout()).show(toolBarPanel, "bracketCard");
    canvas.requestFocusInWindow();
}//GEN-LAST:event_curlyBracketsTBActionPerformed

private void bubbleTBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bubbleTBActionPerformed
    setPainterTool(new BubblePainterTool(canvas, bubbleToolbar,
            statusPanel.getPainterInfoPanel()));

    ((CardLayout) toolBarPanel.getLayout()).show(toolBarPanel, "bubbleCard");
    canvas.requestFocusInWindow();
}//GEN-LAST:event_bubbleTBActionPerformed

    private class UploadAction implements Runnable, NotificationListener {

        private ClipDataRow clipDataRow;
        private ImageUploader imageUploader;
        private LoadingViewPanel loadingPanel;

        public UploadAction(ClipDataRow clipDataRow, ImageUploader imageUploader) {
            this.clipDataRow = clipDataRow;
            this.imageUploader = imageUploader;

            loadingPanel = new LoadingViewPanel(this);
            loadingPanel.getLabel().setText("Trwa wysyłanie zdjecia " + clipDataRow.getShortText() + " na BankFotek...");
            clipDataRow.setCurrentView(loadingPanel);

            MainFrame.getInstance().refreshCurrentView();
        }

        public void run() {
            imageUploader.cancel();
            restoreView();
        }

        public void notify(String msg) {
            loadingPanel.getLabel().setText("Trwa wysyłanie zdjecia "
                    + clipDataRow.getShortText() + " (" + msg + ")  na BankFotek...");
        }

        public void restoreView() {
            clipDataRow.setCurrentView(clipDataRow.getDefaultView());
            MainFrame.getInstance().refreshCurrentView();
        }
    }

    @Override
    public void canvasChanged(CanvasPanel canvas) {
        undoJB.setEnabled(canvas.canUndo());
        redoJB.setEnabled(canvas.canRedo());
    }

    private void setProperTool(AbstractPainter painter) {
        buttonGroup.clearSelection();

        clearPainterTool();
        if (painter instanceof PencilPainter) {

            setPainterTool(new PencilPainterTool(canvas,
                    pencilToobar, (PencilPainter) painter, statusPanel.getPainterInfoPanel()));

            ((CardLayout) toolBarPanel.getLayout()).show(toolBarPanel, "pencilCard");
            pencilTB.setSelected(true);
        } else if (painter instanceof LinePainter) {
            setPainterTool(new LinePainterTool(canvas,
                    lineToolbar, (LinePainter) painter, statusPanel.getPainterInfoPanel()));
            ((CardLayout) toolBarPanel.getLayout()).show(toolBarPanel, "lineCard");
            lineTB.setSelected(true);
        } else if (painter instanceof OvalPainter) {
            setPainterTool(new OvalPainterTool(canvas,
                    ovalToolbar, (OvalPainter) painter, statusPanel.getPainterInfoPanel()));
            ((CardLayout) toolBarPanel.getLayout()).show(toolBarPanel, "ovalCard");
            ovalTB.setSelected(true);
        } else if (painter instanceof TextPainter) {
            setPainterTool(new TextPainterTool(canvas, textToolbar,
                    (TextPainter) painter, statusPanel.getPainterInfoPanel(), editCurrentCB));
            ((CardLayout) toolBarPanel.getLayout()).show(toolBarPanel, "textCard");
            textTB.setSelected(true);
        } else if (painter instanceof ImagePainter) {
            //System.out.println("dfadfsa");
            setPainterTool(new SelectionPainterTool(canvas, selectionToolbar, (ImagePainter) painter,
                    statusPanel.getPainterInfoPanel()));
            ((CardLayout) toolBarPanel.getLayout()).show(toolBarPanel, "selectionCard");
            selectionTB.setSelected(true);
        } else if (painter instanceof RectanglePainter) {
            setPainterTool(new RectanglePainterTool(canvas, rectangleToolbar, (RectanglePainter) painter,
                    statusPanel.getPainterInfoPanel()));

            ((CardLayout) toolBarPanel.getLayout()).show(toolBarPanel, "rectangleCard");
            rectangleTB.setSelected(true);
        } else if (painter instanceof BracketPainter) {
            setPainterTool(new BracketPainterTool(canvas, bracketToolbar, (BracketPainter) painter,
                    statusPanel.getPainterInfoPanel()));

            ((CardLayout) toolBarPanel.getLayout()).show(toolBarPanel, "bracketCard");

            curlyBracketsTB.setSelected(true);
        } else if (painter instanceof BubblePainter) {
            setPainterTool(new BubblePainterTool(canvas, bubbleToolbar, (BubblePainter) painter,
                    statusPanel.getPainterInfoPanel()));

            ((CardLayout) toolBarPanel.getLayout()).show(toolBarPanel, "bubbleCard");

            bubbleTB.setSelected(true);
        } else {
            useDefaultTool();
        }
    }

    private void setProperTool(AbstractPainterTool painter) {
        buttonGroup.clearSelection();

        clearPainterTool();
        if (painter instanceof PencilPainterTool) {

            setPainterTool(new PencilPainterTool(canvas,
                    pencilToobar, statusPanel.getPainterInfoPanel()));

            ((CardLayout) toolBarPanel.getLayout()).show(toolBarPanel, "pencilCard");
            pencilTB.setSelected(true);
        } else if (painter instanceof LinePainterTool) {
            setPainterTool(new LinePainterTool(canvas,
                    lineToolbar, statusPanel.getPainterInfoPanel()));
            ((CardLayout) toolBarPanel.getLayout()).show(toolBarPanel, "lineCard");
            lineTB.setSelected(true);
        } else if (painter instanceof OvalPainterTool) {
            setPainterTool(new OvalPainterTool(canvas,
                    ovalToolbar, statusPanel.getPainterInfoPanel()));
            ((CardLayout) toolBarPanel.getLayout()).show(toolBarPanel, "ovalCard");
            ovalTB.setSelected(true);
        } else if (painter instanceof TextPainterTool) {
            setPainterTool(new TextPainterTool(canvas, textToolbar,
                    statusPanel.getPainterInfoPanel(), editCurrentCB));
            ((CardLayout) toolBarPanel.getLayout()).show(toolBarPanel, "textCard");
            textTB.setSelected(true);
        } else if (painter instanceof SelectionPainterTool) {
            //System.out.println("dfadfsa");
            setPainterTool(new SelectionPainterTool(canvas, selectionToolbar,
                    statusPanel.getPainterInfoPanel()));
            ((CardLayout) toolBarPanel.getLayout()).show(toolBarPanel, "selectionCard");
            selectionTB.setSelected(true);
        } else if (painter instanceof RectanglePainterTool) {
            setPainterTool(new RectanglePainterTool(canvas, rectangleToolbar,
                    statusPanel.getPainterInfoPanel()));

            ((CardLayout) toolBarPanel.getLayout()).show(toolBarPanel, "rectangleCard");
            rectangleTB.setSelected(true);
        } else if (painter instanceof BracketPainterTool) {
            setPainterTool(new BracketPainterTool(canvas, bracketToolbar,
                    statusPanel.getPainterInfoPanel()));

            ((CardLayout) toolBarPanel.getLayout()).show(toolBarPanel, "bracketCard");
            curlyBracketsTB.setSelected(true);
        } else {
            useDefaultTool();
        }
    }
    private Action undoAction = new AbstractAction() {

        @Override
        public void actionPerformed(ActionEvent e) {
            undoJBActionPerformed(e);
        }
    };
    private Action redoAction = new AbstractAction() {

        @Override
        public void actionPerformed(ActionEvent e) {
            redoJBActionPerformed(e);
        }
    };
    private Action pasteAction = new AbstractAction() {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (canvas == null) {
                return;
            }

            Transferable t = Toolkit.getDefaultToolkit().getSystemClipboard().getContents(null);

            if (t == null) {
                return;
            }

            AbstractPainter painter = null;
            try {
                if (t.isDataFlavorSupported(ImagePainter.getDataFlavor())) {
                    painter = new ImagePainter((Image) t.getTransferData(ImagePainter.getDataFlavor()));
                } else if (t.isDataFlavorSupported(LinePainter.getDataFlavor())) {
                    painter = ((LinePainter) t.getTransferData(LinePainter.getDataFlavor())).clonePainter();
                } else if (t.isDataFlavorSupported(OvalPainter.getDataFlavor())) {
                    painter = ((OvalPainter) t.getTransferData(OvalPainter.getDataFlavor())).clonePainter();
                } else if (t.isDataFlavorSupported(PencilPainter.getDataFlavor())) {
                    painter = ((PencilPainter) t.getTransferData(PencilPainter.getDataFlavor())).clonePainter();
                } else if (t.isDataFlavorSupported(RectanglePainter.getDataFlavor())) {
                    painter = ((RectanglePainter) t.getTransferData(RectanglePainter.getDataFlavor())).clonePainter();
                } else if (t.isDataFlavorSupported(BracketPainter.getDataFlavor())) {
                    painter = ((BracketPainter) t.getTransferData(BracketPainter.getDataFlavor())).clonePainter();
                } else if (t.isDataFlavorSupported(DataFlavor.stringFlavor)) {
                    if (currentPainterTool != null && currentPainterTool instanceof TextPainterTool
                            && canvas.getCurrentPainter() instanceof TextPainter) {
                        return;
                    }
                    TextPainter textPainter = new TextPainter();

                    String text = (String) t.getTransferData(DataFlavor.stringFlavor);

                    textPainter.setText(text);

                    textPainter.setFontFamily(textToolbar.getFontFamily());
                    textPainter.setFontSize(textToolbar.getFontSize());
                    textPainter.setColor(CommonProperties.getInstance().getColor());
                    textPainter.setAlignment(textToolbar.getTextAlignment());
                    textPainter.setBold(textToolbar.isBold());
                    textPainter.setItalic(textToolbar.isItalic());
                    textPainter.setStrikethrough(textToolbar.isStrikethrough());
                    textPainter.setUnderlining(textToolbar.isUnderlining());

                    painter = textPainter;
                }
            } catch (UnsupportedFlavorException ex) {
                ErrorManager.errorMinor(GraphicsViewPanel.class, ex);
            } catch (IOException ex) {
                ErrorManager.errorMinor(GraphicsViewPanel.class, ex);
            }

            if (painter == null) {
                return;
            }

            canvas.saveCurrentPainter();

            Point p = canvasScrollPane.getViewport().getViewPosition();
            painter.move((int) (p.x / canvas.getZoom()), (int) (p.y / canvas.getZoom()));

            if (canvas.isEditCurrent() == false) {
                canvas.setEditCurrent(true);
                editCurrentCB.setSelected(true);
            }

            setProperTool(painter);
            PainterAction action = new PainterAction(canvas, painter);
            canvas.addAction(action);

            canvas.requestFocusInWindow();
            canvas.repaint();
        }
    };
    private Action copyAction = new AbstractAction() {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (currentPainterTool == null) {
                return;
            }

            if (canvas.isEditCurrent() == false
                    && canvas.getSelectionFrame() == null) {
                return;
            }
            currentPainterTool.copyToClipboard();
        }
    };
    private Action cutAction = new AbstractAction() {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (currentPainterTool == null) {
                return;
            }

            currentPainterTool.cutToClipboard();
        }
    };
    private Action deleteAction = new AbstractAction() {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (currentPainterTool == null) {
                return;
            }

            currentPainterTool.delete();
        }
    };
    private Action resizeAction = new AbstractAction() {

        @Override
        public void actionPerformed(ActionEvent e) {
            resizeJBActionPerformed(e);
        }
    };
    private MouseListener canvasMouseListener = new MouseAdapter() {

        @Override
        public void mouseReleased(MouseEvent e) {
            if (editCurrentCB.isSelected() == false) {
                statusPanel.getPainterInfoPanel().setVisible(false);
            }
        }
    };
    private MouseMotionListener canvasMouseMotionListener = new MouseMotionAdapter() {

        @Override
        public void mouseMoved(MouseEvent e) {
            Point p = e.getPoint();
            statusPanel.getPositionPanel().setMouseX(p.x);
            statusPanel.getPositionPanel().setMouseY(p.y);
        }

        @Override
        public void mouseDragged(MouseEvent e) {
            Point p = e.getPoint();
            statusPanel.getPositionPanel().setMouseX(p.x);
            statusPanel.getPositionPanel().setMouseY(p.y);
        }
    };
    private ComponentListener canvasComponentListener = new ComponentAdapter() {

        @Override
        public void componentResized(ComponentEvent e) {
            statusPanel.getPositionPanel().setCanvasSize(canvas.getSize());
        }
    };

    @Override
    public ViewType getType() {
        return ViewType.IMAGE_EDITOR;
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private jmulticlip.gui.graph.tools.toolbar.BracketToolbar bracketToolbar;
    private javax.swing.JToggleButton bubbleTB;
    private jmulticlip.gui.graph.tools.toolbar.BubbleToolbar bubbleToolbar;
    private javax.swing.ButtonGroup buttonGroup;
    private jmulticlip.gui.graph.CanvasPanel canvas;
    private javax.swing.JPanel canvasPanel;
    private javax.swing.JScrollPane canvasScrollPane;
    private javax.swing.JButton colorJB;
    private javax.swing.JToggleButton curlyBracketsTB;
    private jmulticlip.gui.graph.tools.toolbar.DefaultToolbar defaultToolbar;
    private javax.swing.JCheckBox editCurrentCB;
    private javax.swing.JToggleButton eraseJB;
    private javax.swing.JToggleButton lineTB;
    private jmulticlip.gui.graph.tools.toolbar.LineToolbar lineToolbar;
    private javax.swing.JPanel mainToolbarPanel;
    private javax.swing.JToggleButton ovalTB;
    private jmulticlip.gui.graph.tools.toolbar.OvalToolbar ovalToolbar;
    private javax.swing.JToolBar painterToolbar;
    private javax.swing.JToggleButton pencilTB;
    private jmulticlip.gui.graph.tools.toolbar.PencilToolbar pencilToobar;
    private javax.swing.JToggleButton rectangleTB;
    private jmulticlip.gui.graph.tools.toolbar.RectangleToolbar rectangleToolbar;
    private javax.swing.JButton redoJB;
    private javax.swing.JButton resizeJB;
    private javax.swing.JToggleButton selectionTB;
    private jmulticlip.gui.graph.tools.toolbar.SelectionToolbar selectionToolbar;
    private javax.swing.JSeparator separator1;
    private javax.swing.JSeparator separator3;
    private jmulticlip.gui.graph.StatusPanel statusPanel;
    private javax.swing.JToggleButton textTB;
    private jmulticlip.gui.graph.tools.toolbar.TextToolbar textToolbar;
    private javax.swing.JPanel toolBarPanel;
    private javax.swing.JPanel toolbarPanel;
    private javax.swing.JButton undoJB;
    private javax.swing.JButton uploadJB;
    private javax.swing.JComboBox uploadServerCB;
    private javax.swing.JButton zoomInJB;
    private javax.swing.JButton zoomOriginalJB;
    private javax.swing.JButton zoomOutJB;
    // End of variables declaration//GEN-END:variables
}
