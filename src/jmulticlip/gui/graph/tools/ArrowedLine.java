package jmulticlip.gui.graph.tools;

import java.awt.*;
import java.awt.geom.*;

/**
 *
 * @author Revers
 */
public class ArrowedLine {

    private AffineTransform affineTransform = new AffineTransform();
    // private static Line2D.Double line = new Line2D.Double(0, 0, 100, 100);
    private Polygon arrowHead = new Polygon();
    private static final int SIZE = 5;

    public enum Type {

        NONE, LEADING, TRAILING, BOTH
    };
    private Type type;
    private int x1;
    private int y1;
    private int x2;
    private int y2;

    public ArrowedLine(Type type, int x1, int y1, int x2, int y2) {
        this.type = type;
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x2;
        this.y2 = y2;
        arrowHead.addPoint(0, SIZE);
        arrowHead.addPoint(-SIZE, -SIZE);
        arrowHead.addPoint(SIZE, -SIZE);
    }

    public ArrowedLine(Type type) {
        this(type, 0, 0, 0, 0);
    }

    public ArrowedLine() {
        this(Type.NONE);
    }

    public int getArrowSize() {
        return SIZE;
    }

    public void draw(Graphics2D g2) {

        g2.drawLine(x1, y1, x2, y2);
        switch (type) {
            case NONE:
                break;
            case LEADING:
                drawArrowHead(g2, x1, y1, x2, y2);
                break;
            case TRAILING:
                drawArrowHead(g2, x2, y2, x1, y1);
                break;
            case BOTH:
                drawArrowHead(g2, x1, y1, x2, y2);
                drawArrowHead(g2, x2, y2, x1, y1);
                break;
        }
    }

    public void draw(Graphics2D g2, int x1, int y1, int x2, int y2) {
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x2;
        this.y2 = y2;

        draw(g2);
    }
    private static final int[] Y_COORDS = {SIZE, -SIZE, -SIZE};
    private static final int[] X_COORDS = {0, -SIZE, SIZE};

    private void drawArrowHead(Graphics2D g2d, int x1, int y1, int x2, int y2) {
        affineTransform.setToIdentity();

        double angle = Math.atan2(y2 - y1, x2 - x1);
        AffineTransform at = g2d.getTransform();
        affineTransform.translate(at.getTranslateX() + x2, at.getTranslateY() + y2);
        affineTransform.rotate((angle - Math.PI / 2d));

        Graphics2D g = (Graphics2D) g2d.create();
        g.setTransform(affineTransform);

        BasicStroke stroke = (BasicStroke) g2d.getStroke();
        int tickness = (int) stroke.getLineWidth();

        g.fillPolygon(new int[]{0, -SIZE - tickness, SIZE + tickness},
                new int[]{SIZE + tickness, -SIZE - tickness, -SIZE - tickness}, 3);

        g.dispose();
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public int getX1() {
        return x1;
    }

    public void setX1(int x1) {
        this.x1 = x1;
    }

    public int getX2() {
        return x2;
    }

    public void setX2(int x2) {
        this.x2 = x2;
    }

    public int getY1() {
        return y1;
    }

    public void setY1(int y1) {
        this.y1 = y1;
    }

    public int getY2() {
        return y2;
    }

    public void setY2(int y2) {
        this.y2 = y2;
    }

    @Override
    public String toString() {
        return super.toString() + "[" + type + "]";
    }
}
