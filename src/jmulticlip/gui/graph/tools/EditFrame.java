package jmulticlip.gui.graph.tools;

import jmulticlip.gui.graph.util.Cursors;
import jmulticlip.gui.graph.util.RectCorners;
import jmulticlip.gui.graph.tools.action.PainterListener;
import jmulticlip.gui.graph.tools.painter.AbstractPainter;
import jmulticlip.gui.graph.tools.action.ResizePainterAction;
import jmulticlip.gui.graph.tools.action.MovePainterAction;
import java.awt.*;
import java.awt.event.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import jmulticlip.gui.graph.CanvasPanel;

/**
 *
 * @author Revers
 * 
 */
public class EditFrame implements MouseMotionListener, MouseListener,
        PainterListener, Cursors {

    private enum GrabState {

        NONE, MIDDLE, N, S, W, E, NW, NE, SW, SE
    }
    private static final int MOUSE_OFFSET = 1;
    private GrabState state = GrabState.NONE;
    private final Image moveImage = new ImageIcon(getClass().getResource("/img/move.png")).getImage();
    private static final int DASH_SIZE = 8;
    private static final int GAP_SIZE = 4;
    private CanvasPanel canvas;
    private AbstractPainter painter;
    private boolean started;
    private Rectangle bounds = new Rectangle();
    private Point moveImagePos = new Point();
    private Point lastMousePoint;
    private boolean canMoveByIcon;
    private Cursor lastCursor = CURSOR_DEFAULT;
    private boolean resizing;
    private boolean moving;
    private boolean drawFrame = true;
    private boolean enabled = true;
    private Rectangle lastBounds = new Rectangle();
    private GrabState flipStateX = GrabState.NONE;
    private GrabState flipStateY = GrabState.NONE;
    private RectCorners corners = new RectCorners();
    private Point moveOriginalPoint = new Point(-1, -1);
    private boolean mousePressed;
    private boolean useMargins = true;
    // Do ResizePainterAction:
    private Rectangle origBounds;
    private Resizable.Flip origFlip;

    public EditFrame(CanvasPanel canvas) {
        this.canvas = canvas;
    }

    public EditFrame(CanvasPanel canvas, AbstractPainter painter) {
        this.canvas = canvas;
        this.painter = painter;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public CanvasPanel getCanvas() {
        return canvas;
    }

    public AbstractPainter getPainter() {
        return painter;
    }

    public void setPainter(AbstractPainter painter) {
        if (this.painter != null) {
            this.painter.removePainterListener(this);
        }
        this.painter = painter;
        if (painter != null) {
            painter.addPainterListener(this);
            updateBounds();
        } else {
            canvas.setCursor(CURSOR_DEFAULT);
        }
    }

    public boolean isStarted() {
        return started;
    }

    public void updateBounds() {
        if (painter == null) {
            return;
        }

        updateBounds(painter.getBounds(), canvas.getZoom());
    }

    private void updateBounds(Rectangle painterBounds, float zoom) {

        int boundsMargin = painter.getBoundsMargin();
        if (useMargins) {
            bounds.x = (int) ((painterBounds.x - boundsMargin) * zoom);
            bounds.y = (int) ((painterBounds.y - boundsMargin) * zoom);
            bounds.width = (int) ((painterBounds.width + 2 * boundsMargin) * zoom);
            bounds.height = (int) ((painterBounds.height + 2 * boundsMargin) * zoom);
        } else {
            bounds.x = (int) (painterBounds.x * zoom);
            bounds.y = (int) (painterBounds.y * zoom);
            bounds.width = (int) (painterBounds.width * zoom);
            bounds.height = (int) (painterBounds.height * zoom);
        }

        corners.setCorners(bounds);
        setMoveIconCoords();
    }

    private void setMoveIconCoords() {
        Dimension canvasDim = new Dimension();
        canvasDim.width = (int) (canvas.getCanvasImage().getWidth() * canvas.getZoom());
        canvasDim.height = (int) (canvas.getCanvasImage().getHeight() * canvas.getZoom());
        moveImagePos.x = bounds.x + bounds.width;
        moveImagePos.y = bounds.y + bounds.height;
        int halfWidth = moveImage.getWidth(null) / 2;
        int halfHeight = moveImage.getHeight(null) / 2;
        if (bounds.x + bounds.width + halfWidth <= canvasDim.width
                && bounds.y + bounds.height + halfHeight <= canvasDim.height) {
            moveImagePos.x = bounds.x + bounds.width;
            moveImagePos.y = bounds.y + bounds.height;
        } else if (bounds.x - halfWidth >= 0 && bounds.y - halfHeight >= 0) {
            moveImagePos.x = bounds.x - moveImage.getWidth(null);
            moveImagePos.y = bounds.y - moveImage.getHeight(null);
        } else if (bounds.x + bounds.width + halfWidth <= canvasDim.width
                && bounds.y - halfHeight >= 0) {
            moveImagePos.x = bounds.x + bounds.width;
            moveImagePos.y = bounds.y - moveImage.getHeight(null);
        } else if (bounds.x - halfWidth >= 0
                && bounds.y + bounds.height + halfHeight <= canvasDim.height) {
            moveImagePos.x = bounds.x - moveImage.getWidth(null);
            moveImagePos.y = bounds.y + bounds.height;
        }
    }

    public void draw(Graphics2D g2) {
        if (painter == null || painter.isEditable() == false
                || resizing || moving || drawFrame == false
                || enabled == false) {
            return;
        }
        int width = bounds.width;
        int height = bounds.height;
        if (width > 0) {
            width--;
        }
        if (height > 0) {
            height--;
        }

        drawHorizontalLine(g2, bounds.x, bounds.y, width);
        drawHorizontalLine(g2, bounds.x, bounds.y + bounds.height, width);
        drawVerticalLine(g2, bounds.x, bounds.y, bounds.height);
        drawVerticalLine(g2, bounds.x + width, bounds.y, bounds.height);

        if (painter.isMoveable() && painter.isMoveByInterior() == false) {
            g2.drawImage(moveImage, moveImagePos.x, moveImagePos.y, null);
        }
    }

    public boolean isOnMoveImage(Point p) {
        if (p.x >= moveImagePos.x && p.x <= moveImagePos.x + moveImage.getWidth(null)
                && p.y >= moveImagePos.y && p.y <= moveImagePos.y + moveImage.getHeight(null)) {
            return true;
        }

        return false;
    }

    private void drawHorizontalLine(Graphics2D g2, int x, int y, int width) {

        int xOrig = x;

        for (; x <= xOrig + width; x++) {
            if (y < 0 || y >= canvas.getHeight()
                    || x < 0 || x >= canvas.getWidth()) {
                continue;
            }

            if ((x - xOrig) % (DASH_SIZE + GAP_SIZE) > DASH_SIZE) {
                continue;
            }
            Color color = new Color(canvas.getCanvasImage().getRGB(
                    (int) (x / canvas.getZoom()), (int) (y / canvas.getZoom())));
            g2.setColor(getInvertedColor(color));
            g2.drawLine(x, y, x, y);
        }
    }

    private void drawVerticalLine(Graphics2D g2, int x, int y, int height) {

        int yOrig = y;

        for (; y <= yOrig + height; y++) {
            if (y < 0 || y >= canvas.getHeight()
                    || x < 0 || x >= canvas.getWidth()) {
                continue;
            }
            if ((y - yOrig) % (DASH_SIZE + GAP_SIZE) > DASH_SIZE) {
                continue;
            }

            Color color = new Color(canvas.getCanvasImage().getRGB(
                    (int) (x / canvas.getZoom()), (int) (y / canvas.getZoom())));
            g2.setColor(getInvertedColor(color));
            g2.drawLine(x, y, x, y);
        }

    }

    private Color getInvertedColor(Color color) {
        return new Color(255 - color.getRed(), 255 - color.getGreen(),
                255 - color.getBlue());
    }

    public void start() {
        started = true;

        canvas.addMouseListener(EditFrame.this);
        canvas.addMouseMotionListener(EditFrame.this);
    }

    public void finish() {
        started = false;
        canvas.removeMouseListener(this);
        canvas.removeMouseMotionListener(this);
    }

    @Override
    public void mouseDragged(MouseEvent e) {

        if (enabled == false || painter == null || painter.isEditable() == false) {
            return;
        }

        Point p = canvas.getNotZoomedPoint(e.getPoint());
        if (mousePressed == false) {
            mousePressed = true;
            lastMousePoint = p;
            setProperState(e.getPoint(), MOUSE_OFFSET);
            moveOriginalPoint.x = -1;
            moveOriginalPoint.y = -1;
        }

        if (canMoveByIcon || state == GrabState.MIDDLE) {
            moving = true;
            int dx = p.x - lastMousePoint.x;
            int dy = p.y - lastMousePoint.y;
            lastMousePoint = p;

            painter.move(dx, dy);
            canvas.repaint();
            e.consume();
            return;
        }

        dragResizeAction(e);
    }

    private void resizeRect(Rectangle rect, MouseEvent e, Point p) {
        //Point p = e.getPoint();
        switch (state) {

            case NW: {
                int dx = corners.upperLeft.x - p.x;
                int dy = corners.upperLeft.y - p.y;

                rect.x -= dx;
                rect.width += dx;
                rect.y -= dy;
                rect.height += dy;

                corners.upperLeft.x = rect.x;
                corners.upperLeft.y = rect.y;
                e.consume();
                break;
            }
            case NE: {
                int dx = corners.upperRight.x - p.x;
                int dy = corners.upperRight.y - p.y;

                rect.width -= dx;
                rect.y -= dy;
                rect.height += dy;

                corners.upperRight.x = rect.x + rect.width;
                corners.upperRight.y = rect.y;
                e.consume();
                break;
            }
            case SW: {
                int dx = corners.bottomLeft.x - p.x;
                int dy = corners.bottomLeft.y - p.y;

                rect.x -= dx;
                rect.width += dx;
                rect.height -= dy;

                corners.bottomLeft.x = rect.x;
                corners.bottomLeft.y = rect.y + rect.height;
                e.consume();
                break;
            }
            case SE: {
                int dx = corners.bottomRight.x - p.x;
                int dy = corners.bottomRight.y - p.y;

                rect.width -= dx;
                rect.height -= dy;

                corners.bottomRight.x = rect.x + rect.width;
                corners.bottomRight.y = rect.y + rect.height;
                e.consume();
                break;
            }
            case W: {
                int dx = corners.upperLeft.x - p.x;

                rect.x -= dx;
                rect.width += dx;

                corners.upperLeft.x = rect.x;
                e.consume();
                break;
            }

            case E: {
                int dx = corners.upperRight.x - p.x;

                rect.width -= dx;

                corners.upperRight.x = rect.x + rect.width;
                e.consume();
                break;
            }
            case S: {
                int dy = corners.bottomRight.y - p.y;

                rect.height -= dy;

                corners.bottomRight.y = rect.y + rect.height;
                e.consume();
                break;
            }
            case N: {
                int dy = corners.upperLeft.y - p.y;

                rect.y -= dy;
                rect.height += dy;

                corners.upperLeft.y = rect.y;
                e.consume();
                break;
            }
        }
        if (rect.width < 0) {
            rect.x += rect.width;
            rect.width = -rect.width;
        }
        if (rect.height < 0) {
            rect.y += rect.height;
            rect.height = -rect.height;
        }
    }
    private boolean firstDrag = true;

    private void dragResizeAction(MouseEvent e) {

        if (state == GrabState.NONE) {
            return;
        }

        resizing = true;

        int xDiff = 0;
        int yDiff = 0;
        if (firstDrag) {
            e.consume();
            firstDrag = false;

            Rectangle r = (Rectangle) painter.getBounds().clone();
            Point p = e.getPoint();

            float zoom = canvas.getZoom();

            r.x = (int) (r.x * zoom);
            r.y = (int) (r.y * zoom);
            r.width = (int) (r.width * zoom);
            r.height = (int) (r.height * zoom);

            if (p.x < r.x) {
                xDiff = r.x - p.x;
            } else if (p.x > r.x + r.width) {
                xDiff = r.x + r.width - p.x;
            }

            if (p.y < r.y) {
                yDiff = r.y - p.y;
            } else if (p.y > r.y + r.height) {
                yDiff = r.y + r.height - p.y;
            }

            Point point = e.getLocationOnScreen();
            point.x += xDiff;
            point.y += yDiff;
            try {
                Robot robot = new Robot();
                robot.mouseMove(point.x, point.y);
            } catch (AWTException ex) {
                Logger.getLogger(EditFrame.class.getName()).log(Level.SEVERE, null, ex);
            }

            useMargins = false;
            updateBounds();

            p.x += xDiff;
            p.y += yDiff;
        }

        Rectangle rect = (Rectangle) bounds.clone();
        Point p = e.getPoint();
        resizeRect(rect, e, p);

        if (rect.height == lastBounds.height && rect.width == lastBounds.width) {
            return;
        }

        corners.setCorners(rect);

        int boundsMargin = painter.getBoundsMargin();
        Rectangle cpRect = (Rectangle) rect.clone();
        if (useMargins) {
            rect.x += boundsMargin;
            rect.y += boundsMargin;
            rect.height -= 2 * boundsMargin;
            rect.width -= 2 * boundsMargin;
            cpRect.x += (int) (boundsMargin * canvas.getZoom());
            cpRect.y += (int) (boundsMargin * canvas.getZoom());
            cpRect.width -= (int) (2 * boundsMargin * canvas.getZoom());
            cpRect.height -= (int) (2 * boundsMargin * canvas.getZoom());
        }

        int trueX = (int) (cpRect.x / canvas.getZoom());
        int trueY = (int) (cpRect.y / canvas.getZoom());
        int trueWidth = Math.round(cpRect.width / canvas.getZoom());
        int trueHeight = Math.round(cpRect.height / canvas.getZoom());

        Resizable resizablePainter = ((Resizable) painter);

        if (trueWidth > 0 && trueHeight > 0) {

            resizablePainter.resize(trueX, trueY, trueWidth, trueHeight);
        }
        updateBounds(rect, 1.0f);

        if (flipStateX != state && (bounds.x == lastBounds.x + lastBounds.width
                || bounds.x + bounds.width == lastBounds.x)) {
            resizablePainter.flipY();
            flipStateX = state;
        }

        if (flipStateY != state && (bounds.y == lastBounds.y + lastBounds.height
                || bounds.y + bounds.height == lastBounds.y)) {
            resizablePainter.flipX();
            flipStateY = state;
        }

        lastBounds = (Rectangle) bounds.clone();

        canvas.repaint();
        setProperState(p, 0);
    }

    @Override
    public void mouseMoved(MouseEvent e) {

        if (enabled == false || painter == null || painter.isEditable() == false) {
            return;
        }

        setProperState(e.getPoint(), MOUSE_OFFSET);
        if (painter.isMoveByInterior()) {
            return;
        }

        if (painter.isMoveable() && isOnMoveImage(e.getPoint())) {
            if (canMoveByIcon == false) {
                lastCursor = canvas.getCursor();
            }
            canMoveByIcon = true;
            canvas.setCursor(CURSOR_HAND);
            e.consume();
            return;
        } else if (canMoveByIcon == true) {
            canMoveByIcon = false;
            canvas.setCursor(lastCursor);
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
        if (enabled == false || painter == null || painter.isEditable() == false) {
            return;
        }

        mousePressed = true;

        if (state != GrabState.NONE) {
            e.consume();
            if (state != GrabState.MIDDLE) {
                origBounds = (Rectangle) painter.getBounds().clone();
                origFlip = ((Resizable) painter).getFlip();
            }
        }

        if (painter.getBounds().contains(canvas.getNotZoomedPoint(e.getPoint())) == false
                && bounds.contains(e.getPoint())) {
            e.consume();
        }

        lastMousePoint = canvas.getNotZoomedPoint(e.getPoint());

        updateBounds();
        if (painter.isMoveable() == false) {
            return;
        }
        Point p = e.getPoint();
        if ((isOnMoveImage(p) && painter.isMoveByInterior() == false)
                || (painter.getBounds().contains(canvas.getNotZoomedPoint(p))
                && painter.isMoveByInterior())) {
            Rectangle rect = painter.getBounds();
            moveOriginalPoint.x = rect.x;
            moveOriginalPoint.y = rect.y;
            drawFrame = false;
            e.consume();
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        firstDrag = true;
        useMargins = true;
        mousePressed = false;
        if (enabled == false || painter == null || painter.isEditable() == false) {
            return;
        }
        if (resizing) {
            ResizePainterAction action = new ResizePainterAction(canvas, painter, origBounds, origFlip);
            canvas.addAction(action);
        }

        drawFrame = true;
        resizing = false;
        moving = false;
        if (moveOriginalPoint.x != -1 && moveOriginalPoint.y != -1) {
            MovePainterAction action = new MovePainterAction(canvas, painter,
                    moveOriginalPoint.x, moveOriginalPoint.y);
            canvas.addAction(action);
        }

        moveOriginalPoint.x = -1;
        moveOriginalPoint.y = -1;
        updateBounds();
        flipStateX = GrabState.NONE;
        flipStateY = GrabState.NONE;
        lastBounds = new Rectangle();



        canvas.repaint();
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    private boolean handleOneWallDrag(Point p, int range) {
        if (resizing == false) {
            return false;
        }

        switch (state) {
            case S:
            case N:
            case W:
            case E:
                break;
            default:
                return false;
        }

        final int OFFSET = 10000000;
        if (isNearX(p, corners.upperLeft.x,
                corners.upperLeft.y - OFFSET, corners.bottomLeft.y + OFFSET, range)) {
            canvas.setCursor(CURSOR_RESIZE_W);
            state = GrabState.W;
            return true;
        } else if (isNearX(p, corners.upperRight.x,
                corners.upperRight.y - OFFSET, corners.bottomRight.y + OFFSET, range)) {
            canvas.setCursor(CURSOR_RESIZE_E);
            state = GrabState.E;
            return true;
        } else if (isNearY(p, corners.upperRight.y,
                corners.upperLeft.x - OFFSET, corners.upperRight.x + OFFSET, range)) {
            canvas.setCursor(CURSOR_RESIZE_N);
            state = GrabState.N;
            return true;
        } else if (isNearY(p, corners.bottomRight.y,
                corners.bottomLeft.x - OFFSET, corners.bottomRight.x + OFFSET, range)) {
            canvas.setCursor(CURSOR_RESIZE_S);
            state = GrabState.S;
            return true;
        }

        return false;
    }

    private void setProperState(Point p, int range) {
        if (painter.isEditable() == false || (painter.isMoveable() == false
                && painter.isResizable() == false)) {
            return;
        }
        if (corners.upperLeft.x == corners.bottomRight.x
                || corners.upperLeft.y == corners.bottomRight.y) {
            return;
        }

        if (painter.isResizable() == false) {
            if (corners.isInside(p)) {
                if (resizing == false) {
                    if (painter.isMoveable() && painter.isMoveByInterior()) {
                        canvas.setCursor(CURSOR_MOVE);
                        state = GrabState.MIDDLE;
                    } else {
                        canvas.setCursor(CURSOR_DEFAULT);
                        state = GrabState.NONE;
                    }
                }
            } else {
                if (resizing == false) {
                    canvas.setCursor(CURSOR_DEFAULT);
                    state = GrabState.NONE;
                }
            }

            return;
        }

        if (handleOneWallDrag(p, range)) {
            return;
        }

        if (isNear(corners.upperLeft, p, range)) {
            canvas.setCursor(CURSOR_RESIZE_NW);
            state = GrabState.NW;
        } else if (isNear(corners.upperRight, p, range)) {
            canvas.setCursor(CURSOR_RESIZE_NE);
            state = GrabState.NE;
        } else if (isNear(corners.bottomLeft, p, range)) {
            canvas.setCursor(CURSOR_RESIZE_SW);
            state = GrabState.SW;
        } else if (isNear(corners.bottomRight, p, range)) {
            canvas.setCursor(CURSOR_RESIZE_SE);
            state = GrabState.SE;
        } else if (isNearX(p, corners.upperLeft.x,
                corners.upperLeft.y, corners.bottomLeft.y, range)) {
            canvas.setCursor(CURSOR_RESIZE_W);
            state = GrabState.W;
        } else if (isNearX(p, corners.upperRight.x,
                corners.upperRight.y, corners.bottomRight.y, range)) {
            canvas.setCursor(CURSOR_RESIZE_E);
            state = GrabState.E;
        } else if (isNearY(p, corners.upperRight.y,
                corners.upperLeft.x, corners.upperRight.x, range)) {
            canvas.setCursor(CURSOR_RESIZE_N);
            state = GrabState.N;
        } else if (isNearY(p, corners.bottomRight.y,
                corners.bottomLeft.x, corners.bottomRight.x, range)) {
            canvas.setCursor(CURSOR_RESIZE_S);
            state = GrabState.S;
        } else if (corners.isInside(p)) {
            if (resizing == false) {
                if (painter.isMoveable() && painter.isMoveByInterior()) {
                    canvas.setCursor(CURSOR_MOVE);
                    state = GrabState.MIDDLE;
                } else {
                    canvas.setCursor(CURSOR_DEFAULT);
                    state = GrabState.NONE;
                }
            }
        } else {
            if (resizing == false) {
                canvas.setCursor(CURSOR_DEFAULT);
                state = GrabState.NONE;
            }
        }
    }

    private boolean isNearY(Point mousePoint, int y, int x1, int x2, int range) {
        if (mousePoint.x <= x2 && mousePoint.x >= x1
                && mousePoint.y <= y + range && mousePoint.y >= y - range) {
            return true;
        }

        return false;
    }

    private boolean isNearX(Point mousePoint, int x, int y1, int y2, int range) {
        if (mousePoint.x <= x + range && mousePoint.x >= x - range
                && mousePoint.y >= y1 && mousePoint.y <= y2) {
            return true;
        }

        return false;
    }

    private boolean isNear(Point targetPoint, Point mousePoint, int range) {
        if (mousePoint.x <= targetPoint.x + range && mousePoint.x >= targetPoint.x - range
                && mousePoint.y <= targetPoint.y + range && mousePoint.y >= targetPoint.y - range) {
            return true;
        }

        return false;
    }

    @Override
    public void painterChanged(AbstractPainter painter) {
        updateBounds();
    }
}
