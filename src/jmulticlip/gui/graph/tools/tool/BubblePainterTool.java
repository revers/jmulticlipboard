package jmulticlip.gui.graph.tools.tool;

import jmulticlip.gui.graph.tools.toolbar.BubbleToolbar;
import jmulticlip.gui.graph.components.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import jmulticlip.gui.graph.CanvasPanel;
import jmulticlip.gui.graph.CommonProperties;
import jmulticlip.gui.graph.CommonProperties.Type;
import jmulticlip.gui.graph.CommonPropertyChangeListener;
import jmulticlip.gui.graph.PainterInfoPanel;
import jmulticlip.gui.graph.tools.painter.AbstractPainter;
import jmulticlip.gui.graph.tools.action.DeletePainterAction;
import jmulticlip.gui.graph.tools.painter.BubblePainter;
import jmulticlip.gui.graph.tools.action.PainterAction;
import jmulticlip.gui.graph.tools.action.PainterListener;
import jmulticlip.gui.graph.tools.TransferableElement;

/**
 *
 * @author Revers
 */
public class BubblePainterTool extends AbstractPainterTool<BubblePainter> implements
        MouseListener, MouseMotionListener, KeyListener, PainterListener {

    private Point startingPoint;
    private boolean shiftPressed;
    private BubbleToolbar bubbleToolbar;
    private boolean started;
    private PainterInfoPanel painterInfo;
    private CommonPropertyChangeListener commonPropertyChangeListener = new CommonPropertyChangeListener() {

        @Override
        public void propertyChanged(Object property, Type type) {

            if (painter == null || canvas.isEditCurrent() == false) {
                return;
            }

            switch (type) {
                case FILL_COLOR:
                    painter.setFillColor((Color) property);
                    break;
                case COLOR:
                    painter.setColor((Color) property);
                    break;
                case FILL:
                    painter.setFill((Boolean) property);
                    break;
                case LINE_WIDTH:
                    painter.setLineWidth((Float) property);
                    break;
                case DASH_STROKE:
                    painter.setDashStroke((BasicStroke) property);
                    break;
                default:
                    return;
            }

            canvas.repaint();
        }
    };

    public BubblePainterTool(CanvasPanel canvas,
            BubbleToolbar bubbleToolbar, PainterInfoPanel painterInfo) {
        this(canvas, bubbleToolbar, null, painterInfo);
    }

    public BubblePainterTool(CanvasPanel canvas,
            BubbleToolbar bubbleToolbar, BubblePainter painter, PainterInfoPanel painterInfo) {
        super(canvas);
        this.bubbleToolbar = bubbleToolbar;
        this.painterInfo = painterInfo;
        painterInfo.setRectLabels();
        if (painter != null) {
            setPainter(painter);
            painter.addPainterListener(this);
            painterChanged(painter);
            canvas.setCurrentPainter(painter);
        }
    }

    @Override
    public void painterChanged(AbstractPainter painter) {
        Rectangle rect = painter.getBounds();
        painterInfo.setInfoTitle("Chmurka");
        painterInfo.setInfoX(rect.x);
        painterInfo.setInfoY(rect.y);
        painterInfo.setInfoWidth(rect.width);
        painterInfo.setInfoHeight(rect.height);
        painterInfo.setVisible(true);
    }

    @Override
    protected void initToolbars(BubblePainter painter) {
        CommonProperties.getInstance().setColor(painter.getColor());
        CommonProperties.getInstance().setFillColor(painter.getFillColor());

        BasicStroke painterStroke = painter.getStroke();
        float tickness = painterStroke.getLineWidth();

        bubbleToolbar.setLineWidth(tickness);
//        bubbleToolbar.setDashStroke(painterStroke);
        bubbleToolbar.setFill(painter.isFill());
    }

    @Override
    public void start() {
        started = true;
        getCanvasPanel().addMouseListener(this);
        getCanvasPanel().addMouseMotionListener(this);
        getCanvasPanel().addKeyListener(this);

        CommonProperties.getInstance().addPropertyChangeListener(
                commonPropertyChangeListener,
                Type.COLOR,
                Type.FILL_COLOR,
                Type.FILL,
                Type.LINE_WIDTH,
                Type.DASH_STROKE);

        if (painter != null) {
            painterInfo.setVisible(true);
        }
    }

    @Override
    public void finish() {
        started = false;
        if (painter != null) {
            painter.removePainterListener(this);
        }
        painterInfo.setVisible(false);

        getCanvasPanel().removeMouseListener(this);
        getCanvasPanel().removeMouseMotionListener(this);
        getCanvasPanel().removeKeyListener(this);

        canvas.saveCurrentPainter();

        CommonProperties.getInstance().removePropertyChangeListener(
                commonPropertyChangeListener);

        canvas.repaint();
    }

    @Override
    public boolean isStarted() {
        return started;
    }

    @Override
    public void delete() {
        if (painter != null) {
            DeletePainterAction action = new DeletePainterAction(canvas, painter);
            canvas.addAction(action);

            painter = null;
            canvas.repaint();
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
        if (e.isConsumed()) {
            return;
        }
        getCanvasPanel().saveCurrentPainter();
        canvas.requestFocusInWindow();
        startingPoint = canvas.getNotZoomedPoint(e.getPoint());
        if (painter != null) {
            painter.removePainterListener(this);
        }
        painter = new BubblePainter(CommonProperties.getInstance().getColor(),
                CommonProperties.getInstance().getFillColor(),
                startingPoint.x, startingPoint.y, 1, 1);

        painter.setFill(bubbleToolbar.isFill());
//        painter.setDashStroke(bubbleToolbar.getDashStroke());
        painter.setLineWidth(bubbleToolbar.getLineWidth());

        painter.addPainterListener(this);
        painter.setEditable(false);

        getCanvasPanel().addAction(new PainterAction(canvas, painter));
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        if (e.isConsumed()) {
            return;
        }

        painter.setEditable(true);
        getCanvasPanel().repaint();
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    public void rotate(Point p, double angle) {
        double x = p.x;
        p.x = (int) Math.round(x * Math.cos(angle) - p.y * Math.sin(angle));
        p.y = (int) Math.round(x * Math.sin(angle) + p.y * Math.cos(angle));
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        if (e.isConsumed()) {
            return;
        }
        Point p = canvas.getNotZoomedPoint(e.getPoint());
        int dx = p.x - startingPoint.x;
        int dy = p.y - startingPoint.y;
        if (shiftPressed == true) {
            Point point = new Point();

            point.x = Math.round((float) (p.x + startingPoint.x) / 2.0f);
            point.y = Math.round((float) (p.y + startingPoint.y) / 2.0f);

            int diameter = (int) Math.round(Math.sqrt(dx * dx + dy * dy));
            int radius = diameter / 2;
            point.x -= radius;
            point.y -= radius;

            painter.setX(point.x);
            painter.setY(point.y);
            painter.setWidth(diameter);
            painter.setHeight(diameter);

        } else {
            painter.setX(startingPoint.x);
            painter.setY(startingPoint.y);
            //     System.out.printf("dx = %d; dy = %d%n", dx, dy);
            painter.setWidth(dx);
            painter.setHeight(dy);
        }
        //updateRectInfo();

        getCanvasPanel().repaint();
    }

    @Override
    public void mouseMoved(MouseEvent e) {
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_SHIFT) {
            shiftPressed = true;
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_SHIFT) {
            shiftPressed = false;
        }
    }

    @Override
    public boolean copyToClipboard() {
        if (painter == null) {
            return false;
        }

        BubblePainter painterCopy = (BubblePainter) painter.clonePainter();

        TransferableElement element = new TransferableElement(painterCopy, BubblePainter.getDataFlavor());
        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(element, null);

        return true;
    }
}
