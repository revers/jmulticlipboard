package jmulticlip.gui.graph.tools.tool;

import java.awt.Color;
import jmulticlip.gui.graph.CanvasPanel;
import jmulticlip.gui.graph.tools.painter.AbstractPainter;

/**
 *
 * @author Revers
 */
public abstract class AbstractPainterTool<T extends AbstractPainter> {

    protected CanvasPanel canvas;
    protected T painter;

    protected void setPainter(T painter) {
        this.painter = painter;
        initToolbars(painter);
    }

    public T getPainter() {
        return painter;
    }

    public AbstractPainterTool(CanvasPanel canvas) {
        this.canvas = canvas;
        //this(canvas, null);
    }

//    public AbstractPainterTool(CanvasPanel canvas, T painter) {
//        this.canvas = canvas;
//        this.painter = painter;
//        if (painter != null) {
//            initToolbars(painter);
//        }
//    }
    public CanvasPanel getCanvasPanel() {
        return canvas;
    }

    public void setCanvasPanel(CanvasPanel canvasPanel) {
        this.canvas = canvasPanel;
    }

    public abstract void start();

    public abstract void finish();

    public abstract boolean isStarted();

    public abstract void delete();

    protected abstract void initToolbars(T painter);

    public boolean copyToClipboard() {
        return false;
    }

    public boolean cutToClipboard() {
        boolean cpy = copyToClipboard();
        if (cpy) {
            delete();
            return true;
        }
        return false;
    }
}
