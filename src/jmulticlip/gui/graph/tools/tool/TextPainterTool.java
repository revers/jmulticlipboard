package jmulticlip.gui.graph.tools.tool;

import jmulticlip.gui.graph.tools.toolbar.TextToolbar;
import java.util.logging.Level;
import java.util.logging.Logger;
import jmulticlip.gui.graph.components.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.font.TextHitInfo;
import java.awt.font.TextLayout;
import java.awt.geom.Point2D;

import jmulticlip.core.ErrorManager;
import jmulticlip.gui.graph.CanvasPanel;
import jmulticlip.gui.graph.CommonProperties;
import jmulticlip.gui.graph.CommonProperties.Type;
import jmulticlip.gui.graph.CommonPropertyChangeListener;
import jmulticlip.gui.graph.PainterInfoPanel;
import jmulticlip.gui.graph.tools.painter.AbstractPainter;
import jmulticlip.gui.graph.tools.action.PainterAction;
import jmulticlip.gui.graph.tools.action.PainterListener;
import jmulticlip.gui.graph.tools.painter.TextPainter;

/**
 *
 * @author Revers
 */
public class TextPainterTool extends AbstractPainterTool<TextPainter> implements
        MouseListener, KeyListener, ActionListener, MouseMotionListener, Runnable,
        PainterListener, CommonPropertyChangeListener {

//    private static final Cursor CURSOR_HAND = new Cursor(Cursor.HAND_CURSOR);
    private static final Cursor CURSOR_DEFAULT = Cursor.getDefaultCursor();
    private static final Cursor CURSOR_TEXT = new Cursor(Cursor.TEXT_CURSOR);
    private TextToolbar textToolbar;
    private Point mousePressedPoint;
    //   private Point startingPoint = new Point();
    // private boolean canMovePainter;
    private CaretTimer caretTimer;
    private boolean showCaret = true;
    private boolean started;
    private PainterInfoPanel painterInfo;
    private JCheckBox editCurrentCB;
    private Action pasteAction = new AbstractAction() {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (painter == null) {
                return;
            }
            Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();

            Transferable transferable = clipboard.getContents(null);
            if (transferable != null) {
                if (transferable.isDataFlavorSupported(DataFlavor.stringFlavor)) {
                    try {
                        String text = (String) transferable.getTransferData(DataFlavor.stringFlavor);
                        text = text.replace("\n", "\n\r");
                        if (painter.getSelectionBegin() != painter.getSelectionEnd()) {
                            deleteSelection();
                        }
                        painter.insert(painter.getCaretPosition(), text);
                        canvas.repaint();
                    } catch (Exception ex) {
                        ErrorManager.errorMinor(TextPainterTool.class, ex);
                    }
                }
            }
        }
    };
    private Action copyAction = new AbstractAction() {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (painter == null) {
                return;
            }
            if (painter.getSelectionBegin() == painter.getSelectionEnd()) {
                return;
            }

            String text = null;
            if (painter.getSelectionBegin() < painter.getSelectionEnd()) {
                text = painter.getText().substring(painter.getSelectionBegin(),
                        painter.getSelectionEnd());
            } else {
                text = painter.getText().substring(painter.getSelectionEnd(),
                        painter.getSelectionBegin());
            }

            text = text.replace("\n", "\n\r");

            if (text.length() == 0) {
                return;
            }

            StringSelection ss = new StringSelection(text);
            Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);
        }
    };
    private Action cutAction = new AbstractAction() {

        @Override
        public void actionPerformed(ActionEvent e) {
            copyAction.actionPerformed(e);
            if (painter.getSelectionBegin() == painter.getSelectionEnd()) {
                return;
            }
            deleteSelection();
        }
    };
    private Action selectAllAction = new AbstractAction() {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (painter == null) {
                return;
            }
            painter.setSelectionBegin(TextPainter.FIRST_VALID_POSITION);
            painter.setSelectionEnd(painter.getTextLength());
        }
    };
    KeyStroke ctrlV = KeyStroke.getKeyStroke(KeyEvent.VK_V, KeyEvent.CTRL_MASK, false);
    KeyStroke ctrlC = KeyStroke.getKeyStroke(KeyEvent.VK_C, KeyEvent.CTRL_MASK, false);
    KeyStroke ctrlX = KeyStroke.getKeyStroke(KeyEvent.VK_X, KeyEvent.CTRL_MASK, false);
    KeyStroke ctrlA = KeyStroke.getKeyStroke(KeyEvent.VK_A, KeyEvent.CTRL_MASK, false);

    public TextPainterTool(CanvasPanel canvas,
            TextToolbar textToolbar, PainterInfoPanel painterInfo, JCheckBox editCurrentCB) {
        this(canvas, textToolbar, null, painterInfo, editCurrentCB);
    }

    public TextPainterTool(CanvasPanel canvas,
            TextToolbar textToolbar, TextPainter painter,
            PainterInfoPanel painterInfo, JCheckBox editCurrentCB) {
        super(canvas);

        this.textToolbar = textToolbar;
        this.editCurrentCB = editCurrentCB;
        this.painterInfo = painterInfo;
        if (canvas.isEditCurrent() == false) {
            canvas.setEditCurrent(true);
            editCurrentCB.setSelected(true);
        }

        painterInfo.setRectLabels();
        if (painter != null) {
            setPainter(painter);
            painter.addPainterListener(this);
            painterChanged(painter);
            canvas.setCurrentPainter(painter);
        }
    }

    @Override
    public void painterChanged(AbstractPainter painter) {
        Rectangle rect = painter.getBounds();
        painterInfo.setInfoTitle("Tekst");
        painterInfo.setInfoX(rect.x);
        painterInfo.setInfoY(rect.y);
        painterInfo.setInfoWidth(rect.width);
        painterInfo.setInfoHeight(rect.height);
        painterInfo.setVisible(true);
    }

    @Override
    protected void initToolbars(TextPainter painter) {
        if (caretTimer != null) {
            caretTimer.stop();
        }
        showCaret = true;
        //  painter.setCaretPosition(1);
        caretTimer = new CaretTimer(painter);
        caretTimer.start();
        // colorComboBox.getModel().setSelectedItem(painter.getColor());
        CommonProperties.getInstance().setColor(painter.getColor());

        textToolbar.getBoldTB().setSelected(painter.isBold());
        textToolbar.getItalicTB().setSelected(painter.isItalic());
        textToolbar.getStrikeTB().setSelected(painter.isStrikethrough());
        textToolbar.getUnderlineTB().setSelected(painter.isUnderlining());
        textToolbar.getFontSizeCB().setSelectedItem(new Integer(painter.getFontSize()));

        String family = painter.getFontFamily();
        ComboBoxModel model = textToolbar.getFontFamilyCB().getModel();
        for (int i = 0; i < model.getSize(); i++) {
            if (((FontSample) model.getElementAt(i)).getFont().getFamily().equals(family)) {
                textToolbar.getFontFamilyCB().setSelectedIndex(i);
                break;
            }
        }

        boolean leftSelected = false;
        boolean rightSelected = false;
        boolean centerSelected = false;

        switch (painter.getAlignment()) {
            case CENTER:
                centerSelected = true;
                break;
            case LEFT:
                leftSelected = true;
                break;
            case RIGHT:
                rightSelected = true;
                break;
        }

        textToolbar.getLeftAlignTB().setSelected(leftSelected);
        textToolbar.getRightAlignTB().setSelected(rightSelected);
        textToolbar.getCenterAlignTB().setSelected(centerSelected);

        //    painter.reinit();
    }

    private void setupPainter() {
        if (painter == null) {
            return;
        }

        painter.setColor(CommonProperties.getInstance().getColor());

        String fontFamily = textToolbar.getFontFamilyCB().getSelectedItem().toString();
        painter.setFontFamily(fontFamily);
        String fontSize = textToolbar.getFontSizeCB().getSelectedItem().toString();
        painter.setFontSize(Integer.parseInt(fontSize));

        if (textToolbar.getBoldTB().isSelected()) {
            painter.setBold(true);
        } else {
            painter.setBold(false);
        }

        if (textToolbar.getCenterAlignTB().isSelected()) {
            painter.setAlignment(TextPainter.Alignment.CENTER);
        } else if (textToolbar.getLeftAlignTB().isSelected()) {
            painter.setAlignment(TextPainter.Alignment.LEFT);
        } else if (textToolbar.getRightAlignTB().isSelected()) {
            painter.setAlignment(TextPainter.Alignment.RIGHT);
        }

        if (textToolbar.getItalicTB().isSelected()) {
            painter.setItalic(true);
        } else {
            painter.setItalic(false);
        }

        if (textToolbar.getStrikeTB().isSelected()) {
            painter.setStrikethrough(true);
        } else {
            painter.setStrikethrough(false);
        }
        if (textToolbar.getUnderlineTB().isSelected()) {
            painter.setUnderlining(true);
        } else {
            painter.setUnderlining(false);
        }

        painter.reinit();
    }

    @Override
    public void start() {
        started = true;
        getCanvasPanel().addMouseListener(this);
        getCanvasPanel().addMouseMotionListener(this);
        getCanvasPanel().addKeyListener(this);
        editCurrentCB.setEnabled(false);

        textToolbar.getBoldTB().addActionListener(this);
        textToolbar.getCenterAlignTB().addActionListener(this);
        textToolbar.getFontFamilyCB().addActionListener(this);
        textToolbar.getFontSizeCB().addActionListener(this);
        textToolbar.getItalicTB().addActionListener(this);
        textToolbar.getLeftAlignTB().addActionListener(this);
        textToolbar.getRightAlignTB().addActionListener(this);
        textToolbar.getStrikeTB().addActionListener(this);
        textToolbar.getUnderlineTB().addActionListener(this);
        CommonProperties.getInstance().addPropertyChangeListener(this, Type.COLOR);
        canvas.setCursor(CURSOR_TEXT);

        canvas.registerKeyboardAction(pasteAction, "paste",
                ctrlV, JComponent.WHEN_IN_FOCUSED_WINDOW);
        canvas.registerKeyboardAction(copyAction, "copy",
                ctrlC, JComponent.WHEN_IN_FOCUSED_WINDOW);
        canvas.registerKeyboardAction(cutAction, "cut",
                ctrlX, JComponent.WHEN_IN_FOCUSED_WINDOW);
        canvas.registerKeyboardAction(selectAllAction, "selectAll",
                ctrlA, JComponent.WHEN_IN_FOCUSED_WINDOW);
        if (painter != null) {
            painterInfo.setVisible(true);
        }
    }

    @Override
    public void finish() {
        started = false;
        if (painter != null) {
            painter.removePainterListener(this);
        }

        getCanvasPanel().removeMouseListener(this);
        getCanvasPanel().removeMouseMotionListener(this);
        getCanvasPanel().removeKeyListener(this);

        textToolbar.getBoldTB().removeActionListener(this);
        textToolbar.getCenterAlignTB().removeActionListener(this);
        textToolbar.getFontFamilyCB().removeActionListener(this);
        textToolbar.getFontSizeCB().removeActionListener(this);
        textToolbar.getItalicTB().removeActionListener(this);
        textToolbar.getLeftAlignTB().removeActionListener(this);
        textToolbar.getRightAlignTB().removeActionListener(this);
        textToolbar.getStrikeTB().removeActionListener(this);
        textToolbar.getUnderlineTB().removeActionListener(this);
        CommonProperties.getInstance().removePropertyChangeListener(this);
        canvas.setCursor(CURSOR_DEFAULT);
        savePainter();

        canvas.unregisterKeyboardAction(ctrlV);
        canvas.unregisterKeyboardAction(ctrlC);
        canvas.unregisterKeyboardAction(ctrlX);
        canvas.unregisterKeyboardAction(ctrlA);

        editCurrentCB.setEnabled(true);
        canvas.repaint();

//        canvas.clearEditFrame();
    }

    @Override
    public boolean isStarted() {
        return started;
    }

    @Override
    public void delete() {
    }

    private void savePainter() {
        if (painter == null) {
            return;
        }

        if (caretTimer != null) {
            caretTimer.stop();
        }
        painter.setCaretVisible(false);

        if (painter.getText().trim().equals("") == false) {
            getCanvasPanel().saveCurrentPainter();
        }

        getCanvasPanel().abortCurrentPainter();

//        painter.setDrawMoveIcon(false);
        painter.setSelectionBegin(TextPainter.FIRST_VALID_POSITION);
        painter.setSelectionEnd(TextPainter.FIRST_VALID_POSITION);
        painter = null;
        canvas.repaint();
    }

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
        if (e.isConsumed()) {
            //System.out.println("consumed);")
            return;
        }
        // getCanvasPanel().saveCurrentPainter();

        mousePressedPoint = canvas.getNotZoomedPoint(e.getPoint());

        if (painter != null/* && painter.isOnMoveIcon(mousePressedPoint) == false*/
                //painter.isOnTextArea(mousePressedPoint)
                && painter.getBounds().contains(mousePressedPoint) == false) {
            savePainter();
        }

        if (painter == null) {
            if (painter != null) {
                painter.removePainterListener(this);
            }
            if (canvas.isEditCurrent() == false) {
                canvas.setEditCurrent(true);
                editCurrentCB.setSelected(true);
            }
            painter = new TextPainter();
            painter.addPainterListener(this);

            caretTimer = new CaretTimer(painter);
            caretTimer.start();
            setupPainter();
            painter.setX(mousePressedPoint.x);
            painter.setY(mousePressedPoint.y);
            getCanvasPanel().addAction(new PainterAction(canvas, painter));
            getCanvasPanel().requestFocusInWindow();
            getCanvasPanel().repaint();
        } else {
            int index = painter.getCharIndexAt(canvas.getNotZoomedPoint(e.getPoint()));
            if (index != -1) {
                painter.setCaretPosition(index);
                painter.setSelectionBegin(index);
                painter.setSelectionEnd(index);
                //   painter.setSelectionEnd(painter.getText().length());
                canvas.repaint();
            }
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        showCaret = true;
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (painter == null) {
            return;
        }

        switch (e.getKeyCode()) {
            case KeyEvent.VK_SHIFT:
                if (painter.getSelectionBegin() == painter.getSelectionEnd()) {
                    painter.setSelectionBegin(painter.getCaretPosition());
                    painter.setSelectionEnd(painter.getCaretPosition());
                }
                break;
            case KeyEvent.VK_DELETE: {
                if (e.isControlDown() || e.isShiftDown()) {
                    return;
                }
                if (painter.getSelectionBegin() != painter.getSelectionEnd()) {
                    deleteSelection();
                    return;
                }
                int caretPos = painter.getCaretPosition();
                if (caretPos >= painter.getTextLength()) {
                    return;
                }

                if (caretPos <= painter.getTextLength() + 2
                        && painter.charAt(caretPos) == '\n'
                        && painter.charAt(caretPos + 1) == '\r') {
                    painter.delete(caretPos, caretPos + 2);
                } else {
                    painter.deleteCharAt(caretPos);
                }

                return;
            }
            case KeyEvent.VK_BACK_SPACE: {
                if (e.isControlDown() || e.isShiftDown()) {
                    return;
                }
                if (painter.getSelectionBegin() != painter.getSelectionEnd()) {
                    deleteSelection();
                    return;
                }
                int caretPos = painter.getCaretPosition();
                if (caretPos < 2) {
                    return;
                }

                if (painter.charAt(caretPos - 2) == '\n'
                        && painter.charAt(caretPos - 1) == '\r') {
                    painter.delete(caretPos - 2, caretPos);
                } else {
                    painter.deleteCharAt(caretPos - 1);
                }
                return;
            }
            case KeyEvent.VK_ENTER:
                if (e.isControlDown() || e.isShiftDown()) {
                    return;
                }
                if (painter.getSelectionBegin() != painter.getSelectionEnd()) {
                    deleteSelection();
                }
                painter.insert(painter.getCaretPosition(), "\n\r");
                return;

            case KeyEvent.VK_ESCAPE:
                savePainter();
                return;
            case KeyEvent.VK_LEFT: {
                if (e.isControlDown()) {
                    return;
                }
                int caretPos = painter.getCaretPosition();
                if (caretPos > 1) {
                    caretPos--;
                    if (painter.charAt(caretPos) == '\r' && caretPos > 1) {
                        caretPos--;
                    }
                    painter.setCaretPosition(caretPos);
                    if (e.isShiftDown()) {
                        painter.setSelectionEnd(caretPos);
                    } else {
                        painter.setSelectionEnd(caretPos);
                        painter.setSelectionBegin(caretPos);
                    }
                }
                e.consume();
                return;
            }
            case KeyEvent.VK_RIGHT: {
                if (e.isControlDown()) {
                    return;
                }
                int caretPos = painter.getCaretPosition();
                if (caretPos < painter.getTextLength()) {
                    caretPos++;
                    painter.setCaretPosition(caretPos);
                    if (e.isShiftDown()) {
                        painter.setSelectionEnd(caretPos);
                    } else {
                        painter.setSelectionEnd(caretPos);
                        painter.setSelectionBegin(caretPos);
                    }
                }
                e.consume();
                return;
            }

            case KeyEvent.VK_UP: {
                painter.moveCaretUp();
                int caretPos = painter.getCaretPosition();
                if (e.isShiftDown()) {
                    painter.setSelectionEnd(caretPos);
                } else {
                    painter.setSelectionEnd(caretPos);
                    painter.setSelectionBegin(caretPos);
                }
                e.consume();
                return;
            }

            case KeyEvent.VK_DOWN: {
                painter.moveCaretDown();
                int caretPos = painter.getCaretPosition();
                if (e.isShiftDown()) {
                    painter.setSelectionEnd(caretPos);
                } else {
                    painter.setSelectionEnd(caretPos);
                    painter.setSelectionBegin(caretPos);
                }
                e.consume();
                return;
            }

            case KeyEvent.VK_HOME: {
                e.consume();
                if (e.isShiftDown() && e.isControlDown()) {
//                    if (painter.isSelectionVisible()) {
//                        painter.setSelectionEnd(Math.max(painter.getSelectionBegin(),
//                                painter.getSelectionEnd()));
//                    } else {
//                        painter.setSelectionEnd(painter.getCaretPosition());
//                    }
                    painter.setCaretPosition(TextPainter.FIRST_VALID_POSITION);
                    painter.setSelectionEnd(TextPainter.FIRST_VALID_POSITION);
                    return;
                } else if (e.isControlDown()) {
                    painter.setCaretPosition(TextPainter.FIRST_VALID_POSITION);
                    painter.setSelectionBegin(TextPainter.FIRST_VALID_POSITION);
                    painter.setSelectionEnd(TextPainter.FIRST_VALID_POSITION);
                    return;
                }

                painter.moveCaretToLineBegin();
                if (e.isShiftDown()) {
                    painter.setSelectionEnd(painter.getCaretPosition());
                } else {
                    //int caretPos = painter.getCaretPosition();
                    painter.setSelectionEnd(painter.getCaretPosition());
                    painter.setSelectionBegin(painter.getCaretPosition());
                }


                return;
            }

            case KeyEvent.VK_END: {
                e.consume();
                if (e.isShiftDown() && e.isControlDown()) {
                    int endPos = painter.getTextLength();
                    painter.setCaretPosition(endPos);
                    painter.setSelectionEnd(endPos);
                    return;
                } else if (e.isControlDown()) {
                    int endPos = painter.getTextLength();
                    painter.setCaretPosition(endPos);
                    painter.setSelectionBegin(endPos);
                    painter.setSelectionEnd(endPos);
                    return;
                }

                painter.moveCaretToLineEnd();
                int caretPos = painter.getCaretPosition();
                if (e.isShiftDown()) {
                    painter.setSelectionEnd(caretPos);
                } else {
                    painter.setSelectionEnd(caretPos);
                    painter.setSelectionBegin(caretPos);
                }


                return;
            }
        }

        if (e.getKeyCode() < (int) ' ' || e.isControlDown()) {
            return;
        }

        if (painter.getSelectionBegin() != painter.getSelectionEnd()) {
            deleteSelection();
        }

        int caretPos = painter.getCaretPosition();
        //  System.out.println("caretPos = " + caretPos);
        painter.insert(caretPos, e.getKeyChar());
        painter.setSelectionEnd(caretPos);
        painter.setSelectionBegin(caretPos);
        // painter.setCaretPosition(caretPos + 1);
        canvas.repaint();
    }

    private void deleteSelection() {
        if (painter.getSelectionBegin() < painter.getSelectionEnd()) {
//            if (painter.getSelectionBegin() == 0) {
//                System.out.println("C) caretPos == 0 !!");
//            }
            painter.delete(painter.getSelectionBegin(), painter.getSelectionEnd());
        } else {
//            if (painter.getSelectionBegin() == 0) {
//                System.out.println("D) caretPos == 0 !!");
//            }
            painter.delete(painter.getSelectionEnd(), painter.getSelectionBegin());
        }
        painter.setSelectionBegin(TextPainter.FIRST_VALID_POSITION);
        painter.setSelectionEnd(TextPainter.FIRST_VALID_POSITION);
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }

    @Override
    public void propertyChanged(Object property, Type type) {
        if (type != Type.COLOR) {
            return;
        }

        setupPainter();
        canvas.repaint();

        new Thread(this).start();


    }

    @Override
    public void actionPerformed(ActionEvent e) {
        setupPainter();
        canvas.repaint();

        new Thread(this).start();
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        if (e.isConsumed()) {
            painter.setCaretVisible(false);
            showCaret = false;
            return;
        } else {
            showCaret = true;
        }
        if (painter == null) {
            return;
        }

        //Point p = canvas.getZoomedPoint(e.getPoint());

//        if (canMovePainter) {
//            canvasPanel.setCursor(CURSOR_DEFAULT);
//
//            int dx = p.x - mousePressedPoint.x;
//            int dy = p.y - mousePressedPoint.y;
//
//            painter.setX(startingPoint.x + dx);
//            painter.setY(startingPoint.y + dy);
//        } else {
        int index = painter.getCharIndexAt(canvas.getNotZoomedPoint(e.getPoint()));
        if (index != -1) {
            painter.setCaretPosition(index);
            painter.setSelectionEnd(index);

            canvas.repaint();
        }
        //   }
        canvas.repaint();
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        if (e.isConsumed() || painter == null) {
            return;
        }

//        if (painter.isOnMoveIcon(canvas.getZoomedPoint(e.getPoint()))) {
//            canDrag = true;
//            startingPoint.x = painter.getX();
//            startingPoint.y = painter.getY();
//            canvasPanel.setCursor(CURSOR_HAND);
//
//
//        } else {
        //      canMovePainter = false;
        if (painter.getBounds().contains(canvas.getNotZoomedPoint(e.getPoint()))) {
            canvas.setCursor(CURSOR_TEXT);
        } else {
            canvas.setCursor(CURSOR_DEFAULT);
        }


        //  }
    }

    @Override
    public void run() {
        try {
            Thread.sleep(200);
        } catch (InterruptedException ex) {
        }
        canvas.requestFocusInWindow();
    }

    private class CaretTimer implements ActionListener {

        private javax.swing.Timer timer = new javax.swing.Timer(300, this);
        private TextPainter painter;
        private boolean caretVisible;

        public CaretTimer(TextPainter painter) {
            this.painter = painter;
        }

        public TextPainter getPainter() {
            return painter;
        }

        public void setPainter(TextPainter painter) {
            this.painter = painter;
        }

        public void actionPerformed(ActionEvent e) {
            if (showCaret) {
                caretVisible = !caretVisible;
                painter.setCaretVisible(caretVisible);
                canvas.repaint();
            } else {
                painter.setCaretVisible(false);
            }
        }

        public void start() {
            timer.start();
        }

        public void stop() {
            timer.stop();
        }

        public void restart() {
            timer.restart();
        }
    }

    @Override
    public boolean copyToClipboard() {
        return true;
    }
}
