package jmulticlip.gui.graph.tools.tool;

import jmulticlip.gui.graph.CommonProperties.Type;
import jmulticlip.gui.graph.components.ColorComboBox;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import jmulticlip.gui.graph.CanvasPanel;
import jmulticlip.gui.graph.CommonProperties;
import jmulticlip.gui.graph.CommonPropertyChangeListener;
import jmulticlip.gui.graph.GraphicsViewPanel;
import jmulticlip.gui.graph.PainterInfoPanel;
import jmulticlip.gui.graph.tools.painter.AbstractPainter;
import jmulticlip.gui.graph.tools.tool.AbstractPainterTool;
import jmulticlip.gui.graph.tools.action.DeletePainterAction;
import jmulticlip.gui.graph.tools.action.PainterAction;
import jmulticlip.gui.graph.tools.action.PainterListener;
import jmulticlip.gui.graph.tools.painter.PencilPainter;
import jmulticlip.gui.graph.tools.toolbar.PencilToolbar;
import jmulticlip.gui.graph.tools.TransferableElement;

/**
 *
 * @author Revers
 */
public class PencilPainterTool extends AbstractPainterTool<PencilPainter> implements
        MouseListener, MouseMotionListener, PainterListener {

    //private PencilPainter painter;
    private PencilToolbar pencilToolbar;
    private boolean started;
    private PainterInfoPanel painterInfo;
    private CommonPropertyChangeListener commonPropertyChangeListener = new CommonPropertyChangeListener() {

        @Override
        public void propertyChanged(Object property, Type type) {
            if (painter == null || canvas.isEditCurrent() == false) {
                return;
            }

            switch (type) {
                case COLOR:
                    painter.setColor((Color) property);
                    break;

                case LINE_WIDTH:
                    painter.setLineWidth((Float) property);
                    break;

                default:
                    return;
            }

            canvas.repaint();
        }
    };

    public PencilPainterTool(CanvasPanel canvas, PencilToolbar pencilToolbar, PainterInfoPanel painterInfo) {
        this(canvas, pencilToolbar, null, painterInfo);
    }

    public PencilPainterTool(CanvasPanel canvas,
            PencilToolbar pencilToolbar, PencilPainter painter, PainterInfoPanel painterInfo) {
        super(canvas);

        this.pencilToolbar = pencilToolbar;
        this.painterInfo = painterInfo;
        painterInfo.setRectLabels();

        if (painter != null) {
            setPainter(painter);

            painter.addPainterListener(this);
            painterChanged(painter);
            canvas.setCurrentPainter(painter);
        }
    }

    @Override
    public void painterChanged(AbstractPainter painter) {
        Rectangle rect = painter.getBounds();
        painterInfo.setInfoTitle("Ołówek");
        painterInfo.setInfoX(rect.x);
        painterInfo.setInfoY(rect.y);
        painterInfo.setInfoWidth(rect.width);
        painterInfo.setInfoHeight(rect.height);
        painterInfo.setVisible(true);
    }

    @Override
    protected void initToolbars(PencilPainter painter) {
        //colorComboBox.getModel().setSelectedItem(painter.getColor());
        if (canvas.isEditCurrent() == false) {
            return;
        }

        CommonProperties.getInstance().setColor(painter.getColor());
        pencilToolbar.setLineWidth(painter.getLineWidth());
    }

    @Override
    public void start() {
        started = true;

        getCanvasPanel().addMouseListener(this);
        getCanvasPanel().addMouseMotionListener(this);

        CommonProperties.getInstance().addPropertyChangeListener(commonPropertyChangeListener,
                Type.COLOR, Type.LINE_WIDTH);
        //colorComboBox.addActionListener(colorChangeListener);
        if (painter != null) {
            painterInfo.setVisible(true);
        }
    }

    @Override
    public void finish() {
        started = false;
        if (painter != null) {
            painter.removePainterListener(this);
        }
        painterInfo.setVisible(false);

        getCanvasPanel().saveCurrentPainter();
        getCanvasPanel().removeMouseListener(this);
        getCanvasPanel().removeMouseMotionListener(this);
        getCanvasPanel().abortCurrentPainter();

        CommonProperties.getInstance().removePropertyChangeListener(commonPropertyChangeListener);
        canvas.repaint();
    }

    @Override
    public boolean isStarted() {
        return started;
    }

    @Override
    public void delete() {
        if (painter != null) {
            DeletePainterAction action = new DeletePainterAction(canvas, painter);
            canvas.addAction(action);

            painter = null;
            canvas.repaint();
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
        if (e.isConsumed()) {
            return;
        }
        getCanvasPanel().saveCurrentPainter();
        if (painter != null) {
            painter.removePainterListener(this);
        }
        painter = new PencilPainter(CommonProperties.getInstance().getColor());
        painter.addPainterListener(this);
        painter.setEditable(false);
        painter.setLineWidth(pencilToolbar.getLineWidth());
        getCanvasPanel().addAction(new PainterAction(canvas, painter));
        painter.addPoint(canvas.getNotZoomedPoint(e.getPoint()));
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        if (e.isConsumed()) {
            return;
        }

        painter.setEditable(true);
        getCanvasPanel().repaint();
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        if (e.isConsumed()) {
            return;
        }
        painter.addPoint(canvas.getNotZoomedPoint(e.getPoint()));
        getCanvasPanel().repaint();
    }

    @Override
    public void mouseMoved(MouseEvent e) {
    }

    @Override
    public boolean copyToClipboard() {
        if (painter == null) {
            return false;
        }

        PencilPainter painterCopy = (PencilPainter) painter.clonePainter();

        TransferableElement element = new TransferableElement(painterCopy, PencilPainter.getDataFlavor());
        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(element, null);

        return true;
    }
}
