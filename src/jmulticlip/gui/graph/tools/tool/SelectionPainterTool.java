package jmulticlip.gui.graph.tools.tool;

import jmulticlip.gui.graph.tools.toolbar.SelectionToolbar;
import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.KeyStroke;
import jmulticlip.gui.graph.CanvasPanel;
import jmulticlip.gui.graph.PainterInfoPanel;
import jmulticlip.gui.graph.tools.painter.AbstractPainter;
import jmulticlip.gui.graph.util.Cursors;
import jmulticlip.gui.graph.tools.action.DeletePainterAction;
import jmulticlip.gui.graph.tools.action.DoublePainterAction;
import jmulticlip.gui.graph.tools.painter.ImagePainter;
import jmulticlip.gui.graph.tools.action.PainterAction;
import jmulticlip.gui.graph.tools.action.PainterListener;
import jmulticlip.gui.graph.util.RectCorners;
import jmulticlip.gui.graph.tools.painter.RectanglePainter;
import jmulticlip.gui.graph.tools.SelectionFrame;
import jmulticlip.gui.graph.tools.TransferableElement;

/**
 *
 * @author Revers
 */
public class SelectionPainterTool extends AbstractPainterTool<ImagePainter>
        implements MouseListener, MouseMotionListener, Cursors, PainterListener {

    private SelectionFrame selectionFrame;

    @Override
    protected void initToolbars(ImagePainter painter) {
    }

    private enum GrabState {

        NONE, MIDDLE, N, S, W, E, NW, NE, SW, SE
    }
    private static final int MOUSE_OFFSET = 1;
    private GrabState state = GrabState.NONE;
    private RectCorners corners = new RectCorners();
    private boolean resizing;
    private Point lastMousePoint;
    private boolean started;
    private boolean useDefaultCursor = true;
    private PainterInfoPanel painterInfo;
    private SelectionToolbar toolbar;
    private Action selectAllAction = new AbstractAction() {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (selectionFrame == null) {
                selectionFrame = new SelectionFrame(canvas);
                canvas.setSelectionFrame(selectionFrame);

                canvas.saveCurrentPainter();
            }
            painterInfo.setVisible(false);
            selectionFrame.setX(0);
            selectionFrame.setY(0);
            selectionFrame.setWidth(canvas.getCanvasImage().getWidth());
            selectionFrame.setHeight(canvas.getCanvasImage().getHeight());

            painterInfo.setInfoX(selectionFrame.getX());
            painterInfo.setInfoY(selectionFrame.getY());
            painterInfo.setInfoWidth(selectionFrame.getWidth());
            painterInfo.setInfoHeight(selectionFrame.getHeight());
            painterInfo.setVisible(true);
            canvas.setCursor(CURSOR_MOVE);
            state = GrabState.MIDDLE;
            //setProperState(canvas.getNotZoomedPoint(e.getPoint()), MOUSE_OFFSET);
            corners.setCorners(selectionFrame.getBounds());
            System.out.println("selectAllAction");
            canvas.repaint();
        }
    };
    private KeyStroke ctrlA = KeyStroke.getKeyStroke(KeyEvent.VK_A, KeyEvent.CTRL_MASK, true);

    public SelectionPainterTool(CanvasPanel canvas, SelectionToolbar toolbar,
            PainterInfoPanel painterInfo) {
        this(canvas, toolbar, null, painterInfo);
    }

    public SelectionPainterTool(CanvasPanel canvas, SelectionToolbar toolbar, ImagePainter painter, PainterInfoPanel painterInfo) {
        super(canvas);
        this.toolbar = toolbar;
        this.painterInfo = painterInfo;
        painterInfo.setRectLabels();
        if (painter != null) {
            setPainter(painter);
            painter.addPainterListener(this);
            painterChanged(painter);
            canvas.setCurrentPainter(painter);
        }
    }

    @Override
    public void painterChanged(AbstractPainter painter) {
        Rectangle rect = painter.getBounds();
        painterInfo.setInfoTitle("Obrazek");
        painterInfo.setInfoX(rect.x);
        painterInfo.setInfoY(rect.y);
        painterInfo.setInfoWidth(rect.width);
        painterInfo.setInfoHeight(rect.height);
        painterInfo.setVisible(true);
    }

    @Override
    public boolean copyToClipboard() {
        if (selectionFrame != null) {
            Rectangle rect = selectionFrame.getBounds();

            BufferedImage image = new BufferedImage(rect.width, rect.height,
                    BufferedImage.TYPE_INT_RGB);
            Graphics2D g2 = image.createGraphics();
            g2.drawImage(canvas.getCanvasImage(), 0, 0, rect.width, rect.height, rect.x,
                    rect.y, rect.x + rect.width, rect.y + rect.height, null);
            g2.dispose();

            TransferableElement element = new TransferableElement(image, ImagePainter.getDataFlavor());
            Toolkit.getDefaultToolkit().getSystemClipboard().setContents(element, null);
            //ClipboardManager.setClipboard(image);
            return true;
        } else if (painter != null) {
            TransferableElement element = new TransferableElement(painter.getImage(), ImagePainter.getDataFlavor());
            Toolkit.getDefaultToolkit().getSystemClipboard().setContents(element, null);
            //ClipboardManager.setClipboard();
            return true;
        }
        return false;
    }

    public CanvasPanel getCanvas() {
        return canvas;
    }

    public void clear() {
        if (selectionFrame == null) {
            return;
        }
        state = GrabState.NONE;
        selectionFrame.clear();
    }

    public void start() {
        started = true;
        canvas.addMouseListener(this);
        canvas.addMouseMotionListener(this);
        canvas.registerKeyboardAction(selectAllAction, "select all",
                ctrlA, JComponent.WHEN_IN_FOCUSED_WINDOW);
        if (painter != null) {
            painterInfo.setVisible(true);
        }

        //toolbar.getCopyButton().addActionListener();
    }

    public void finish() {
        started = false;

        if (painter != null) {
            painter.removePainterListener(this);
        }
        painterInfo.setVisible(false);

        canvas.removeMouseListener(this);
        canvas.removeMouseMotionListener(this);
//        canvas.clearEditFrame();
        canvas.setSelectionFrame(null);
        // System.out.println("finish");
        canvas.saveCurrentPainter();
        canvas.setCursor(CURSOR_DEFAULT);
        canvas.unregisterKeyboardAction(ctrlA);
        canvas.repaint();

    }

    @Override
    public boolean isStarted() {
        return started;
    }

    @Override
    public void delete() {
        if (selectionFrame != null) {
            Rectangle rect = selectionFrame.getBounds();
            RectanglePainter rectPainter = new RectanglePainter(Color.white, Color.white, rect.x, rect.y,
                    rect.width, rect.height);
            rectPainter.setFill(true);
            rectPainter.setEditable(false);
            rectPainter.setMoveable(false);
            rectPainter.setResizable(false);

            PainterAction action = new PainterAction(canvas, rectPainter);
            canvas.addAction(action);
            selectionFrame.clear();
            selectionFrame = null;
            state = GrabState.NONE;
            canvas.setCursor(CURSOR_DEFAULT);
            canvas.repaint();
        } else if (painter != null) {

            DeletePainterAction action = new DeletePainterAction(canvas, painter);
            canvas.addAction(action);
            painter.removePainterListener(this);
            painterInfo.setVisible(false);
            painter = null;

            state = GrabState.NONE;
            canvas.setCursor(CURSOR_DEFAULT);
            canvas.repaint();
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
        if (e.isConsumed()) {
            return;
        }

        lastMousePoint = canvas.getNotZoomedPoint(e.getPoint());

        if (state == GrabState.NONE) {
            if (selectionFrame == null) {
                selectionFrame = new SelectionFrame(canvas);
                canvas.setSelectionFrame(selectionFrame);

                canvas.saveCurrentPainter();
            }
            painterInfo.setVisible(false);
            selectionFrame.setX(lastMousePoint.x);
            selectionFrame.setY(lastMousePoint.y);
            selectionFrame.setWidth(0);
            selectionFrame.setHeight(0);
//            bounds.x = p.x;
//            bounds.y = p.y;
//            bounds.width = 0;
//            bounds.height = 0;

            useDefaultCursor = true;
            corners.setCorners(selectionFrame.getBounds());
        }// else {
        //   restoreCursor = false;
        // }



        canvas.repaint();
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        if (e.isConsumed() || selectionFrame == null) {
            return;
        }

        useDefaultCursor = false;
        resizing = false;
        setProperState(canvas.getNotZoomedPoint(e.getPoint()), MOUSE_OFFSET);
        corners.setCorners(selectionFrame.getBounds());
        state = GrabState.NONE;
        canvas.setCursor(CURSOR_DEFAULT);
        // canvas.repaint();
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    private void selectionDrag() {
        Rectangle rect = selectionFrame.getBounds();

        BufferedImage image = new BufferedImage(rect.width, rect.height,
                BufferedImage.TYPE_INT_RGB);
        Graphics2D g2 = image.createGraphics();
        g2.drawImage(canvas.getCanvasImage(), 0, 0, rect.width, rect.height, rect.x,
                rect.y, rect.x + rect.width, rect.y + rect.height, null);
        g2.dispose();

        RectanglePainter rectPainter = new RectanglePainter(Color.white, Color.white, rect.x, rect.y,
                rect.width, rect.height);
        rectPainter.setFill(true);
        rectPainter.setEditable(false);
        rectPainter.setMoveable(false);
        rectPainter.setResizable(false);
        ImagePainter imgPainter = new ImagePainter(image);

        imgPainter.setX(rect.x);
        imgPainter.setY(rect.y);
        canvas.setSelectionFrame(null);
        selectionFrame = null;
        DoublePainterAction action = new DoublePainterAction(canvas, rectPainter, imgPainter);

        painter = imgPainter;
        painter.addPainterListener(this);
        state = GrabState.NONE;
        canvas.addAction(action);
        canvas.repaint();
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        if (e.isConsumed() || selectionFrame == null) {
            return;
        }
        if (canvas.getSelectionFrame() != null) {
            Rectangle rect = canvas.getSelectionFrame().getBounds();
            painterInfo.setInfoTitle("Zaznaczenie");
            painterInfo.setInfoX(rect.x);
            painterInfo.setInfoY(rect.y);
            painterInfo.setInfoWidth(rect.width);
            painterInfo.setInfoHeight(rect.height);
            painterInfo.setVisible(true);
        }

        Point p = canvas.getNotZoomedPoint(e.getPoint());

        if (state == GrabState.MIDDLE) {
            selectionDrag();
            return;
        }

        resizing = true;

        Rectangle rect = (Rectangle) selectionFrame.getBounds().clone();

        if (state == GrabState.NONE) {
            state = GrabState.SE;
        }
        switch (state) {

            case NW: {
                int dx = corners.upperLeft.x - p.x;
                int dy = corners.upperLeft.y - p.y;

                rect.x -= dx;
                rect.width += dx;
                rect.y -= dy;
                rect.height += dy;

                corners.upperLeft.x = rect.x;
                corners.upperLeft.y = rect.y;
                e.consume();
                break;
            }
            case NE: {
                int dx = corners.upperRight.x - p.x;
                int dy = corners.upperRight.y - p.y;

                rect.width -= dx;
                rect.y -= dy;
                rect.height += dy;

                corners.upperRight.x = rect.x + rect.width;
                corners.upperRight.y = rect.y;
                e.consume();
                break;
            }
            case SW: {
                int dx = corners.bottomLeft.x - p.x;
                int dy = corners.bottomLeft.y - p.y;

                rect.x -= dx;
                rect.width += dx;
                rect.height -= dy;

                corners.bottomLeft.x = rect.x;
                corners.bottomLeft.y = rect.y + rect.height;
                e.consume();
                break;
            }
            case SE: {
                int dx = corners.bottomRight.x - p.x;
                int dy = corners.bottomRight.y - p.y;

                rect.width -= dx;
                rect.height -= dy;

                corners.bottomRight.x = rect.x + rect.width;
                corners.bottomRight.y = rect.y + rect.height;
                e.consume();
                break;
            }
            case W: {
                int dx = corners.upperLeft.x - p.x;

                rect.x -= dx;
                rect.width += dx;

                corners.upperLeft.x = rect.x;
                e.consume();
                break;
            }

            case E: {
                int dx = corners.upperRight.x - p.x;

                rect.width -= dx;

                corners.upperRight.x = rect.x + rect.width;
                e.consume();
                break;
            }
            case S: {
                int dy = corners.bottomRight.y - p.y;

                rect.height -= dy;

                corners.bottomRight.y = rect.y + rect.height;
                e.consume();
                break;
            }
            case N: {
                int dy = corners.upperLeft.y - p.y;

                rect.y -= dy;
                rect.height += dy;

                corners.upperLeft.y = rect.y;
                e.consume();
                break;
            }
        }
        if (rect.width < 0) {
            rect.x += rect.width;
            rect.width = -rect.width;
        }
        if (rect.height < 0) {
            rect.y += rect.height;
            rect.height = -rect.height;
        }

        if (rect.x < 0) {
            rect.width += rect.x;
            rect.x = 0;
        } else if (rect.x + rect.width > canvas.getCanvasImage().getWidth()) {
            rect.width = canvas.getCanvasImage().getWidth() - rect.x;
        }

        if (rect.y < 0) {
            rect.height += rect.y;
            rect.y = 0;
        } else if (rect.y + rect.height > canvas.getCanvasImage().getHeight()) {
            rect.height = canvas.getCanvasImage().getHeight() - rect.y;
        }

        if (rect.equals(selectionFrame.getBounds())) {
            return;
        }

        selectionFrame.setX(rect.x);
        selectionFrame.setY(rect.y);
        selectionFrame.setWidth(rect.width);
        selectionFrame.setHeight(rect.height);
        corners.setCorners(rect);

        setProperState(p, 0);
        canvas.repaint();
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        if (e.isConsumed() || selectionFrame == null) {
            return;
        }

        setProperState(canvas.getNotZoomedPoint(e.getPoint()), MOUSE_OFFSET);
    }

    private boolean handleOneWallDrag(Point p, int range) {
        if (resizing == false) {
            return false;
        }

        switch (state) {
            case S:
            case N:
            case W:
            case E:
                break;
            default:
                return false;
        }

        final int OFFSET = 10000000;
        if (isNearX(p, corners.upperLeft.x,
                corners.upperLeft.y - OFFSET, corners.bottomLeft.y + OFFSET, range)) {
            canvas.setCursor(CURSOR_RESIZE_W);
            state = GrabState.W;
            return true;
        } else if (isNearX(p, corners.upperRight.x,
                corners.upperRight.y - OFFSET, corners.bottomRight.y + OFFSET, range)) {
            canvas.setCursor(CURSOR_RESIZE_E);
            state = GrabState.E;
            return true;
        } else if (isNearY(p, corners.upperRight.y,
                corners.upperLeft.x - OFFSET, corners.upperRight.x + OFFSET, range)) {
            canvas.setCursor(CURSOR_RESIZE_N);
            state = GrabState.N;
            return true;
        } else if (isNearY(p, corners.bottomRight.y,
                corners.bottomLeft.x - OFFSET, corners.bottomRight.x + OFFSET, range)) {
            canvas.setCursor(CURSOR_RESIZE_S);
            state = GrabState.S;
            return true;
        }

        return false;
    }

    private void setProperState(Point p, int range) {

        if (corners.upperLeft.x == corners.bottomRight.x
                || corners.upperLeft.y == corners.bottomRight.y) {
            return;
        }

        if (handleOneWallDrag(p, range)) {
            return;
        }
        Cursor cursor = null;

        if (isNear(corners.upperLeft, p, range)) {
            cursor = CURSOR_RESIZE_NW;
            state = GrabState.NW;
        } else if (isNear(corners.upperRight, p, range)) {
            cursor = CURSOR_RESIZE_NE;
            state = GrabState.NE;
        } else if (isNear(corners.bottomLeft, p, range)) {
            cursor = CURSOR_RESIZE_SW;
            state = GrabState.SW;
        } else if (isNear(corners.bottomRight, p, range)) {
            cursor = CURSOR_RESIZE_SE;
            state = GrabState.SE;
        } else if (isNearX(p, corners.upperLeft.x,
                corners.upperLeft.y, corners.bottomLeft.y, range)) {
            cursor = CURSOR_RESIZE_W;
            state = GrabState.W;
        } else if (isNearX(p, corners.upperRight.x,
                corners.upperRight.y, corners.bottomRight.y, range)) {
            cursor = CURSOR_RESIZE_E;
            state = GrabState.E;
        } else if (isNearY(p, corners.upperRight.y,
                corners.upperLeft.x, corners.upperRight.x, range)) {
            cursor = CURSOR_RESIZE_N;
            state = GrabState.N;
        } else if (isNearY(p, corners.bottomRight.y,
                corners.bottomLeft.x, corners.bottomRight.x, range)) {
            cursor = CURSOR_RESIZE_S;
            state = GrabState.S;
        } else if (corners.isInside(p)) {
            if (resizing == false) {
                if (canvas.isEditCurrent()) {
                    cursor = CURSOR_MOVE;
                    state = GrabState.MIDDLE;
                } else {
                    cursor = CURSOR_DEFAULT;
                    state = GrabState.NONE;
                }
            }
        } else {
            if (resizing == false) {
                cursor = CURSOR_DEFAULT;
                state = GrabState.NONE;
            }
        }

        if (useDefaultCursor) {
            canvas.setCursor(CURSOR_DEFAULT);
        } else {
            canvas.setCursor(cursor);
        }
    }

    //private boolean
    private boolean isNearY(Point mousePoint, int y, int x1, int x2, int range) {
        if (mousePoint.x <= x2 && mousePoint.x >= x1
                && mousePoint.y <= y + range && mousePoint.y >= y - range) {
            return true;
        }

        return false;
    }

    private boolean isNearX(Point mousePoint, int x, int y1, int y2, int range) {
        if (mousePoint.x <= x + range && mousePoint.x >= x - range
                && mousePoint.y >= y1 && mousePoint.y <= y2) {
            return true;
        }

        return false;
    }

    private boolean isNear(Point targetPoint, Point mousePoint, int range) {
        if (mousePoint.x <= targetPoint.x + range && mousePoint.x >= targetPoint.x - range
                && mousePoint.y <= targetPoint.y + range && mousePoint.y >= targetPoint.y - range) {
            return true;
        }

        return false;
    }
}
