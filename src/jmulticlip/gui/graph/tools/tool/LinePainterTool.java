package jmulticlip.gui.graph.tools.tool;

import jmulticlip.gui.graph.tools.toolbar.LineToolbar;
import java.util.Arrays;
import jmulticlip.gui.graph.components.StrokeComboBox;
import jmulticlip.gui.graph.components.ColorComboBox;
import jmulticlip.gui.graph.components.ArrowComboBox;
import java.awt.*;
import java.awt.datatransfer.DataFlavor;
import java.awt.event.*;
import javax.swing.ComboBoxModel;
import jmulticlip.gui.graph.CanvasPanel;
import jmulticlip.gui.graph.CommonProperties;
import jmulticlip.gui.graph.CommonProperties.Type;
import jmulticlip.gui.graph.CommonPropertyChangeListener;
import jmulticlip.gui.graph.PainterInfoPanel;
import jmulticlip.gui.graph.tools.painter.AbstractPainter;
import jmulticlip.gui.graph.tools.ArrowedLine;
import jmulticlip.gui.graph.tools.action.DeletePainterAction;
import jmulticlip.gui.graph.tools.painter.LinePainter;
import jmulticlip.gui.graph.tools.action.PainterAction;
import jmulticlip.gui.graph.tools.action.PainterListener;
import jmulticlip.gui.graph.tools.TransferableElement;

/**
 *
 * @author Revers
 */
public class LinePainterTool extends AbstractPainterTool<LinePainter> implements
        MouseListener, MouseMotionListener, KeyListener, PainterListener, ActionListener {

    private Point startingPoint;
    private boolean shiftPressed;
    private LineToolbar lineToolbar;
    private boolean started;
    private PainterInfoPanel painterInfo;
    private CommonPropertyChangeListener commonPropertyChangeListener = new CommonPropertyChangeListener() {

        @Override
        public void propertyChanged(Object property, Type type) {

            if (painter == null || canvas.isEditCurrent() == false) {
                return;
            }

            switch (type) {
                case COLOR:
                    painter.setColor((Color) property);
                    break;
                case LINE_WIDTH:
                    painter.setLineWidth((Float) property);
                    break;
                case DASH_STROKE:
                    painter.setDashStroke((BasicStroke) property);
                    break;
                default:
                    return;
            }

            canvas.repaint();
        }
    };

    public LinePainterTool(CanvasPanel canvas,
            LineToolbar lineToolBar, PainterInfoPanel painterInfo) {
        this(canvas, lineToolBar, null, painterInfo);
    }

    public LinePainterTool(CanvasPanel canvas,
            LineToolbar lineToolBar, LinePainter painter, PainterInfoPanel painterInfo) {
        super(canvas);//, painter);
        this.lineToolbar = lineToolBar;
        this.painterInfo = painterInfo;
        painterInfo.setLineLabels();
        if (painter != null) {
            setPainter(painter);
            painter.addPainterListener(this);
            painterChanged(painter);
            canvas.setCurrentPainter(painter);
        }
    }

    @Override
    public void painterChanged(AbstractPainter painter) {
        LinePainter line = (LinePainter) painter;
        Point shift = line.getShift();
        int x1 = line.getX1() + shift.x;
        int y1 = line.getY1() + shift.y;
        int x2 = line.getX2() + shift.x;
        int y2 = line.getY2() + shift.y;

        painterInfo.setInfoTitle("Linia");
        painterInfo.setInfoX(x1);
        painterInfo.setInfoY(y1);
        painterInfo.setInfoWidth(x2);
        painterInfo.setInfoHeight(y2);
        painterInfo.setVisible(true);
    }

    @Override
    protected void initToolbars(LinePainter painter) {
        if (canvas.isEditCurrent() == false) {
            return;
        }
        CommonProperties.getInstance().setColor(painter.getColor());

        BasicStroke painterStroke = painter.getStroke();
        float tickness = painterStroke.getLineWidth();

        lineToolbar.setLineWidth(tickness);
        lineToolbar.setDashStroke(painterStroke);
        lineToolbar.setArrowedLine(painter.getArrowedLine());
    }

    @Override
    public void start() {
        started = true;
        getCanvasPanel().addMouseListener(this);
        getCanvasPanel().addMouseMotionListener(this);
        getCanvasPanel().addKeyListener(this);

        lineToolbar.getArrowComboBox().addActionListener(this);

        CommonProperties.getInstance().addPropertyChangeListener(
                commonPropertyChangeListener,
                Type.COLOR,
                Type.DASH_STROKE,
                Type.LINE_WIDTH);

        if (painter != null) {
            painterInfo.setVisible(true);
        }
    }

    @Override
    public void finish() {
        started = false;
        painterInfo.setVisible(false);
        if (painter != null) {
            painter.removePainterListener(this);
        }

        getCanvasPanel().saveCurrentPainter();
        getCanvasPanel().removeMouseListener(this);
        getCanvasPanel().removeMouseMotionListener(this);
        getCanvasPanel().removeKeyListener(this);
        lineToolbar.getArrowComboBox().removeActionListener(this);

        CommonProperties.getInstance().removePropertyChangeListener(commonPropertyChangeListener);
        canvas.repaint();
    }

    @Override
    public boolean isStarted() {
        return started;
    }

    @Override
    public void delete() {
        if (painter != null) {
            DeletePainterAction action = new DeletePainterAction(canvas, painter);
            canvas.addAction(action);

            painter = null;
            canvas.repaint();
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
        if (e.isConsumed()) {
            return;
        }

        if (painter != null) {
            getCanvasPanel().saveCurrentPainter();
        }

        startingPoint = canvas.getNotZoomedPoint(e.getPoint());
        if (painter != null) {
            painter.removePainterListener(this);
        }
        painter = new LinePainter(CommonProperties.getInstance().getColor(),
                startingPoint.x, startingPoint.y,
                startingPoint.x, startingPoint.y);
        painter.addPainterListener(this);
        painter.setEditable(false);
        painter.setArrowedLine(lineToolbar.getArrowedLine());
        painter.setDashStroke(lineToolbar.getDashStroke());
        painter.setLineWidth(lineToolbar.getLineWidth());
        getCanvasPanel().addAction(new PainterAction(canvas, painter));
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        if (e.isConsumed()) {
            return;
        }

        getCanvasPanel().repaint();
        painter.setEditable(true);
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        if (e.isConsumed()) {
            return;
        }

        Point p = canvas.getNotZoomedPoint(e.getPoint());

        if (shiftPressed == true) {
            int dx = p.x - startingPoint.x;
            int dy = p.y - startingPoint.y;
            if (Math.abs(dx) > Math.abs(dy)) {
                painter.setX2(p.x);
                painter.setY2(startingPoint.y);
            } else {
                painter.setX2(startingPoint.x);
                painter.setY2(p.y);
            }
        } else {
            painter.setX2(p.x);
            painter.setY2(p.y);
        }
        getCanvasPanel().repaint();
    }

    @Override
    public void mouseMoved(MouseEvent e) {
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_SHIFT) {
            shiftPressed = true;
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_SHIFT) {
            shiftPressed = false;
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if (painter != null && canvas.isEditCurrent()) {
            painter.setArrowedLine(lineToolbar.getArrowedLine());
            canvas.repaint();
        }
    }

    @Override
    public boolean copyToClipboard() {
        if (painter == null) {
            return false;
        }

        LinePainter painterCopy = (LinePainter) painter.clonePainter();

        TransferableElement element = new TransferableElement(painterCopy, LinePainter.getDataFlavor());
        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(element, null);

        return true;
    }
}
