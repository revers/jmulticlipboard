/*
 * BubbleToolbar.java
 *
 * Created on 2011-09-05, 16:05:05
 */
package jmulticlip.gui.graph.tools.toolbar;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ComboBoxModel;
import javax.swing.Icon;
import javax.swing.JToggleButton;
import javax.swing.SwingUtilities;
import jmulticlip.gui.graph.CommonProperties;
import jmulticlip.gui.graph.CommonProperties.Type;
import jmulticlip.gui.graph.CommonPropertyChangeListener;
import jmulticlip.gui.graph.components.ColorPopupMenu;
import jmulticlip.gui.graph.components.ImageComboBox;
import jmulticlip.gui.graph.components.StrokeComboBox;
import jmulticlip.util.GUIUtils;

/**
 *
 * @author Revers
 */
public class BubbleToolbar extends javax.swing.JPanel implements CommonPropertyChangeListener {

    private static final int TYPE_SPEECH_INDEX = 0;
    private static final int TYPE_THOUGHT_INDEX = 1;
    private static final int TYPE_SHOUT_INDEX = 2;
    
    private static final int DIRECTION_DOWN_INDEX = 0;
    private static final int DIRECTION_UP_INDEX = 1;
    private static final int DIRECTION_LEFT_INDEX = 2;
    private static final int DIRECTION_RIGHT_INDEX = 3;
    private ColorPopupMenu colorPopup;
    private Icon[] typeIcons;
    private Icon[] directionIcons;

    /** Creates new form BubbleToolbar */
    public BubbleToolbar() {

        typeIcons = new Icon[3];
        typeIcons[TYPE_SPEECH_INDEX] = GUIUtils.getIcon("src/img/bubble_speech_16-2.png");
        typeIcons[TYPE_THOUGHT_INDEX] = GUIUtils.getIcon("src/img/bubble_thought_16-2.png");
        typeIcons[TYPE_SHOUT_INDEX] = GUIUtils.getIcon("src/img/bubble_shout_16-8.png");

        directionIcons = new Icon[4];
        directionIcons[DIRECTION_DOWN_INDEX] = GUIUtils.getIcon("src/img/bubble_down.png");
        directionIcons[DIRECTION_UP_INDEX] = GUIUtils.getIcon("src/img/bubble_up.png");
        directionIcons[DIRECTION_LEFT_INDEX] = GUIUtils.getIcon("src/img/bubble_left.png");
        directionIcons[DIRECTION_RIGHT_INDEX] = GUIUtils.getIcon("src/img/bubble_right.png");

        initComponents();

        colorPopup = new ColorPopupMenu(fillColorJB, Type.FILL_COLOR);

        Color fillColor = CommonProperties.getInstance().getFillColor();
        fillColorJB.setIcon(GUIUtils.getColorIcon(fillColor, ColorPopupMenu.COLOR_BUTTON_ICON_SIZE.width,
                ColorPopupMenu.COLOR_BUTTON_ICON_SIZE.height, true));

        GUIUtils.makeButtonFlat(fillColorJB);

        GUIUtils.makeButtonFlat(filledTB);
        GUIUtils.makeButtonFlat(unfilledTB);

//        GUIUtils.makeButtonFlat(bubbleDownTB);
//        GUIUtils.makeButtonFlat(bubbleUpTB);
//        GUIUtils.makeButtonFlat(bubbleLeftTB);
//        GUIUtils.makeButtonFlat(bubbleRightTB);

//        GUIUtils.makeButtonFlat(bubbleSpeechTB);
//        GUIUtils.makeButtonFlat(bubbleShoutTB);
//        GUIUtils.makeButtonFlat(bubbleThoughtTB);

        filledTB.addActionListener(fillActionListener);
        unfilledTB.addActionListener(fillActionListener);

        ticknessCB.addActionListener(lineWidthActionListener);

        CommonProperties.getInstance().addPropertyChangeListener(
                this,
                Type.FILL,
                Type.FILL_COLOR,
                Type.LINE_WIDTH);

        typeCB.setIcons(typeIcons);

        directionCB.setIcons(directionIcons);
    }

    @Override
    public void propertyChanged(Object property, Type type) {

        if (type == Type.FILL) {
            boolean filled = (Boolean) property;
            if (unfilledTB.isSelected()) {
                if (filled) {
                    filledTB.setSelected(true);
                    setFillColorEnabled(true);
                }
            } else if (filled == false) {
                unfilledTB.setSelected(true);
                setFillColorEnabled(false);
            }

        } else if (type == Type.LINE_WIDTH) {
            float lineWidth = getLineWidth();
            float commLineWidth = CommonProperties.getInstance().getLineWidth();
            if (commLineWidth != lineWidth) {
                setLineWidth(commLineWidth);
            }
        } else if (type == Type.FILL_COLOR) {
            Color fillColor = CommonProperties.getInstance().getFillColor();
            fillColorJB.setIcon(GUIUtils.getColorIcon(fillColor, ColorPopupMenu.COLOR_BUTTON_ICON_SIZE.width,
                    ColorPopupMenu.COLOR_BUTTON_ICON_SIZE.height, true));
        }
    }

    public float getLineWidth() {
        return ((BasicStroke) ticknessCB.getSelectedItem()).getLineWidth();
    }

    public void setLineWidth(float lineWidth) {
        ComboBoxModel model = ticknessCB.getModel();
        for (int i = 0; i < model.getSize(); i++) {
            if (((BasicStroke) model.getElementAt(i)).getLineWidth() == lineWidth) {
                ticknessCB.setSelectedIndex(i);
                break;
            }
        }
    }

    private void setFillColorEnabled(boolean enabled) {
        fillColorJB.setEnabled(enabled);
        fillColorJL.setEnabled(enabled);
    }

    public boolean isFill() {
        return filledTB.isSelected();
    }

    public void setFill(boolean fill) {
        if (fill) {
            filledTB.setSelected(true);
            setFillColorEnabled(true);
        } else {
            unfilledTB.setSelected(true);
            setFillColorEnabled(false);
        }
    }

    public StrokeComboBox getTicknessComboBox() {
        return ticknessCB;
    }

    public JToggleButton getFilledToggleButton() {
        return filledTB;
    }

    public JToggleButton getUnfilledToggleButton() {
        return unfilledTB;
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        bubbleGroup = new javax.swing.ButtonGroup();
        directionGroup = new javax.swing.ButtonGroup();
        fillGroup = new javax.swing.ButtonGroup();
        ticknessJL = new javax.swing.JLabel();
        ticknessCB = new jmulticlip.gui.graph.components.StrokeComboBox();
        separator1 = new javax.swing.JSeparator();
        toolNameJL = new javax.swing.JLabel();
        separator2 = new javax.swing.JSeparator();
        separator3 = new javax.swing.JSeparator();
        typeTxtJL = new javax.swing.JLabel();
        separator4 = new javax.swing.JSeparator();
        jSeparator1 = new javax.swing.JSeparator();
        unfilledTB = new javax.swing.JToggleButton();
        filledTB = new javax.swing.JToggleButton();
        filledTxtJL = new javax.swing.JLabel();
        separator5 = new javax.swing.JSeparator();
        fillColorJL = new javax.swing.JLabel();
        fillColorJB = new javax.swing.JButton();
        separator6 = new javax.swing.JSeparator();
        separator7 = new javax.swing.JSeparator();
        jSeparator2 = new javax.swing.JSeparator();
        typeJL = new javax.swing.JLabel();
        typeCB = new jmulticlip.gui.graph.components.ImageComboBox();
        directionCB = new jmulticlip.gui.graph.components.ImageComboBox();

        setLayout(new java.awt.GridBagLayout());

        ticknessJL.setText("Grubość:"); // NOI18N
        ticknessJL.setFocusable(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 5);
        add(ticknessJL, gridBagConstraints);

        ticknessCB.setSelectedIndex(0);
        ticknessCB.setFocusable(false);
        ticknessCB.setPreferredSize(new java.awt.Dimension(60, 19));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 10);
        add(ticknessCB, gridBagConstraints);

        separator1.setOrientation(javax.swing.SwingConstants.VERTICAL);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        add(separator1, gridBagConstraints);

        toolNameJL.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        toolNameJL.setText("Chmurka");
        toolNameJL.setFocusable(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 5);
        add(toolNameJL, gridBagConstraints);

        separator2.setOrientation(javax.swing.SwingConstants.VERTICAL);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 10);
        add(separator2, gridBagConstraints);

        separator3.setOrientation(javax.swing.SwingConstants.VERTICAL);
        separator3.setMinimumSize(new java.awt.Dimension(2, 0));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 5);
        add(separator3, gridBagConstraints);

        typeTxtJL.setText("Kierunek:");
        typeTxtJL.setFocusable(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 9;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 5);
        add(typeTxtJL, gridBagConstraints);

        separator4.setOrientation(javax.swing.SwingConstants.VERTICAL);
        separator4.setMinimumSize(new java.awt.Dimension(2, 0));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 15;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 5);
        add(separator4, gridBagConstraints);

        jSeparator1.setOrientation(javax.swing.SwingConstants.VERTICAL);
        jSeparator1.setMinimumSize(new java.awt.Dimension(2, 0));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 19;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.weightx = 1.0;
        add(jSeparator1, gridBagConstraints);

        fillGroup.add(unfilledTB);
        unfilledTB.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/elipse_unfilled.png"))); // NOI18N
        unfilledTB.setSelected(true);
        unfilledTB.setFocusable(false);
        unfilledTB.setMinimumSize(new java.awt.Dimension(26, 22));
        unfilledTB.setPreferredSize(new java.awt.Dimension(26, 22));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 13;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 5);
        add(unfilledTB, gridBagConstraints);

        fillGroup.add(filledTB);
        filledTB.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/elipse_filled.png"))); // NOI18N
        filledTB.setFocusable(false);
        filledTB.setMinimumSize(new java.awt.Dimension(26, 22));
        filledTB.setPreferredSize(new java.awt.Dimension(26, 22));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 14;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 5);
        add(filledTB, gridBagConstraints);

        filledTxtJL.setText("Wypełnienie:");
        filledTxtJL.setFocusable(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 5);
        add(filledTxtJL, gridBagConstraints);

        separator5.setOrientation(javax.swing.SwingConstants.VERTICAL);
        separator5.setMinimumSize(new java.awt.Dimension(2, 0));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 13;
        gridBagConstraints.gridy = 0;
        add(separator5, gridBagConstraints);

        fillColorJL.setText("Kolor wypełnienia:");
        fillColorJL.setEnabled(false);
        fillColorJL.setFocusable(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 16;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 5);
        add(fillColorJL, gridBagConstraints);

        fillColorJB.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/blackSquare16.png"))); // NOI18N
        fillColorJB.setEnabled(false);
        fillColorJB.setFocusable(false);
        fillColorJB.setMinimumSize(new java.awt.Dimension(26, 22));
        fillColorJB.setPreferredSize(new java.awt.Dimension(26, 22));
        fillColorJB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fillColorJBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 17;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 5);
        add(fillColorJB, gridBagConstraints);

        separator6.setOrientation(javax.swing.SwingConstants.VERTICAL);
        separator6.setMinimumSize(new java.awt.Dimension(2, 0));
        separator6.setRequestFocusEnabled(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 18;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        add(separator6, gridBagConstraints);

        separator7.setOrientation(javax.swing.SwingConstants.VERTICAL);
        separator7.setMinimumSize(new java.awt.Dimension(2, 0));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 11;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 5);
        add(separator7, gridBagConstraints);

        jSeparator2.setOrientation(javax.swing.SwingConstants.VERTICAL);
        jSeparator2.setMinimumSize(new java.awt.Dimension(2, 0));
        jSeparator2.setPreferredSize(new java.awt.Dimension(2, 0));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 5);
        add(jSeparator2, gridBagConstraints);

        typeJL.setText("Typ:");
        typeJL.setFocusable(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 5);
        add(typeJL, gridBagConstraints);

        typeCB.setFocusable(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 7;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 5);
        add(typeCB, gridBagConstraints);

        directionCB.setFocusable(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 5);
        add(directionCB, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

private void fillColorJBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fillColorJBActionPerformed
    SwingUtilities.updateComponentTreeUI(colorPopup);
    colorPopup.show(this, fillColorJB.getX(), fillColorJB.getY() + 26);
}//GEN-LAST:event_fillColorJBActionPerformed
    private ActionListener fillActionListener = new ActionListener() {

        @Override
        public void actionPerformed(ActionEvent e) {

            CommonProperties commonProperties = CommonProperties.getInstance();
            if (filledTB.isSelected()) {
                if (commonProperties.isFill() == false) {
                    commonProperties.setFill(true);
                    setFillColorEnabled(true);
                }
            } else if (commonProperties.isFill() == true) {
                commonProperties.setFill(false);
                setFillColorEnabled(false);
            }
        }
    };
    private ActionListener lineWidthActionListener = new ActionListener() {

        @Override
        public void actionPerformed(ActionEvent e) {

            CommonProperties commonProperties = CommonProperties.getInstance();
            float lineWidth = getLineWidth();
            if (commonProperties.getLineWidth() != lineWidth) {
                commonProperties.setLineWidth(lineWidth);
            }
        }
    };
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup bubbleGroup;
    private jmulticlip.gui.graph.components.ImageComboBox directionCB;
    private javax.swing.ButtonGroup directionGroup;
    private javax.swing.JButton fillColorJB;
    private javax.swing.JLabel fillColorJL;
    private javax.swing.ButtonGroup fillGroup;
    private javax.swing.JToggleButton filledTB;
    private javax.swing.JLabel filledTxtJL;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator separator1;
    private javax.swing.JSeparator separator2;
    private javax.swing.JSeparator separator3;
    private javax.swing.JSeparator separator4;
    private javax.swing.JSeparator separator5;
    private javax.swing.JSeparator separator6;
    private javax.swing.JSeparator separator7;
    private jmulticlip.gui.graph.components.StrokeComboBox ticknessCB;
    private javax.swing.JLabel ticknessJL;
    private javax.swing.JLabel toolNameJL;
    private jmulticlip.gui.graph.components.ImageComboBox typeCB;
    private javax.swing.JLabel typeJL;
    private javax.swing.JLabel typeTxtJL;
    private javax.swing.JToggleButton unfilledTB;
    // End of variables declaration//GEN-END:variables
}
