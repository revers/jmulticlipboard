/*
 * LineToolBar.java
 *
 * Created on 2010-09-09, 13:46:31
 */
package jmulticlip.gui.graph.tools.toolbar;

import java.awt.BasicStroke;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import javax.swing.ComboBoxModel;
import javax.swing.JToggleButton;
import jmulticlip.gui.graph.CommonProperties;
import jmulticlip.gui.graph.CommonProperties.Type;
import jmulticlip.gui.graph.CommonPropertyChangeListener;
import jmulticlip.gui.graph.components.ArrowComboBox;
import jmulticlip.gui.graph.components.StrokeComboBox;
import jmulticlip.gui.graph.components.StrokeComboBoxModel;
import jmulticlip.gui.graph.tools.ArrowedLine;
import jmulticlip.util.GUIUtils;

/**
 *
 * @author Revers
 */
public class LineToolbar extends javax.swing.JPanel implements CommonPropertyChangeListener {

    private StrokeComboBoxModel dashModel = new StrokeComboBoxModel(StrokeComboBox.DASH_STROKES);

    /** Creates new form LineToolBar */
    public LineToolbar() {
        initComponents();

        ticknessCB.addActionListener(lineWidthActionListener);

        dashCB.addActionListener(dashStrokeActionListener);

        CommonProperties.getInstance().addPropertyChangeListener(
                this,
                Type.FILL,
                Type.LINE_WIDTH,
                Type.DASH_STROKE);
    }

    @Override
    public void propertyChanged(Object property, Type type) {

        if (type == Type.LINE_WIDTH) {
            float lineWidth = getLineWidth();
            float commLineWidth = CommonProperties.getInstance().getLineWidth();
            if (commLineWidth != lineWidth) {
                setLineWidth(commLineWidth);
            }
        } else if (type == Type.DASH_STROKE) {
            BasicStroke actualDashStroke = getDashStroke();
            BasicStroke commDashStroke = CommonProperties.getInstance().getDashStroke();
            if (isSameDashStroke(actualDashStroke, commDashStroke) == false) {
                setDashStroke(commDashStroke);
            }
        }
    }

    public ArrowComboBox getArrowComboBox() {
        return arrowCB;
    }

    public ArrowedLine getArrowedLine() {
        return arrowCB.getSelectedArrowedLine();
    }

    public void setArrowedLine(ArrowedLine arrowedLine) {
        arrowCB.setSelectedItem(arrowedLine);
    }

    public float getLineWidth() {
        return ((BasicStroke) ticknessCB.getSelectedItem()).getLineWidth();
    }

    public void setLineWidth(float lineWidth) {
        ComboBoxModel model = ticknessCB.getModel();
        for (int i = 0; i < model.getSize(); i++) {
            if (((BasicStroke) model.getElementAt(i)).getLineWidth() == lineWidth) {
                ticknessCB.setSelectedIndex(i);
                break;
            }
        }
    }

    public BasicStroke getDashStroke() {
        return dashCB.getSelectedStroke();
    }

    public void setDashStroke(BasicStroke painterStroke) {
        ComboBoxModel model = dashCB.getModel();

        for (int i = 0; i < model.getSize(); i++) {
            BasicStroke modelStroke = ((BasicStroke) model.getElementAt(i));

            if (isSameDashStroke(modelStroke, painterStroke)) {
                dashCB.setSelectedIndex(i);
                break;
            }
        }
    }

    private boolean isSameDashStroke(BasicStroke dashStrokeA, BasicStroke dashStrokeB) {
        if (dashStrokeA.getEndCap() == dashStrokeB.getEndCap()
                && dashStrokeA.getLineJoin() == dashStrokeB.getLineJoin()
                && dashStrokeA.getMiterLimit() == dashStrokeB.getMiterLimit()
                && dashStrokeA.getDashPhase() == dashStrokeB.getDashPhase()
                && Arrays.equals(dashStrokeA.getDashArray(), dashStrokeB.getDashArray())) {
            return true;
        }
        return false;
    }

    public StrokeComboBox getTicknessComboBox() {
        return ticknessCB;
    }

    public StrokeComboBox getDashComboBox() {
        return dashCB;
    }
    private ActionListener lineWidthActionListener = new ActionListener() {

        @Override
        public void actionPerformed(ActionEvent e) {

            CommonProperties commonProperties = CommonProperties.getInstance();
            float lineWidth = getLineWidth();
            if (commonProperties.getLineWidth() != lineWidth) {
                commonProperties.setLineWidth(lineWidth);
            }
        }
    };
    private ActionListener dashStrokeActionListener = new ActionListener() {

        @Override
        public void actionPerformed(ActionEvent e) {

            CommonProperties commonProperties = CommonProperties.getInstance();

            BasicStroke actualDashStroke = getDashStroke();
            BasicStroke commDashStroke = commonProperties.getDashStroke();
            if (isSameDashStroke(actualDashStroke, commDashStroke) == false) {
                commonProperties.setDashStroke(actualDashStroke);
            }
        }
    };

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        jSeparator2 = new javax.swing.JSeparator();
        ticknessCB = new jmulticlip.gui.graph.components.StrokeComboBox();
        arrowCB = new jmulticlip.gui.graph.components.ArrowComboBox();
        ticknessJL = new javax.swing.JLabel();
        arrowJL = new javax.swing.JLabel();
        dashJL = new javax.swing.JLabel();
        dashCB = new jmulticlip.gui.graph.components.StrokeComboBox();
        jSeparator1 = new javax.swing.JSeparator();
        toolLabel = new javax.swing.JLabel();
        jSeparator3 = new javax.swing.JSeparator();
        jSeparator4 = new javax.swing.JSeparator();
        jSeparator5 = new javax.swing.JSeparator();
        jSeparator6 = new javax.swing.JSeparator();

        setLayout(new java.awt.GridBagLayout());

        ticknessCB.setSelectedIndex(0);
        ticknessCB.setFocusable(false);
        ticknessCB.setPreferredSize(new java.awt.Dimension(60, 19));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 10);
        add(ticknessCB, gridBagConstraints);

        arrowCB.setSelectedIndex(0);
        arrowCB.setFocusable(false);
        arrowCB.setPreferredSize(new java.awt.Dimension(60, 19));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 9;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 10);
        add(arrowCB, gridBagConstraints);

        ticknessJL.setText("Grubość:");
        ticknessJL.setFocusable(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 5);
        add(ticknessJL, gridBagConstraints);

        arrowJL.setText("Strzałki:");
        arrowJL.setFocusable(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 5);
        add(arrowJL, gridBagConstraints);

        dashJL.setText("Przerywanie:");
        dashJL.setFocusable(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 5);
        add(dashJL, gridBagConstraints);

        dashCB.setModel(dashModel);
        dashCB.setSelectedIndex(0);
        dashCB.setFocusable(false);
        dashCB.setPreferredSize(new java.awt.Dimension(60, 19));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 10);
        add(dashCB, gridBagConstraints);

        jSeparator1.setOrientation(javax.swing.SwingConstants.VERTICAL);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.weighty = 1.0;
        add(jSeparator1, gridBagConstraints);

        toolLabel.setFont(new java.awt.Font("Tahoma", 1, 11));
        toolLabel.setText("Linia");
        toolLabel.setFocusable(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 5);
        add(toolLabel, gridBagConstraints);

        jSeparator3.setOrientation(javax.swing.SwingConstants.VERTICAL);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 10);
        add(jSeparator3, gridBagConstraints);

        jSeparator4.setOrientation(javax.swing.SwingConstants.VERTICAL);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 7;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 10);
        add(jSeparator4, gridBagConstraints);

        jSeparator5.setOrientation(javax.swing.SwingConstants.VERTICAL);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        add(jSeparator5, gridBagConstraints);

        jSeparator6.setOrientation(javax.swing.SwingConstants.VERTICAL);
        jSeparator6.setMinimumSize(new java.awt.Dimension(2, 0));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 15;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        add(jSeparator6, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private jmulticlip.gui.graph.components.ArrowComboBox arrowCB;
    private javax.swing.JLabel arrowJL;
    private jmulticlip.gui.graph.components.StrokeComboBox dashCB;
    private javax.swing.JLabel dashJL;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JSeparator jSeparator6;
    private jmulticlip.gui.graph.components.StrokeComboBox ticknessCB;
    private javax.swing.JLabel ticknessJL;
    private javax.swing.JLabel toolLabel;
    // End of variables declaration//GEN-END:variables
}
