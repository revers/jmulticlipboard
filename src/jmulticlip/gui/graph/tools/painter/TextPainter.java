package jmulticlip.gui.graph.tools.painter;

import java.awt.*;
import java.awt.datatransfer.DataFlavor;
import java.awt.font.*;
import java.text.*;
import java.util.*;
import javax.swing.ImageIcon;

/**
 *
 * @author Revers
 */
public class TextPainter extends AbstractPainter {

    // poniewaz zawsze na poczatku jest \r
    public static final int FIRST_VALID_POSITION = 1;

    public enum Alignment {

        LEFT, CENTER, RIGHT;
    }

    private class TextMetrics {

        public float ascent;
        public float descent;
        public float leading;
        public float lineHeight;
    }
    private static final FontRenderContext DEFAULT_FRC = new FontRenderContext(null, true, false);
    private Hashtable<TextAttribute, Object> attributeMap = new Hashtable<TextAttribute, Object>();
    //private final Image moveImage = new ImageIcon(getClass().getResource("/img/move.png")).getImage();
    // private Font font = new Font(attributeMap);
    // private AttributedString text = new AttributedString("", attributeMap);
    private StringBuilder textBuilder;
    private String[] lines;
    private String fontFamily;
    private int fontSize;
    private Alignment alignment = Alignment.LEFT;
    private Color color;
    private int x;
    private int y;
    private int caretPosition = FIRST_VALID_POSITION;
    private boolean caretVisible;
    private boolean italic;
    private boolean bold;
    private boolean strikethrough;
    private boolean underlining;
    private int width;
    private int height;
    //  private boolean drawMoveIcon;
    private int selectionBegin;
    private int selectionEnd;
    private TextMetrics textMetrics = new TextMetrics();
    //  private boolean moveable;

    public TextPainter() {
        attributeMap.put(TextAttribute.FAMILY, "Times New Roman");
        attributeMap.put(TextAttribute.SIZE, new Float(12.0));
        attributeMap.put(TextAttribute.KERNING, TextAttribute.KERNING_ON);

        marginSize = 5;
        moveByInterior = false;
        setText("\r");
    }

//    private TextPainter(StringBuilder textBuilder, String[] lines, String fontFamily,
//            int fontSize, Color color, int x, int y, boolean caretVisible,
//            boolean italic, boolean bold, boolean strikethrough, boolean underlining,
//            int width, int height, int selectionBegin, int selectionEnd) {
//        this.textBuilder = textBuilder;
//        this.lines = lines;
//        this.fontFamily = fontFamily;
//        this.fontSize = fontSize;
//        this.color = color;
//        this.x = x;
//        this.y = y;
//        this.caretVisible = caretVisible;
//        this.italic = italic;
//        this.bold = bold;
//        this.strikethrough = strikethrough;
//        this.underlining = underlining;
//        this.width = width;
//        this.height = height;
//        this.selectionBegin = selectionBegin;
//        this.selectionEnd = selectionEnd;
//    }
    @Override
    public void move(int xShift, int yShift) {
        x += xShift;
        y += yShift;
        calcBounds();
    }

    @Override
    public int getBoundsMargin() {
        return 5;
    }

    @Override
    public boolean isMoveable() {
        return true;
    }

    @Override
    public boolean isResizable() {
        return false;
    }

    @Override
    protected void calcBounds() {
        bounds.x = x;
        bounds.y = y;
        bounds.height = height;
        bounds.width = width;
        addShift(bounds);

        firePainterChanged(this);

    }

    public int getSelectionBegin() {
        return selectionBegin;
    }

    public void setSelectionBegin(int selectionBegin) {
        this.selectionBegin = selectionBegin;
    }

    public int getSelectionEnd() {
        return selectionEnd;
    }

    public void setSelectionEnd(int selectionEnd) {
        this.selectionEnd = selectionEnd;
    }

    public String getFontFamily() {
        return fontFamily;
    }

    public void setFontFamily(String fontFamily) {
        this.fontFamily = fontFamily;
        attributeMap.put(TextAttribute.FAMILY, fontFamily);
    }

    public int getFontSize() {
        return fontSize;
    }

    public void setFontSize(int fontSize) {
        this.fontSize = fontSize;

        attributeMap.put(TextAttribute.SIZE, new Float((float) fontSize));

    }

    public boolean isBold() {
        return bold;
    }

    public void setBold(boolean bold) {
        this.bold = bold;
        if (bold) {
            attributeMap.put(TextAttribute.WEIGHT, TextAttribute.WEIGHT_BOLD);
        } else {
            attributeMap.remove(TextAttribute.WEIGHT);
        }
        reinit();
    }

    public boolean isItalic() {
        return italic;
    }

    public void setItalic(boolean italic) {
        this.italic = italic;
        if (italic) {
            attributeMap.put(TextAttribute.POSTURE, TextAttribute.POSTURE_OBLIQUE);
        } else {
            attributeMap.remove(TextAttribute.POSTURE);
        }
        reinit();
    }

    public boolean isStrikethrough() {
        return strikethrough;
    }

    public void setStrikethrough(boolean strikethrough) {
        this.strikethrough = strikethrough;
        if (strikethrough) {
            attributeMap.put(TextAttribute.STRIKETHROUGH, TextAttribute.STRIKETHROUGH_ON);
        } else {
            attributeMap.remove(TextAttribute.STRIKETHROUGH);
        }
        reinit();
    }

    public boolean isUnderlining() {
        return underlining;
    }

    public void setUnderlining(boolean underlining) {
        this.underlining = underlining;
        if (underlining) {
            attributeMap.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);
        } else {
            attributeMap.remove(TextAttribute.UNDERLINE);
        }
        reinit();
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }

    public Alignment getAlignment() {
        return alignment;
    }

    public void setAlignment(Alignment alignment) {
        this.alignment = alignment;
        reinit();
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

//    public Font getFont() {
//        return font;
//    }
//
//    public void setFont(Font font) {
//        this.font = font;
//    }
    public String getText() {
        String result = textBuilder.toString();
        if (result.equals("\r")) {
            return "";
        } else {
            return result;
        }
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
        shiftPoint.x = 0;
        calcBounds();
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
        shiftPoint.y = 0;
        calcBounds();
    }

    public int getTextLength() {
        return textBuilder.length();
    }

    public char charAt(int index) {
        return textBuilder.charAt(index);
    }

    public void insert(int offset, char c) {
        //  System.out.println("offset = " + offset + "; char = '" + c + "'");
        textBuilder.insert(offset, c);
        reinit();
        caretPosition = offset + 1;
    }

    public void insert(int offset, String s) {
        //      System.out.println("offset = " + offset + "; s = '" + s + "'");
        textBuilder.insert(offset, s);
        reinit();
        caretPosition = offset + s.length();
    }

    public void append(char c) {
        textBuilder.append(c);
        reinit();
        caretPosition = textBuilder.length();
    }

    public void append(String text) {
        textBuilder.append(text);
        reinit();
        caretPosition = textBuilder.length();
    }

    public void deleteCharAt(int index) {
        textBuilder.deleteCharAt(index);
        reinit();
        caretPosition = index;
    }

    public void delete(int start, int end) {
        textBuilder.delete(start, end);
        reinit();
        caretPosition = start;
    }

    public void reinit() {
        width = 0;
        height = 0;
        lines = textBuilder.toString().split("\n");

        for (int i = 0; i < lines.length; i++) {
            TextLayout textLayout = null;
            if (lines[i].equals("")) {
                textLayout = new TextLayout("\r", attributeMap, DEFAULT_FRC);
            } else {
                textLayout = new TextLayout(lines[i], attributeMap, DEFAULT_FRC);
            }

            if (i == 0) {
                textMetrics.ascent = textLayout.getAscent();
                textMetrics.descent = textLayout.getDescent();
                textMetrics.leading = textLayout.getLeading();
                textMetrics.lineHeight = textMetrics.ascent + textMetrics.descent
                        + textMetrics.leading;
            }

            int w = Math.round(textLayout.getAdvance());
            if (w > width) {
                width = w;
            }
            if (width == 0) {
                width = 1;
            }

            height += textMetrics.lineHeight;
        }

        calcBounds();
    }

    public void setText(String text) {
        if (text.equals("")) {
            text = "\r";
        } else if (text.startsWith("\r") == false) {
            this.textBuilder = new StringBuilder("\r" + text);
        } else {
            this.textBuilder = new StringBuilder(text);
        }
        reinit();

    }

    @Override
    protected void drawImpl(Graphics2D g2) {
        g2.setColor(color);

        float drawPosX = (float) x;
        float drawPosY = (float) y;

        //  g2.drawRect(x, y, width, height);
//        if (drawMoveIcon) {
//            g2.drawImage(moveImage, x + width, y + height, null);
//        }

        int pos = 0;
        boolean carretAppeared = false;

        for (String line : lines) {
            TextLayout layout = null;
            if (line.equals("")) {
                layout = new TextLayout("\r", attributeMap, DEFAULT_FRC);
            } else {
                layout = new TextLayout(line, attributeMap, DEFAULT_FRC);
            }

            drawPosY += layout.getAscent();
            switch (alignment) {
                case LEFT:
                    drawPosX = (float) x;
                    break;
                case CENTER:
                    drawPosX = (float) (width - layout.getAdvance()) / 2.0f + (float) x;
                    break;
                case RIGHT:
                    drawPosX = (float) width - layout.getAdvance() + (float) x;

            }

            if (selectionBegin != selectionEnd) {
                int localSelectionBegin = selectionBegin;
                int localSelectionEnd = selectionEnd;

                if (localSelectionBegin > localSelectionEnd) {
                    int temp = localSelectionBegin;
                    localSelectionBegin = localSelectionEnd;
                    localSelectionEnd = temp;
                }
                if (pos + line.length() >= localSelectionBegin
                        && pos <= localSelectionEnd) {
                    g2.setColor(Color.LIGHT_GRAY);
                    int selBegin = FIRST_VALID_POSITION;
                    int selEnd = FIRST_VALID_POSITION;
                    if (localSelectionBegin > pos) {
                        selBegin = localSelectionBegin - pos;
                    }
                    if (localSelectionEnd > pos + line.length()) {
                        selEnd = line.length();
                    } else {
                        selEnd = localSelectionEnd - pos;
                    }

                    Shape highlight = layout.getLogicalHighlightShape(selBegin,
                            selEnd);

                    g2.translate(drawPosX, drawPosY);
                    g2.fill(highlight);
                    g2.translate(-drawPosX, -drawPosY);
                    g2.setColor(color);
                }
            }

            layout.draw(g2, drawPosX, drawPosY);

            if (caretVisible && pos + line.length() >= caretPosition && carretAppeared == false) {
                // g2.setColor(Color.red);
                Shape[] carets = layout.getCaretShapes(caretPosition - pos);
                g2.translate(drawPosX, drawPosY);
                g2.draw(carets[0]);
                g2.translate(-drawPosX, -drawPosY);
                carretAppeared = true;

            }
            pos += line.length();
            if (pos < getTextLength()) {
                pos++;
            }
            drawPosY += layout.getDescent() + layout.getLeading();
        }
    }
//
//    public int getLineHeight() {
//        return textMetrics.lineHeight;
//    }

    public void moveCaretUp() {
        Point caretPoint = getCaretPoint();

        // caretPoint.x += 3;
        caretPoint.y -= textMetrics.lineHeight - 2;
        caretPosition = getCharIndexAt(caretPoint);
        //    System.out.println("caretPosition = " + caretPosition);
        //  System.out.printf("caretPoint.x = %d; caretPoint.y = %d%n", caretPoint.x, caretPoint.y);
//        if (caretPosition < textBuilder.length()) {
//            caretPosition++;
//        }

        if (caretPosition == 0) {
            caretPosition++;
        }
//        if (caretPosition < textBuilder.length()
//                && charAt(caretPosition) == '\r') {
//            caretPosition--;
//        }
        caretVisible = true;
    }

    public void moveCaretDown() {
        Point caretPoint = getCaretPoint();
        //   caretPoint.x += 3;
        caretPoint.y += textMetrics.lineHeight + 2;
        caretPosition = getCharIndexAt(caretPoint);
//        if (caretPosition < textBuilder.length()) {
//            caretPosition++;
//        }
        if (caretPosition == 0) {
            caretPosition++;
        }

//        if (caretPosition < textBuilder.length()
//                && charAt(caretPosition) == '\r') {
//            caretPosition--;
//        }
        caretVisible = true;
    }

    public void moveCaretToLineBegin() {
        int pos = 0;
        int line = 0;
        while (line < lines.length && pos < caretPosition) {
            pos += lines[line++].length() + 1;
        }
        if (line == 0) {
            caretPosition = 0;
            return;
        }
//        System.out.printf("line nr = %d; pos = %d; caretPos = %d;"
//                + " line length = %d%n",
//                (i - 1), pos, caretPosition, lines[i - 1].length());
        pos -= lines[line - 1].length();
        caretPosition = pos;
        caretVisible = true;
    }

    public void moveCaretToLineEnd() {
        int pos = 0;
        int line = 0;
        while (line < lines.length && pos < caretPosition) {
            pos += lines[line++].length() + 1;
        }
        if (line == 0) {
            caretPosition = 0;
            return;
        }

        caretPosition = pos - 1;
        caretVisible = true;
    }

    public boolean isSelectionVisible() {
        return selectionBegin != selectionEnd;
    }

    public boolean isCaretVisible() {
        return caretVisible;
    }

    public void setCaretVisible(boolean caretVisible) {
        this.caretVisible = caretVisible;
    }

    public int getCaretPosition() {
        return caretPosition;
    }

    public void setCaretPosition(int carentPosition) {
        this.caretPosition = carentPosition;
    }

    public Point getCaretPoint() {
        int position = 0;
        int lineNo = 0;
        while (lineNo < lines.length && position <= caretPosition) {
            position += lines[lineNo++].length();
            // if (lineNo < lines.length) {
            position++;
            //  }
        }
        if (lineNo > 0) {
            lineNo--;
        }

        // if (lineNo > 0) {
        position -= lines[lineNo].length() + 1;
        //  }

        Point caretPoint = new Point(0, 0);

        int length = caretPosition - position;// + 1;

//        System.out.printf("length = %d; lineNo = %d; position = %d; "
//                + "caretPosition = %d text.length = %d linePosY = %fline.length = %d%n",
//                length, lineNo, position, caretPosition, textBuilder.length(),
//                lineNo * textMetrics.lineHeight, lines[lineNo].length());

        String text = null;
        if (length == 0) {
            text = "\r";
        } else {

            text = lines[lineNo].substring(0, length);
        }

        //  System.out.println("text substring = " + text);

        //if (text.length() == 0) {
        //  System.out.println("Pusta linia: \"" + text + "\"");
//        for (int j = 0; j < lines.length; j++) {
//            System.out.printf("[%d] '%s'; contains r = %b%n", j, lines[j], lines[j].contains("\r"));
//        }
        // }

        TextLayout layout = new TextLayout(text, attributeMap, DEFAULT_FRC);

        caretPoint.y = (int) ((float) lineNo * textMetrics.lineHeight);

        if (length != 0) {
            caretPoint.x = Math.round(layout.getAdvance());
        }

        switch (alignment) {
            case LEFT:
                break;
            case CENTER:
                layout = new TextLayout(lines[lineNo], attributeMap, DEFAULT_FRC);
                caretPoint.x += (int) (((float) width - layout.getAdvance()) / 2.0f);
                break;
            case RIGHT:
                layout = new TextLayout(lines[lineNo], attributeMap, DEFAULT_FRC);
                caretPoint.x += (int) ((float) width - layout.getAdvance());
                break;
        }

        caretPoint.x += x;
        caretPoint.y += y;
        return caretPoint;
    }

    public int getCharIndexAt(Point p) {
        int result = 0;
        if (getText().equals("")) {
            return result;
        }
        float clickY = (float) (p.getY() - (float) y);
        if (clickY < 0.0f) {
            clickY = 0.0f;
            // return result;
        }


        //    TextLayout textLayout = new TextLayout("A", attributeMap, DEFAULT_FRC);
        float oneRowHeight = textMetrics.ascent + textMetrics.descent + textMetrics.leading;
        //textLayout.getAscent() + textLayout.getDescent() + textLayout.getLeading();
        float rowsHeight = oneRowHeight * lines.length;
        int row = 0;
        if (clickY > rowsHeight) {
            //clickY = rowsHeight;
            row = lines.length - 1;
            // return result;
        } else {
            row = (int) (clickY / oneRowHeight);
        }
        float drawPosX = x;

        TextLayout textLayout = null;
        if (lines[row].equals("")) {
            textLayout = new TextLayout("\r", attributeMap, DEFAULT_FRC);
        } else {
            textLayout = new TextLayout(lines[row], attributeMap, DEFAULT_FRC);
        }
        switch (alignment) {
            case LEFT:
                break;
            case CENTER:
                drawPosX = (float) (width - textLayout.getAdvance()) / 2.0f + (float) x;
                break;
            case RIGHT:
                drawPosX = (float) width - textLayout.getAdvance() + (float) x;
        }

        float clickX = (float) (p.getX() - drawPosX);
        //   System.out.printf("clickX = %f; clickY = %f;%n", clickX, clickY);

        TextHitInfo position = textLayout.hitTestChar(clickX, clickY);
        int pos = 0;
        for (int i = 0; i < row; i++) {
            pos += lines[i].length() + 1;
        }
        result = position.getInsertionIndex() + pos;

        return result;
    }
//    public boolean isOnMoveIcon(Point p) {
//        int x = this.x + width;
//        int y = this.y + height;
//        if (p.x >= x && p.x <= x + moveImage.getWidth(null) && p.y >= y
//                && p.y <= y + moveImage.getHeight(null)) {
//            return true;
//        }
//
//        return false;
//    }
//    public boolean isOnTextArea(Point p) {
//        if (p.x >= x && p.x <= x + width && p.y >= y
//                && p.y <= y + height) {
//            return true;
//        }
//
//        return false;
//    }

    public static DataFlavor getDataFlavor() {
        return DataFlavor.stringFlavor;
    }

    @Override
    public AbstractPainter clonePainter() {
        TextPainter textPainter = new TextPainter();

        textPainter.setText(getText());
        textPainter.setFontFamily(getFontFamily());
        textPainter.setFontSize(getFontSize());
        textPainter.setColor(getColor());
        textPainter.setAlignment(getAlignment());
        textPainter.setBold(isBold());
        textPainter.setItalic(isItalic());
        textPainter.setStrikethrough(isStrikethrough());
        textPainter.setUnderlining(isUnderlining());

        return textPainter;
    }
}
