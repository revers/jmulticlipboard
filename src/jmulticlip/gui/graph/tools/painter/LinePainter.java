package jmulticlip.gui.graph.tools.painter;

import java.awt.*;
import java.awt.datatransfer.DataFlavor;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import jmulticlip.core.ErrorManager;
import jmulticlip.gui.graph.tools.ArrowedLine;
import jmulticlip.gui.graph.tools.Resizable;

/**
 *
 * @author Revers
 */
public class LinePainter extends AbstractPainter implements Resizable {

    private ArrowedLine arrowedLine = new ArrowedLine(ArrowedLine.Type.NONE);
    private Color color;
    private int x1;
    private int y1;
    private int x2;
    private int y2;
    private BasicStroke stroke;
    private boolean risingLine = true;

    public LinePainter(Color color, int x1, int y1, int x2, int y2) {
        this.color = color;
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x2;
        this.y2 = y2;

        stroke = new BasicStroke(1.0f);
        bounds = new Rectangle();
        calcDirection();
        calcBounds();
    }

    public LinePainter() {
    }

    private void calcDirection() {
        double m = (double) (y2 - y1) / (double) (x2 - x1);
        double angle = Math.atan(m);
        if (angle >= 0) {
            risingLine = false;
        } else {
            risingLine = true;
        }
    }

    @Override
    protected void calcBounds() {
        bounds.x = Math.min(x1, x2);
        bounds.y = Math.min(y1, y2);
        bounds.width = Math.max(x1, x2) - bounds.x;
        bounds.height = Math.max(y1, y2) - bounds.y;

        addShift(bounds);

//        int margin = getMargin();
//
//        bounds.x -= margin;
//        bounds.y -= margin;
//        bounds.width += margin * 2;
//        bounds.height += margin * 2;

        firePainterChanged(this);
    }

    @Override
    public int getBoundsMargin() {
        //  int margin = 0;
        int margin = Math.round(stroke.getLineWidth());

        int arrowMargin = margin + 4;
        switch (arrowedLine.getType()) {
            case BOTH:
            case LEADING:
            case TRAILING:
                margin += arrowMargin;
                break;
        }
        return margin;
    }

    @Override
    protected void drawImpl(Graphics2D g2) {
        Color oldColor = g2.getColor();
        Stroke oldStroke = g2.getStroke();
        g2.setColor(color);
        g2.setStroke(stroke);

        arrowedLine.draw(g2, x1, y1, x2, y2);
        g2.setColor(oldColor);
        g2.setStroke(oldStroke);
    }

    public BasicStroke getStroke() {
        return stroke;
    }

    public void setStroke(BasicStroke stroke) {
        this.stroke = stroke;
        calcBounds();
    }

    public float getLineWidth() {
        return stroke.getLineWidth();
    }

    public void setLineWidth(float lineWidth) {
        stroke = new BasicStroke(lineWidth, stroke.getEndCap(), stroke.getLineJoin(),
                stroke.getMiterLimit(), stroke.getDashArray(), stroke.getDashPhase());
        calcBounds();
    }

    public void setDashStroke(BasicStroke dashStroke) {
        stroke = new BasicStroke(getLineWidth(), dashStroke.getEndCap(), dashStroke.getLineJoin(),
                dashStroke.getMiterLimit(), dashStroke.getDashArray(), dashStroke.getDashPhase());
        calcBounds();
    }

    public ArrowedLine getArrowedLine() {
        return arrowedLine;
    }

    public void setArrowedLine(ArrowedLine arrowedLine) {
        this.arrowedLine = arrowedLine;
        calcBounds();
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public int getX1() {
        return x1;
    }

    public void setX1(int x1) {
        this.x1 = x1;
        calcDirection();
        calcBounds();
    }

    public int getX2() {
        return x2;
    }

    public void setX2(int x2) {
        this.x2 = x2;
        calcDirection();
        calcBounds();
    }

    public int getY1() {
        return y1;
    }

    public void setY1(int y1) {
        this.y1 = y1;
        calcDirection();
        calcBounds();
    }

    public int getY2() {
        return y2;
    }

    public void setY2(int y2) {
        this.y2 = y2;
        calcDirection();
        calcBounds();
    }

    @Override
    public boolean isResizable() {
        return true;
    }

    @Override
    public boolean isMoveable() {
        return true;
    }

    @Override
    public void flipY() {
        int xMin = Math.min(x1, x2);
        int yMin = Math.min(y1, y2);
        int xMax = Math.max(x1, x2);
        int yMax = Math.max(y1, y2);

        risingLine = !risingLine;
        setCoords(xMin, yMin, xMax, yMax);
    }

    @Override
    public void flipX() {
        int xMin = Math.min(x1, x2);
        int yMin = Math.min(y1, y2);
        int xMax = Math.max(x1, x2);
        int yMax = Math.max(y1, y2);

        risingLine = !risingLine;

        setCoords(xMin, yMin, xMax, yMax);
    }

    @Override
    public void setFlip(Resizable.Flip flip) {
        switch (flip) {
            case FIRST:
            case THIRD:
                risingLine = true;
                break;
            case SECOND:
            case FOURTH:
                risingLine = false;
                break;
        }

        int xMin = Math.min(x1, x2);
        int yMin = Math.min(y1, y2);
        int xMax = Math.max(x1, x2);
        int yMax = Math.max(y1, y2);

        setCoords(xMin, yMin, xMax, yMax);
    }

    @Override
    public Resizable.Flip getFlip() {
        if (risingLine) {
            return Resizable.Flip.FIRST;
        } else {
            return Resizable.Flip.SECOND;
        }
    }

    private void setCoords(int xMin, int yMin, int xMax, int yMax) {
        if (risingLine) {
            x1 = xMin;
            y1 = yMax;
            x2 = xMax;
            y2 = yMin;
        } else {
            x1 = xMin;
            y1 = yMin;
            x2 = xMax;
            y2 = yMax;
        }
    }

    @Override
    public void resize(int newX, int newY, int newWidth, int newHeight) {
        //int margin = getMargin();

        int xMin = newX;// + margin;
        int yMin = newY;// + margin;
        int xMax = xMin + newWidth;// - 2 * margin;
        int yMax = yMin + newHeight;// - 2 * margin;

        setCoords(xMin, yMin, xMax, yMax);

        shiftPoint.x = 0;
        shiftPoint.y = 0;
        calcBounds();
    }
//    String colorType = DataFlavor.javaJVMLocalObjectMimeType +
//                   ";class=java.awt.Color";
    private static DataFlavor dataFlavor;

    static {
        try {
            dataFlavor = new DataFlavor(DataFlavor.javaJVMLocalObjectMimeType + ";class="
                    + LinePainter.class.getName());
        } catch (ClassNotFoundException ex) {
            ErrorManager.errorMinor(LinePainter.class, ex);
        }
    }

    public static DataFlavor getDataFlavor() {
        return dataFlavor;
    }

    @Override
    public AbstractPainter clonePainter() {
        LinePainter painterCopy = new LinePainter(getColor(), getX1(),
                getY1(), getX2(), getY2());
        painterCopy.setArrowedLine(getArrowedLine());
        painterCopy.setStroke(getStroke());

        painterCopy.moveToOrgin();
        return painterCopy;
    }
}
