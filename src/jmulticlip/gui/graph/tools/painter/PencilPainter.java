package jmulticlip.gui.graph.tools.painter;

import java.awt.Graphics2D;
import java.awt.*;
import java.awt.datatransfer.DataFlavor;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import jmulticlip.core.ErrorManager;

/**
 *
 * @author Revers
 */
public class PencilPainter extends AbstractPainter {

    private ArrayList<Point> pointList;
    private Color color;
    private BasicStroke stroke;

    public PencilPainter(Color color) {
        this.pointList = new ArrayList<Point>();
        this.color = color;
        stroke = new BasicStroke();
    }

    public PencilPainter(ArrayList<Point> pointList, Color color) {
        this.pointList = pointList;
        this.color = color;
        stroke = new BasicStroke();
        calcBounds();
    }

    public void addPoint(Point p) {
        pointList.add(p);
        calcBounds();
    }

    @Override
    protected void calcBounds() {
        if (pointList.isEmpty()) {
            return;
        }
        int minX = pointList.get(0).x;
        int maxX = minX;
        int minY = pointList.get(0).y;
        int maxY = minY;

        for (int i = 1; i < pointList.size(); i++) {
            Point p = pointList.get(i);
            if (p.x < minX) {
                minX = p.x;
            } else if (p.x > maxX) {
                maxX = p.x;
            }

            if (p.y < minY) {
                minY = p.y;
            } else if (p.y > maxY) {
                maxY = p.y;
            }
        }

        int width = maxX - minX;
        int height = maxY - minY;
        if (width != 0 && height == 0) {
            height = 1;
        } else if (width == 0 && height != 0) {
            width = 1;
        } else if (width == 0 && height == 0) {
            width = 1;
            height = 1;
        }
        bounds.x = minX;
        bounds.y = minY;
        bounds.width = width;
        bounds.height = height;

//        int lineWidth = Math.round(stroke.getLineWidth());
//        bounds.x -= lineWidth + 1;
//        bounds.y -= lineWidth + 1;
//        bounds.width += lineWidth * 2 + 2;
//        bounds.height += lineWidth * 2 + 2;

        addShift(bounds);
        firePainterChanged(this);
    }

    @Override
    public int getBoundsMargin() {
        return Math.round(stroke.getLineWidth()) + 1;
    }

    public BasicStroke getStroke() {
        return stroke;
    }

    public void setStroke(BasicStroke stroke) {
        this.stroke = stroke;
        calcBounds();
    }

    public float getLineWidth() {
        return stroke.getLineWidth();
    }

    public void setLineWidth(float lineWidth) {
        stroke = new BasicStroke(lineWidth, stroke.getEndCap(), stroke.getLineJoin(),
                stroke.getMiterLimit(), stroke.getDashArray(), stroke.getDashPhase());
        calcBounds();
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public ArrayList<Point> getPointList() {
        return pointList;
    }

    public void setPointList(ArrayList<Point> pointList) {
        this.pointList = pointList;
        calcBounds();
    }

    @Override
    protected void drawImpl(Graphics2D g2) {
        if (pointList.isEmpty()) {
            return;
        }
        Color oldColor = g2.getColor();
        Stroke oldStroke = g2.getStroke();
        g2.setColor(color);
        g2.setStroke(stroke);

        Point firstPoint = pointList.get(0);
        g2.drawLine(firstPoint.x, firstPoint.y, firstPoint.x, firstPoint.y);
        for (int i = 1; i < pointList.size(); i++) {
            Point secondPoint = pointList.get(i);
            g2.drawLine(firstPoint.x, firstPoint.y, secondPoint.x, secondPoint.y);
            firstPoint = secondPoint;
        }

        g2.setColor(oldColor);
        g2.setStroke(oldStroke);
    }

    @Override
    public boolean isResizable() {
        return false;
    }

    @Override
    public boolean isMoveable() {
        return true;
    }

    @Override
    public AbstractPainter clonePainter() {
        ArrayList<Point> pointsCopy = new ArrayList<Point>();
        for (Point p : getPointList()) {
            pointsCopy.add(new Point(p.x, p.y));
        }
        PencilPainter painter = new PencilPainter(pointsCopy, getColor());
        painter.setStroke(getStroke());

        painter.moveToOrgin();
        return painter;
    }
    private static DataFlavor dataFlavor;

    static {
        try {
            dataFlavor = new DataFlavor(DataFlavor.javaJVMLocalObjectMimeType + ";class="
                    + PencilPainter.class.getName());
        } catch (ClassNotFoundException ex) {
            ErrorManager.errorMinor(PencilPainter.class, ex);
        }
    }

    public static DataFlavor getDataFlavor() {
        return dataFlavor;
    }
}
