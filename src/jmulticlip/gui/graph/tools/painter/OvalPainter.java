package jmulticlip.gui.graph.tools.painter;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.*;
import java.awt.datatransfer.DataFlavor;
import jmulticlip.core.ErrorManager;
import jmulticlip.gui.graph.tools.Resizable;

/**
 *
 * @author Revers
 */
public class OvalPainter extends AbstractPainter implements Resizable {

    private Color color;
    private Color fillColor;
    private BasicStroke stroke = new BasicStroke();
    private int x;
    private int y;
    private int width;
    private int height;
    private boolean fill;

    public OvalPainter(Color color, Color fillColor, int x, int y, int width, int height) {
        this.color = color;
        this.fillColor = fillColor;
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        bounds = new Rectangle(x, y, width, height);
    }

    public boolean isFill() {
        return fill;
    }

    public void setFill(boolean fill) {
        this.fill = fill;
    }

    public BasicStroke getStroke() {
        return stroke;
    }

    public void setStroke(BasicStroke stroke) {
        this.stroke = stroke;
        calcBounds();
    }

    public float getLineWidth() {
        return stroke.getLineWidth();
    }

    public void setLineWidth(float lineWidth) {
        stroke = new BasicStroke(lineWidth, stroke.getEndCap(), stroke.getLineJoin(),
                stroke.getMiterLimit(), stroke.getDashArray(), stroke.getDashPhase());
        calcBounds();
    }

    public void setDashStroke(BasicStroke dashStroke) {
        stroke = new BasicStroke(getLineWidth(), dashStroke.getEndCap(), dashStroke.getLineJoin(),
                dashStroke.getMiterLimit(), dashStroke.getDashArray(), dashStroke.getDashPhase());
        calcBounds();
    }

    @Override
    protected void calcBounds() {
        int local_x = x;
        int local_y = y;
        int local_height = height;
        int local_width = width;

        if (local_width < 0) {
            local_x += local_width;
            local_width = -local_width;
        }

        if (local_height < 0) {
            local_y += local_height;
            local_height = -local_height;
        }
        bounds.x = local_x + shiftPoint.x;
        bounds.y = local_y + shiftPoint.y;
        bounds.width = local_width;
        bounds.height = local_height;

        firePainterChanged(this);
    }

    @Override
    public int getBoundsMargin() {
        return Math.round(stroke.getLineWidth()) + 1;
    }

    @Override
    protected void drawImpl(Graphics2D g2) {
        Color oldColor = g2.getColor();
        Stroke oldStroke = g2.getStroke();

        g2.setStroke(stroke);
        int local_x = x;
        int local_y = y;
        int local_height = height;
        int local_width = width;

        if (local_width < 0) {
            local_x += local_width;
            local_width = -local_width;
        }

        if (local_height < 0) {
            local_y += local_height;
            local_height = -local_height;
        }

        if (fill) {
            g2.setColor(fillColor);
            g2.fillOval(local_x, local_y, local_width, local_height);
        }
        g2.setColor(color);
        g2.drawOval(local_x, local_y, local_width, local_height);

        g2.setColor(oldColor);
        g2.setStroke(oldStroke);
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public Color getFillColor() {
        return fillColor;
    }

    public void setFillColor(Color fillColor) {
        this.fillColor = fillColor;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
        calcBounds();
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
        calcBounds();
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
        shiftPoint.x = 0;
        calcBounds();
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
        shiftPoint.y = 0;
        calcBounds();
    }

    @Override
    public boolean isResizable() {
        return true;
    }

    @Override
    public boolean isMoveable() {
        return true;
    }

    @Override
    public void resize(int newX, int newY, int newWidth, int newHeight) {
        //  int margin = (int) (stroke.getLineWidth());

        x = newX;// + margin;
        y = newY;// + margin;
        width = newWidth;// - 2 * margin;
        height = newHeight;// - 2 * margin;
        shiftPoint.x = 0;
        shiftPoint.y = 0;
        calcBounds();
    }

    @Override
    public void flipX() {
    }

    @Override
    public void flipY() {
    }

    @Override
    public void setFlip(Resizable.Flip flip) {
    }

    @Override
    public Flip getFlip() {
        return null;
    }
    private static DataFlavor dataFlavor;

    static {
        try {
            dataFlavor = new DataFlavor(DataFlavor.javaJVMLocalObjectMimeType + ";class="
                    + OvalPainter.class.getName());
        } catch (ClassNotFoundException ex) {
            ErrorManager.errorMinor(OvalPainter.class, ex);
        }
    }

    public static DataFlavor getDataFlavor() {
        return dataFlavor;
    }

    @Override
    public AbstractPainter clonePainter() {
        OvalPainter painter = new OvalPainter(getColor(), getFillColor(), getX(),
                getY(), getWidth(), getHeight());
        painter.setStroke(getStroke());
        painter.setFill(fill);

        painter.moveToOrgin();
        return painter;
    }
}
