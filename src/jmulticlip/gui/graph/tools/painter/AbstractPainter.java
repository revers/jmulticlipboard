package jmulticlip.gui.graph.tools.painter;

import jmulticlip.gui.graph.tools.action.PainterListener;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.geom.AffineTransform;
import java.io.Serializable;
import java.util.ArrayList;
import jmulticlip.gui.graph.CanvasPanel;

/**
 *
 * @author Revers
 */
public abstract class AbstractPainter implements Serializable {

    private AffineTransform origTransform = new AffineTransform();
    protected Point shiftPoint = new Point();
    protected boolean editable = true;
    protected boolean moveByInterior = true;
    protected Rectangle bounds = new Rectangle();
    protected int marginSize;
    protected boolean visible = true;
    private ArrayList<PainterListener> listeners = new ArrayList<PainterListener>();

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

//    public int getMarginSize() {
//        return marginSize;
//    }

//    public void setMarginSize(int marginSize) {
//        this.marginSize = marginSize;
//    }

    public void addPainterListener(PainterListener listener) {
        listeners.add(listener);
    }

    public void removePainterListener(PainterListener listener) {
        listeners.remove(listener);
    }

    public void removeAllPainterListeners() {
        listeners.clear();
    }

    protected void firePainterChanged(AbstractPainter painter) {
        for (PainterListener listener : listeners) {
            listener.painterChanged(painter);
        }
    }

    public boolean isMoveByInterior() {
        return moveByInterior;
    }

    public void setMoveByInterior(boolean moveByInterior) {
        this.moveByInterior = moveByInterior;
    }

    public boolean isEditable() {
        return editable;
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }

    public void move(int xShift, int yShift) {
        shiftPoint.x += xShift;
        shiftPoint.y += yShift;
        calcBounds();
    }

    public void moveToOrgin() {
        Rectangle rect = getBounds();
        move(-rect.x, -rect.y);
    }

    public Point getShift() {
        return shiftPoint;
    }

    protected void addShift(Rectangle bounds) {
        bounds.x += shiftPoint.x;
        bounds.y += shiftPoint.y;
    }

    public void draw(Graphics2D g2) {
        AffineTransform at = g2.getTransform();
        origTransform.setTransform(at);
        at.setToIdentity();
        g2.translate(shiftPoint.x, shiftPoint.y);
        drawImpl(g2);
        g2.setTransform(origTransform);
    }

    public Rectangle getBounds() {
        return bounds;
    }

    public int getBoundsMargin() {
        return 0;
    }

    public abstract boolean isMoveable();

    public abstract boolean isResizable();

    public abstract AbstractPainter clonePainter();

    protected abstract void drawImpl(Graphics2D g2);

    protected abstract void calcBounds();
}
