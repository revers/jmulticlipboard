package jmulticlip.gui.graph.tools.painter;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.*;
import java.awt.datatransfer.DataFlavor;
import java.awt.geom.GeneralPath;
import jmulticlip.core.ErrorManager;
import jmulticlip.gui.graph.tools.Resizable;

/**
 *
 * @author Revers
 */
public class BracketPainter extends AbstractPainter implements Resizable {

    public static enum Type {

        LEFT, RIGHT, UPPER, LOWER
    }
    private static final float DEFAULT_ARC_LENGTH = 20;
    private Color color;
    private BasicStroke stroke = new BasicStroke();
    private int x;
    private int y;
    private int width;
    private int height;
    private GeneralPath generalPath;
    private Type type = Type.LEFT;

    public BracketPainter(Color color, int x, int y, int width, int height) {
        this(color, x, y, width, height, Type.LEFT);
    }

    public BracketPainter(Color color, int x, int y, int width, int height, Type type) {
        this.type = type;
        this.color = color;
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        bounds = new Rectangle(x, y, width, height);
    }

    private void updateGeneralPath(int x, int y, int width, int height) {
        switch (type) {
            case LEFT:
                generalPath = getLeftCurlyBracket(x, y, width, height);
                break;
            case RIGHT:
                generalPath = getRightCurlyBracket(x, y, width, height);
                break;
            case UPPER:
                generalPath = getUpperCurlyBracket(x, y, width, height);
                break;
            case LOWER:
                generalPath = getLowerCurlyBracket(x, y, width, height);
                break;
        }
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public BasicStroke getStroke() {
        return stroke;
    }

    public void setStroke(BasicStroke stroke) {
        this.stroke = stroke;
        calcBounds();
    }

    public float getLineWidth() {
        return stroke.getLineWidth();
    }

    public void setLineWidth(float lineWidth) {
        stroke = new BasicStroke(lineWidth, stroke.getEndCap(), stroke.getLineJoin(),
                stroke.getMiterLimit(), stroke.getDashArray(), stroke.getDashPhase());
        calcBounds();
    }

    @Override
    protected void calcBounds() {
        int local_x = x;
        int local_y = y;
        int local_height = height;
        int local_width = width;

        if (local_width < 0) {
            local_x += local_width;
            local_width = -local_width;
        }

        if (local_height < 0) {
            local_y += local_height;
            local_height = -local_height;
        }
        bounds.x = local_x + shiftPoint.x;
        bounds.y = local_y + shiftPoint.y;
        bounds.width = local_width;
        bounds.height = local_height;

        firePainterChanged(this);
    }

    @Override
    public int getBoundsMargin() {
        return Math.round(stroke.getLineWidth()) + 1;
    }

    @Override
    protected void drawImpl(Graphics2D g2) {
        Color oldColor = g2.getColor();
        Stroke oldStroke = g2.getStroke();
        g2.setColor(color);
        g2.setStroke(stroke);
        int local_x = x;
        int local_y = y;
        int local_height = height;
        int local_width = width;

        if (local_width < 0) {
            local_x += local_width;
            local_width = -local_width;
        }

        if (local_height < 0) {
            local_y += local_height;
            local_height = -local_height;
        }


        //  g2.drawOval(local_x, local_y, local_width, local_height);
        updateGeneralPath(local_x, local_y, local_width, local_height);
        g2.draw(generalPath);

//        g2.drawBracket(bounds.x - shiftPoint.x, bounds.y - shiftPoint.y,
//                bounds.width, bounds.height);
        g2.setColor(oldColor);
        g2.setStroke(oldStroke);
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
        calcBounds();
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
        calcBounds();
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
        shiftPoint.x = 0;
        calcBounds();
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
        shiftPoint.y = 0;
        calcBounds();
    }

    @Override
    public boolean isResizable() {
        return true;
    }

    @Override
    public boolean isMoveable() {
        return true;
    }

    @Override
    public void resize(int newX, int newY, int newWidth, int newHeight) {
        //  int margin = (int) (stroke.getLineWidth());

        x = newX;// + margin;
        y = newY;// + margin;
        width = newWidth;// - 2 * margin;
        height = newHeight;// - 2 * margin;
        shiftPoint.x = 0;
        shiftPoint.y = 0;
        calcBounds();
    }

    @Override
    public void flipX() {
    }

    @Override
    public void flipY() {
    }

    @Override
    public void setFlip(Resizable.Flip flip) {
    }

    @Override
    public Flip getFlip() {
        return null;
    }
    private static DataFlavor dataFlavor;

    static {
        try {
            dataFlavor = new DataFlavor(DataFlavor.javaJVMLocalObjectMimeType + ";class="
                    + BracketPainter.class.getName());
        } catch (ClassNotFoundException ex) {
            ErrorManager.errorMinor(BracketPainter.class, ex);
        }
    }

    public static DataFlavor getDataFlavor() {
        return dataFlavor;
    }

    @Override
    public AbstractPainter clonePainter() {
        BracketPainter painter = new BracketPainter(getColor(), getX(),
                getY(), getWidth(), getHeight());
        painter.setStroke(getStroke());
        painter.setType(type);
//        painter.setGeneralPath(generalPath);

        painter.moveToOrgin();
        return painter;
    }

    private GeneralPath getLowerCurlyBracket(float x, float y, float width, float height) {

        if (width <= 0.0f) {
            width = 1.0f;
        }
        if (height <= 0.0f) {
            height = 1.0f;
        }

        float yArcLength = DEFAULT_ARC_LENGTH;
        float xArcLength = DEFAULT_ARC_LENGTH;

        if (xArcLength * 4.0f > width) {
            xArcLength = width / 4.0f;
        }

        if (yArcLength * 2.0f > height) {
            yArcLength = height / 2.0f;
        }

        GeneralPath gp = new GeneralPath();

        float yCurrent = y;
        float xCurrent = x;
        gp.moveTo(xCurrent, yCurrent);

        yCurrent = y + height - yArcLength;
        xCurrent += xArcLength;

        float xControl = x;
        float yControl = yCurrent;

        gp.quadTo(xControl, yControl, xCurrent, yCurrent);

        float horizSegmentLength = (width - 4.0f * xArcLength) / 2.0f;

        if (horizSegmentLength > 0.0f) {
            xCurrent += horizSegmentLength;
            gp.lineTo(xCurrent, yCurrent);
        }

        yCurrent += yArcLength;
        xCurrent += xArcLength;

        xControl = xCurrent;
        gp.quadTo(xControl, yControl, xCurrent, yCurrent);

        xCurrent += xArcLength;
        yCurrent -= yArcLength;

        gp.quadTo(xControl, yControl, xCurrent, yCurrent);

        if (horizSegmentLength > 0.0f) {
            xCurrent += horizSegmentLength;
            gp.lineTo(xCurrent, yCurrent);
        }

        xCurrent += xArcLength;
        xControl = xCurrent;

        yCurrent = y;

        gp.quadTo(xControl, yControl, xCurrent, yCurrent);

        gp.moveTo(x, y);
        gp.closePath();
        return gp;
    }

    private GeneralPath getUpperCurlyBracket(float x, float y, float width, float height) {

        if (width <= 0.0f) {
            width = 1.0f;
        }
        if (height <= 0.0f) {
            height = 1.0f;
        }

        float yArcLength = DEFAULT_ARC_LENGTH;
        float xArcLength = DEFAULT_ARC_LENGTH;

        if (xArcLength * 4.0f > width) {
            xArcLength = width / 4.0f;
        }

        if (yArcLength * 2.0f > height) {
            yArcLength = height / 2.0f;
        }

        GeneralPath gp = new GeneralPath();

        float yCurrent = y + height;
        float xCurrent = x;
        gp.moveTo(xCurrent, yCurrent);

        yCurrent = y + yArcLength;
        xCurrent += xArcLength;

        float xControl = x;
        float yControl = yCurrent;

        gp.quadTo(xControl, yControl, xCurrent, yCurrent);

        float horizSegmentLength = (width - 4.0f * xArcLength) / 2.0f;

        if (horizSegmentLength > 0.0f) {
            xCurrent += horizSegmentLength;
            gp.lineTo(xCurrent, yCurrent);
        }

        yCurrent -= yArcLength;
        xCurrent += xArcLength;

        xControl = xCurrent;
        gp.quadTo(xControl, yControl, xCurrent, yCurrent);

        xCurrent += xArcLength;
        yCurrent += yArcLength;

        gp.quadTo(xControl, yControl, xCurrent, yCurrent);

        if (horizSegmentLength > 0.0f) {
            xCurrent += horizSegmentLength;
            gp.lineTo(xCurrent, yCurrent);
        }

        xCurrent += xArcLength;
        xControl = xCurrent;

        yCurrent = y + height;

        gp.quadTo(xControl, yControl, xCurrent, yCurrent);

        gp.moveTo(x, y + height);
        gp.closePath();
        return gp;
    }

    private GeneralPath getLeftCurlyBracket(float x, float y, float width, float height) {

        if (width == 0.0f) {
            width = 1.0f;
        }
        if (height == 0.0f) {
            height = 1.0f;
        }

        float yArcLength = DEFAULT_ARC_LENGTH;
        float xArcLength = DEFAULT_ARC_LENGTH;

        if (xArcLength * 2.0f > width) {
            xArcLength = width / 2.0f;
        }

        if (yArcLength * 4.0f > height) {
            yArcLength = height / 4.0f;
        }

        float xControl = x + xArcLength;
        float yControl = y;

        GeneralPath gp = new GeneralPath();

        float yCurrent = y;
        float xCurrent = x + width;
        gp.moveTo(xCurrent, yCurrent);

        yCurrent = y + yArcLength;
        xCurrent = xControl;

        gp.quadTo(xControl, yControl, xCurrent, yCurrent);

        float vertSegmentLength = (height - 4.0f * yArcLength) / 2.0f;

        if (vertSegmentLength > 0.0f) {
            yCurrent += vertSegmentLength;
            gp.lineTo(xCurrent, yCurrent);
        }

        yCurrent += yArcLength;
        xCurrent -= xArcLength;

        yControl = yCurrent;
        gp.quadTo(xControl, yControl, xCurrent, yCurrent);

        xCurrent += xArcLength;
        yCurrent += yArcLength;

        gp.quadTo(xControl, yControl, xCurrent, yCurrent);

        if (vertSegmentLength > 0.0f) {
            yCurrent += vertSegmentLength;
            gp.lineTo(xCurrent, yCurrent);
        }

        yCurrent += yArcLength;
        yControl = yCurrent;

        xCurrent = x + width;

        gp.quadTo(xControl, yControl, xCurrent, yCurrent);

        gp.moveTo(xCurrent, yCurrent);
        gp.closePath();
        return gp;
    }

    private GeneralPath getRightCurlyBracket(float x, float y, float width, float height) {

        if (width == 0.0f) {
            width = 1.0f;
        }
        if (height == 0.0f) {
            height = 1.0f;
        }

        float yArcLength = DEFAULT_ARC_LENGTH;
        float xArcLength = DEFAULT_ARC_LENGTH;

        if (xArcLength * 2.0f > width) {
            xArcLength = width / 2.0f;
        }

        if (yArcLength * 4.0f > height) {
            yArcLength = height / 4.0f;
        }

        float xControl = x + width - xArcLength;
        float yControl = y;

        GeneralPath gp = new GeneralPath();
        gp.moveTo(x, y);

        float yCurrent = y + yArcLength;
        float xCurrent = xControl;

        gp.quadTo(xControl, yControl, xCurrent, yCurrent);

        float vertSegmentLength = (height - 4.0f * yArcLength) / 2.0f;

        if (vertSegmentLength > 0.0f) {
            yCurrent += vertSegmentLength;
            gp.lineTo(xCurrent, yCurrent);
        }

        yCurrent += yArcLength;
        xCurrent += xArcLength;

        yControl = yCurrent;
        gp.quadTo(xControl, yControl, xCurrent, yCurrent);

        xCurrent -= xArcLength;
        yCurrent += yArcLength;

        gp.quadTo(xControl, yControl, xCurrent, yCurrent);

        if (vertSegmentLength > 0.0f) {
            yCurrent += vertSegmentLength;
            gp.lineTo(xCurrent, yCurrent);
        }

        yCurrent += yArcLength;
        yControl = yCurrent;

        xCurrent = x;

        gp.quadTo(xControl, yControl, xCurrent, yCurrent);

        gp.moveTo(x, y);
        gp.closePath();
        return gp;
    }
}
