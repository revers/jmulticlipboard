package jmulticlip.gui.graph.tools.painter;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.datatransfer.DataFlavor;
import java.awt.image.BufferedImage;
import javax.swing.ImageIcon;

/**
 *
 * @author Revers
 */
public class ImagePainter extends AbstractPainter {

    private Image image;
    private int x;
    private int y;

    public ImagePainter(Image image) {
        this.image = image;
        calcBounds();
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
        shiftPoint.x = 0;
        calcBounds();
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
        shiftPoint.y = 0;
        calcBounds();
    }

    @Override
    public void drawImpl(Graphics2D g2) {
        g2.drawImage(image, x, y, null);
        // g2.setColor(Color.red);
        //  g2.fillRect(0, 0, bounds.width, bounds.height);
    }

    public Image getImage() {
        return image;
    }

    @Override
    public boolean isResizable() {
        return false;
    }

    @Override
    public boolean isMoveable() {
        return true;
    }

    @Override
    protected void calcBounds() {
        bounds.x = x;// + shiftPoint.x;
        bounds.y = y;// + shiftPoint.y;
        bounds.width = image.getWidth(null);
        bounds.height = image.getHeight(null);
        addShift(bounds);
        firePainterChanged(this);
    }

    public static DataFlavor getDataFlavor() {
        return DataFlavor.imageFlavor;
    }

    @Override
    public AbstractPainter clonePainter() {
        if (image == null) {
            ImagePainter painter = new ImagePainter(image);
            painter.moveToOrgin();
            return painter;
        }

        BufferedImage img = new BufferedImage(image.getWidth(null),
                image.getHeight(null), BufferedImage.TYPE_INT_RGB);
        Graphics2D g2 = img.createGraphics();
        g2.drawImage(image, 0, 0, null);
        g2.dispose();

        ImagePainter painter = new ImagePainter(img);
        painter.moveToOrgin();
        return painter;
    }
}
