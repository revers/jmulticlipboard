package jmulticlip.gui.graph.tools;

import java.awt.datatransfer.*;
import java.io.*;

/**
 *
 * @author Revers
 */
public class TransferableElement implements Transferable {

    private Object element;
    private DataFlavor dataFlavor;

    public TransferableElement(Object element, DataFlavor flavor) {
        this.element = element;
        this.dataFlavor = flavor;
    }

    @Override
    public DataFlavor[] getTransferDataFlavors() {
        return new DataFlavor[]{dataFlavor};
    }

    @Override
    public boolean isDataFlavorSupported(DataFlavor flavor) {
        return dataFlavor.equals(flavor);
    }

    @Override
    public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException, IOException {
        if (!dataFlavor.equals(flavor)) {
            throw new UnsupportedFlavorException(flavor);
        }
        return element;
    }
}
