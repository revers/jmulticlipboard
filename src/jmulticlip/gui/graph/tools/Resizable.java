package jmulticlip.gui.graph.tools;

/**
 *
 * @author Revers
 */
public interface Resizable {

    /*
     * Flip
     * 1 to stan podstawowy
     * +---+---+
     * | 4 | 3 |
     * +---+---+
     * | 1 | 2 |
     * +---+---+
     */

    public enum Flip {

        FIRST, SECOND, THIRD, FOURTH
    }

    public void resize(int newX, int newY, int newWidth, int newHeight);

    public void setFlip(Flip flip);

    public Flip getFlip();

    public void flipX();

    public void flipY();
}
