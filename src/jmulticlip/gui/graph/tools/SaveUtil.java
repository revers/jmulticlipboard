package jmulticlip.gui.graph.tools;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.*;
import java.util.*;
import javax.imageio.ImageIO;
import javax.swing.filechooser.*;
import javax.swing.*;
import jmulticlip.core.ErrorManager;

/**
 *
 * @author Revers
 */
public class SaveUtil {

    private static final String SAVE_FILE_PREF = "sfp";
    private static final String[] EXTENSIONS = {".png", ".jpg", ".gif"};
    private static String ext;

    public static void saveImage(BufferedImage img, JComponent parent) {
        File saveFile = getSaveFile(parent);

        if (saveFile == null) {
            return;
        }

        try {
            if (ext.equals(".png")) {
                ImageIO.write(img, "png", saveFile);
            } else if (ext.equals(".jpg")) {
                ImageIO.write(img, "jpeg", saveFile);
            } else if (ext.equals(".gif")) {
                ImageIO.write(img, "gif", saveFile);
            }

        } catch (IOException ex) {
            ErrorManager.errorNormal(SaveUtil.class, ex, "Cannot save image!!");
        }
    }

    private static File getSaveFile(JComponent parent) {

        //  UIManager.put("FileChooser.saveDialogTitleText", "Zapisz jako...");
        //  UIManager.put("FileChooser.cancelButtonText", "Anuluj");
        //     UIManager.put("FileChooser.saveButtonText", "Zapisz");

        String defaultValue = File.listRoots()[0].getPath();
        Preferences prefs = Preferences.userNodeForPackage(SaveUtil.class);
        String path = prefs.get(SAVE_FILE_PREF, defaultValue);

        final JFileChooser fileChooser = new JFileChooser(path);
        SwingUtilities.updateComponentTreeUI(fileChooser);
        fileChooser.removeChoosableFileFilter(fileChooser.getAcceptAllFileFilter());

        String fileName = path;
        if (!fileName.endsWith("/") && !fileName.endsWith("\\")) {
            fileName += "\\";
        }
        ext = EXTENSIONS[0];
        fileName += "Obrazek" + ext;
        File file = new File(fileName);
        fileChooser.setSelectedFile(file);

        //fileChooser.addChoosableFileFilter(
        javax.swing.filechooser.FileFilter[] filters =
                new javax.swing.filechooser.FileFilter[EXTENSIONS.length];
        for (int i = 0; i < filters.length; i++) {
            final String extStr = EXTENSIONS[i];
            filters[i] = new javax.swing.filechooser.FileFilter() {

                public boolean accept(File f) {
                    return f.isDirectory()
                            || f.getName().toLowerCase().endsWith(extStr);
                }

                public String getDescription() {
                    return "*" + extStr;
                }
            };

            fileChooser.addChoosableFileFilter(filters[i]);
        }

        fileChooser.setFileFilter(filters[0]);

        fileChooser.addPropertyChangeListener(new PropertyChangeListener() {

            public void propertyChange(PropertyChangeEvent evt) {
                if (evt.getPropertyName().equals("fileFilterChanged")) {
                    String desc = ((javax.swing.filechooser.FileFilter) evt.getNewValue()).getDescription();
                    for (String s : EXTENSIONS) {
                        if (desc.endsWith(s)) {
                            ext = s;
                        }
                    }
                }
            }
        });

        while (true) {
            int returnVal = fileChooser.showSaveDialog(parent);
            if (returnVal == JFileChooser.APPROVE_OPTION) {

                File selectedFile = fileChooser.getSelectedFile();

                if (selectedFile != null) {
                    if (selectedFile.getName().toLowerCase().endsWith(ext) == false) {
                        selectedFile = new File(selectedFile.getAbsolutePath()
                                + ext);
                    }
                    if (selectedFile.exists()) {
                        Object[] options = {"Tak", "Nie"};
                        int n = JOptionPane.showOptionDialog(parent,
                                "Czy chcesz nadpisać plik:\n"
                                + selectedFile.getName(),
                                "Podany plik istnieje!",
                                JOptionPane.YES_NO_OPTION,
                                JOptionPane.WARNING_MESSAGE, null, options,
                                options[0]);

                        if (n == 1) {
                            return null;
                        }

                        if (!selectedFile.delete()) {
                            String msg = "Nie mogę nadpisać pliku:\n"
                                    + selectedFile.getName() + "\n"
                                    + "Spróbuj inną nazwę.";
                            JOptionPane.showMessageDialog(parent, msg,
                                    "Użyj innej nazwy",
                                    JOptionPane.ERROR_MESSAGE);
                            continue;
                        }
                    }

                    prefs.put(SAVE_FILE_PREF, selectedFile.getParent());

                    return selectedFile;
                }
            } else {
                break;
            }
        }
        return null;
    }
}
