package jmulticlip.gui.graph.tools;

import java.awt.*;
import jmulticlip.gui.graph.CanvasPanel;

/**
 *
 * @author Revers
 */
public class SelectionFrame {

    private CanvasPanel canvas;
    private Rectangle bounds = new Rectangle();

    public SelectionFrame(CanvasPanel canvas) {
        this.canvas = canvas;
    }

    public void clear() {
        bounds.x = 0;
        bounds.y = 0;
        bounds.width = 0;
        bounds.height = 0;
    }

    public int getHeight() {
        return bounds.height;
    }

    public void setHeight(int height) {
        bounds.height = height;
    }

    public int getWidth() {
        return bounds.width;
    }

    public void setWidth(int width) {
        bounds.width = width;
    }

    public int getX() {
        return bounds.x;
    }

    public void setX(int x) {
        bounds.x = x;
    }

    public int getY() {
        return bounds.y;
    }

    public void setY(int y) {
        bounds.y = y;
    }

    public void draw(Graphics2D g2) {
        if (bounds.width == 0 || bounds.height == 0) {
            return;
        }

        int x = (int) (bounds.x * canvas.getZoom());
        int y = (int) (bounds.y * canvas.getZoom());
        int width = (int) (bounds.width * canvas.getZoom());
        int height = (int) (bounds.height * canvas.getZoom());
        if (width > 0) {
            width--;
        }
        if (height > 0) {
            height--;
        }

        drawHorizontalLine(g2, x, y, width);
        drawHorizontalLine(g2, x, y + height, width);
        drawVerticalLine(g2, x, y, height);
        drawVerticalLine(g2, x + width, y, height);

//        drawHorizontalLine(g2, bounds.x, bounds.y, bounds.width);
//        drawHorizontalLine(g2, bounds.x, bounds.y + bounds.height, bounds.width);
//        drawVerticalLine(g2, bounds.x, bounds.y, bounds.height);
//        drawVerticalLine(g2, bounds.x + bounds.width, bounds.y, bounds.height);
    }

    private void drawHorizontalLine(Graphics2D g2, int x, int y, int width) {

        int xOrig = x;

        for (; x <= xOrig + width; x++) {
            if (y < 0 || y >= canvas.getHeight()
                    || x < 0 || x >= canvas.getWidth()) {
                continue;
            }

            Color color = new Color(canvas.getCanvasImage().getRGB(
                    (int) (x / canvas.getZoom()), (int) (y / canvas.getZoom())));
            g2.setColor(getInvertedColor(color));
            g2.drawLine(x, y, x, y);
        }
    }

    private void drawVerticalLine(Graphics2D g2, int x, int y, int height) {

        int yOrig = y;

        for (; y <= yOrig + height; y++) {
            if (y < 0 || y >= canvas.getHeight()
                    || x < 0 || x >= canvas.getWidth()) {
                continue;
            }

            Color color = new Color(canvas.getCanvasImage().getRGB(
                    (int) (x / canvas.getZoom()), (int) (y / canvas.getZoom())));
            g2.setColor(getInvertedColor(color));
            g2.drawLine(x, y, x, y);
        }

    }

    private Color getInvertedColor(Color color) {
        return new Color(255 - color.getRed(), 255 - color.getGreen(),
                255 - color.getBlue());
    }

    public Rectangle getBounds() {
        return bounds;
    }

    public void move(int xShift, int yShift) {
        bounds.x += xShift;
        bounds.y += yShift;
    }
}
