package jmulticlip.gui.graph.tools.action;

import jmulticlip.gui.graph.CanvasPanel;
import jmulticlip.gui.graph.tools.painter.AbstractPainter;

/**
 *
 * @author Revers
 */
public class DoublePainterAction implements CanvasAction {

    private CanvasPanel canvas;
    private AbstractPainter firstPainter;
    private AbstractPainter secondPainter;

    public DoublePainterAction(CanvasPanel canvas, AbstractPainter firstPainter,
            AbstractPainter secondPainter) {
        this.canvas = canvas;
        this.firstPainter = firstPainter;
        this.secondPainter = secondPainter;
        canvas.setCurrentPainter(firstPainter);
        canvas.saveCurrentPainter();
        canvas.setCurrentPainter(secondPainter);
    }

    public CanvasPanel getCanvas() {
        return canvas;
    }

    public AbstractPainter getFirstPainter() {
        return firstPainter;
    }

    public AbstractPainter getSecondPainter() {
        return secondPainter;
    }

    @Override
    public void onUndo(CanvasPanel canvas) {
        canvas.undoPainter();
        canvas.undoPainter();
    }

    @Override
    public void onRedo(CanvasPanel canvas) {
        canvas.redoPainter();
        canvas.redoPainter();
    }
}
