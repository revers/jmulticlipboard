package jmulticlip.gui.graph.tools.action;

import jmulticlip.gui.graph.tools.painter.AbstractPainter;
import jmulticlip.gui.graph.tools.painter.AbstractPainter;

/**
 *
 * @author Revers
 */
public interface PainterListener {

    public void painterChanged(AbstractPainter painter);
}
