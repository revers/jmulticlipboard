package jmulticlip.gui.graph.tools.action;

import jmulticlip.gui.graph.CanvasPanel;
import jmulticlip.gui.graph.tools.painter.AbstractPainter;

/**
 *
 * @author Revers
 */
public class PainterAction implements CanvasAction {

    private AbstractPainter painter;
    private CanvasPanel canvas;

    public PainterAction(CanvasPanel canvas, AbstractPainter painter) {
        this.painter = painter;
        this.canvas = canvas;
        canvas.setCurrentPainter(painter);
    }

    public CanvasPanel getCanvas() {
        return canvas;
    }

    public AbstractPainter getPainter() {
        return painter;
    }

    @Override
    public void onUndo(CanvasPanel canvas) {
        canvas.undoPainter();
    }

    @Override
    public void onRedo(CanvasPanel canvas) {
        canvas.redoPainter();
    }
}
