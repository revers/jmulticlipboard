package jmulticlip.gui.graph.tools.action;

import java.awt.Image;
//import jmulticlip.gui.painters.CanvasAction;
import java.util.*;
import jmulticlip.gui.graph.CanvasPanel;
import jmulticlip.gui.graph.util.UndoableList;

/**
 *
 * @author Revers
 */
public class CanvasActionList<E extends CanvasAction> extends UndoableList<E> {

    private CanvasPanel canvas;

    public CanvasActionList(CanvasPanel canvas) {
        this.canvas = canvas;
    }

    @Override
    public boolean undoAdd() {
        if (super.undoAdd()) {
            get(index).onUndo(canvas);
            return true;
        }
        return false;
    }

    @Override
    public boolean redoAdd() {
        if (super.redoAdd()) {
            get(index - 1).onRedo(canvas);
            return true;
        }
        return false;
    }
}
