package jmulticlip.gui.graph.tools.action;

import java.awt.Point;
import java.awt.Rectangle;
import jmulticlip.gui.graph.CanvasPanel;
import jmulticlip.gui.graph.tools.painter.AbstractPainter;

/**
 *
 * @author Revers
 */
public class MovePainterAction implements CanvasAction {

    private AbstractPainter painter;
    private CanvasPanel canvas;
    private Point undoShift = new Point();

    public MovePainterAction(CanvasPanel canvas, AbstractPainter painter,
            int origX, int origY) {
        this.painter = painter;
        this.canvas = canvas;
        Rectangle rect = painter.getBounds();
        undoShift.x = origX - rect.x;
        undoShift.y = origY - rect.y;
    }

    public CanvasPanel getCanvas() {
        return canvas;
    }

    public AbstractPainter getPainter() {
        return painter;
    }

    @Override
    public void onUndo(CanvasPanel canvas) {
        painter.move(undoShift.x, undoShift.y);
        canvas.setCurrentPainter(painter);
        canvas.repaint();
    }

    @Override
    public void onRedo(CanvasPanel canvas) {
        painter.move(-undoShift.x, -undoShift.y);
        canvas.setCurrentPainter(painter);
        canvas.repaint();
    }
}
