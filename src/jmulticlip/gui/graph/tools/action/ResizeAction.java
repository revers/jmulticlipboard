package jmulticlip.gui.graph.tools.action;

import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Image;
import jmulticlip.gui.graph.CanvasPanel;

/**
 *
 * @author Revers
 */
public class ResizeAction implements CanvasAction {

    private Image newBackgroundImage;
    private Image origBackgroundImage;
    private int origMinPainterIndex;
    private int newMinPainterIndex;

    public ResizeAction(Image resizedImage, CanvasPanel canvas) {
        this.newBackgroundImage = resizedImage;
        origBackgroundImage = canvas.getBackgroundImage();
        origMinPainterIndex = canvas.getMinPainterIndex();
        newMinPainterIndex = canvas.getPainterCount();
        canvas.setMinPainterIndex(newMinPainterIndex);
        canvas.setBackgroundImage(resizedImage);

    }

    public Image getNewBackgroundImage() {
        return newBackgroundImage;
    }

    public int getNewMinPainterIndex() {
        return newMinPainterIndex;
    }

    public Image getOrigBackgroundImage() {
        return origBackgroundImage;
    }

    public int getOrigMinPainterIndex() {
        return origMinPainterIndex;
    }

    @Override
    public void onUndo(CanvasPanel canvas) {
        if (canvas.getZoom() != 1.0f) {
            canvas.setZoom(1.0f);
        }
        canvas.setBackgroundImage(origBackgroundImage, origMinPainterIndex);
        canvas.setLastPainterCurrent();
    }

    @Override
    public void onRedo(CanvasPanel canvas) {
        if (canvas.getZoom() != 1.0f) {
            canvas.setZoom(1.0f);
        }
        canvas.setBackgroundImage(newBackgroundImage, newMinPainterIndex);
    }
}
