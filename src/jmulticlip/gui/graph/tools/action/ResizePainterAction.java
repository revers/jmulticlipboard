package jmulticlip.gui.graph.tools.action;

import java.awt.Point;
import java.awt.Rectangle;
import jmulticlip.gui.graph.CanvasPanel;
import jmulticlip.gui.graph.tools.painter.AbstractPainter;
import jmulticlip.gui.graph.tools.Resizable;

/**
 *
 * @author Revers
 */
public class ResizePainterAction implements CanvasAction {

    private AbstractPainter painter;
    private CanvasPanel canvas;
    private Rectangle origBounds;
    private Rectangle newBounds;
    private Resizable.Flip origFlip;
    private Resizable.Flip newFlip;

    public ResizePainterAction(CanvasPanel canvas, AbstractPainter painter,
            Rectangle origBounds, Resizable.Flip origFlip) {
        this.painter = painter;
        this.canvas = canvas;
        this.origFlip = origFlip;
        this.origBounds = origBounds;
        newBounds = (Rectangle) painter.getBounds().clone();
        newFlip = ((Resizable) painter).getFlip();
    }

    public CanvasPanel getCanvas() {
        return canvas;
    }

    public AbstractPainter getPainter() {
        return painter;
    }

    @Override
    public void onUndo(CanvasPanel canvas) {
        ((Resizable) painter).resize(origBounds.x, origBounds.y, origBounds.width, origBounds.height);
        if (origFlip != null) {
            ((Resizable) painter).setFlip(origFlip);
        }
        canvas.setCurrentPainter(painter);
        canvas.repaint();
    }

    @Override
    public void onRedo(CanvasPanel canvas) {

        ((Resizable) painter).resize(newBounds.x, newBounds.y, newBounds.width, newBounds.height);
        if (newFlip != null) {
            ((Resizable) painter).setFlip(newFlip);
        }
        
        canvas.setCurrentPainter(painter);
        canvas.repaint();
    }
}
