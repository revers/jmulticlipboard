package jmulticlip.gui.graph.tools.action;

import jmulticlip.gui.graph.CanvasPanel;

/**
 *
 * @author Revers
 */
public interface CanvasAction {

    public void onUndo(CanvasPanel canvas);

    public void onRedo(CanvasPanel canvas);
}
