package jmulticlip.gui.graph.tools.action;

import jmulticlip.gui.graph.CanvasPanel;
import jmulticlip.gui.graph.tools.painter.AbstractPainter;

/**
 *
 * @author Revers
 */
public class DeletePainterAction implements CanvasAction {

    private AbstractPainter painter;
    private CanvasPanel canvas;

    public DeletePainterAction(CanvasPanel canvas, AbstractPainter painter) {
        this.painter = painter;
        this.canvas = canvas;
        canvas.setCurrentPainter(painter);
       // canvas.saveCurrentPainter();
        painter.setEditable(false);
        painter.setVisible(false);
    }

    public CanvasPanel getCanvas() {
        return canvas;
    }

    public AbstractPainter getPainter() {
        return painter;
    }

    @Override
    public void onUndo(CanvasPanel canvas) {

        painter.setVisible(true);
        painter.setEditable(true);
        canvas.setCurrentPainter(painter);
       // canvas.dispatchPainterSwitchedEvent();
        canvas.repaint();
    }

    @Override
    public void onRedo(CanvasPanel canvas) {

        painter.setVisible(false);
        painter.setEditable(false);

        canvas.setCurrentPainter(null);
      //  canvas.dispatchPainterSwitchedEvent();
        canvas.repaint();
    }
}
