package jmulticlip.gui.graph.tools;

import jmulticlip.gui.graph.tools.action.ResizeAction;
import jmulticlip.gui.graph.tools.action.CanvasActionList;
import java.util.logging.Level;
import java.util.logging.Logger;
import jmulticlip.gui.graph.CanvasPanel;
import java.awt.*;
import java.awt.image.*;
import javax.swing.SwingUtilities;

/**
 *
 * @author Revers
 */
public class CanvasResizer {

    private CanvasPanel canvas;
//    private int newWidth;
//    private int newHeight;

    public CanvasResizer(CanvasPanel canvas) {
        this.canvas = canvas;
    }

//    private void resize() {
//        BufferedImage oldImage = canvas.renderCanvasImage();
//        BufferedImage image = null;
//
//        if (oldImage.getWidth() > newWidth && oldImage.getHeight() > newHeight) {
//            image = getScaledInstance(oldImage,
//                    newWidth, newHeight, RenderingHints.VALUE_INTERPOLATION_BILINEAR, true);
//        } else {
//            image = getScaledInstance(oldImage,
//                    newWidth, newHeight, RenderingHints.VALUE_INTERPOLATION_BICUBIC, true);
//        }
//
//        ResizeAction resizeAction = new ResizeAction(image, canvas);
//        canvas.addAction(resizeAction);
//    }
    public void resize(int newWidth, int newHeight) {
        //     this.newHeight = newHeight;
        //    this.newWidth = newWidth;
        if (canvas.getZoom() != 1.0f) {
            canvas.setZoom(1.0f);
        }
        CanvasActionList list = canvas.getCanvasActionList();

        if (list.getLastElement() != null && list.getLastElement() instanceof ResizeAction
                && list.canUndo()) {
            list.undoAdd();

            //  SwingUtilities.invokeLater(this);

        }// else {
        //  resize();
        //}
        // resize();

        BufferedImage oldImage = canvas.getCanvasImage();
        BufferedImage image = null;

        if (oldImage.getWidth() > newWidth && oldImage.getHeight() > newHeight) {
            image = getScaledInstance(oldImage,
                    newWidth, newHeight, RenderingHints.VALUE_INTERPOLATION_BILINEAR, true);
        } else {
            image = getScaledInstance(oldImage,
                    newWidth, newHeight, RenderingHints.VALUE_INTERPOLATION_BICUBIC, true);
        }

        ResizeAction resizeAction = new ResizeAction(image, canvas);
        canvas.addAction(resizeAction);
    }

//    @Override
//    public void run() {
//        new Thread() {
//
//            @Override
//            public void run() {
//                resize();
//            }
//        }.start();
//    }
    /**
     * Convenience method that returns a scaled instance of the
     * provided {@code BufferedImage}.
     *
     * @param img the original image to be scaled
     * @param targetWidth the desired width of the scaled instance,
     *    in pixels
     * @param targetHeight the desired height of the scaled instance,
     *    in pixels
     * @param hint one of the rendering hints that corresponds to
     *    {@code RenderingHints.KEY_INTERPOLATION} (e.g.
     *    {@code RenderingHints.VALUE_INTERPOLATION_NEAREST_NEIGHBOR},
     *    {@code RenderingHints.VALUE_INTERPOLATION_BILINEAR},
     *    {@code RenderingHints.VALUE_INTERPOLATION_BICUBIC})
     * @param higherQuality if true, this method will use a multi-step
     *    scaling technique that provides higher quality than the usual
     *    one-step technique (only useful in down-scaling cases, where
     *    {@code targetWidth} or {@code targetHeight} is
     *    smaller than the original dimensions, and generally only when
     *    the {@code BILINEAR} hint is specified)
     * @return a scaled version of the original {@codey BufferedImage}
     */
    public static BufferedImage getScaledInstance(BufferedImage img,
            int targetWidth,
            int targetHeight,
            Object hint,
            boolean higherQuality) {
        int type = (img.getTransparency() == Transparency.OPAQUE)
                ? BufferedImage.TYPE_INT_RGB : BufferedImage.TYPE_INT_ARGB;
        BufferedImage ret = (BufferedImage) img;
        int w, h;
        if (higherQuality && (img.getWidth() > targetWidth && img.getHeight() > targetHeight)) {
            // Use multi-step technique: start with original size, then
            // scale down in multiple passes with drawImage()
            // until the target size is reached
            w = img.getWidth();
            h = img.getHeight();
        } else {
            // Use one-step technique: scale directly from original
            // size to target size with a single drawImage() call
            w = targetWidth;
            h = targetHeight;
        }

        do {
            if (higherQuality && w > targetWidth) {
                w /= 2;
                if (w < targetWidth) {
                    w = targetWidth;
                }
            }

            if (higherQuality && h > targetHeight) {
                h /= 2;
                if (h < targetHeight) {
                    h = targetHeight;
                }
            }

            BufferedImage tmp = new BufferedImage(w, h, type);
            Graphics2D g2 = tmp.createGraphics();
            g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, hint);
            g2.drawImage(ret, 0, 0, w, h, null);
            g2.dispose();

            ret = tmp;
        } while (w != targetWidth || h != targetHeight);

        return ret;
    }
}
