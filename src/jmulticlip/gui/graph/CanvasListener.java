package jmulticlip.gui.graph;

import jmulticlip.gui.graph.CanvasPanel;

/**
 *
 * @author Revers
 */
public interface CanvasListener {

//    public enum PainterType {
//
//        NONE, PENCIL, OVAL, LINE, TEXT, IMAGE
//    }

   // public void painterSwitched(CanvasPanel canvas, PainterType type);
//    public void painterAdded(CanvasPanel canvas);

    public void canvasChanged(CanvasPanel canvas);
}
