package jmulticlip.gui.graph;

/**
 *
 * @author Revers
 */
public interface CommonPropertyChangeListener {

    public void propertyChanged(Object property, CommonProperties.Type type);
}
