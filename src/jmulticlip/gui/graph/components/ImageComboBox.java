package jmulticlip.gui.graph.components;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

/**
 *
 * @author Revers
 */
public class ImageComboBox extends JComboBox {

    private Icon[] icons;
    private ComboBoxRenderer comboRenderer = new ComboBoxRenderer();

    public ImageComboBox() {
        super(createIntegerArray(3));
        icons = new Icon[0];

        setRenderer(comboRenderer);
    }

    public ImageComboBox(Icon[] icons) {
        super(createIntegerArray(icons.length));
        this.icons = icons;


        //renderer.setPreferredSize(new Dimension(200, 130));
        setRenderer(comboRenderer);
    }

    private static Integer[] createIntegerArray(int elements) {
        Integer[] intArray = new Integer[elements];
        for (int i = 0; i < elements; i++) {
            intArray[i] = new Integer(i);
        }

        return intArray;
    }

    public int getIconsSelectedIndex() {
        return ((Integer) getSelectedItem()).intValue();
    }

    public Icon[] getIcons() {
        return icons;
    }

    public void setIcons(Icon[] icons) {
        this.icons = icons;
        setModel(new DefaultComboBoxModel(createIntegerArray(icons.length)));
    }

    class ComboBoxRenderer extends JLabel
            implements ListCellRenderer {

        private EmptyBorder defaultEmptyBorder = new EmptyBorder(3, 0, 3, 0);
        private EmptyBorder listEmptyBorder = new EmptyBorder(0, 0, 0, 0);

        public ComboBoxRenderer() {
            setOpaque(true);
            setHorizontalAlignment(CENTER);
            setVerticalAlignment(CENTER);


        }

        public Component getListCellRendererComponent(
                JList list,
                Object value,
                int index,
                boolean isSelected,
                boolean cellHasFocus) {
            if (index >= 0) {
                setBorder(defaultEmptyBorder);
            } else {
                setBorder(listEmptyBorder);
            }

            int selectedIndex = ((Integer) value).intValue();

            if (isSelected) {
                setBackground(list.getSelectionBackground());
                setForeground(list.getSelectionForeground());
            } else {
                setBackground(list.getBackground());
                setForeground(list.getForeground());
            }

            if (selectedIndex >= 0 && selectedIndex < icons.length) {
                setIcon(icons[selectedIndex]);
                setText(null);
            } else {
                setIcon(null);
                setText(((Integer) value).toString());
            }

            return this;
        }
    }
}
