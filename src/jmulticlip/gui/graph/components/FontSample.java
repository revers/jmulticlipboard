package jmulticlip.gui.graph.components;

import java.awt.*;
import java.awt.font.*;
import java.awt.geom.Rectangle2D;
import java.text.*;
import java.awt.image.*;
import java.util.Hashtable;
import java.util.Locale;
import javax.swing.*;

/**
 *
 * @author Revers
 */
public class FontSample implements Runnable {

    private static final FontRenderContext FRC = new FontRenderContext(null, true, false);
    private static final String TEXT = "The Quick Brown Fox";
    private Hashtable<TextAttribute, Object> attributeMap = new Hashtable<TextAttribute, Object>();
    private Font font;
    private int size;
    private int width;
    private int height;
    private ImageIcon icon;
    private IconObserver observer;

    public FontSample(Font font) {
        this(font, font.getSize());
    }

    public FontSample(Font font, int fontSize) {
        this.size = fontSize;
        this.font = font;
        attributeMap.put(TextAttribute.FAMILY, font.getFamily());
        attributeMap.put(TextAttribute.SIZE, new Float(size));
        attributeMap.put(TextAttribute.KERNING, TextAttribute.KERNING_ON);
    }

    public static FontSample[] getAllFontSamples() {
        return getAllFontSamples(15);
    }

    public static FontSample[] getAllFontSamples(int fontSize) {
        String[] fonts = GraphicsEnvironment.getLocalGraphicsEnvironment().getAvailableFontFamilyNames(new Locale("pl", "PL"));
        FontSample[] samples = new FontSample[fonts.length];

        for (int i = 0; i < fonts.length; i++) {
            samples[i] = new FontSample(new Font(fonts[i], Font.PLAIN, fontSize));
        }

        return samples;
    }

    public Font getFont() {
        return font;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
        attributeMap.put(TextAttribute.SIZE, new Float(size));
        icon = null;
    }

    public Icon getSampleIcon(IconObserver observer) {
        return getSampleIcon(observer, Integer.MAX_VALUE, Integer.MAX_VALUE);
    }

    public Icon getSampleIcon(IconObserver observer, int maxWidth, int maxHeight) {
        if (icon == null) {
            this.observer = observer;
            this.width = maxWidth;
            this.height = maxHeight;
            new Thread(this).start();
            return null;
        }
        return icon;
    }

    @Override
    public void run() {
        BufferedImage image = new BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g = (Graphics2D) image.getGraphics();

        int w = width;// (int) rect.getWidth();
        int h = height;// (int) rect.getHeight();

        FontMetrics fm = g.getFontMetrics(font);
        Rectangle2D rect = fm.getStringBounds(TEXT, g);

        if (w == Integer.MAX_VALUE) {
            w = (int) rect.getWidth();

        }
        if (h == Integer.MAX_VALUE) {
            h = (int) rect.getHeight();
        }


//        if (w < width) {
//            width = w;
//        } else if (w > width) {
//            w = width;
//        }
//
//        if (h < height) {
//            height = h;
//        } else if (h > height) {
//            h = height;
//        }

        image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        g = (Graphics2D) image.getGraphics();

        g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);


        g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER,
                0.0f));

        g.setColor(Color.black);
        g.fillRect(0, 0, width, height);

        g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER,
                1.0f));

//        g.setColor(Color.red);
//        g.drawRect(0, 0, width - 1, height - 1);
//        g.drawLine(0, height / 2, width, height / 2);
//        g.setColor(Color.black);

        int xPos = 0;//(int) (width - rect.getWidth()) / 2;
        int yPos = (int) (height - rect.getHeight()) / 2 + fm.getAscent();

        g.setFont(font);
        g.drawString(TEXT, xPos, yPos);

        g.dispose();
        icon = new ImageIcon(image);
        if (observer != null) {
            observer.iconLoaded(icon);
        }
    }

    @Override
    public String toString() {
        return font.getFamily();
    }
}
