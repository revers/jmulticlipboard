package jmulticlip.gui.graph.components;

import java.awt.*;

import javax.swing.*;

public class ColorComboBoxRenderer extends JPanel implements ListCellRenderer {
    //private Color[] colors;

    private Color color;
    private static final Font font = new Font("Tahoma", Font.BOLD, 11);
    private boolean drawMoreString = false;
    private String moreString = "Więcej...";

    public ColorComboBoxRenderer() {
        //this.colors = colors;
        setOpaque(true);
        setPreferredSize(new Dimension(15, 18));
        // setHorizontalAlignment(JLabel.CENTER);
        // setVerticalAlignment(CENTER);
    }

    @Override
    public Component getListCellRendererComponent(JList list, Object value,
            int index, boolean isSelected, boolean cellHasFocus) {

        //int selectedIndex = ((Integer) value).intValue();
        //color = colors[selectedIndex];
        color = (Color) value;
        if (index == list.getModel().getSize() - 1) {
            drawMoreString = true;
        } else {
            drawMoreString = false;
        }

        if (isSelected) {
            setBackground(list.getSelectionBackground());
            setForeground(list.getSelectionForeground());
        } else {
            setBackground(list.getBackground());
            setForeground(list.getForeground());
        }
        return this;
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        g.setColor(color);

        if (drawMoreString) {
            Graphics2D g2 = (Graphics2D) g;
            g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
                    RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
            g2.setRenderingHint(RenderingHints.KEY_RENDERING,
                    RenderingHints.VALUE_RENDER_QUALITY);
            g2.setFont(font);
            g2.drawString(moreString, 4, 13);
        } else {
            g.fillRect(4, 2, getWidth() - 8, getHeight() - 5);
            g.setColor(Color.black);
            g.drawRect(4, 2, getWidth() - 8, getHeight() - 5);
        }

    }
}
