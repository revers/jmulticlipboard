package jmulticlip.gui.graph.components;

import java.awt.*;
import javax.swing.*;

public class StrokeComboBoxRenderer extends JPanel implements ListCellRenderer {
    //private Color[] colors;

    private Stroke stroke;

    public StrokeComboBoxRenderer() {
        //this.colors = colors;
        setOpaque(true);
        setPreferredSize(new Dimension(15, 15));
        // setHorizontalAlignment(JLabel.CENTER);
        // setVerticalAlignment(CENTER);
    }

    @Override
    public Component getListCellRendererComponent(JList list, Object value,
            int index, boolean isSelected, boolean cellHasFocus) {

        if (value != null) {
            stroke = (Stroke) value;
        } else {
            stroke = (Stroke) list.getModel().getElementAt(0);
        }


        if (isSelected) {
            setBackground(list.getSelectionBackground());
            setForeground(list.getSelectionForeground());
        } else {
            setBackground(list.getBackground());
            setForeground(list.getForeground());
        }

        return this;
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        Graphics2D g2 = (Graphics2D) g;
        g2.setStroke(stroke);
        int yCenter = getHeight() / 2;
        g2.drawLine(5, yCenter, getWidth() - 5, yCenter);
    }
}
