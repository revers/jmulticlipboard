package jmulticlip.gui.graph.components;

import java.awt.*;
import javax.swing.*;
import javax.swing.event.*;

/**
 *
 * @author Revers
 */
public class FontComboBoxModel extends DefaultComboBoxModel {

    private FontSample[] fonts;
  //  private Font selectedFont;

    public FontComboBoxModel() {
        fonts = FontSample.getAllFontSamples();
    }

//    public Font getFont(String fontName) {
//        for (Font f : fonts) {
//            if (f.getFontName().equals(fontName)) {
//                return f;
//            }
//        }
//        return null;
//    }
//    public void setSelectedItem(Object anItem) {
//        selectedFont = (Font) anItem;
//    }
//
//    public Object getSelectedItem() {
//        return selectedFont;
//    }
    @Override
    public int getSize() {
        return fonts.length;
    }

    @Override
    public Object getElementAt(int index) {
        if(index == -1) {
            return fonts[0];
        }
        return fonts[index];
    }
//    public void addListDataListener(ListDataListener l) {
//        // Listeners are not supported.
//    }
//
//    public void removeListDataListener(ListDataListener l) {
//        // Listeners are not supported.
//    }
}
