package jmulticlip.gui.graph.components;

/**
 *
 * @author Revers
 */
import java.awt.*;
import javax.swing.*;
import jmulticlip.gui.graph.tools.ArrowedLine;

public class ArrowComboBox extends JComboBox {

    public ArrowComboBox() {
        this(new ArrowedLine[]{
                    new ArrowedLine(ArrowedLine.Type.NONE),
                    new ArrowedLine(ArrowedLine.Type.LEADING),
                    new ArrowedLine(ArrowedLine.Type.TRAILING),
                    new ArrowedLine(ArrowedLine.Type.BOTH),});

    }

    public ArrowComboBox(ArrowedLine[] arrows) {
        setModel(new ArrowComboBoxModel(arrows));
        setRenderer(new ArrowComboBoxRenderer());
    }

    public ArrowedLine getSelectedArrowedLine() {
        if (getSelectedIndex() == -1) {
            return (ArrowedLine) getModel().getElementAt(0);
        } else {
            return (ArrowedLine) getSelectedItem();
        }
    }
}
