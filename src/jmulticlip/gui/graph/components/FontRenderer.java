package jmulticlip.gui.graph.components;

import java.util.*;
import java.awt.*;
import javax.swing.*;

public class FontRenderer extends JComboBox {

    static final long serialVersionUID = 1000;
    static GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
    static HashMap<String, Font> fontmap = new HashMap<String, Font>();
    static Font fonts[] = ge.getAllFonts();
    public static Font selectedfont;
    JComponent component;
    float size;

    protected class FontCellRenderer extends JLabel implements ListCellRenderer {

        static final long serialVersionUID = 1001;

        public FontCellRenderer() {
            setOpaque(true);
        }

        public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
            setText(value.toString());
            size = getFont().getSize2D();

            if (index != -1) {
                setFont(fonts[index].deriveFont(size));
            }

            Color background;
            Color foreground;

            if (isSelected) {
                background = Color.RED;
                foreground = Color.WHITE;
            } else {
                background = Color.WHITE;
                foreground = Color.BLACK;
            }
            ;

            setBackground(background);
            setForeground(foreground);

            return this;
        }
    }

    public FontRenderer(JComponent component) {
        this.setRenderer(new FontCellRenderer());
        for (int i = 0; i < fonts.length; i++) {
            addItem(fonts[i].getFontName());
            // Log.println(getClass().getName() + ": font name=" + fonts[i].getFontName());
        }
        this.component = component;
    }

    @Override
    public Object getSelectedItem() {
        Object object = super.getSelectedItem();

        int index = super.getSelectedIndex();

//        Log.println(object.getClass().getName() + ": " + object.toString() + " size=" + size + " index=" + index);
        selectedfont = fonts[index];
        component.setFont(fonts[index].deriveFont(size));

        return object;
    }

    public static void main(String[] arguments) {
        JFrame f = new JFrame("Font renderer");
        JLabel l = new JLabel("Example Text");
        FontRenderer r = new FontRenderer(l);
        JPanel p = new JPanel(new FlowLayout());

        f.setContentPane(p);
        p.add(r);
        p.add(l);
        f.pack();
        f.setVisible(true);

    }
}
