package jmulticlip.gui.graph.components;

import java.awt.Color;

/**
 *
 * @author Revers
 */
public class MyColor extends Color {

    public MyColor(Color color) {
        super(color.getRed(), color.getGreen(), color.getBlue());
    }


    @Override
    public boolean equals(Object obj) {
        return this == obj;
    }

    public boolean sameAs(Color color) {
        if(getRGB() == color.getRGB()) {
            return true;
        }
        return false;
    }
}
