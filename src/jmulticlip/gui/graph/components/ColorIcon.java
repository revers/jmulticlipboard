package jmulticlip.gui.graph.components;

import java.awt.*;
import javax.swing.*;
import jmulticlip.util.GUIUtils;

/**
 *
 * @author Revers
 */
public class ColorIcon extends ImageIcon {

    private Icon smallerIcon;
    private Color color;

    public ColorIcon(Color color, int biggerIconWidth, int biggerIconHeight) {
        super(GUIUtils.getColorImage(color, biggerIconWidth, biggerIconHeight));
    }

    public ColorIcon(Color color, int biggerIconWidth,
            int biggerIconHeight, int smallerIconWidth, int smallerIconHeight) {
        super(GUIUtils.getColorImage(color, biggerIconWidth, biggerIconHeight, true));
        this.color = color;
        smallerIcon = GUIUtils.getColorIcon(color, smallerIconWidth, smallerIconHeight, true);
    }

    public Icon getSmallerIcon() {
        return smallerIcon;
    }

    public void setSmallerIcon(Icon smallerIcon) {
        this.smallerIcon = smallerIcon;
    }

    public Color getColor() {
        return color;
    }
}
