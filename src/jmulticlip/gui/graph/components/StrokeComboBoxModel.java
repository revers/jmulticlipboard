package jmulticlip.gui.graph.components;

import java.awt.*;
import javax.swing.*;

public class StrokeComboBoxModel extends DefaultComboBoxModel {

    private BasicStroke[] strokes;

    public StrokeComboBoxModel(BasicStroke[] stokes) {
        this.strokes = stokes;
    }

    @Override
    public Object getElementAt(int index) {
        return strokes[index];
    }

    @Override
    public int getSize() {
        return strokes.length;
    }
}
