package jmulticlip.gui.graph.components;

/**
 *
 * @author Revers
 */
import java.awt.*;
import javax.swing.*;

public class StrokeComboBox extends JComboBox {

    public static final BasicStroke[] DASH_STROKES = {
        new BasicStroke(1.0f,
        BasicStroke.CAP_SQUARE,
        BasicStroke.JOIN_MITER,
        10.0f),
        new BasicStroke(1.0f,
        BasicStroke.CAP_BUTT,
        BasicStroke.JOIN_MITER,
        10.0f, new float[] {10.0f}, 0.0f),
        new BasicStroke(1, BasicStroke.CAP_BUTT,
        BasicStroke.JOIN_MITER, 10.0f,
        new float[]{12, 4, 4, 4}, 0)
    };

    public StrokeComboBox() {
        this(new BasicStroke[]{new BasicStroke(1.0f), new BasicStroke(2.0f),
                    new BasicStroke(3.0f), new BasicStroke(4.0f),
                    new BasicStroke(5.0f), new BasicStroke(6.0f), new BasicStroke(7.0f),
                    new BasicStroke(8.0f)});

    }

    public StrokeComboBox(BasicStroke[] strokes) {
        setModel(new StrokeComboBoxModel(strokes));
        setRenderer(new StrokeComboBoxRenderer());
    }

    public BasicStroke getSelectedStroke() {
        if (getSelectedIndex() == -1) {
            return (BasicStroke) getModel().getElementAt(0);
        } else {
            return (BasicStroke) getSelectedItem();
        }
    }
}
