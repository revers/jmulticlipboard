package jmulticlip.gui.graph.components;

import java.awt.*;
import java.awt.geom.AffineTransform;
import javax.swing.*;
import jmulticlip.gui.graph.tools.ArrowedLine;

public class ArrowComboBoxRenderer extends JPanel implements ListCellRenderer {
    //private Color[] colors;

    private static final ArrowedLine DEFAULT_ARROW = new ArrowedLine();
    private ArrowedLine arrow;
    private int index = 0;

    public ArrowComboBoxRenderer() {
        //this.colors = colors;
        setOpaque(true);
        setPreferredSize(new Dimension(15, 15));
        // setHorizontalAlignment(JLabel.CENTER);
        // setVerticalAlignment(CENTER);
    }

    @Override
    public Component getListCellRendererComponent(JList list, Object value,
            int index, boolean isSelected, boolean cellHasFocus) {

        this.index = index;
        if (value == null) {
            arrow = DEFAULT_ARROW;
        } else {
            arrow = (ArrowedLine) value;
        }

        if (isSelected) {
            setBackground(list.getSelectionBackground());
            setForeground(list.getSelectionForeground());
        } else {
            setBackground(list.getBackground());
            setForeground(list.getForeground());
        }

        return this;
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);


        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        int yCenter = getHeight() / 2;
        int x1 = 5;
        int y1 = yCenter;
        int x2 = getWidth() - 5;
        int y2 = yCenter;
        
       arrow.draw(g2, x1, y1, x2, y2);
        

//        g2.drawLine(x1, y1, x2, y2);
//        switch (arrow.getType()) {
//            case LEADING:
//                g2.fillPolygon(new int[]{x2 - 4, x2 + 1, x2 - 4}, new int[]{y2 - 4, y2, y2 + 4}, 3);
//                break;
//            case TRAILING:
//                g2.fillPolygon(new int[]{x1 + 4, x1 - 1, x1 + 4}, new int[]{y1 - 4, y1, y1 + 4}, 3);
//                break;
//            case BOTH:
//                g2.fillPolygon(new int[]{x2 - 4, x2 + 1, x2 - 4}, new int[]{y2 - 4, y2, y2 + 4}, 3);
//                g2.fillPolygon(new int[]{x1 + 4, x1 - 1, x1 + 4}, new int[]{y1 - 4, y1, y1 + 4}, 3);
//                break;
//        }
    }
}
