package jmulticlip.gui.graph.components;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import jmulticlip.gui.graph.CommonProperties;
import jmulticlip.gui.graph.CommonProperties.Type;
import jmulticlip.gui.graph.CommonPropertyChangeListener;
import jmulticlip.gui.graph.GraphicsViewPanel;
import jmulticlip.util.GUIUtils;

/**
 *
 * @author Revers
 */
public class ColorPopupMenu extends JPopupMenu implements CommonPropertyChangeListener {

    public static final Dimension COLOR_MENU_ICON_SIZE = new Dimension(50, 15);
    public static final Dimension COLOR_BUTTON_ICON_SIZE = new Dimension(16, 16);
    private JButton colorButton;
    private Type type;

    public ColorPopupMenu(JButton colorButton, Type colorType) {
        this.colorButton = colorButton;
        this.type = colorType;
        initPopup();

        CommonProperties.getInstance().addPropertyChangeListener(this, colorType);
    }

    private void initPopup() {
        addColorMenuItem(Color.BLACK);
        addColorMenuItem(Color.red);
        addColorMenuItem(Color.blue);
        addColorMenuItem(Color.green);
        addColorMenuItem(Color.orange);
        addColorMenuItem(Color.CYAN);
        addColorMenuItem(Color.white);
        addColorMenuItem(Color.DARK_GRAY);

        JMenuItem moreColorsItem = new JMenuItem("More...");
        GUIUtils.setBoldFont(moreColorsItem);
        moreColorsItem.setBorder(new EmptyBorder(2, 8, 2, 0));
        add(moreColorsItem);

        moreColorsItem.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                // TODO: I18N
                Color color = JColorChooser.showDialog(ColorPopupMenu.this,
                        "Wybierz Kolor", CommonProperties.getInstance().getColor());

                if (color != null) {
                    setCommonPropertyColor(color);
                    colorButton.setIcon(GUIUtils.getColorIcon(color, COLOR_BUTTON_ICON_SIZE.width,
                            COLOR_BUTTON_ICON_SIZE.height, true));
                }
            }
        });
    }

    private void setCommonPropertyColor(Color color) {
        if (type == Type.COLOR) {
            CommonProperties.getInstance().setColor(color);
        } else if (type == Type.FILL_COLOR) {
            CommonProperties.getInstance().setFillColor(color);
        }
    }

    private void addColorMenuItem(Color color) {
        ComponentMenuItem colorMenuItem = new ComponentMenuItem(new JLabel(new ColorIcon(color,
                COLOR_MENU_ICON_SIZE.width,
                COLOR_MENU_ICON_SIZE.height,
                COLOR_BUTTON_ICON_SIZE.width,
                COLOR_BUTTON_ICON_SIZE.height)));

        colorMenuItem.addActionListener(colorMenuActionListener);
        add(colorMenuItem);
    }

    @Override
    public void propertyChanged(Object property, Type type) {
        colorButton.setIcon(GUIUtils.getColorIcon((Color) property, COLOR_BUTTON_ICON_SIZE.width,
                COLOR_BUTTON_ICON_SIZE.height, true));
    }
    private ActionListener colorMenuActionListener = new ActionListener() {

        @Override
        public void actionPerformed(ActionEvent e) {
            ComponentMenuItem cmi = (ComponentMenuItem) e.getSource();
            JLabel colorLabel = (JLabel) cmi.getMenuItemComponent();
            ColorIcon colorIcon = (ColorIcon) colorLabel.getIcon();

            setCommonPropertyColor(colorIcon.getColor());
            colorButton.setIcon(colorIcon.getSmallerIcon());
        }
    };
}
