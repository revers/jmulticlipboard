package jmulticlip.gui.graph.components;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.event.*;
import static java.awt.GridBagConstraints.*;

/**
 *
 * @author Revers
 */
public class FontComboBoxRenderer extends JPanel
        implements ListCellRenderer, IconObserver {

    private static final int MAX_WIDTH = 200;
    private static final int MAX_HEIGHT = 30;
    private JLabel textLabel = new JLabel();
    private JLabel iconLabel = new JLabel();
    private JLabel viewLabel = new JLabel();
 //   private JList list;

    public FontComboBoxRenderer() {
        setOpaque(true);
        //setHorizontalTextPosition(JLabel.LEFT);
        textLabel.setFont(getFont().deriveFont(15.0f));
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.anchor = WEST;
        c.insets = new Insets(0, 0, 0, 5);
        c.fill = NONE;
        c.weightx = 1.0;
        add(textLabel, c);
        
        c.gridx = 1;
        c.anchor = EAST;        
        add(iconLabel, c);
        
        setBorder(new EmptyBorder(0, 5, 0, 0));
    }

    public Component getListCellRendererComponent(JList list, Object value,
            int index, boolean isSelected, boolean cellHasFocus) {
      //  this.list = list;
        Icon icon = null;
        FontSample sample = null;
        if (index == -1) {
            sample = (FontSample) list.getModel().getElementAt(list.getSelectedIndex());
            viewLabel.setText(sample.toString());
           // setBackground(Color.white);
            return viewLabel;
        } else {
            sample = (FontSample) value;
            icon = sample.getSampleIcon(this, MAX_WIDTH, MAX_HEIGHT);
        }

        textLabel.setText(sample.toString());
        iconLabel.setIcon(icon);


        Color background;
        Color foreground;

        if (isSelected) {
            background = list.getSelectionBackground();//Color.blue;//Color.RED;
            foreground = list.getSelectionForeground();//Color.white;

            //      System.out.println(list.getSelectionForeground());
        } else {
            background = Color.WHITE;
            foreground = Color.BLACK;
        }
        setBackground(background);
        setForeground(foreground);
        textLabel.setForeground(foreground);

        return this;
    }

    @Override
    public void iconLoaded(Icon icon) {
       // if (list != null) {
        //    list.repaint();
      //  }
    }
}
