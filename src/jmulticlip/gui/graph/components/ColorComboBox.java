package jmulticlip.gui.graph.components;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class ColorComboBox extends JComboBox implements ActionListener {

    public ColorComboBox() {
        this(new MyColor[]{new MyColor(Color.BLACK), new MyColor(MyColor.red),
                    new MyColor(MyColor.blue), new MyColor(MyColor.green),
                    new MyColor(MyColor.orange), new MyColor(MyColor.CYAN),
                    new MyColor(MyColor.white), new MyColor(MyColor.DARK_GRAY)});

    }

    public ColorComboBox(MyColor[] colors) {
        setModel(new ColorComboBoxModel(colors));
        setRenderer(new ColorComboBoxRenderer());
        addActionListener(this);

        UIManager.put("MyColorChooser.cancelText", "Anuluj");
        UIManager.put("MyColorChooser.okText", "OK");
        UIManager.put("MyColorChooser.previewText", "Podgląd");
        UIManager.put("MyColorChooser.resetText", "Reset");
        UIManager.put("MyColorChooser.rgbBlueText", "Niebieski");
        UIManager.put("MyColorChooser.rgbGreenText", "Zielony");
        UIManager.put("MyColorChooser.rgbRedText", "Czerowny");
        UIManager.put("MyColorChooser.sampleText", "Przykładowy tekst");
        UIManager.put("MyColorChooser.swatchesNameText", "Próbki");
        UIManager.put("MyColorChooser.swatchesRecentText", "Ostatnie:");
    }

    public MyColor getSelectedColor() {
        if (getSelectedIndex() == -1) {
            return (MyColor) getModel().getElementAt(0);
        } else {
            return (MyColor) getSelectedItem();
        }
    }

    public void setColor(Color color) {
        ColorComboBoxModel model = (ColorComboBoxModel) getModel();

        for (int i = 0; i < model.getSize() - 2; i++) {
            if (((MyColor) model.getElementAt(i)).sameAs(color)) {
                setSelectedIndex(i);
                return;
            }
        }

        model.setColor(getModel().getSize() - 1, new MyColor(color));
        removeActionListener(this);
        setSelectedIndex(getModel().getSize() - 1);
        addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        int index = getModel().getSize() - 1;
       
        if (getSelectedIndex() == index) {
            MyColor color = (MyColor) getModel().getElementAt(index);
            Color standardColor = JColorChooser.showDialog(this, "Wybierz Kolor", color);

            if (standardColor != null) {
                color = new MyColor(standardColor);
                ((ColorComboBoxModel) getModel()).setColor(index, color);
                setSelectedIndex(index);
                for (ActionListener listener : getActionListeners()) {
                    if (listener != this) {
                        listener.actionPerformed(e);
                    }
                }
            }
        }
    }
}
