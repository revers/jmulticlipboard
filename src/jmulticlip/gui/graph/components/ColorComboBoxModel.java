package jmulticlip.gui.graph.components;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.ListDataListener;

public class ColorComboBoxModel extends DefaultComboBoxModel {

    private MyColor[] myColors;

    public ColorComboBoxModel(MyColor[] myColors) {
        this.myColors = myColors;
    }

    @Override
    public Object getElementAt(int index) {
        return myColors[index];
    }

    @Override
    public int getSize() {
        return myColors.length;
    }

    public void setColor(int index, MyColor MyColor) {
        myColors[index] = MyColor;
    }
}
