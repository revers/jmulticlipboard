package jmulticlip.gui.graph.components;

import java.awt.*;
import javax.swing.*;
import javax.swing.event.*;

/**
 *
 * @author Revers
 */
public class FontComboBox extends JComboBox {

    private int listMinimumWidth = 250;

    public FontComboBox() {
        //super(new String[] {"one", "two", "three" });
        setModel(new FontComboBoxModel());
        setRenderer(new FontComboBoxRenderer());



        // putClientProperty("JComponent.sizeVariant", "small");
        //setPreferredSize(new Dimension(150, 22));
        //  putClientProperty("JComboBox.isPopDown", Boolean.TRUE);

    }

    public int getListMinimumWidth() {
        return listMinimumWidth;
    }

    public void setListMinimumWidth(int listMinimumWidth) {
        this.listMinimumWidth = listMinimumWidth;
    }

    public int getFontIndex(String fontName) {
        FontComboBoxModel model = (FontComboBoxModel) getModel();
        int size = model.getSize();
        int index = 0;
        for (int i = 0; i < size; i++) {
            if (model.getElementAt(i).toString().equals(fontName)) {
                index = i;
                break;
            }
        }

        return index;
    }

    public Font getSelectedFont() {
        if (getSelectedIndex() == -1) {
            return ((FontSample) getModel().getElementAt(0)).getFont();
        } else {
            return ((FontSample) getSelectedItem()).getFont();
        }
    }
    private boolean layingOut = false;

    @Override
    public void doLayout() {
        try {
            layingOut = true;
            super.doLayout();
        } finally {
            layingOut = false;
        }
    }

    @Override
    public Dimension getSize() {
        Dimension dim = super.getSize();
        if (!layingOut) {
            dim.width = Math.max(dim.width, listMinimumWidth);
            dim.width = Math.min(dim.width, Toolkit.getDefaultToolkit().getScreenSize().width);

        }
        return dim;
    }
}
