package jmulticlip.gui.graph.components;

import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.event.*;
import jmulticlip.util.GUIUtils;

/**
 *
 * @author Revers
 */
public class ComponentMenuItem extends JPanel implements MenuElement {

    private static int eventId = 0;
    //private Color savedBackground;
    private static MenuElement[] NO_SUB_ELEMENTS = new MenuElement[0];
    private ArrayList<ActionListener> actionListenerList = new ArrayList<ActionListener>();
    private Color backgroundColor;
    private Color selectionColor;
    private Component menuItemComponent;
    private Alignment alignment;

    public static enum Alignment {

        LEFT, RIGHT, CENTER
    }

    public ComponentMenuItem(JComponent component) {
        this(component, Alignment.CENTER);
    }

    public ComponentMenuItem(JComponent component, Alignment alignment) {
        this.menuItemComponent = component;
        this.alignment = alignment;
        backgroundColor = GUIUtils.MENU_BACKGROUND_COLOR;
        selectionColor = GUIUtils.MENU_SELECTION_COLOR;

        setBackground(backgroundColor);
        setBorder(new EmptyBorder(3, 5, 3, 5));
        setLayout(new GridBagLayout());

        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.NONE;
        c.weightx = 1.0;

        switch (alignment) {
            case LEFT:
                c.anchor = GridBagConstraints.WEST;
                break;
            case CENTER:
                c.anchor = GridBagConstraints.CENTER;
                break;
            case RIGHT:
                c.anchor = GridBagConstraints.EAST;
                break;
        }

        add(component, c);

        init();
    }

    public Component getMenuItemComponent() {
        return menuItemComponent;
    }

    public Alignment getAlignment() {
        return alignment;
    }

    public Color getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(Color backgroundColor) {
        this.backgroundColor = backgroundColor;
        setBackground(backgroundColor);
    }

    public Color getSelectionColor() {
        return selectionColor;
    }

    public void setSelectionColor(Color selectionColor) {
        this.selectionColor = selectionColor;
    }

    public void addActionListener(ActionListener action) {
        actionListenerList.add(action);
    }

    public void removeActionListener(ActionListener action) {
        actionListenerList.remove(action);
    }

    public void removeAllActionListeners() {
        actionListenerList.clear();
    }

    private void init() {
        updateUI();
        setRequestFocusEnabled(false);
        // Borrows heavily from BasicMenuUI
        MouseInputListener mouseInputListener = new MouseInputListener() {
            // If mouse released over this menu item, activate it

            @Override
            public void mouseReleased(MouseEvent mouseEvent) {
                MenuSelectionManager menuSelectionManager = MenuSelectionManager.defaultManager();
                Point point = mouseEvent.getPoint();
                if ((point.x >= 0) && (point.x < getWidth()) && (point.y >= 0) && (point.y < getHeight())) {
                    menuSelectionManager.clearSelectedPath();
                    // Component automatically handles "selection" at this point
                    // doClick(0); // not necessary
                } else {
                    menuSelectionManager.processMouseEvent(mouseEvent);
                }
            }

            // If mouse moves over menu item, add to selection path, so it becomes
            // armed
            @Override
            public void mouseEntered(MouseEvent mouseEvent) {
                MenuSelectionManager menuSelectionManager = MenuSelectionManager.defaultManager();
                menuSelectionManager.setSelectedPath(getPath());
            }

            // When mouse moves away from menu item, disarm it and select something
            // else
            @Override
            public void mouseExited(MouseEvent mouseEvent) {
                MenuSelectionManager menuSelectionManager = MenuSelectionManager.defaultManager();
                MenuElement path[] = menuSelectionManager.getSelectedPath();
                if (path.length > 1) {
                    MenuElement newPath[] = new MenuElement[path.length - 1];
                    for (int i = 0, c = path.length - 1; i < c; i++) {
                        newPath[i] = path[i];
                    }
                    menuSelectionManager.setSelectedPath(newPath);
                }
            }

            // Pass along drag events
            @Override
            public void mouseDragged(MouseEvent mouseEvent) {
                MenuSelectionManager.defaultManager().processMouseEvent(mouseEvent);
            }

            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
            }

            @Override
            public void mousePressed(MouseEvent mouseEvent) {
            }

            @Override
            public void mouseMoved(MouseEvent mouseEvent) {
            }
        };
        addMouseListener(mouseInputListener);
        addMouseMotionListener(mouseInputListener);
    }

    // MenuElement methods
    @Override
    public Component getComponent() {
        return this;
    }

    @Override
    public MenuElement[] getSubElements() {
        // No subelements
        return NO_SUB_ELEMENTS;
    }

    @Override
    public void menuSelectionChanged(boolean isIncluded) {

        if (isIncluded) {
//            savedBackground = getBackground();
//            if (!savedBackground.equals(Color.BLUE)) {
//                setBackground(Color.BLUE);
//            } else {
//                // In case Background blue, use something different
//                setBackground(Color.RED);
//            }
            setBackground(selectionColor);
        } else {
            setBackground(backgroundColor);
            //setBackground(savedBackground);
            updateUI();
            // If null, get Background from installed look and feel
//            if (savedBackground == null) {
//                updateUI();
//            }
        }
    }

    @Override
    public void processKeyEvent(KeyEvent keyEvent, MenuElement path[], MenuSelectionManager manager) {
        // If user presses space while menu item armed, select it
//        if (getModel().isArmed()) {
//            int keyChar = keyEvent.getKeyChar();
//            if (keyChar == KeyEvent.VK_SPACE) {
//                manager.clearSelectedPath();
//                System.out.println("Selected: JToggleButtonMenuItem, by KeyEvent");
//                doClick(0); // inherited from AbstractButton
//            }
//        }
    }

    @Override
    public void processMouseEvent(MouseEvent mouseEvent, MenuElement path[],
            MenuSelectionManager manager) {
        // For when mouse dragged over menu and button released
        if (mouseEvent.getID() == MouseEvent.MOUSE_RELEASED) {
            manager.clearSelectedPath();
            //       System.out.println("JPanelMenuItem.actionPerformed");

            ActionEvent event = new ActionEvent(this, (eventId++), "actionPerformed");
            for (ActionListener action : actionListenerList) {
                action.actionPerformed(event);
            }
            //  doClick(0); // inherited from AbstractButton
        }
    }

    // Borrows heavily from BasicMenuItemUI.getPath()
    private MenuElement[] getPath() {
        MenuSelectionManager menuSelectionManager = MenuSelectionManager.defaultManager();
        MenuElement oldPath[] = menuSelectionManager.getSelectedPath();
        MenuElement newPath[];
        int oldPathLength = oldPath.length;
        if (oldPathLength == 0) {
            return new MenuElement[0];
        }
        Component parent = getParent();
        if (oldPath[oldPathLength - 1].getComponent() == parent) {
            // Going deeper under the parent menu
            newPath = new MenuElement[oldPathLength + 1];
            System.arraycopy(oldPath, 0, newPath, 0, oldPathLength);
            newPath[oldPathLength] = this;
        } else {
            // Sibling/child menu item currently selected
            int newPathPosition;
            for (newPathPosition = oldPath.length - 1; newPathPosition >= 0; newPathPosition--) {
                if (oldPath[newPathPosition].getComponent() == parent) {
                    break;
                }
            }
            newPath = new MenuElement[newPathPosition + 2];
            System.arraycopy(oldPath, 0, newPath, 0, newPathPosition + 1);
            newPath[newPathPosition + 1] = this;
        }
        return newPath;
    }
}
