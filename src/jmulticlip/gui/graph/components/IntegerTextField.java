package jmulticlip.gui.graph.components;

import java.awt.Container;
import java.awt.Graphics;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.PlainDocument;

/**
 *
 * @author Revers
 */
public class IntegerTextField extends JTextField {

    private boolean acceptOnlyNonNegative;

    public IntegerTextField() {
    }

    public IntegerTextField(int defaultValue) {
        super(Integer.toString(defaultValue));
    }

    public boolean isAcceptOnlyNonNegative() {
        return acceptOnlyNonNegative;
    }

    public void setAcceptOnlyNonNegative(boolean acceptOnlyNonNegative) {
        this.acceptOnlyNonNegative = acceptOnlyNonNegative;
    }

    @Override
    protected Document createDefaultModel() {
        return new IntTextDocument();
    }

    public boolean isTextValid() {
        try {
            Integer.parseInt(getText());
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public int getValue() {
        try {
            return Integer.parseInt(getText());
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    class IntTextDocument extends PlainDocument {

        @Override
        public void insertString(int offs, String str, AttributeSet a)
                throws BadLocationException {
            if (str == null) {
                return;
            }
            String oldString = getText(0, getLength());
            String newString = oldString.substring(0, offs) + str
                    + oldString.substring(offs);
            try {
                int i = Integer.parseInt(newString + "0");
                if (acceptOnlyNonNegative == false) {
                    super.insertString(offs, str, a);
                } else if (i >= 0) {
                    super.insertString(offs, str, a);
                }

            } catch (NumberFormatException e) {
            }
        }
    }
}
