package jmulticlip.gui.graph.components;

import javax.swing.Icon;

/**
 *
 * @author Revers
 */
public interface IconObserver {
    public void iconLoaded(Icon icon);
}
