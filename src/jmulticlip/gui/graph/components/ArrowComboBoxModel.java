package jmulticlip.gui.graph.components;

import java.awt.*;
import javax.swing.*;
import jmulticlip.gui.graph.tools.ArrowedLine;

public class ArrowComboBoxModel extends DefaultComboBoxModel {

    private ArrowedLine[] arrows;

    public ArrowComboBoxModel(ArrowedLine[] arrows) {
        this.arrows = arrows;
    }

    @Override
    public Object getElementAt(int index) {
        return arrows[index];
    }

    @Override
    public int getSize() {
        return arrows.length;
    }
}
