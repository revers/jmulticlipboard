/*
 * CanvasPanel.java
 *
 * Created on 2010-09-03, 22:20:26
 */
package jmulticlip.gui.graph;

import jmulticlip.gui.graph.util.UndoableList;
import jmulticlip.gui.graph.tools.painter.AbstractPainter;
import jmulticlip.gui.graph.tools.action.CanvasAction;
import jmulticlip.gui.graph.tools.action.CanvasActionList;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import javax.swing.*;
import javax.swing.event.*;
import java.util.*;
import jmulticlip.gui.graph.tools.*;

public class CanvasPanel extends javax.swing.JPanel implements Scrollable {

//    public enum Mode {
//
//        SELECTION, EDIT
//    }
    private UndoableList<AbstractPainter> painterList = new UndoableList<AbstractPainter>();
    private CanvasActionList<CanvasAction> actionList = new CanvasActionList<CanvasAction>(this);
    private AbstractPainter currentPainter;
    private ArrayList<CanvasListener> canvasListeners = new ArrayList<CanvasListener>();
    private BufferedImage canvasImage;
    private Image backgroundImage;
    private int minPainterIndex;
    private float scaleFactor = 1.0f;
    private EditFrame editFrame;
    //  private SelectionPainterTool selectionTool;
    //private boolean currentPainterRendered;
    private SelectionFrame selectionFrame;
    private boolean editCurrent = true;
    //   private OLDRectangleMonitor rectMonitor;
    // private Mode mode = Mode.EDIT;

    public CanvasPanel() {
        this(new BufferedImage(1, 1, BufferedImage.TYPE_INT_RGB));
    }

    public CanvasPanel(Image backgroundImage) {
        this.backgroundImage = backgroundImage;
        initComponents();

        fitSize();
        canvasImage = new BufferedImage(backgroundImage.getWidth(null), backgroundImage.getHeight(null), BufferedImage.TYPE_INT_RGB);

        renderCanvasImage();

        editFrame = new EditFrame(this);
        editFrame.start();
    }

    public boolean isEditCurrent() {
        return editCurrent;
    }

    public void setEditCurrent(boolean editCurrent) {
        this.editCurrent = editCurrent;
        editFrame.setEnabled(editCurrent);
    }

    public void setZoom(float scaleFactor) {
        this.scaleFactor = scaleFactor;
        if (scaleFactor == 1.0f) {
            Dimension dim = new Dimension(canvasImage.getWidth(), canvasImage.getHeight());
            setPreferredSize(dim);
            setSize(dim);
        } else {
            int newW = (int) (canvasImage.getWidth() * scaleFactor);
            int newH = (int) (canvasImage.getHeight() * scaleFactor);

            Dimension scaledSize = new Dimension(newW, newH);
            setPreferredSize(scaledSize);
            setSize(scaledSize);
        }

        editFrame.updateBounds();

        repaint();
    }

    public float getZoom() {
        return scaleFactor;
    }

    private void fitSize() {
        Dimension dim = new Dimension(backgroundImage.getWidth(null),
                backgroundImage.getHeight(null));

        setPreferredSize(dim);
        setSize(dim);
    }

    public Image getBackgroundImage() {
        return backgroundImage;
    }

    public void setBackgroundImage(Image backgroundImage, int minPainterIndex) {
        this.minPainterIndex = minPainterIndex;
        setBackgroundImage(backgroundImage);
    }

    public void setBackgroundImage(Image backgroundImage) {
        this.backgroundImage = backgroundImage;
        fitSize();
        canvasImage = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_INT_RGB);        
        renderCanvasImage();
    }

    public int getMinPainterIndex() {
        return minPainterIndex;
    }

    public void setMinPainterIndex(int minPainterIndex) {
        this.minPainterIndex = minPainterIndex;
    }

    public boolean undoPainter() {

        boolean success = painterList.undoAdd();
        setCurrentPainter(painterList.getLastElement());

        if (success) {
            repaint();
        }
        return success;
    }

    public boolean redoPainter() {
        boolean success = painterList.redoAdd();
        setCurrentPainter(painterList.getLastElement());
        if (success) {

            repaint();
        }
        return success;
    }

    public void addAction(CanvasAction action) {
        actionList.add(action);

        for (CanvasListener listener : canvasListeners) {
            listener.canvasChanged(this);
        }
    }

    public int getPainterCount() {
        return painterList.getSize();
    }

    public CanvasActionList getCanvasActionList() {
        return actionList;
    }

    public BufferedImage getCanvasImage() {
        return canvasImage;
    }

    public CanvasListener[] getCanvasListeners() {
        return canvasListeners.toArray(new CanvasListener[]{});
    }

    public void addCanvasListener(CanvasListener canvasListener) {
        canvasListeners.add(canvasListener);
    }

    public void removeCanvasListener(CanvasListener canvasListener) {
        canvasListeners.remove(canvasListener);
    }

    @Override
    protected void paintComponent(Graphics g) {
        renderCanvasImage();

        if (currentPainter != painterList.getLastElement()) {
            renderCurrentPainter();
        }

        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        if (scaleFactor == 1.0f) {
            g2.drawImage(canvasImage, 0, 0, null);
        } else {
            g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
                    RenderingHints.VALUE_INTERPOLATION_BILINEAR);
            g2.drawImage(canvasImage, 0, 0, getWidth(), getHeight(), null);
        }

        if (selectionFrame != null) {
            selectionFrame.draw(g2);
        } else {
            editFrame.draw(g2);
        }
    }

    public AbstractPainter getCurrentPainter() {
        return currentPainter;
    }

    public void setCurrentPainter(AbstractPainter currentPainter) {
        this.currentPainter = currentPainter;

        for (CanvasListener canvasListener : canvasListeners) {
            canvasListener.canvasChanged(this);
        }

        editFrame.setPainter(currentPainter);
    }

    public void setLastPainterCurrent() {
        setCurrentPainter(painterList.getLastElement());
    }

    private void renderCanvasImage() {
        Graphics2D g2 = (Graphics2D) canvasImage.createGraphics();
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);

        g2.drawImage(backgroundImage, 0, 0, null);
        for (int i = minPainterIndex; i < painterList.getSize(); i++) {
            AbstractPainter painter = painterList.get(i);
            if (painter.isVisible()) {
                painter.draw(g2);
            }
        }

        g2.dispose();

//        currentPainterRendered = false;
    }

    private void renderCurrentPainter() {
        if (currentPainter == null) {
            return;
        }
        //  currentPainterRendered = true;
        Graphics2D g2 = (Graphics2D) canvasImage.createGraphics();
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);


        if (currentPainter.isVisible()) {
            currentPainter.draw(g2);
        }
        g2.dispose();
    }

    public void setSelectionFrame(SelectionFrame selectionFrame) {
        this.selectionFrame = selectionFrame;
    }

    public SelectionFrame getSelectionFrame() {
        return selectionFrame;
    }

    public void saveCurrentPainter() {

        if (currentPainter != null) {

            if (currentPainter != painterList.getLastElement()) {
                painterList.add(currentPainter);
            }

            for (CanvasListener canvasListener : canvasListeners) {
                canvasListener.canvasChanged(this);
            }

            editFrame.setPainter(null);
            currentPainter = null;
        }
    }

    public void abortCurrentPainter() {
        setCurrentPainter(null);
    }

    public boolean undo() {
        return actionList.undoAdd();
    }

    public boolean redo() {
        return actionList.redoAdd();
    }

    public boolean canUndo() {
        return actionList.canUndo();
    }

    public boolean canRedo() {
        return actionList.canRedo();
    }

    public Point getNotZoomedPoint(Point p) {
        if (scaleFactor == 1.0f) {
            return p;
        }
        p.x = (int) (p.x / scaleFactor);
        p.y = (int) (p.y / scaleFactor);
        return p;
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    public Dimension getPreferredScrollableViewportSize() {
        return getPreferredSize();
    }

    public int getScrollableUnitIncrement(Rectangle visibleRect, int orientation, int direction) {
        return 5;
    }

    public int getScrollableBlockIncrement(Rectangle visibleRect, int orientation, int direction) {
        return 20;
    }

    public boolean getScrollableTracksViewportWidth() {
        return false;
    }

    public boolean getScrollableTracksViewportHeight() {
        return false;
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
