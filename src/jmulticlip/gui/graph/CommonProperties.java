package jmulticlip.gui.graph;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Stroke;
import java.util.*;
import jmulticlip.gui.graph.components.StrokeComboBox;

/**
 *
 * @author Revers
 */
public class CommonProperties {

    public static enum Type {

        COLOR, FILL, FILL_COLOR, LINE_WIDTH, DASH_STROKE
    }
    private EnumMap<Type, List<CommonPropertyChangeListener>> listenerMap =
            new EnumMap<Type, List<CommonPropertyChangeListener>>(Type.class);
    // properties:
    private Color color = Color.black;
    private Color fillColor = Color.white;
    private boolean fill = false;
    private float lineWidth = 1.0f;
    private BasicStroke dashStroke = StrokeComboBox.DASH_STROKES[0];

    private CommonProperties() {
    }

    public static CommonProperties getInstance() {
        return CommonPropertiesHolder.INSTANCE;
    }

    private static class CommonPropertiesHolder {

        private static final CommonProperties INSTANCE = new CommonProperties();
    }

    public BasicStroke getDashStroke() {
        return dashStroke;
    }

    public void setDashStroke(BasicStroke dashStroke) {
        if (this.dashStroke.equals(dashStroke)) {
            return;
        }
        this.dashStroke = dashStroke;
        firePropertyChanged(dashStroke, Type.DASH_STROKE);
    }

    public boolean isFill() {
        return fill;
    }

    public void setFill(boolean fill) {
        if (this.fill == fill) {
            return;
        }
        this.fill = fill;
        firePropertyChanged((Boolean) fill, Type.FILL);
    }

    public float getLineWidth() {
        return lineWidth;
    }

    public void setLineWidth(float lineWidth) {
        if (this.lineWidth == lineWidth) {
            return;
        }
        this.lineWidth = lineWidth;
        firePropertyChanged((Float) lineWidth, Type.LINE_WIDTH);
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        if (this.color.equals(color)) {
            return;
        }
        this.color = color;
        firePropertyChanged(color, Type.COLOR);
    }

    public Color getFillColor() {
        return fillColor;
    }

    public void setFillColor(Color fillColor) {
        if (this.fillColor.equals(fillColor)) {
            return;
        }
        this.fillColor = fillColor;
        firePropertyChanged(fillColor, Type.FILL_COLOR);
    }

    protected void firePropertyChanged(Object property, Type type) {
        if (listenerMap.containsKey(type) == false) {
            return;
        }

        List<CommonPropertyChangeListener> list = listenerMap.get(type);
        for (CommonPropertyChangeListener listener : list) {
            listener.propertyChanged(property, type);
        }
    }

    public void removeAllPropertyChangeListeners() {
        listenerMap.clear();
    }

    public void removePropertyChangeListener(CommonPropertyChangeListener listener) {
        for (Type type : Type.values()) {
            removeListener(listener, type);
        }
    }

    public void removePropertyChangeListener(CommonPropertyChangeListener listener,
            CommonProperties.Type... types) {

        for (Type type : types) {
            removeListener(listener, type);
        }
    }

    private void removeListener(CommonPropertyChangeListener listener, Type type) {
        if (listenerMap.containsKey(type) == false) {
            return;
        }

        List<CommonPropertyChangeListener> list = listenerMap.get(type);
        list.remove(listener);
    }

    /*
     * Blocks using addPropertyChangeListener without giving any type:
     */
    private void addPropertyChangeListener(CommonPropertyChangeListener listener) {
    }

    public void addPropertyChangeListener(CommonPropertyChangeListener listener,
            CommonProperties.Type... types) {

        for (Type type : types) {
            addListener(listener, type);
        }
    }

    private void addListener(CommonPropertyChangeListener listener, Type type) {
        if (listenerMap.containsKey(type) == false) {
            List<CommonPropertyChangeListener> list = new LinkedList<CommonPropertyChangeListener>();
            list.add(listener);
            listenerMap.put(type, list);
        } else {
            List<CommonPropertyChangeListener> list = listenerMap.get(type);
            list.add(listener);
        }
    }
}
