package jmulticlip.gui;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Rectangle2D;

import javax.swing.*;
import javax.swing.border.*;
import jmulticlip.util.GUIUtils;

/**
 *
 * @author Revers
 */
public class RectBorder implements Border {

    private static final int OFFSET = 3;
    private static final Insets INSETS = new Insets(OFFSET, OFFSET, OFFSET, OFFSET);
    private static final int ARC_LENGTH = 6;
    private Color color;

    public RectBorder() {
        this(Color.black);
    }

    public RectBorder(Color color) {
        this.color = color;
    }

    @Override
    public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        g.setColor(color);
        g.drawRoundRect(x, y, width - 1, height - 1, ARC_LENGTH, ARC_LENGTH);
    }

    @Override
    public Insets getBorderInsets(Component c) {
        return INSETS;
    }

    @Override
    public boolean isBorderOpaque() {
        return true;
    }
}
