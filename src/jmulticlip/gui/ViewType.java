package jmulticlip.gui;

/**
 *
 * @author Revers
 */
public enum ViewType {
    TEXT_EDITOR, IMAGE_EDITOR, LOADING_SCREEN
}
