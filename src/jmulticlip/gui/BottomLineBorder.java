package jmulticlip.gui;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Rectangle2D;

import javax.swing.*;
import javax.swing.border.*;
import jmulticlip.util.GUIUtils;

/**
 *
 * @author Revers
 */
public class BottomLineBorder implements Border {

    private static final int OFFSET = 5;
    private static final Insets INSETS = new Insets(0, 0, OFFSET, 0);
    //  private static final int ARC_LENGTH = 6;
    private Color color;
    private BasicStroke stroke = new BasicStroke();

    public BottomLineBorder() {
        this(Color.black);
    }

    public BottomLineBorder(Color color) {
        this.color = color;
    }

    public int getLineWidth() {
        return (int) stroke.getLineWidth();
    }

    public void setLineWidth(int lineWidth) {
        stroke = new BasicStroke((float) lineWidth);
    }

    @Override
    public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setColor(color);
        Stroke oldStroke = g2.getStroke();
        g2.setStroke(stroke);
        g2.drawLine(0, height - 1, width, height - 1);
        g2.setStroke(oldStroke);
        //g.drawRoundRect(x, y, width - 1, height - 1, ARC_LENGTH, ARC_LENGTH);
    }

    @Override
    public Insets getBorderInsets(Component c) {
        return INSETS;
    }

    @Override
    public boolean isBorderOpaque() {
        return true;
    }
}
