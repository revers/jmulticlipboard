/*
 * TextViewToolbarPanel.java
 *
 * Created on 2011-07-30, 20:17:16
 */
package jmulticlip.gui.text;

import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;


import javax.swing.*;

import javax.swing.border.*;
import javax.swing.event.*;
import jmulticlip.clipboard.ClipDataType;
import jmulticlip.clipboard.ClipboardManager;
import jmulticlip.config.Options;
import jmulticlip.core.ErrorManager;
import jmulticlip.core.InitializationManager;
import jmulticlip.gui.LoadingViewPanel;
import jmulticlip.gui.MainFrame;
import jmulticlip.gui.StatusbarPanel;
import jmulticlip.gui.View;
import jmulticlip.gui.ViewType;
import jmulticlip.gui.navig.ClipDataRow;
import jmulticlip.gui.navig.NavigTablePanel;
import jmulticlip.gui.navig.scripts.ScriptsPanel;
import jmulticlip.gui.shortcut.*;
import jmulticlip.i18n.I18NManager;
import jmulticlip.ifaces.Initializable;
import jmulticlip.ifaces.NotificationListener;
import jmulticlip.ifaces.Resetable;
import jmulticlip.scripts.Script;
import jmulticlip.scripts.impl.dummy.FileScript;
import jmulticlip.util.GUIUtils;
import jmulticlip.util.ImageUploader;
import jmulticlip.util.WavePlayer;
import jmulticlip.util.WklejOrgUploader;
import pl.ugu.revers.logging.i18n.Internationalizable;

/**
 *
 * @author Revers
 */
public class TextViewToolbarPanel extends ShortcutPanel implements Internationalizable,
        Initializable, ChangeListener {

    private I18NManager i18n = I18NManager.getInstance();
    private boolean lastSelected;

    /** Creates new form TextViewToolbarPanel */
    public TextViewToolbarPanel() {
        initComponents();
        i18n.addInternationalizable(this);

        InitializationManager.getInstance().addInitializable(this);

        //  lastSelected = !copyAsFileCB.isSelected();
        copyAsFileCB.addChangeListener(this);

    }

    public void setCopyAsFilesSelected(boolean selected) {

        copyAsFileCB.setSelected(selected);
    }

    public boolean isCopyAsFilesSelected() {
        return copyAsFileCB.isSelected();
    }

    @Override
    public void init() {
        addShortcuts();
    }

    @Override
    public void stateChanged(ChangeEvent e) {
        if (lastSelected == copyAsFileCB.isSelected()) {
            return;
        }

        lastSelected = copyAsFileCB.isSelected();

        if (lastSelected) {
            copyAsFileCB.setFont(GUIUtils.SELECTED_CB_FONT);
        } else {
            copyAsFileCB.setFont(GUIUtils.NORMAL_CB_FONT);
        }

        MainFrame mf = MainFrame.getInstance();
        StatusbarPanel status = mf.getStatusbarPanel();

        ScriptsPanel scriptsPanel = mf.getNavigationPanel().getScriptsPanel();
        scriptsPanel.setEnabled(!lastSelected);

        if (lastSelected) {
            status.addScript(FileScript.INSTANCE);
        }

        repaint();
    }

    private void addShortcuts() {
        MainFrame mf = MainFrame.getInstance();
        ShortcutManager shortcutMgr = ShortcutManager.getInstance();
        JComponent parent = mf.getDefaultTextView();

        KeyStroke findKS = KeyStroke.getKeyStroke(KeyEvent.VK_F, KeyEvent.CTRL_MASK, true);
        AbstractShortcutAction find = new AbstractShortcutAction(Location.MAIN_WINDOW_EDITOR_TEXT,
                parent, ShortcutScopeEnum.LOCATION_SCOPE, "textFind", findKS) {

            public void actionPerformed(ActionEvent e) {
                System.out.println("textEditor.textFind");
            }
        };

        shortcutMgr.registerShortcutAction(find);
        //------------------------------------------------------
        KeyStroke uploadKS = KeyStroke.getKeyStroke(KeyEvent.VK_U, KeyEvent.CTRL_MASK, true);
        AbstractShortcutAction upload = new AbstractShortcutAction(Location.MAIN_WINDOW_EDITOR_TEXT,
                parent, ShortcutScopeEnum.LOCATION_SCOPE, "textUpload", uploadKS) {

            public void actionPerformed(ActionEvent e) {
                uploadJBActionPerformed(e);
            }
        };

        shortcutMgr.registerShortcutAction(upload);
        //------------------------------------------------------
        KeyStroke asFileKS = KeyStroke.getKeyStroke(KeyEvent.VK_F, KeyEvent.CTRL_MASK | KeyEvent.SHIFT_MASK, true);
        AbstractShortcutAction asFile = new AbstractShortcutAction(Location.MAIN_WINDOW_EDITOR_TEXT,
                parent, ShortcutScopeEnum.WINDOW_SCOPE, "textAsFile", asFileKS) {

            public void actionPerformed(ActionEvent e) {
                if (MainFrame.getInstance().getViewContainer().getView().getType() != ViewType.TEXT_EDITOR) {
                    return;
                }

                setCopyAsFilesSelected(!isCopyAsFilesSelected());
            }
        };

        shortcutMgr.registerShortcutAction(asFile);
        //------------------------------------------------------

        KeyStroke pasteServerKS = KeyStroke.getKeyStroke(KeyEvent.VK_S, KeyEvent.CTRL_MASK | KeyEvent.SHIFT_MASK, true);
        AbstractShortcutAction pasteServer = new AbstractShortcutAction(Location.MAIN_WINDOW_EDITOR_TEXT,
                parent, ShortcutScopeEnum.LOCATION_SCOPE, "textPasteServer", pasteServerKS) {

            public void actionPerformed(ActionEvent e) {
                System.out.println("textEditor.pasteServer");
            }
        };

        shortcutMgr.registerShortcutAction(pasteServer);
        //------------------------------------------------------
//
//        KeyStroke saveKS = KeyStroke.getKeyStroke(KeyEvent.VK_S, KeyEvent.CTRL_MASK, true);
//        AbstractShortcutAction save = new AbstractShortcutAction(Location.MAIN_WINDOW_EDITOR_TEXT,
//                parent, ShortcutScopeEnum.LOCATION_SCOPE, "textSave", saveKS) {
//
//            public void actionPerformed(ActionEvent e) {
//                System.out.println("textEditor.textSave");
//            }
//        };
//
//        shortcutMgr.registerShortcutAction(save);
        //------------------------------------------------------
        KeyStroke googleKS = KeyStroke.getKeyStroke(KeyEvent.VK_G, KeyEvent.CTRL_MASK, true);
        AbstractShortcutAction google = new AbstractShortcutAction(Location.MAIN_WINDOW_EDITOR_TEXT,
                parent, ShortcutScopeEnum.WINDOW_SCOPE, "textGoogle", googleKS) {

            public void actionPerformed(ActionEvent e) {
                googleJBActionPerformed(e);
            }
        };

        shortcutMgr.registerShortcutAction(google);
        //------------------------------------------------------
        KeyStroke webKS = KeyStroke.getKeyStroke(KeyEvent.VK_W, KeyEvent.CTRL_MASK, true);
        AbstractShortcutAction web = new AbstractShortcutAction(Location.MAIN_WINDOW_EDITOR_TEXT,
                parent, ShortcutScopeEnum.WINDOW_SCOPE, "textWeb", webKS) {

            public void actionPerformed(ActionEvent e) {
                webJBActionPerformed(e);
            }
        };

        shortcutMgr.registerShortcutAction(web);
        //------------------------------------------------------

        ShortcutLabel findLabel = new ShortcutLabel(find, findJB);
        findLabel.setTextOffsetY(GUIUtils.SHORTCUT_Y_OFFSET);
        addShortcutLabel(findLabel);

        ShortcutLabel uploadLabel = new ShortcutLabel(upload, uploadJB);
        uploadLabel.setTextOffsetY(GUIUtils.SHORTCUT_Y_OFFSET);
        addShortcutLabel(uploadLabel);

        ShortcutLabel asFileLabel = new ShortcutLabel(asFile, copyAsFileCB);
        asFileLabel.setTextOffsetY(GUIUtils.SHORTCUT_Y_OFFSET);
        addShortcutLabel(asFileLabel);

//        ShortcutLabel saveLabel = new ShortcutLabel(save, saveJB);
//        saveLabel.setTextOffsetY(GUIUtils.SHORTCUT_Y_OFFSET);
//        addShortcutLabel(saveLabel);

        ShortcutLabel pasteServerLabel = new ShortcutLabel(pasteServer, pasteServerCB);
        pasteServerLabel.setTextOffsetY(GUIUtils.SHORTCUT_Y_OFFSET);
        addShortcutLabel(pasteServerLabel);

        ShortcutLabel googleLabel = new ShortcutLabel(google, googleJB);
        googleLabel.setTextOffsetY(GUIUtils.SHORTCUT_Y_OFFSET);
        addShortcutLabel(googleLabel);

        ShortcutLabel webLabel = new ShortcutLabel(web, webJB);
        webLabel.setTextOffsetY(GUIUtils.SHORTCUT_Y_OFFSET);
        addShortcutLabel(webLabel);
    }

    @Override
    public void changeLanguage(Locale language) {
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        pasteServerCB = new javax.swing.JComboBox();
        uploadJB = new javax.swing.JButton();
        separator1 = new javax.swing.JSeparator();
        findJB = new javax.swing.JButton();
        copyAsFileCB = new javax.swing.JCheckBox();
        googleJB = new javax.swing.JButton();
        webJB = new javax.swing.JButton();

        setLayout(new java.awt.GridBagLayout());

        pasteServerCB.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "wklej.org", "pastebin.com" }));
        pasteServerCB.setMaximumSize(new java.awt.Dimension(90, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(16, -4, 0, 0);
        add(pasteServerCB, gridBagConstraints);

        uploadJB.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/upload2.png"))); // NOI18N
        uploadJB.setMaximumSize(new java.awt.Dimension(49, 20));
        uploadJB.setMinimumSize(new java.awt.Dimension(49, 20));
        uploadJB.setPreferredSize(new java.awt.Dimension(44, 22));
        uploadJB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                uploadJBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(16, 5, 0, 0);
        add(uploadJB, gridBagConstraints);

        separator1.setOrientation(javax.swing.SwingConstants.VERTICAL);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(16, 5, 0, 0);
        add(separator1, gridBagConstraints);

        findJB.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/find16.png"))); // NOI18N
        findJB.setMaximumSize(new java.awt.Dimension(49, 20));
        findJB.setMinimumSize(new java.awt.Dimension(49, 20));
        findJB.setPreferredSize(new java.awt.Dimension(44, 22));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(16, 5, 0, 0);
        add(findJB, gridBagConstraints);

        copyAsFileCB.setText("Copy As File(s)");
        copyAsFileCB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                copyAsFileCBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(16, 5, 0, 0);
        add(copyAsFileCB, gridBagConstraints);

        googleJB.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/google16.png"))); // NOI18N
        googleJB.setMaximumSize(new java.awt.Dimension(49, 20));
        googleJB.setMinimumSize(new java.awt.Dimension(49, 20));
        googleJB.setPreferredSize(new java.awt.Dimension(44, 22));
        googleJB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                googleJBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.SOUTH;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(16, 5, 0, 0);
        add(googleJB, gridBagConstraints);

        webJB.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/www2-16.png"))); // NOI18N
        webJB.setMaximumSize(new java.awt.Dimension(49, 20));
        webJB.setMinimumSize(new java.awt.Dimension(49, 20));
        webJB.setPreferredSize(new java.awt.Dimension(44, 22));
        webJB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                webJBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 7;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.SOUTH;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(16, 5, 0, 0);
        add(webJB, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void copyAsFileCBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_copyAsFileCBActionPerformed
    }//GEN-LAST:event_copyAsFileCBActionPerformed

    private class UploadAction implements Runnable, NotificationListener {

        private ClipDataRow clipDataRow;
        private WklejOrgUploader wklejOrgUploader;
        private LoadingViewPanel loadingPanel;

        public UploadAction(ClipDataRow clipDataRow, WklejOrgUploader wklejOrgUploader) {
            this.clipDataRow = clipDataRow;
            this.wklejOrgUploader = wklejOrgUploader;

            loadingPanel = new LoadingViewPanel(this);
            loadingPanel.getLabel().setText("Trwa wysyłanie tekstu '" + clipDataRow.getShortText() + "' na Wklej.org...");
            clipDataRow.setCurrentView(loadingPanel);

            MainFrame.getInstance().refreshCurrentView();
        }

        public void run() {
            wklejOrgUploader.cancel();
            restoreView();
        }

        public void notify(String msg) {
            loadingPanel.getLabel().setText("Trwa wysyłanie tekstu "
                    + clipDataRow.getShortText() + " (" + msg + ")  na Wklej.org...");
        }

        public void restoreView() {
            clipDataRow.setCurrentView(clipDataRow.getDefaultView());
            MainFrame.getInstance().refreshCurrentView();
        }
    }

    private void uploadJBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_uploadJBActionPerformed
        final MainFrame mf = MainFrame.getInstance();
        mf.setBlockShortcuts(true);
        final NavigTablePanel navigTablePanel = mf.getNavigationPanel().getNavigTablePanel();


        final String text = mf.getDefaultTextView().getTextPane().getText();

        final WklejOrgUploader uploader = new WklejOrgUploader();

        WklejOrgUploader.Language[] languages = WklejOrgUploader.Language.values();

        final WklejOrgUploader.Language lang = (WklejOrgUploader.Language) JOptionPane.showInputDialog(
                mf,
                "Choose language:",
                "Language highlighting",
                JOptionPane.PLAIN_MESSAGE,
                null,
                languages,
                WklejOrgUploader.Language.TEXT);

        final ClipDataRow clipData = navigTablePanel.getCurrentClipDataRow();
        final UploadAction action = new UploadAction(clipData, uploader);

        new Thread() {

            @Override
            public void run() {
                try {

                    URL url = uploader.upload(text, lang);
                    ClipboardManager.getInstance().setClipbardText(url.toString());

                    if (Options.getInstance().getSoundAfterUpload()) {
                        WavePlayer.beep();
                    }

                    action.restoreView();
                } catch (IOException ex) {

                    action.restoreView();
                    ErrorManager.errorNormal(TextViewToolbarPanel.class, ex, "Cannot upload text to Wklej.org!!");
                }

                mf.setBlockShortcuts(false);
            }
        }.start();



    }//GEN-LAST:event_uploadJBActionPerformed

    private void googleJBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_googleJBActionPerformed
        View view = MainFrame.getInstance().getViewContainer().getView();
        if (view.getType()
                != ViewType.TEXT_EDITOR) {
            return;
        }
        TextViewPanel textView = (TextViewPanel) view;

        String text = textView.getTextPane().getText().trim();
        String beginLnk = "http://www.google.pl/search?hl=en&rls=ig&q=";
        String urlStr = null;
        try {
            urlStr = beginLnk + URLEncoder.encode(text, "UTF-8");
            URL url = new URL(urlStr);

            Desktop.getDesktop().browse(url.toURI());
            MainFrame.getInstance().setVisible(false);
        } catch (URISyntaxException ex) {
            ErrorManager.errorNormal(TextViewToolbarPanel.class, ex, "Cannot open google URL: " + urlStr);
        } catch (IOException ex) {
            ErrorManager.errorNormal(TextViewToolbarPanel.class, ex, "Cannot open google URL: " + urlStr);
        }


    }//GEN-LAST:event_googleJBActionPerformed

    private void webJBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_webJBActionPerformed
        View view = MainFrame.getInstance().getViewContainer().getView();
        if (view.getType()
                != ViewType.TEXT_EDITOR) {
            return;
        }
        TextViewPanel textView = (TextViewPanel) view;

        String urlStr = textView.getTextPane().getText().trim();
        try {
            URL url = new URL(urlStr);

            Desktop.getDesktop().browse(url.toURI());
            MainFrame.getInstance().setVisible(false);
        } catch (URISyntaxException ex) {
            ErrorManager.errorNormal(TextViewToolbarPanel.class, ex, "Cannot open URL: " + urlStr);
        } catch (IOException ex) {
            ErrorManager.errorNormal(TextViewToolbarPanel.class, ex, "Cannot open URL: " + urlStr);
        }
    }//GEN-LAST:event_webJBActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JCheckBox copyAsFileCB;
    private javax.swing.JButton findJB;
    private javax.swing.JButton googleJB;
    private javax.swing.JComboBox pasteServerCB;
    private javax.swing.JSeparator separator1;
    private javax.swing.JButton uploadJB;
    private javax.swing.JButton webJB;
    // End of variables declaration//GEN-END:variables
}
