/*
 * MainFrame.java
 *
 * Created on 2011-07-23, 08:48:02
 */
package jmulticlip.gui;

import com.sun.jna.Platform;
import jmulticlip.core.TitleAndStatusManager;
import jmulticlip.gui.text.TextViewPanel;
import jmulticlip.gui.navig.NavigationPanel;
import java.awt.*;

import java.awt.event.*;
import java.io.*;
import java.net.*;
import java.util.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import jmulticlip.clipboard.ClipDataType;
import jmulticlip.i18n.*;
import jmulticlip.util.*;
import pl.ugu.revers.logging.i18n.*;
import jmulticlip.util.*;

import static java.awt.GridBagConstraints.*;
import java.awt.image.BufferedImage;
import java.lang.reflect.InvocationTargetException;
import java.util.logging.Level;
import java.util.logging.Logger;
import jmulticlip.clipboard.ClipboardManager;
import jmulticlip.core.InitializationManager;
import jmulticlip.gui.graph.CanvasPanel;
import jmulticlip.gui.graph.GraphicsViewPanel;
import jmulticlip.gui.navig.ClipDataRow;
import jmulticlip.gui.shortcut.AbstractShortcutAction;
import jmulticlip.gui.shortcut.Location;
import jmulticlip.gui.shortcut.ShortcutManager;
import jmulticlip.gui.shortcut.ShortcutScopeEnum;
import jmulticlip.ifaces.Initializable;
import jmulticlip.ifaces.Resetable;
import jmulticlip.screencapture.RedWindow;

import pl.ugu.revers.swing.*;
import pl.ugu.revers.swing.table.*;

/**
 *
 * @author Revers
 */
public class MainFrame extends javax.swing.JFrame implements Internationalizable,
        Resetable, ComponentListener, Initializable, FocusListener {

    private static final Dimension INITIAL_WINDOW_SIZE = new Dimension(1024, 700);
    private I18NManager i18n = I18NManager.getInstance();
    private GraphicsViewPanel imageView = new GraphicsViewPanel();
    private TextViewPanel textView = new TextViewPanel();
    private ListFocusTraversalPolicy navigatorFocusList = new ListFocusTraversalPolicy();
    private boolean blockShortcuts = false;
    private int ctrlTabCounter = 0;
    private boolean navigatorFocused = true;
    private boolean searchFieldFocus = false;
    private boolean navigationPanelFocus = false;
    private String currentAppOnMac;

    /**
     * Creates new form MainFrame
     */
    private MainFrame() {
        HttpURLConnection.setFollowRedirects(false);

        try {
            SwingUtilities.invokeAndWait(() -> create());
//        create();
        } catch (InterruptedException ex) {
            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvocationTargetException ex) {
            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void create() {
        initComponents();
        setTitle(TitleAndStatusManager.APPLICATION_TITLE);

        URL url = ClassLoader.getSystemResource("img/app icon.png");
        Image icon = null;
        if (url != null) {
            icon = new ImageIcon(url).getImage();
        } else {
            icon = Toolkit.getDefaultToolkit().getImage("img/app icon.png");
        }

        viewContainer.setView(textView);

        setIconImage(icon);

        SwingUtils.setSizeAndCenter(this, INITIAL_WINDOW_SIZE.width,
                INITIAL_WINDOW_SIZE.height);
        GUIUtils.useApplicationLAF(this);

        i18n.addInternationalizable(this);

        addComponentListener(this);

        //initialize clipboard manager:
        //  ClipboardManager.getInstance();
        InitializationManager.getInstance().addResetable(this);
        InitializationManager.getInstance().addInitializable(this);

        navigatorFocusList.addComponent(navigationPanel);
        JTextField tf = navigationPanel.getSearchPanel().getSearchTF();
        navigatorFocusList.addComponent(tf);
        tf.addFocusListener(this);
        navigationPanel.addFocusListener(this);

        setFocusTraversalPolicy(navigatorFocusList);

        KeyboardFocusManager kfm
                = KeyboardFocusManager.getCurrentKeyboardFocusManager();
        kfm.addKeyEventDispatcher(new KeyEventDispatcher() {

            public boolean dispatchKeyEvent(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_TAB
                        && e.getModifiers() == KeyEvent.CTRL_MASK) {

                    if (ctrlTabCounter % 2 == 0) {
                        navigatorFocused = !navigatorFocused;
                        changePanel(navigatorFocused);
                    }

                    ctrlTabCounter++;
                    return true;
                }

                return false;
            }
        });
        // exploreTraversalKeys(tf);

        ErrorDialog.setDefaultLAF("de.muntjak.tinylookandfeel.TinyLookAndFeel");
    }

//    private void exploreTraversalKeys(JComponent jc) {
//        Set<AWTKeyStroke> keys = jc.getFocusTraversalKeys(
//                KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS);
//
//        Iterator it = keys.iterator();
//        while (it.hasNext()) {
//            System.out.println(it.next());
//        }
//    }
    public boolean isBlockShortcuts() {
        return blockShortcuts;
    }

    public void setBlockShortcuts(boolean blockShortcuts) {
        this.blockShortcuts = blockShortcuts;
    }

    @Override
    public void init() {
        addShortcuts();
    }

    public void addShortcuts() {
        KeyStroke hideWindowKS = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0, true);
        AbstractShortcutAction hideWindow = new AbstractShortcutAction(Location.MAIN_WINDOW,
                toolbarPanel, ShortcutScopeEnum.WINDOW_SCOPE, "hideWindow", hideWindowKS) {

            public void actionPerformed(ActionEvent e) {
                setVisible(false);
            }
        };

        ShortcutManager.getInstance().registerShortcutAction(hideWindow);

        //--------------------------------------------------------------------------------------
//        KeyStroke changePanelKS = KeyStroke.getKeyStroke(KeyEvent.VK_TAB, KeyEvent.CTRL_MASK, true);
//
//        AbstractShortcutAction changePanel = new AbstractShortcutAction(Location.MAIN_WINDOW,
//                toolbarPanel, ShortcutScopeEnum.WINDOW_SCOPE, "changePanel", changePanelKS) {
//
//            public void actionPerformed(ActionEvent e) {
//                changePanel();
//            }
//        };
//
//        ShortcutManager.getInstance().registerShortcutAction(changePanel);
    }

    private void changePanel(boolean navigatorFocused) {
        this.navigatorFocused = navigatorFocused;

        if (navigatorFocused) {
            navigationPanel.getFocusBorder().setColor(GUIUtils.SELECTED_BORDER_COLOR);
            viewContainer.getFocusBorder().setColor(GUIUtils.UNSELECTED_BORDER_COLOR);

            navigationPanelFocus = true;
            searchFieldFocus = false;
            navigationPanel.requestFocusInWindow();
        } else {
            navigationPanel.getFocusBorder().setColor(GUIUtils.UNSELECTED_BORDER_COLOR);
            viewContainer.getFocusBorder().setColor(GUIUtils.SELECTED_BORDER_COLOR);
            viewContainer.getView().requestFocusInWindow();
            navigationPanelFocus = false;
            searchFieldFocus = false;
        }
        repaint();
    }

    @Override
    public void reset() {
    }

    public void showFrame() {

        if (Platform.isMac()) {
            currentAppOnMac = getCurrentAppOnMac();
            System.out.println("current app = '" + currentAppOnMac + "'");
        }
        InitializationManager.getInstance().resetAll();
        setVisible(true);
        navigationPanel.requestFocusInWindow();

        if (Platform.isMac()) {
            new Thread(() -> hackFocusOnMac()).start();
        }
    }

    private String getCurrentAppOnMac() {
        return executeAppleScript("tell application \"System Events\" to get the name of the first process where it is frontmost");
    }

    private void activateCurrentAppOnMac() {
        executeAppleScript("tell application \"" + currentAppOnMac + "\" to activate");
    }

    private String executeAppleScript(String script) {
        try {
            String[] cmd = {
                "osascript", "-e", script
            };
            Process process = new ProcessBuilder(cmd).start();
            InputStream is = process.getInputStream();
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);

            String result = "";
            String line;

            while ((line = br.readLine()) != null) {
                result += line;
            }
            return result.trim();

        } catch (IOException ex) {
            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
            return "<null>";
        }
    }

    private void hackFocusOnMac() {
        EventQueue.invokeLater(() -> setAlwaysOnTop(true));

        try {
            Thread.sleep(250L);
        } catch (InterruptedException ex) {
            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
        }

        EventQueue.invokeLater(() -> {
            try {
                Robot robot = new Robot();

                PointerInfo pointInfo = MouseInfo.getPointerInfo();
                Point origPoint = pointInfo.getLocation();

                int origX = (int) (origPoint.getX() + 0.5);
                int origY = (int) (origPoint.getY() + 0.5);

                Point location = getLocation();
                robot.mouseMove(location.x + 2, location.y + 2);

                robot.mousePress(InputEvent.BUTTON1_MASK);
                robot.mouseRelease(InputEvent.BUTTON1_MASK);

                robot.mouseMove(origX, origY);

            } catch (AWTException ex) {
                Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        });

        try {
            Thread.sleep(250L);
        } catch (InterruptedException ex) {
            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
        java.awt.EventQueue.invokeLater(() -> setAlwaysOnTop(false));
    }

    public void triggerPaste() {
        if (!Platform.isMac() || currentAppOnMac == null) {

            ClipboardManager clipManager = ClipboardManager.getInstance();
            clipManager.ctrlV();

            return;
        }
        new Thread(() -> {
            activateCurrentAppOnMac();

            try {
                Thread.sleep(100L);
            } catch (InterruptedException ex) {
                Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
            }

            ClipboardManager clipManager = ClipboardManager.getInstance();
            clipManager.ctrlV();
        }).start();
    }

    @Override
    public void componentHidden(ComponentEvent e) {
    }

    @Override
    public void componentMoved(ComponentEvent e) {
    }

    @Override
    public void componentResized(ComponentEvent e) {
        getNavigationPanel().getNavigTablePanel().selectFirstRow();
    }

    @Override
    public void componentShown(ComponentEvent e) {
        getNavigationPanel().getNavigTablePanel().selectFirstRow();
    }

    public void refreshCurrentView() {
        viewContainer.setView(getNavigationPanel().getNavigTablePanel().
                getCurrentClipDataRow().getCurrentView());
    }

    public void setViewFor(ClipDataRow cd) {

        if (cd.getType() == ClipDataType.TEXT || cd.getType() == ClipDataType.FILE_LIST) {
            textView.setText(cd.getFullText());
        } else if (cd.getType() == ClipDataType.IMAGE) {
            CanvasPanel canvas = cd.getCanvas();
            imageView.setCanvas(canvas);
        }

        viewContainer.setView(cd.getCurrentView());
    }

    public GraphicsViewPanel getDefaultImageView() {
        return imageView;
    }

    public TextViewPanel getDefaultTextView() {
        return textView;
    }

    public EditorPanel getViewContainer() {
        return viewContainer;
    }

    public static MainFrame getInstance() {
        return MainFrameHolder.INSTANCE;
    }

    @Override
    public void changeLanguage(Locale language) {
        // TODO: languageChanged
    }

    @Override
    public void focusGained(FocusEvent e) {
        //changePanel(true);
        navigationPanel.getFocusBorder().setColor(GUIUtils.SELECTED_BORDER_COLOR);
        viewContainer.getFocusBorder().setColor(GUIUtils.UNSELECTED_BORDER_COLOR);

        JTextField tf = navigationPanel.getSearchPanel().getSearchTF();
        if (e.getSource() == navigationPanel) {
            navigationPanelFocus = true;
        } else if (e.getSource() == tf) {
            searchFieldFocus = true;
        }

        repaint();
    }

    @Override
    public void focusLost(FocusEvent e) {
        JTextField tf = navigationPanel.getSearchPanel().getSearchTF();
        if (e.getSource() == navigationPanel) {
            navigationPanelFocus = false;
        } else if (e.getSource() == tf) {
            searchFieldFocus = false;
        }

        if (navigationPanelFocus == false && searchFieldFocus == false) {
            navigationPanel.getFocusBorder().setColor(GUIUtils.UNSELECTED_BORDER_COLOR);
            viewContainer.getFocusBorder().setColor(GUIUtils.SELECTED_BORDER_COLOR);
            repaint();
        }
    }

    private static class MainFrameHolder {

        private static final MainFrame INSTANCE = new MainFrame();
    }

    public EditorPanel getEditorPanel() {
        return viewContainer;
    }

    public NavigationPanel getNavigationPanel() {
        return navigationPanel;
    }

    public StatusbarPanel getStatusbarPanel() {
        return statusbarPanel;
    }

    public ToolbarPanel getToolbarPanel() {
        return toolbarPanel;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        statusbarPanel = new jmulticlip.gui.StatusbarPanel();
        toolbarPanel = new jmulticlip.gui.ToolbarPanel();
        navigationPanel = new jmulticlip.gui.navig.NavigationPanel();
        viewContainer = new jmulticlip.gui.EditorPanel();
        menuBar = new javax.swing.JMenuBar();
        fileMenu = new javax.swing.JMenu();
        newDocumentItem = new javax.swing.JMenuItem();
        newImageItem = new javax.swing.JMenuItem();
        separator1 = new javax.swing.JPopupMenu.Separator();
        exitMenuItem = new javax.swing.JMenuItem();
        editMenu = new javax.swing.JMenu();
        settingsMenuItem = new javax.swing.JMenuItem();
        helpMenu = new javax.swing.JMenu();
        updateMenuItem = new javax.swing.JMenuItem();
        aboutMenuItem = new javax.swing.JMenuItem();

        getContentPane().setLayout(new java.awt.GridBagLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 0, 0, 0);
        getContentPane().add(statusbarPanel, gridBagConstraints);

        toolbarPanel.setMinimumSize(new java.awt.Dimension(272, 100));
        toolbarPanel.setPreferredSize(new java.awt.Dimension(272, 66));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        getContentPane().add(toolbarPanel, gridBagConstraints);

        navigationPanel.setMinimumSize(new java.awt.Dimension(250, 50));
        navigationPanel.setPreferredSize(new java.awt.Dimension(300, 50));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 5);
        getContentPane().add(navigationPanel, gridBagConstraints);

        viewContainer.setMinimumSize(new java.awt.Dimension(100, 50));
        viewContainer.setPreferredSize(new java.awt.Dimension(100, 50));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        getContentPane().add(viewContainer, gridBagConstraints);

        fileMenu.setText("File");

        newDocumentItem.setText("New document...");
        fileMenu.add(newDocumentItem);

        newImageItem.setText("New image...");
        fileMenu.add(newImageItem);
        fileMenu.add(separator1);

        exitMenuItem.setText("Exit");
        exitMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exitMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(exitMenuItem);

        menuBar.add(fileMenu);

        editMenu.setText("Edit");

        settingsMenuItem.setText("Settings...");
        editMenu.add(settingsMenuItem);

        menuBar.add(editMenu);

        helpMenu.setText("Help");

        updateMenuItem.setText("Check for update...");
        helpMenu.add(updateMenuItem);

        aboutMenuItem.setText("About...");
        aboutMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                aboutMenuItemActionPerformed(evt);
            }
        });
        helpMenu.add(aboutMenuItem);

        menuBar.add(helpMenu);

        setJMenuBar(menuBar);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void aboutMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_aboutMenuItemActionPerformed
        JOptionPane.showMessageDialog(this,
                "Author:  Kamil Kołaczyński\n"
                + "Contact: kamil.kolaczynski@gmail.com",
                TitleAndStatusManager.APPLICATION_TITLE,
                JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_aboutMenuItemActionPerformed

    private void exitMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exitMenuItemActionPerformed
        System.exit(0);
    }//GEN-LAST:event_exitMenuItemActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        ErrorDialog.handleUncaughtException();

        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                InitializationManager.getInstance().init();

                MainFrame.getInstance().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem aboutMenuItem;
    private javax.swing.JMenu editMenu;
    private javax.swing.JMenuItem exitMenuItem;
    private javax.swing.JMenu fileMenu;
    private javax.swing.JMenu helpMenu;
    private javax.swing.JMenuBar menuBar;
    private jmulticlip.gui.navig.NavigationPanel navigationPanel;
    private javax.swing.JMenuItem newDocumentItem;
    private javax.swing.JMenuItem newImageItem;
    private javax.swing.JPopupMenu.Separator separator1;
    private javax.swing.JMenuItem settingsMenuItem;
    private jmulticlip.gui.StatusbarPanel statusbarPanel;
    private jmulticlip.gui.ToolbarPanel toolbarPanel;
    private javax.swing.JMenuItem updateMenuItem;
    private jmulticlip.gui.EditorPanel viewContainer;
    // End of variables declaration//GEN-END:variables
}
