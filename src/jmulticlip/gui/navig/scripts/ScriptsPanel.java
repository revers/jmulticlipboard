/*
 * ScriptsPanel.java
 *
 * Created on 2011-07-25, 20:39:29
 */
package jmulticlip.gui.navig.scripts;

import jmulticlip.gui.navig.scripts.choosen.ChoosenScriptsDialog;
import java.awt.*;

import java.awt.event.*;
import java.util.*;

import javax.swing.*;

import javax.swing.border.*;
import javax.swing.event.*;
import jmulticlip.core.InitializationManager;
import jmulticlip.gui.MainFrame;
import jmulticlip.gui.StatusbarPanel;
import jmulticlip.gui.shortcut.*;
import jmulticlip.i18n.I18NManager;
import jmulticlip.ifaces.Initializable;
import jmulticlip.ifaces.Resetable;
import jmulticlip.scripts.Script;
import jmulticlip.scripts.ScriptRunner;
import jmulticlip.scripts.impl.*;
import jmulticlip.util.GUIUtils;
import pl.ugu.revers.logging.i18n.Internationalizable;
import pl.ugu.revers.swing.table.*;

/**
 *
 * @author Revers
 */
public class ScriptsPanel extends ShortcutPanel implements Internationalizable,
        Resetable, Initializable, MouseMotionListener, MouseListener, ListSelectionListener {

    private I18NManager i18n = I18NManager.getInstance();
    private static final Column[] COLUMNS = {
        new Column(Boolean.class, "", true),
        new Column(String.class, "Shortcut", false),
        new Column(String.class, "Name", false),
        new Column(String.class, "?", true)};
    private ExtendedTableModel<ScriptDataRow> scriptTableModel =
            new ExtendedTableModel<ScriptDataRow>(COLUMNS);
    private ExtendedTableUtils tableUtils;
    private boolean enabled = true;
    private LinkedList<Script> selectedScriptsList = new LinkedList<Script>();

    /** Creates new form ScriptsPanel */
    public ScriptsPanel() {
        initComponents();

        i18n.addInternationalizable(this);

        scriptTable.addMouseMotionListener(this);
        scriptTable.addMouseListener(this);

        InitializationManager.getInstance().addResetable(this);
        InitializationManager.getInstance().addInitializable(this);

        scriptTable.getSelectionModel().addListSelectionListener(this);

        // ShortcutManager scMgr = ShortcutManager.getInstance();
        // scMgr.registerComponentLocation(Location.MAIN_WINDOW_NAVIGATOR, scriptTable);
    }

    public Script[] getSelectedScripts() {
        return selectedScriptsList.toArray(new Script[]{});
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        int row = scriptTable.getSelectedRow();
        scriptTable.scrollRectToVisible(new Rectangle(scriptTable.getCellRect(row, 0, true)));
    }

    @Override
    public void init() {
        //  System.out.println("INIT");
        //  ActionMap actionMap = scriptTable.getActionMap();
        //    clearTableActionMap(actionMap);
        addShortcuts();
        addScriptDataRows();
    }

    private void clearTableActionMap(ActionMap actionMap) {
        actionMap.put("selectPreviousRowExtendSelection", null);
        actionMap.put("selectLastColumn", null);
        actionMap.put("selectPreviousRowChangeLead", null);
        actionMap.put("startEditing", null);
        actionMap.put("addToSelection", null);
        actionMap.put("extendTo", null);
        actionMap.put("selectFirstRowExtendSelection", null);
        actionMap.put("scrollUpChangeSelection", null);
        actionMap.put("selectFirstColumn", null);
        actionMap.put("selectFirstColumnExtendSelection", null);
        actionMap.put("scrollDownExtendSelection", null);
        actionMap.put("selectLastRow", null);
        actionMap.put("scrollRightChangeSelection", null);
        actionMap.put("selectNextColumnCell", null);
        actionMap.put("cancel", null);
        actionMap.put("moveSelectionTo", null);
        actionMap.put("scrollLeftChangeSelection", null);
        actionMap.put("selectNextRowExtendSelection", null);
        actionMap.put("selectNextColumnChangeLead", null);
        actionMap.put("selectFirstRow", null);
        actionMap.put("selectPreviousColumnChangeLead", null);
        actionMap.put("selectNextRowChangeLead", null);
        actionMap.put("scrollLeftExtendSelection", null);
        actionMap.put("selectNextColumn", null);
        actionMap.put("copy", null);
        actionMap.put("scrollDownChangeSelection", null);
        actionMap.put("selectLastColumnExtendSelection", null);
        actionMap.put("selectPreviousColumnCell", null);
        actionMap.put("selectNextRowCell", null);
        actionMap.put("focusHeader", null);
        actionMap.put("clearSelection", null);
        actionMap.put("cut", null);
        actionMap.put("selectLastRowExtendSelection", null);
        actionMap.put("selectPreviousColumn", null);
        actionMap.put("scrollUpExtendSelection", null);
        actionMap.put("selectPreviousRowCell", null);
        actionMap.put("toggleAndAnchor", null);
        actionMap.put("selectAll", null);
        actionMap.put("paste", null);
        actionMap.put("selectPreviousRow", null);
        actionMap.put("selectPreviousColumnExtendSelection", null);
        actionMap.put("scrollRightExtendSelection", null);
        actionMap.put("selectNextColumnExtendSelection", null);
        actionMap.put("selectNextRow", null);
    }

    private void addShortcuts() {
        ShortcutManager shortcutMgr = ShortcutManager.getInstance();
        JComponent parent = MainFrame.getInstance().getNavigationPanel();

        KeyStroke moveUpKS = KeyStroke.getKeyStroke(KeyEvent.VK_UP, KeyEvent.CTRL_MASK, false);
        AbstractShortcutAction moveUp = new AbstractShortcutAction(Location.MAIN_WINDOW_NAVIGATOR,
                parent, ShortcutScopeEnum.LOCATION_SCOPE, "scriptUp", moveUpKS) {

            public void actionPerformed(ActionEvent e) {
                if (ScriptsPanel.this.enabled == false) {
                    return;
                }
                selectRowAbove();
            }
        };

        shortcutMgr.registerShortcutAction(moveUp);

        KeyStroke moveDownKS = KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, KeyEvent.CTRL_MASK, false);
        AbstractShortcutAction moveDown = new AbstractShortcutAction(Location.MAIN_WINDOW_NAVIGATOR,
                parent, ShortcutScopeEnum.LOCATION_SCOPE, "scriptDown", moveDownKS) {

            public void actionPerformed(ActionEvent e) {
                if (ScriptsPanel.this.enabled == false) {
                    return;
                }
                selectRowBellow();
            }
        };

        shortcutMgr.registerShortcutAction(moveDown);

        KeyStroke toggleUseKS = KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, KeyEvent.CTRL_MASK, false);
        AbstractShortcutAction toggleUse = new AbstractShortcutAction(Location.MAIN_WINDOW_NAVIGATOR,
                parent, ShortcutScopeEnum.LOCATION_SCOPE, "scriptToggleUse", toggleUseKS) {

            public void actionPerformed(ActionEvent e) {
                if (ScriptsPanel.this.enabled == false) {
                    return;
                }
                toggleUseInSelecteScript();
            }
        };

        shortcutMgr.registerShortcutAction(toggleUse);

        KeyStroke manageKS = KeyStroke.getKeyStroke(KeyEvent.VK_M, KeyEvent.CTRL_MASK, true);
        AbstractShortcutAction manage = new AbstractShortcutAction(Location.MAIN_WINDOW_NAVIGATOR,
                parent, ShortcutScopeEnum.LOCATION_SCOPE, "scriptManage", manageKS) {

            public void actionPerformed(ActionEvent e) {
                if (ScriptsPanel.this.enabled == false) {
                    return;
                }
                manageScriptsJBActionPerformed(e);
            }
        };

        shortcutMgr.registerShortcutAction(manage);

        ShortcutLabel moveUpLabel = new ShortcutLabel(moveUp, scriptTableScroll);
        moveUpLabel.addShortcut(moveDown);
        moveUpLabel.addShortcut(toggleUse);
        moveUpLabel.setTextOffsetY(GUIUtils.SHORTCUT_Y_OFFSET);
        moveUpLabel.setDrawFromRight(true);
        addShortcutLabel(moveUpLabel);

        ShortcutLabel manageLabel = new ShortcutLabel(manage, manageScriptsJB);
        manageLabel.setTextOffsetY(GUIUtils.SHORTCUT_Y_OFFSET);
        manageLabel.setDrawFromRight(true);
        addShortcutLabel(manageLabel);
    }

    @Override
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
        super.setEnabled(enabled);

        if (enabled && selectedScriptsList.isEmpty()) {
            manageScriptsJB.setEnabled(false);
        } else {
            manageScriptsJB.setEnabled(enabled);
        }

        scriptTable.setEnabled(enabled);
        scriptTableScroll.setEnabled(enabled);
        scriptTableScroll.getVerticalScrollBar().setEnabled(enabled);
        scriptTableScroll.getHorizontalScrollBar().setEnabled(enabled);
        scriptsJL.setEnabled(enabled);

        MainFrame mf = MainFrame.getInstance();
        StatusbarPanel status = mf.getStatusbarPanel();

        status.clearAllScripts();
        if (enabled) {
            for (Script s : selectedScriptsList) {
                status.addScript(s);
            }
        }
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    private void addScriptDataRows() {
        Script[] scripts = ScriptRunner.getInstance().getEnabledScripts();
        for (Script s : scripts) {
            scriptTableModel.addRow(new ScriptDataRow(s));
        }
    }

    private void toggleUseInSelecteScript() {
        if (scriptTableModel.getRowCount() == 0) {
            return;
        }

        int selectedRow = scriptTable.getSelectedRow();

        if (selectedRow < 0) {
            selectedRow = 0;
            scriptTable.setRowSelectionInterval(selectedRow, selectedRow);
        }

        ScriptDataRow row = scriptTableModel.getDataRow(selectedRow);
        row.setUse(!row.isUse());

        scriptTableModel.fireTableRowsUpdated(selectedRow, selectedRow);
    }

    private void selectRowAbove() {
        if (scriptTableModel.getRowCount() == 0) {
            return;
        }
        int selectedRow = scriptTable.getSelectedRow();

        if (selectedRow <= 0) {
            scriptTable.setRowSelectionInterval(0, 0);
            return;
        }

        selectedRow -= 1;
        scriptTable.setRowSelectionInterval(selectedRow, selectedRow);
    }

    private void selectRowBellow() {
        if (scriptTableModel.getRowCount() == 0) {
            return;
        }
        int selectedRow = scriptTable.getSelectedRow();

        if (selectedRow < 0) {
            scriptTable.setRowSelectionInterval(0, 0);
            return;
        }

        if (selectedRow == scriptTable.getRowCount() - 1) {
            return;
        }

        selectedRow += 1;

        scriptTable.setRowSelectionInterval(selectedRow, selectedRow);
    }

    @Override
    public void reset() {
        if (scriptTableModel.getRowCount() > 0) {
            scriptTable.removeRowSelectionInterval(0, scriptTable.getRowCount() - 1);
            scriptTable.setRowSelectionInterval(0, 0);

            for (ScriptDataRow row : scriptTableModel.getDataList()) {
                row.setUse(false);
            }
            scriptTableModel.fireTableRowsUpdated(0, scriptTableModel.getDataList().size() - 1);
        }

        boolean copyAsFiles =
                MainFrame.getInstance().getDefaultTextView().getTextToolbarPanel().isCopyAsFilesSelected();

        if (copyAsFiles) {
            setEnabled(false);
        } else {
            setEnabled(true);
        }
    }

    private void initTable() {
        scriptTable.setName("scriptTable");
        tableUtils = new ExtendedTableUtils(scriptTable, scriptTableModel);
        //  tableUtils.setLocale(TableUtils.LOCALE_PL);
        //   tableUtils.addShortcuts();
        tableUtils.setHeaderSelectableColumns(ScriptDataRow.COLUMN_USE);
        tableUtils.setBoldHeader(true);
        tableUtils.setReorderingAllowed(false);
        tableUtils.addDoubleClickResizeing();
        scriptTable.setRowHeight(scriptTable.getRowHeight() + 2);
        scriptTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);


        LabelCellRenderer labelCellRenderer = new LabelCellRenderer();
        tableUtils.setColumnCellRenderer(ScriptDataRow.COLUMN_SHORTCUT, new ShortcutCellRenderer(ShortcutCellRenderer.CENTER));
        tableUtils.setColumnCellRenderer(ScriptDataRow.COLUMN_NAME, labelCellRenderer);
        tableUtils.setColumnCellRenderer(ScriptDataRow.COLUMN_USE, new CheckBoxCellRenderer());

        tableUtils.setColumnCellEditor(ScriptDataRow.COLUMN_DESCRIPTION, new ImageCellEditor());
        tableUtils.setColumnCellRenderer(ScriptDataRow.COLUMN_DESCRIPTION, new ImageCellRenderer());

        tableUtils.setInitColumnSizePercent(0.1f, 0.3f, 0.50f, 0.1f);

        scriptTableModel.addTableModelListener(new TableModelListener() {

            public void tableChanged(TableModelEvent e) {
                if (e.getType() == TableModelEvent.UPDATE) {

                    for (int i = e.getFirstRow(); i <= e.getLastRow(); i++) {
                        ScriptDataRow row = scriptTableModel.getDataRow(i);
                        Script script = row.getScript();
                        if (row.isUse()) {

                            selectedScriptsList.add(script);
                            MainFrame.getInstance().getStatusbarPanel().addScript(script);
                        } else {
                            selectedScriptsList.remove(row.getScript());
                            MainFrame.getInstance().getStatusbarPanel().removeScript(script);
                        }
                    }
                    manageScriptsJB.setEnabled(!selectedScriptsList.isEmpty());
                }
            }
        });
    }

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
        setCursor(Cursor.getDefaultCursor());
    }

    @Override
    public void mouseDragged(MouseEvent e) {
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        if (scriptTable.columnAtPoint(e.getPoint()) == ScriptDataRow.COLUMN_DESCRIPTION) {
            setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        } else {
            setCursor(Cursor.getDefaultCursor());
        }
    }

//    private void addShortcuts() {
//        KeyStroke ks1 = KeyStroke.getKeyStroke(KeyEvent.VK_UP, KeyEvent.CTRL_MASK, true);
//
//        ShortcutLabel sc1 = new ShortcutLabel("script table", scriptTableScroll, this,
//                JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT, ShortcutScopeEnum.PARENT_COMPONENT_SCOPE, null, ks1);
//
//        KeyStroke ks2 = KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, KeyEvent.CTRL_MASK, true);
//        sc1.addKeyStroke(ks2, null);
//
//        KeyStroke ks3 = KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0, true);
//        sc1.addKeyStroke(ks3, null);
//
//        sc1.setDrawFromRight(true);
//        sc1.setTextOffsetY(6);
//        addShortcut(sc1);
//
//        KeyStroke ks4 = KeyStroke.getKeyStroke(KeyEvent.VK_M, KeyEvent.CTRL_MASK, true);
//
//        ShortcutLabel sc2 = new ShortcutLabel("manage scripts", manageScriptsJB, this,
//                JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT, ShortcutScopeEnum.PARENT_COMPONENT_SCOPE, null, ks4);
//        sc2.setDrawFromRight(true);
//        sc2.setTextOffsetY(4);
//        addShortcut(sc2);
//
//    }
    @Override
    public void changeLanguage(Locale language) {
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        scriptsJL = new javax.swing.JLabel();
        scriptTableScroll = new javax.swing.JScrollPane();
        scriptTable = new javax.swing.JTable();
        manageScriptsJB = new javax.swing.JButton();

        setMinimumSize(new java.awt.Dimension(100, 50));
        setPreferredSize(new java.awt.Dimension(100, 100));
        setLayout(new java.awt.GridBagLayout());

        scriptsJL.setText("Scripts:");
        scriptsJL.setFocusable(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        add(scriptsJL, gridBagConstraints);

        scriptTableScroll.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        scriptTableScroll.setFocusable(false);

        scriptTable.setModel(scriptTableModel);
        scriptTable.setFillsViewportHeight(true);
        scriptTable.setFocusable(false);
        scriptTable.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        initTable();
        scriptTableScroll.setViewportView(scriptTable);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 0, 0);
        add(scriptTableScroll, gridBagConstraints);

        manageScriptsJB.setText("Manage choosen scripts...");
        manageScriptsJB.setEnabled(false);
        manageScriptsJB.setFocusable(false);
        manageScriptsJB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                manageScriptsJBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(16, 0, 0, 0);
        add(manageScriptsJB, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void manageScriptsJBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_manageScriptsJBActionPerformed
        if (manageScriptsJB.isEnabled() == false) {
            return;
        }

        selectedScriptsList = ChoosenScriptsDialog.manageScripts(selectedScriptsList);
        StatusbarPanel status = MainFrame.getInstance().getStatusbarPanel();
        status.clearAllScripts();

        for (ScriptDataRow row : scriptTableModel.getDataList()) {
            row.setUse(false);
        }

        for (Script s : selectedScriptsList) {
            //   status.addScript(s);

            for (ScriptDataRow row : scriptTableModel.getDataList()) {
                if (row.getScript() == s) {
                    row.setUse(true);
                    break;
                }
            }
        }

        selectedScriptsList.clear();
        scriptTableModel.fireTableRowsUpdated(0, scriptTable.getRowCount() - 1);

    }//GEN-LAST:event_manageScriptsJBActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton manageScriptsJB;
    private javax.swing.JTable scriptTable;
    private javax.swing.JScrollPane scriptTableScroll;
    private javax.swing.JLabel scriptsJL;
    // End of variables declaration//GEN-END:variables
}
