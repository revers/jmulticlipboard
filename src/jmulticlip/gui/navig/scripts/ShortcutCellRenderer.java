package jmulticlip.gui.navig.scripts;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Insets;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableCellRenderer;

public class ShortcutCellRenderer extends DefaultTableCellRenderer {

    private static final Color COLOR = new Color(100, 100, 100);
    private static final Font FONT = new Font("Arial", Font.PLAIN, 10);
    public static final int ALIGNMENT_LEFT = JLabel.LEFT;
    public static final int ALIGNMENT_RIGHT = JLabel.RIGHT;
    public static final int ALIGNMENT_CENTER = JLabel.CENTER;
    private int alignment;
    private static final EmptyBorder BORDER = new EmptyBorder(new Insets(0,
            4, 0, 4));

    public ShortcutCellRenderer() {
        this(ALIGNMENT_CENTER);
    }

    public ShortcutCellRenderer(int alignment) {
        this.alignment = alignment;
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value,
            boolean isSelected, boolean hasFocus, int row, int column) {

        Component c = super.getTableCellRendererComponent(table, value,
                isSelected, hasFocus, row, column);

        if (c instanceof JLabel) {
            JLabel label = ((JLabel) c);
            label.setBorder(BORDER);
            label.setFont(FONT);
            label.setForeground(COLOR);
            label.setHorizontalAlignment(alignment);
        }

        if (isSelected) {
            //  setBackground(table.getSelectionBackground());
            c.setBackground(table.getSelectionBackground());
        } else {
            //setBackground(table.getBackground());
            c.setBackground(table.getBackground());
        }

        c.setEnabled(table.isEnabled());
//		if(row % 2 == 0)
//			label.setBackground(COLOR);
//		else
//			label.setBackground(Color.white);

        return c;
    }
}