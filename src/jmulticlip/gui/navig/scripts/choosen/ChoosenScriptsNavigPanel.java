/*
 * ChooseScriptsNavigPanel.java
 *
 * Created on 2011-07-31, 09:48:00
 */
package jmulticlip.gui.navig.scripts.choosen;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.LinkedList;
import java.util.Locale;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.KeyStroke;
import jmulticlip.core.InitializationManager;
import jmulticlip.gui.ListFocusTraversalPolicy;
import jmulticlip.gui.MainFrame;
import jmulticlip.gui.shortcut.AbstractShortcutAction;
import jmulticlip.gui.shortcut.DefaultShortcutAction;
import jmulticlip.gui.shortcut.Location;
import jmulticlip.gui.shortcut.ShortcutLabel;
import jmulticlip.gui.shortcut.ShortcutManager;
import jmulticlip.gui.shortcut.ShortcutPanel;
import jmulticlip.gui.shortcut.ShortcutScopeEnum;
import jmulticlip.i18n.I18NManager;
import jmulticlip.ifaces.Initializable;
import jmulticlip.scripts.Script;
import jmulticlip.util.GUIUtils;
import pl.ugu.revers.logging.i18n.Internationalizable;

/**
 *
 * @author Revers
 */
public class ChoosenScriptsNavigPanel extends ShortcutPanel
        implements Internationalizable {

    private I18NManager i18n = I18NManager.getInstance();
    private ChoosenScriptsDialog dialog;

    public ChoosenScriptsNavigPanel(ChoosenScriptsDialog dialog) {
        this.dialog = dialog;
        initComponents();

        i18n.addInternationalizable(this);

        ShortcutManager scMgr = ShortcutManager.getInstance();
        scMgr.registerComponentLocation(Location.CHOOSEN_SCRIPTS_DIALOG, cancelJB);
        scMgr.registerComponentLocation(Location.CHOOSEN_SCRIPTS_DIALOG, okJB);
        scMgr.registerComponentLocation(Location.CHOOSEN_SCRIPTS_DIALOG, deleteJB);
        scMgr.registerComponentLocation(Location.CHOOSEN_SCRIPTS_DIALOG, moveUpJB);
        scMgr.registerComponentLocation(Location.CHOOSEN_SCRIPTS_DIALOG, moveDownJB);
        scMgr.registerComponentLocation(Location.CHOOSEN_SCRIPTS_DIALOG, moveBottomJB);
        scMgr.registerComponentLocation(Location.CHOOSEN_SCRIPTS_DIALOG, moveTopJB);

//        ActionMap actionMap = dialog.scriptTable.getActionMap();
//        clearTableActionMap(actionMap);
//        clearButtonAction(okJB);
//        clearButtonAction(cancelJB);
//        clearButtonAction(deleteJB);
//        clearButtonAction(moveUpJB);
//        clearButtonAction(moveDownJB);
//        clearButtonAction(moveTopJB);
//        clearButtonAction(moveBottomJB);

        addShortcuts();
    }

//    private void clearButtonAction(JButton button) {
//        ActionMap actionMap = button.getActionMap();
//        actionMap.put("pressed", null);
//        actionMap.put("released", null);
//    }
    private void addShortcuts() {
        ShortcutManager shortcutMgr = ShortcutManager.getInstance();
        JComponent parent = dialog.rootPanel;
        //------------------------------------------------------
        KeyStroke moveUpKS = KeyStroke.getKeyStroke(KeyEvent.VK_UP, KeyEvent.CTRL_MASK, false);
        AbstractShortcutAction moveUp = new AbstractShortcutAction(Location.CHOOSEN_SCRIPTS_DIALOG,
                parent, ShortcutScopeEnum.LOCATION_SCOPE, "moveUp", moveUpKS) {

            public void actionPerformed(ActionEvent e) {
                moveUpJBActionPerformed(e);
            }
        };

        shortcutMgr.registerShortcutAction(moveUp);
        //------------------------------------------------------
        KeyStroke moveDownKS = KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, KeyEvent.CTRL_MASK, false);
        AbstractShortcutAction moveDown = new AbstractShortcutAction(Location.CHOOSEN_SCRIPTS_DIALOG,
                parent, ShortcutScopeEnum.LOCATION_SCOPE, "moveDown", moveDownKS) {

            public void actionPerformed(ActionEvent e) {
                moveDownJBActionPerformed(e);
            }
        };

        shortcutMgr.registerShortcutAction(moveDown);
        //------------------------------------------------------
        KeyStroke moveTopKS = KeyStroke.getKeyStroke(KeyEvent.VK_UP, KeyEvent.CTRL_MASK | KeyEvent.SHIFT_MASK, false);
        AbstractShortcutAction moveTop = new AbstractShortcutAction(Location.CHOOSEN_SCRIPTS_DIALOG,
                parent, ShortcutScopeEnum.LOCATION_SCOPE, "moveTop", moveTopKS) {

            public void actionPerformed(ActionEvent e) {
                moveTopJBActionPerformed(e);
            }
        };

        shortcutMgr.registerShortcutAction(moveTop);
        //------------------------------------------------------
        KeyStroke moveBottomKS = KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, KeyEvent.CTRL_MASK | KeyEvent.SHIFT_MASK, false);
        AbstractShortcutAction moveBottom = new AbstractShortcutAction(Location.CHOOSEN_SCRIPTS_DIALOG,
                parent, ShortcutScopeEnum.LOCATION_SCOPE, "moveBottom", moveBottomKS) {

            public void actionPerformed(ActionEvent e) {
                moveBottomJBActionPerformed(e);
            }
        };

        shortcutMgr.registerShortcutAction(moveBottom);
        //------------------------------------------------------
        KeyStroke deleteKS = KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0, false);
        AbstractShortcutAction delete = new AbstractShortcutAction(Location.CHOOSEN_SCRIPTS_DIALOG,
                parent, ShortcutScopeEnum.LOCATION_SCOPE, "delete", deleteKS) {

            public void actionPerformed(ActionEvent e) {
                deleteJBActionPerformed(e);
            }
        };

        shortcutMgr.registerShortcutAction(delete);
        //------------------------------------------------------
        KeyStroke okKS = KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true);
        AbstractShortcutAction ok = new AbstractShortcutAction(Location.CHOOSEN_SCRIPTS_DIALOG,
                parent, ShortcutScopeEnum.LOCATION_SCOPE, "ok", okKS) {

            public void actionPerformed(ActionEvent e) {
                okJBActionPerformed(e);
            }
        };

        shortcutMgr.registerShortcutAction(ok);
        //------------------------------------------------------
        KeyStroke cancelKS = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0, true);
        AbstractShortcutAction cancel = new AbstractShortcutAction(Location.CHOOSEN_SCRIPTS_DIALOG,
                parent, ShortcutScopeEnum.LOCATION_SCOPE, "cancel", cancelKS) {

            public void actionPerformed(ActionEvent e) {
                cancelJBActionPerformed(e);
            }
        };

        shortcutMgr.registerShortcutAction(cancel);
        //------------------------------------------------------
        ActionMap actionMap = dialog.scriptTable.getActionMap();
        //------------------------------------------------------
        Action selectNextRowAction = actionMap.get("selectNextRow");

        KeyStroke selectNextKS = KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, 0, false);
        DefaultShortcutAction selectNext = new DefaultShortcutAction(Location.CHOOSEN_SCRIPTS_DIALOG,
                parent, ShortcutScopeEnum.LOCATION_SCOPE, "selectNext", selectNextKS, selectNextRowAction);
        selectNext.setSource(dialog.scriptTable);

        shortcutMgr.registerShortcutAction(selectNext);
        //------------------------------------------------------
        Action selectPrevRowAction = actionMap.get("selectPreviousRow");

        KeyStroke selectPrevKS = KeyStroke.getKeyStroke(KeyEvent.VK_UP, 0, false);
        DefaultShortcutAction selectPrev = new DefaultShortcutAction(Location.CHOOSEN_SCRIPTS_DIALOG,
                parent, ShortcutScopeEnum.LOCATION_SCOPE, "selectPreviousRow", selectPrevKS, selectPrevRowAction);
        selectPrev.setSource(dialog.scriptTable);

        shortcutMgr.registerShortcutAction(selectPrev);
        //------------------------------------------------------
        Action selectNextRowExtendSelectionAction = actionMap.get("selectNextRowExtendSelection");

        KeyStroke selectNextRowExtendSelectionKS = KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, KeyEvent.SHIFT_MASK, false);
        DefaultShortcutAction selectNextRowExtendSelection = new DefaultShortcutAction(Location.CHOOSEN_SCRIPTS_DIALOG,
                parent, ShortcutScopeEnum.LOCATION_SCOPE, "selectNextRowExtendSelection",
                selectNextRowExtendSelectionKS, selectNextRowExtendSelectionAction);
        selectNextRowExtendSelection.setSource(dialog.scriptTable);

        shortcutMgr.registerShortcutAction(selectNextRowExtendSelection);
        //------------------------------------------------------
        Action selectPreviousRowExtendSelectionAction = actionMap.get("selectPreviousRowExtendSelection");

        KeyStroke selectPreviousRowExtendSelectionKS = KeyStroke.getKeyStroke(KeyEvent.VK_UP, KeyEvent.SHIFT_MASK, false);
        DefaultShortcutAction selectPreviousRowExtendSelection = new DefaultShortcutAction(Location.CHOOSEN_SCRIPTS_DIALOG,
                parent, ShortcutScopeEnum.LOCATION_SCOPE, "selectPreviousRowExtendSelection",
                selectPreviousRowExtendSelectionKS, selectPreviousRowExtendSelectionAction);
        selectPreviousRowExtendSelection.setSource(dialog.scriptTable);

        shortcutMgr.registerShortcutAction(selectPreviousRowExtendSelection);
        //------------------------------------------------------
        Action selectAllAction = actionMap.get("selectAll");

        KeyStroke selectAllKS = KeyStroke.getKeyStroke(KeyEvent.VK_A, KeyEvent.CTRL_MASK, false);
        DefaultShortcutAction selectAll = new DefaultShortcutAction(Location.CHOOSEN_SCRIPTS_DIALOG,
                parent, ShortcutScopeEnum.LOCATION_SCOPE, "selectAll",
                selectAllKS, selectAllAction);

        selectAll.setSource(dialog.scriptTable);
        shortcutMgr.registerShortcutAction(selectAll);
        //------------------------------------------------------
        ShortcutLabel moveUpLabel = new ShortcutLabel(moveUp, moveUpJB);
        moveUpLabel.setTextOffsetY(GUIUtils.SHORTCUT_Y_OFFSET);
        addShortcutLabel(moveUpLabel);

        ShortcutLabel moveDownLabel = new ShortcutLabel(moveDown, moveDownJB);
        moveDownLabel.setTextOffsetY(GUIUtils.SHORTCUT_Y_OFFSET);
        addShortcutLabel(moveDownLabel);

        ShortcutLabel moveTopLabel = new ShortcutLabel(moveTop, moveTopJB);
        moveTopLabel.setTextOffsetY(GUIUtils.SHORTCUT_Y_OFFSET);
        addShortcutLabel(moveTopLabel);

        ShortcutLabel moveBottomLabel = new ShortcutLabel(moveBottom, moveBottomJB);
        moveBottomLabel.setTextOffsetY(GUIUtils.SHORTCUT_Y_OFFSET);
        addShortcutLabel(moveBottomLabel);

        ShortcutLabel deleteLabel = new ShortcutLabel(delete, deleteJB);
        deleteLabel.setTextOffsetY(GUIUtils.SHORTCUT_Y_OFFSET);
        addShortcutLabel(deleteLabel);

        ShortcutLabel cancelLabel = new ShortcutLabel(cancel, cancelJB);
        cancelLabel.setTextOffsetY(GUIUtils.SHORTCUT_Y_OFFSET);
        addShortcutLabel(cancelLabel);

        ShortcutLabel okLabel = new ShortcutLabel(ok, okJB);
        okLabel.setTextOffsetY(GUIUtils.SHORTCUT_Y_OFFSET);
        addShortcutLabel(okLabel);
    }

    private void clearTableActionMap(ActionMap actionMap) {
        actionMap.put("selectPreviousRowExtendSelection", null);
        actionMap.put("selectLastColumn", null);
        actionMap.put("selectPreviousRowChangeLead", null);
        actionMap.put("startEditing", null);
        actionMap.put("addToSelection", null);
        actionMap.put("extendTo", null);
        actionMap.put("selectFirstRowExtendSelection", null);
        actionMap.put("scrollUpChangeSelection", null);
        actionMap.put("selectFirstColumn", null);
        actionMap.put("selectFirstColumnExtendSelection", null);
        actionMap.put("scrollDownExtendSelection", null);
        actionMap.put("selectLastRow", null);
        actionMap.put("scrollRightChangeSelection", null);
        actionMap.put("selectNextColumnCell", null);
        actionMap.put("cancel", null);
        actionMap.put("moveSelectionTo", null);
        actionMap.put("scrollLeftChangeSelection", null);
        actionMap.put("selectNextRowExtendSelection", null);
        actionMap.put("selectNextColumnChangeLead", null);
        actionMap.put("selectFirstRow", null);
        actionMap.put("selectPreviousColumnChangeLead", null);
        actionMap.put("selectNextRowChangeLead", null);
        actionMap.put("scrollLeftExtendSelection", null);
        actionMap.put("selectNextColumn", null);
        actionMap.put("copy", null);
        actionMap.put("scrollDownChangeSelection", null);
        actionMap.put("selectLastColumnExtendSelection", null);
        actionMap.put("selectPreviousColumnCell", null);
        actionMap.put("selectNextRowCell", null);
        actionMap.put("focusHeader", null);
        actionMap.put("clearSelection", null);
        actionMap.put("cut", null);
        actionMap.put("selectLastRowExtendSelection", null);
        actionMap.put("selectPreviousColumn", null);
        actionMap.put("scrollUpExtendSelection", null);
        actionMap.put("selectPreviousRowCell", null);
        actionMap.put("toggleAndAnchor", null);
        actionMap.put("selectAll", null);
        actionMap.put("paste", null);
        actionMap.put("selectPreviousRow", null);
        actionMap.put("selectPreviousColumnExtendSelection", null);
        actionMap.put("scrollRightExtendSelection", null);
        actionMap.put("selectNextColumnExtendSelection", null);
        actionMap.put("selectNextRow", null);
    }

    /** Creates new form ChooseScriptsNavigPanel */
    public ChoosenScriptsNavigPanel() {
        this(null);
    }

//    private void addShortcuts() {
//        addShortcut(moveUpJB, "scripts.moveUp", KeyStroke.getKeyStroke(KeyEvent.VK_UP, KeyEvent.CTRL_MASK, true));
//        addShortcut(moveDownJB, "scripts.moveDown", KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, KeyEvent.CTRL_MASK, true));
//
//        addShortcut(moveTopJB, "scripts.moveTop",
//                KeyStroke.getKeyStroke(KeyEvent.VK_UP, KeyEvent.SHIFT_MASK | KeyEvent.CTRL_MASK, true));
//        addShortcut(moveBottomJB, "scripts.moveBottom",
//                KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, KeyEvent.SHIFT_MASK | KeyEvent.CTRL_MASK, true));
//
//        addShortcut(deleteJB, "scripts.delete", KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0, true));
//
//        addShortcut(cancelJB, "scripts.cancel", KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0, true));
//        addShortcut(okJB, "scripts.ok", KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true));
//
//
//    }
//    private void addShortcut(JComponent comp, String name, KeyStroke ks) {
//        //KeyStroke ks4 = KeyStroke.getKeyStroke(KeyEvent.VK_M, KeyEvent.CTRL_MASK, true);
//
//        ShortcutLabel sc2 = new ShortcutLabel(name, comp, this,
//                JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT, ShortcutScopeEnum.PARENT_LOCATION_SCOPE, null, ks);
//       // sc2.setDrawFromRight(true);
//        sc2.setTextOffsetY(4);
//        addShortcut(sc2);
//    }
    @Override
    public void changeLanguage(Locale language) {
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        moveUpJB = new javax.swing.JButton();
        moveDownJB = new javax.swing.JButton();
        moveTopJB = new javax.swing.JButton();
        moveBottomJB = new javax.swing.JButton();
        deleteJB = new javax.swing.JButton();
        cancelJB = new javax.swing.JButton();
        okJB = new javax.swing.JButton();

        setLayout(new java.awt.GridBagLayout());

        moveUpJB.setText("Move Up");
        moveUpJB.setFocusable(false);
        moveUpJB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                moveUpJBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(8, 0, 0, 0);
        add(moveUpJB, gridBagConstraints);

        moveDownJB.setText("Move Down");
        moveDownJB.setFocusable(false);
        moveDownJB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                moveDownJBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(18, 0, 0, 0);
        add(moveDownJB, gridBagConstraints);

        moveTopJB.setText("Move Top");
        moveTopJB.setFocusable(false);
        moveTopJB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                moveTopJBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(18, 0, 0, 0);
        add(moveTopJB, gridBagConstraints);

        moveBottomJB.setText("Move Bottom");
        moveBottomJB.setFocusable(false);
        moveBottomJB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                moveBottomJBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(18, 0, 0, 0);
        add(moveBottomJB, gridBagConstraints);

        deleteJB.setText("Delete");
        deleteJB.setFocusable(false);
        deleteJB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteJBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(18, 0, 0, 0);
        add(deleteJB, gridBagConstraints);

        cancelJB.setText("Cancel");
        cancelJB.setFocusable(false);
        cancelJB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelJBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(18, 0, 0, 0);
        add(cancelJB, gridBagConstraints);

        okJB.setText("OK");
        okJB.setFocusable(false);
        okJB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                okJBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.SOUTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(18, 0, 0, 0);
        add(okJB, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void moveUpJBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_moveUpJBActionPerformed
        dialog.tableUtils.moveSelectedRowsUp();
        dialog.enumerateRows();
    }//GEN-LAST:event_moveUpJBActionPerformed

    private void moveDownJBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_moveDownJBActionPerformed
        dialog.tableUtils.moveSelectedRowsDown();
        dialog.enumerateRows();
    }//GEN-LAST:event_moveDownJBActionPerformed

    private void moveTopJBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_moveTopJBActionPerformed
        dialog.tableUtils.moveSelectedRowsTop();
        dialog.enumerateRows();
    }//GEN-LAST:event_moveTopJBActionPerformed

    private void moveBottomJBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_moveBottomJBActionPerformed
        dialog.tableUtils.moveSelectedRowsBottom();
        dialog.enumerateRows();
    }//GEN-LAST:event_moveBottomJBActionPerformed

    private void deleteJBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteJBActionPerformed
        dialog.tableUtils.removeSelectedRows();
        dialog.enumerateRows();
        if (dialog.scriptTable.getRowCount() > 0) {
            dialog.scriptTable.setRowSelectionInterval(0, 0);
        }
    }//GEN-LAST:event_deleteJBActionPerformed

    private void okJBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_okJBActionPerformed
        LinkedList<Script> scriptList = new LinkedList<Script>();
        for (ChoosenScriptDataRow row : dialog.scriptTableModel.getDataList()) {
            scriptList.add(row.getScript());
        }
        dialog.scriptList = scriptList;
        dialog.dispose();
    }//GEN-LAST:event_okJBActionPerformed

    private void cancelJBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelJBActionPerformed
        dialog.dispose();
    }//GEN-LAST:event_cancelJBActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton cancelJB;
    private javax.swing.JButton deleteJB;
    private javax.swing.JButton moveBottomJB;
    private javax.swing.JButton moveDownJB;
    private javax.swing.JButton moveTopJB;
    private javax.swing.JButton moveUpJB;
    private javax.swing.JButton okJB;
    // End of variables declaration//GEN-END:variables
}
