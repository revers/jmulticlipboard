package jmulticlip.gui.navig.scripts.choosen;

import jmulticlip.gui.navig.scripts.*;
import jmulticlip.scripts.Script;
import pl.ugu.revers.swing.table.DataRow;

/**
 *
 * @author Revers
 */
public class ChoosenScriptDataRow implements DataRow {

    public static final int COLUMN_INDEX = 0;
    public static final int COLUMN_NAME = 1;
    private int index;
    private String name;
    private Script script;

    public ChoosenScriptDataRow(Script script) {
        this.script = script;
        this.name = script.getName();
    }

    public ChoosenScriptDataRow(int index, String name) {
        this.index = index;
        this.name = name;
    }

    @Override
    public Object getValue(int column) {
        switch (column) {
            case COLUMN_INDEX:
                return getIndex();
            case COLUMN_NAME:
                return getName();
        }
        return null;
    }

    @Override
    public boolean setValue(int column, Object value) {
        switch (column) {
            case COLUMN_INDEX:
                setIndex((Integer) value);
                return true;
            case COLUMN_NAME:
                setName((String) value);
                return true;
        }
        return false;
    }

    public Script getScript() {
        return script;
    }

    public void setScript(Script script) {
        this.script = script;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
