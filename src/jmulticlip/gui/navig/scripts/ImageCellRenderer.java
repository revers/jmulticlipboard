package jmulticlip.gui.navig.scripts;

import java.awt.Color;
import java.awt.Component;
import java.awt.Insets;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.Icon;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableCellRenderer;
import jmulticlip.util.GUIUtils;

/**
 *
 * @author Revers
 */
public class ImageCellRenderer extends DefaultTableCellRenderer  {

    private static final EmptyBorder BORDER = new EmptyBorder(new Insets(0,
            4, 0, 4));
    
    private Icon icon = GUIUtils.getInstance().getHelpIcon();

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
            boolean hasFocus, int row, int column) {

        Component c = super.getTableCellRendererComponent(table, value,
                isSelected, hasFocus, row, column);

        if (c instanceof JLabel) {
            JLabel label = ((JLabel) c);
            label.setBorder(BORDER);

            label.setText("");
            label.setIcon(icon);
        }

        if (isSelected) {
            c.setBackground(table.getSelectionBackground());
        } else {
            c.setBackground(table.getBackground());
        }

        c.setEnabled(table.isEnabled());

        return c;
    }
}
