package jmulticlip.gui.navig.scripts;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.*;
import jmulticlip.util.GUIUtils;

import pl.ugu.revers.swing.table.*;

/**
 *
 * @author Revers
 */
public class ImageCellEditor extends DefaultCellEditor implements MouseListener {

    protected JLabel iconLabel;
    private String cellValue;
    //  private boolean isPushed;
    private Icon icon = GUIUtils.getInstance().getHelpIcon();
    private ScriptDataRow scriptDataRow;
    // private Point mousePoint;
    public JPopupMenu popup;
    // private JTable table;
    private DescriptionPopupMenu descriptionPopup;

    public ImageCellEditor() {
        super(new JCheckBox());

        iconLabel = new JLabel();
        iconLabel.setOpaque(true);

        popup = new JPopupMenu();
        //    OptionsPopupMenu mme = new OptionsPopupMenu();
        descriptionPopup = new DescriptionPopupMenu();

        popup.add(descriptionPopup);

        iconLabel.addMouseListener(this);
        // iconLabel.addMouseMotionListener(this);
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value,
            boolean isSelected, int row, int column) {
        if (isSelected) {
            iconLabel.setForeground(table.getSelectionForeground());
            iconLabel.setBackground(table.getSelectionBackground());
        } else {
            iconLabel.setForeground(table.getForeground());
            iconLabel.setBackground(table.getBackground());
        }
        //   this.table = table;
        scriptDataRow = (ScriptDataRow) ((ExtendedTableModel<ScriptDataRow>) table.getModel()).getDataRow(row);
        cellValue = (value == null) ? "" : value.toString();
        // button.setText(label);
        iconLabel.setIcon(icon);
        //  isPushed = true;
        return iconLabel;
    }

    @Override
    public Object getCellEditorValue() {
        // if (isPushed) {
        // 
        // 
        //JOptionPane.showMessageDialog(button, label + ": Ouch!");
        //   System.out.println(cellValue + ": Ouch!");
        //  }
        //  isPushed = false;


        return cellValue;
    }

    @Override
    public boolean stopCellEditing() {
        //  isPushed = false;
        return super.stopCellEditing();
    }

    @Override
    protected void fireEditingStopped() {
        super.fireEditingStopped();
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        fireEditingStopped();
    }

    @Override
    public void mousePressed(MouseEvent e) {
        Point mousePoint = e.getPoint();

        if (scriptDataRow != null) {
            SwingUtilities.updateComponentTreeUI(popup);
            
            descriptionPopup.setTitle(scriptDataRow.getScript().getClass().getName());
            descriptionPopup.setText(scriptDataRow.getScript().getDescription());
            popup.show(iconLabel, mousePoint.x, mousePoint.y);
        }

        fireEditingStopped();
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        fireEditingStopped();
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    class DescriptionPopupMenu extends JPanel implements MenuElement {

        private final int insets = 5;
        private Color bgColor = new Color(255, 255, 225);
        private JTextArea textArea = new JTextArea();
        private JLabel titleJL = new JLabel();
        Dimension dim = new Dimension(50, 50);

        public DescriptionPopupMenu() {
            setBackground(bgColor);
            //setLayout(new GridLayout(2, 1));
            setLayout(new BorderLayout(insets, insets));
            titleJL.setFont(titleJL.getFont().deriveFont(Font.BOLD));
            setBorder(new EmptyBorder(insets, insets, insets, insets));

            
            setPreferredSize(dim);
            textArea.setWrapStyleWord(true);
            textArea.setEditable(false);
            textArea.setEnabled(false);
            textArea.setBackground(bgColor);
            textArea.setSelectedTextColor(bgColor);
            textArea.setForeground(Color.black);
            textArea.setDisabledTextColor(Color.black);
            add(titleJL, BorderLayout.NORTH);
            add(textArea, BorderLayout.CENTER);
            //   add(caseSensitiveCB);
            // add(regexpCB);
        }

        public void setTitle(String title) {
            titleJL.setText(title);
            dim = new Dimension(titleJL.getPreferredSize().width + 2*insets, dim.height);
        }

        public void setText(String txt) {
            textArea.setText(txt);
            Dimension prefSize = textArea.getPreferredSize();
            Dimension newDim = new Dimension(dim.width, dim.height);
            if(newDim.width < prefSize.width) {
                newDim.width = prefSize.width;
            }
            
            int offset = 30;
            if(newDim.height < prefSize.height + offset) {
                newDim.height = prefSize.height + offset;
            }
            setPreferredSize(newDim);
        }

        @Override
        public Component getComponent() {
            return this;
        }

        @Override
        public MenuElement[] getSubElements() {
            return new MenuElement[0];
        }

        @Override
        public void menuSelectionChanged(boolean isIncluded) {
        }

        public void processMouseEvent(MouseEvent e, MenuElement path[],
                MenuSelectionManager manager) {
        }

        public void processKeyEvent(KeyEvent e, MenuElement path[],
                MenuSelectionManager manager) {
        }
    }
}
