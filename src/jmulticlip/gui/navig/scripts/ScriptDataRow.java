package jmulticlip.gui.navig.scripts;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import jmulticlip.gui.shortcut.ShortcutHelper;
import jmulticlip.scripts.Script;
import jmulticlip.util.GUIUtils;
import pl.ugu.revers.swing.table.DataRow;

/**
 *
 * @author Revers
 */
public class ScriptDataRow implements DataRow {

    public static final int COLUMN_USE = 0;
    public static final int COLUMN_SHORTCUT = 1;
    public static final int COLUMN_NAME = 2;
    public static final int COLUMN_DESCRIPTION = 3;
    public boolean use;
    public String shortcut;
    public String name;
    private Script script;

    public ScriptDataRow(Script script) {
        this.script = script;
    }

    @Override
    public Object getValue(int column) {
        switch (column) {
            case COLUMN_USE:
                return isUse();
            case COLUMN_SHORTCUT:
                return ShortcutHelper.getKeyStrokeName(script.getKeyStroke());
            case COLUMN_NAME:
                return script.getName();
            case COLUMN_DESCRIPTION:
                return script.getDescription();
        }
        return null;
    }

    @Override
    public boolean setValue(int column, Object value) {
        switch (column) {
            case COLUMN_USE:
                setUse((Boolean) value);
                return true;
        }
        return false;
    }

    public Script getScript() {
        return script;
    }

    public boolean isUse() {
        return use;
    }

    public void setUse(boolean use) {
        this.use = use;
    }
}
