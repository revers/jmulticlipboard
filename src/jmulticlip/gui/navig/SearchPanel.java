/*
 * SearchPanel.java
 *
 * Created on 2011-07-25, 20:28:10
 */
package jmulticlip.gui.navig;

import java.awt.*;
import java.awt.event.*;
import java.util.Locale;

import javax.swing.*;

import javax.swing.border.*;
import javax.swing.event.*;
import jmulticlip.core.InitializationManager;
import jmulticlip.gui.MainFrame;
import jmulticlip.gui.shortcut.*;
import jmulticlip.i18n.I18NManager;
import jmulticlip.ifaces.Initializable;
import jmulticlip.util.GUIUtils;
import pl.ugu.revers.logging.i18n.Internationalizable;
import pl.ugu.revers.swing.ExtendedFormattedTextField;

/**
 *
 * @author Revers
 */
public class SearchPanel extends ShortcutPanel implements Internationalizable,
        ActionListener, FocusListener, Initializable {

    private I18NManager i18n = I18NManager.getInstance();
    private JCheckBox caseSensitiveCB = new JCheckBox("Uwzdlędnij wielkość liter");
    private JCheckBox regexpCB = new JCheckBox("Wyrażenie regularne");
    private JButton moreJB = new JButton("Więcej...");
    private JPopupMenu popup;

    /** Creates new form SearchPanel */
    public SearchPanel() {
        initComponents();

        popup = new JPopupMenu();
        OptionsPopupMenu mme = new OptionsPopupMenu();
        popup.add(mme);

        regexpCB.addActionListener(this);
        caseSensitiveCB.addActionListener(this);

        i18n.addInternationalizable(this);

        optionsHB.setText("Advanced search");

        regexpCB.setFocusable(false);
        caseSensitiveCB.setFocusable(false);
        moreJB.setFocusable(false);
        //  addShortcuts();

        searchTF.addFocusListener(this);

        InitializationManager.getInstance().addInitializable(this);
    }

    @Override
    public void init() {
        addShortcuts();
    }

    private void addShortcuts() {
        MainFrame mf = MainFrame.getInstance();
        ShortcutManager shortcutMgr = ShortcutManager.getInstance();
        JComponent parent = mf.getNavigationPanel();

        //------------------------------------------------------
        KeyStroke focusSearchKS = KeyStroke.getKeyStroke(KeyEvent.VK_F, KeyEvent.CTRL_MASK, true);
        AbstractShortcutAction focusSearch = new AbstractShortcutAction(Location.MAIN_WINDOW_NAVIGATOR,
                parent, ShortcutScopeEnum.LOCATION_SCOPE, "searchFocus", focusSearchKS) {

            public void actionPerformed(ActionEvent e) {
                searchTF.requestFocusInWindow();
            }
        };

        shortcutMgr.registerShortcutAction(focusSearch);

        //------------------------------------------------------
        KeyStroke clearSearchKS = KeyStroke.getKeyStroke(KeyEvent.VK_C, KeyEvent.ALT_MASK, true);
        AbstractShortcutAction clearSearch = new AbstractShortcutAction(Location.MAIN_WINDOW_NAVIGATOR,
                parent, ShortcutScopeEnum.LOCATION_SCOPE, "clearSearch", clearSearchKS) {

            public void actionPerformed(ActionEvent e) {
                clearSearchAction(e);
            }
        };

        shortcutMgr.registerShortcutAction(clearSearch);
        clearJB.addActionListener(clearSearch);
        //------------------------------------------------------ 

        KeyStroke searchActionKS = KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true);
        AbstractShortcutAction searchAction = new AbstractShortcutAction(Location.MAIN_WINDOW_NAVIGATOR,
                searchTF, ShortcutScopeEnum.LOCATION_SCOPE, "clearSearch", searchActionKS) {

            public void actionPerformed(ActionEvent e) {
                searchAction(e);
            }
        };

        shortcutMgr.registerShortcutAction(searchAction);

        //searchTF.addActionListener(serachAction);
        searchJB.addActionListener(searchAction);
        //------------------------------------------------------ 

        ShortcutLabel focusSearchLabel = new ShortcutLabel(focusSearch, searchTF);
        focusSearchLabel.setTextOffsetY(GUIUtils.SHORTCUT_Y_OFFSET);
        addShortcutLabel(focusSearchLabel);

        ShortcutLabel searchLabel = new ShortcutLabel(searchAction, searchJB);
        searchLabel.setDrawFromRight(true);
        searchLabel.setTextOffsetY(GUIUtils.SHORTCUT_Y_OFFSET);
        addShortcutLabel(searchLabel);

        ShortcutLabel clearSearchLabel = new ShortcutLabel(clearSearch, clearJB);
        clearSearchLabel.setDrawFromRight(true);
        clearSearchLabel.setTextOffsetY(GUIUtils.SHORTCUT_Y_OFFSET);
        addShortcutLabel(clearSearchLabel);
    }

    private void clearSearchAction(ActionEvent e) {
        System.out.println("clearAction");
    }

    private void searchAction(ActionEvent e) {
        System.out.println("searchAction");
    }

    public JCheckBox getCaseSensitiveCB() {
        return caseSensitiveCB;
    }

    public JButton getClearJB() {
        return clearJB;
    }

    public JButton getMoreJB() {
        return moreJB;
    }

    public HyperlinkButton getOptionsHB() {
        return optionsHB;
    }

    public JCheckBox getRegexpCB() {
        return regexpCB;
    }

    public JButton getSearchJB() {
        return searchJB;
    }

    public ExtendedFormattedTextField getSearchTF() {
        return searchTF;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (regexpCB.isSelected() || caseSensitiveCB.isSelected()) {
            optionsHB.setText("Advanced search (Modified)");
        } else {
            optionsHB.setText("Advanced search");
        }
    }

    @Override
    public void changeLanguage(Locale language) {
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        optionsHB = new jmulticlip.gui.navig.HyperlinkButton();
        searchTF = new pl.ugu.revers.swing.ExtendedFormattedTextField();
        searchJB = new javax.swing.JButton();
        clearJB = new javax.swing.JButton();

        setLayout(new java.awt.GridBagLayout());

        optionsHB.setText("Advanced Search (Modfied)");
        optionsHB.setFocusable(false);
        optionsHB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                optionsHBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        add(optionsHB, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.SOUTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(16, 0, 0, 0);
        add(searchTF, gridBagConstraints);

        searchJB.setText("Search");
        searchJB.setFocusable(false);
        searchJB.setMinimumSize(new java.awt.Dimension(65, 20));
        searchJB.setPreferredSize(new java.awt.Dimension(65, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.SOUTH;
        gridBagConstraints.insets = new java.awt.Insets(16, 5, 0, 0);
        add(searchJB, gridBagConstraints);

        clearJB.setText("Clear");
        clearJB.setEnabled(false);
        clearJB.setFocusable(false);
        clearJB.setMinimumSize(new java.awt.Dimension(57, 20));
        clearJB.setPreferredSize(new java.awt.Dimension(57, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.SOUTH;
        gridBagConstraints.insets = new java.awt.Insets(16, 5, 0, 0);
        add(clearJB, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void optionsHBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_optionsHBActionPerformed
        SwingUtilities.updateComponentTreeUI(popup);
        popup.show(this, optionsHB.getX(), optionsHB.getY() + 20);
    }//GEN-LAST:event_optionsHBActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton clearJB;
    private jmulticlip.gui.navig.HyperlinkButton optionsHB;
    private javax.swing.JButton searchJB;
    private pl.ugu.revers.swing.ExtendedFormattedTextField searchTF;
    // End of variables declaration//GEN-END:variables

    @Override
    public void focusGained(FocusEvent e) {
        MainFrame.getInstance().getNavigationPanel().getNavigTablePanel().setTabVisible(true);
    }

    @Override
    public void focusLost(FocusEvent e) {
        MainFrame.getInstance().getNavigationPanel().getNavigTablePanel().setTabVisible(false);
    }

    class OptionsPopupMenu extends JPanel implements MenuElement {

        public OptionsPopupMenu() {
            //setBackground(Color.red);
            setLayout(new GridLayout(3, 1, 5, 5));
            int insets = 3;
            setBorder(new EmptyBorder(insets, insets, insets, insets));
            Dimension dim = new Dimension(200, 70);
            setPreferredSize(dim);
            add(caseSensitiveCB);
            add(regexpCB);

            moreJB.setBorderPainted(false);
            add(moreJB);
        }

        @Override
        public Component getComponent() {
            return this;
        }

        @Override
        public MenuElement[] getSubElements() {
            return new MenuElement[0];
        }

        @Override
        public void menuSelectionChanged(boolean isIncluded) {
        }

        @Override
        public void processMouseEvent(MouseEvent e, MenuElement path[],
                MenuSelectionManager manager) {
        }

        @Override
        public void processKeyEvent(KeyEvent e, MenuElement path[],
                MenuSelectionManager manager) {
        }
    }
}
