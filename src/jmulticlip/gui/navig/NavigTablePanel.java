/*
 * NavigTablePanel.java
 *
 * Created on 2011-07-25, 20:29:15
 */
package jmulticlip.gui.navig;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.util.*;

import javax.swing.*;

import java.io.*;
import javax.swing.border.*;
import javax.swing.event.*;
import jmulticlip.clipboard.ClipDataType;
import jmulticlip.clipboard.ClipboardManager;
import jmulticlip.clipboard.FileSelection;
import jmulticlip.core.ErrorManager;
import jmulticlip.core.InitializationManager;
import jmulticlip.gui.MainFrame;
import jmulticlip.gui.ViewType;
import jmulticlip.gui.graph.CanvasPanel;
import jmulticlip.gui.graph.GraphicsViewPanel;
import jmulticlip.gui.shortcut.*;
import jmulticlip.gui.text.TextViewPanel;
import jmulticlip.i18n.I18NManager;
import jmulticlip.ifaces.Initializable;
import jmulticlip.ifaces.Resetable;
import jmulticlip.scripts.Script;
import jmulticlip.scripts.ScriptRunner;
import jmulticlip.util.GUIUtils;
import pl.ugu.revers.logging.i18n.Internationalizable;
import pl.ugu.revers.swing.table.*;

/**
 *
 * @author Revers
 */
public class NavigTablePanel extends ShortcutPanel implements Internationalizable,
        Resetable, Initializable, ListSelectionListener {

    private I18NManager i18n = I18NManager.getInstance();
    private static final Column[] COLUMNS = {
        new Column(Integer.class, "#", false),
        new Column(String.class, "Typ", false),
        new Column(String.class, "Fragment", false),
        new Column(Boolean.class, "Zablokuj", true),};// "Lock"
    private ExtendedTableModel<ClipDataRow> clipTableModel =
            new ExtendedTableModel<ClipDataRow>(COLUMNS);
    private ExtendedTableUtils tableUtils;
    private ClipDataRow currentClipDataRow;
    private ShortcutLabel tabLabel;

    /** Creates new form NavigTablePanel */
    public NavigTablePanel() {
        initComponents();
        // addShortcuts();
        addListeners();

        i18n.addInternationalizable(this);
        InitializationManager.getInstance().addResetable(this);
        InitializationManager.getInstance().addInitializable(this);

        GUIUtils.makeButtonFlatWithBoreder(moveTopJB);
        GUIUtils.makeButtonFlatWithBoreder(moveBottomJB);
        GUIUtils.makeButtonFlatWithBoreder(moveDownJB);
        GUIUtils.makeButtonFlatWithBoreder(moveUpJB);
        GUIUtils.makeButtonFlatWithBoreder(deleteJB);
    }

    public String[] getSelecteRows() {

        int[] selectedIndices = clipTable.getSelectedRows();
        String[] result = new String[selectedIndices.length];
        for (int i = 0; i < result.length; i++) {
            ClipDataRow row = clipTableModel.getDataRow(selectedIndices[i]);
            result[i] = row.getFullText();
        }

        return result;
    }

    @Override
    public void init() {
        addShortcuts();
    }

    public void addShortcuts() {
        MainFrame mf = MainFrame.getInstance();
        ShortcutManager shortcutMgr = ShortcutManager.getInstance();
        JComponent parent = mf.getNavigationPanel();

        //------------------------------------------------------
        ActionMap actionMap = clipTable.getActionMap();
        //------------------------------------------------------
        Action selectNextRowAction = actionMap.get("selectNextRow");

        KeyStroke selectNextKS = KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, 0, false);
        DefaultShortcutAction selectNext = new DefaultShortcutAction(Location.MAIN_WINDOW_NAVIGATOR,
                parent, ShortcutScopeEnum.LOCATION_SCOPE, "selectNext", selectNextKS, selectNextRowAction);
        selectNext.setSource(clipTable);

        shortcutMgr.registerShortcutAction(selectNext);
        //------------------------------------------------------
        Action selectPrevRowAction = actionMap.get("selectPreviousRow");

        KeyStroke selectPrevKS = KeyStroke.getKeyStroke(KeyEvent.VK_UP, 0, false);
        DefaultShortcutAction selectPrev = new DefaultShortcutAction(Location.MAIN_WINDOW_NAVIGATOR,
                parent, ShortcutScopeEnum.LOCATION_SCOPE, "selectPreviousRow", selectPrevKS, selectPrevRowAction);
        selectPrev.setSource(clipTable);

        shortcutMgr.registerShortcutAction(selectPrev);
        //------------------------------------------------------
        Action selectNextRowExtendSelectionAction = actionMap.get("selectNextRowExtendSelection");

        KeyStroke selectNextRowExtendSelectionKS = KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, KeyEvent.SHIFT_MASK, false);
        DefaultShortcutAction selectNextRowExtendSelection = new DefaultShortcutAction(Location.MAIN_WINDOW_NAVIGATOR,
                parent, ShortcutScopeEnum.LOCATION_SCOPE, "selectNextRowExtendSelection",
                selectNextRowExtendSelectionKS, selectNextRowExtendSelectionAction);
        selectNextRowExtendSelection.setSource(clipTable);

        shortcutMgr.registerShortcutAction(selectNextRowExtendSelection);
        //------------------------------------------------------
        Action selectPreviousRowExtendSelectionAction = actionMap.get("selectPreviousRowExtendSelection");

        KeyStroke selectPreviousRowExtendSelectionKS = KeyStroke.getKeyStroke(KeyEvent.VK_UP, KeyEvent.SHIFT_MASK, false);
        DefaultShortcutAction selectPreviousRowExtendSelection = new DefaultShortcutAction(Location.MAIN_WINDOW_NAVIGATOR,
                parent, ShortcutScopeEnum.LOCATION_SCOPE, "selectPreviousRowExtendSelection",
                selectPreviousRowExtendSelectionKS, selectPreviousRowExtendSelectionAction);
        selectPreviousRowExtendSelection.setSource(clipTable);

        shortcutMgr.registerShortcutAction(selectPreviousRowExtendSelection);
        //------------------------------------------------------
        Action selectAllAction = actionMap.get("selectAll");

        KeyStroke selectAllKS = KeyStroke.getKeyStroke(KeyEvent.VK_A, KeyEvent.CTRL_MASK, false);
        DefaultShortcutAction selectAll = new DefaultShortcutAction(Location.MAIN_WINDOW_NAVIGATOR,
                parent, ShortcutScopeEnum.LOCATION_SCOPE, "selectAll",
                selectAllKS, selectAllAction);

        selectAll.setSource(clipTable);
        shortcutMgr.registerShortcutAction(selectAll);
        //------------------------------------------------------

        KeyStroke pasteAndHideKS = KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true);
        AbstractShortcutAction pasteAndHide = new AbstractShortcutAction(Location.MAIN_WINDOW_NAVIGATOR,
                parent, ShortcutScopeEnum.LOCATION_SCOPE, "pastAndHide", pasteAndHideKS) {

            public void actionPerformed(ActionEvent e) {
                if (MainFrame.getInstance().isBlockShortcuts()) {
                    return;
                }
                pastAndHideAction(e);
            }
        };

        shortcutMgr.registerShortcutAction(pasteAndHide);
        //------------------------------------------------------
        KeyStroke copyAndHideKS = KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, KeyEvent.SHIFT_MASK, true);
        AbstractShortcutAction copyAndHide = new AbstractShortcutAction(Location.MAIN_WINDOW_NAVIGATOR,
                parent, ShortcutScopeEnum.LOCATION_SCOPE, "copyAndHide", copyAndHideKS) {

            public void actionPerformed(ActionEvent e) {
                if (MainFrame.getInstance().isBlockShortcuts()) {
                    return;
                }
                copyAndHideAction(e);
            }
        };

        shortcutMgr.registerShortcutAction(copyAndHide);
        //------------------------------------------------------
        KeyStroke runSriptsInEditorKS = KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, KeyEvent.CTRL_MASK, true);
        AbstractShortcutAction runSriptsInEditor = new AbstractShortcutAction(Location.MAIN_WINDOW_NAVIGATOR,
                parent, ShortcutScopeEnum.LOCATION_SCOPE, "runSriptsInEditor", runSriptsInEditorKS) {

            public void actionPerformed(ActionEvent e) {
                if (MainFrame.getInstance().isBlockShortcuts()) {
                    return;
                }
                runSriptsInEditorAction(e);
            }
        };

        shortcutMgr.registerShortcutAction(runSriptsInEditor);
        //------------------------------------------------------

        KeyStroke tabKS = KeyStroke.getKeyStroke(KeyEvent.VK_TAB, 0, true);
        AbstractShortcutAction tab = new AbstractShortcutAction(Location.MAIN_WINDOW_NAVIGATOR,
                parent, ShortcutScopeEnum.LOCATION_SCOPE, "tab", tabKS) {

            public void actionPerformed(ActionEvent e) {
            }
        };

        //------------------------------------------------------

        ShortcutLabel pasteAndHideLabel = new ShortcutLabel(selectPrev, clipTableScroll);
        pasteAndHideLabel.addShortcut(selectNext);
        pasteAndHideLabel.addShortcut(pasteAndHide);
        pasteAndHideLabel.addShortcut(copyAndHide);
        pasteAndHideLabel.addShortcut(runSriptsInEditor);
        pasteAndHideLabel.setTextOffsetY(GUIUtils.SHORTCUT_Y_OFFSET);
        pasteAndHideLabel.setDrawFromRight(true);
        addShortcutLabel(pasteAndHideLabel);

        tabLabel = new ShortcutLabel(tab, clipTableScroll);
        tabLabel.setTextOffsetY(GUIUtils.SHORTCUT_Y_OFFSET);
        tabLabel.setShowShortcut(false);
        addShortcutLabel(tabLabel);
    }

    public void setTabVisible(boolean visible) {
        tabLabel.setShowShortcut(visible);
        repaint();
    }

    private void pastAndHideAction(ActionEvent e) {

        System.out.println("pasteAndHideAction");
        copyAndHideAction(e);
        
        MainFrame mf = MainFrame.getInstance();
        mf.triggerPaste();
    }

    private void copyAndHideAction(ActionEvent e) {
        System.out.println("copyAndHideAction");
        MainFrame mf = MainFrame.getInstance();

        String[] selectedRows = getSelecteRows();
        if (selectedRows.length == 0) {
            mf.setVisible(false);
            return;
        }

        ClipboardManager clipManager = ClipboardManager.getInstance();

        if (mf.getViewContainer().getView().getType() == ViewType.IMAGE_EDITOR) {

            GraphicsViewPanel graphicsView = (GraphicsViewPanel) mf.getViewContainer().getView();
            Image img = graphicsView.getImage();
//            System.out.println("222imag = " + img + "; width = "
//                    + img.getWidth(null) + "; height = " + img.getHeight(null) + "\n\t" + img.getSource());
            clipManager.setClipboardImage(img);

            mf.setVisible(false);
            return;
        }

        Script[] scripts = mf.getNavigationPanel().getScriptsPanel().getSelectedScripts();

        String result = null;

        try {
            result = ScriptRunner.getInstance().runScirpts(scripts, selectedRows);
        } catch (Exception ex) {
            ErrorManager.errorNormal(NavigTablePanel.class, ex, "Cannot execute scripts!!");

            StringBuilder sb = new StringBuilder();

            sb.append(selectedRows[0]);
            
            for (int i = 1; i < selectedRows.length; i++) {
                sb.append('\n').append(selectedRows[i]);
            }
            result = sb.toString();
        }

        clipManager.setClipbardText(result);
        mf.setVisible(false);

    }

    private void runSriptsInEditorAction(ActionEvent e) {
        System.out.println("runSriptsInEditorAction");
    }

    @Override
    public void reset() {
        if (clipTableModel.getRowCount() > 0) {
            clipTable.removeRowSelectionInterval(0, clipTable.getRowCount() - 1);
            clipTable.setRowSelectionInterval(0, 0);
        }
    }

    public void addTestData() {
        MainFrame mf = MainFrame.getInstance();
        ClipDataRow row = new ClipDataRow(mf.getDefaultTextView(), "tableUtils = new ExtendedTableUtils(clipTable, clipTableModel);\n"
                + "        tableUtils.setHeaderSelectableColumns(ClipDataRow.COLUMN_KEEP_FIXED_POSITION);\n"
                + "        tableUtils.addShortcuts();\n"
                + "\n"
                + "        tableUtils.setBoldHeader();\n"
                + "        tableUtils.setReorderingAllowed(false);\n"
                + "        \n"
                + "        tableUtils.addDoubleClickResizeing();\n"
                + "        clipTable.setRowHeight(clipTable.getRowHeight() + 2);\n"
                + "        clipTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);\n"
                + "\n"
                + "        LabelCellRenderer labelCellRenderer = new LabelCellRenderer();");
        clipTableModel.addRow(row);
    }

    private void initTable() {
        clipTable.setName("clipTable");
        tableUtils = new ExtendedTableUtils(clipTable, clipTableModel);
        tableUtils.setHeaderSelectableColumns(ClipDataRow.COLUMN_KEEP_FIXED_POSITION);
        // tableUtils.addShortcuts();

        tableUtils.setBoldHeader();
        tableUtils.setReorderingAllowed(false);

        tableUtils.addDoubleClickResizeing();
        clipTable.setRowHeight(clipTable.getRowHeight() + 2);
        clipTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        LabelCellRenderer labelCellRenderer = new LabelCellRenderer();
        tableUtils.setColumnCellRenderer(ClipDataRow.COLUMN_INDEX, labelCellRenderer);
        tableUtils.setColumnCellRenderer(ClipDataRow.COLUMN_SHORT_TEXT, labelCellRenderer);
        tableUtils.setColumnCellRenderer(ClipDataRow.COLUMN_KEEP_FIXED_POSITION, new CheckBoxCellRenderer());

        tableUtils.setColumnCellRenderer(ClipDataRow.COLUMN_TYPE, new ClipImageCellRenderer());
        tableUtils.setInitColumnSizePercent(0.1f, 0.1f, 0.65f, 0.15f);
    }

    private void enumerateRows() {
        ArrayList<ClipDataRow> rows = clipTableModel.getDataList();
        for (int i = 0; i < rows.size(); i++) {
            rows.get(i).setIndex(i + 1);
        }
    }

    public ClipDataRow getCurrentClipDataRow() {
        return currentClipDataRow;
    }

    public void addClipDataRow(ClipDataRow cd) {
        currentClipDataRow = cd;

        // TODO: 
        //    MainFrame mf = MainFrame.getInstance();
        //    mf.getEditorPanel().setView(cd.getCurrentView());

        clipTableModel.addRowAtBeginning(cd);
        enumerateRows();
        clipTable.removeRowSelectionInterval(0, clipTable.getRowCount() - 1);
        clipTable.setRowSelectionInterval(0, 0);
        clipTable.repaint();
    }

    public void addTextData(String text) {
        MainFrame mf = MainFrame.getInstance();
        ClipDataRow cd = new ClipDataRow(mf.getDefaultTextView(), text);
        currentClipDataRow = cd;
        // mf.getEditorPanel().setView(cd.getCurrentView());
        //setTextViewAsCurrent();

        clipTableModel.addRowAtBeginning(cd);
        enumerateRows();
        clipTable.removeRowSelectionInterval(0, clipTable.getRowCount() - 1);
        clipTable.setRowSelectionInterval(0, 0);
        clipTable.repaint();
    }

    public void selectFirstRow() {
        if (clipTableModel.getRowCount() == 0) {
            return;
        }
        if (clipTable.getRowCount() > 1) {
            clipTable.removeRowSelectionInterval(1, clipTable.getRowCount() - 1);
        }
        // obsluzone przez selection listener
        clipTable.addRowSelectionInterval(0, 0);

        // TODO: 
//        MainFrame mf = MainFrame.getInstance();
//        //  mf.get.setText(clipTableModel.getDataRow(0).getFullText());
//        mf.setViewFor(clipTableModel.getDataRow(0));
//        clipTable.requestFocusInWindow();
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {

        MainFrame mf = MainFrame.getInstance();
        if (e.getValueIsAdjusting() == false) {

            if (clipTable.getSelectedRow() != -1) {

                int[] rows = clipTable.getSelectedRows();

                ClipDataRow row = clipTableModel.getDataRow(
                        rows[0]);

                currentClipDataRow = row;

                if (row.getType() == ClipDataType.IMAGE) {
                    mf.setViewFor(row);
                    mf.getNavigationPanel().getScriptsPanel().setEnabled(false);
                    clipTable.requestFocusInWindow();
                } else {
                    mf.getNavigationPanel().getScriptsPanel().setEnabled(true);
                    mf.setViewFor(row);
                    TextViewPanel textView = ((TextViewPanel) row.getDefaultView());
                    for (int i = 1; i < rows.length; i++) {
                        textView.appendText("\n"
                                + clipTableModel.getDataRow(
                                rows[i]).getFullText());
                    }
                    clipTable.requestFocusInWindow();
                }

                if (currentClipDataRow.getType() == ClipDataType.FILE_LIST) {
                    MainFrame.getInstance().getDefaultTextView().getTextToolbarPanel().setCopyAsFilesSelected(true);
                } else {
                    MainFrame.getInstance().getDefaultTextView().getTextToolbarPanel().setCopyAsFilesSelected(false);
                }

            }
        }

    }

    private void addListeners() {

        clipTable.getSelectionModel().addListSelectionListener(this);

//         clipTable.addKeyListener(new KeyListener() {
//
//            @Override
//            public void keyPressed(KeyEvent e) {
//
//                if (e.getKeyCode() == KeyEvent.VK_SHIFT) {
//                    shiftPressed = true;
//                } else if (e.getKeyCode() == KeyEvent.VK_ENTER) {
//                    e.consume();
//                    copyAction();
//                }
//            }
//
//            @Override
//            public void keyReleased(KeyEvent e) {
//                if (e.getKeyCode() == KeyEvent.VK_SHIFT) {
//                    shiftPressed = false;
//                }
//                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
//                    e.consume();
//                }
//            }
//
//            @Override
//            public void keyTyped(KeyEvent e) {
//                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
//                    e.consume();
//                }
//            }
//        });
//
//       
//
//        moveUpJB.addActionListener(new ActionListener() {
//
//            @Override
//            public void actionPerformed(ActionEvent e) {
////                clipTable.printColumnSizes();
//                System.out.println(getSize());
//                System.out.println("dividerLocation = "
//                        + splitPane.getDividerLocation());
//                rightScrollPane.getHorizontalScrollBar().setValue(
//                        rightScrollPane.getHorizontalScrollBar().getMinimum());
//                goUpAction();
//            }
//        });
//
//        moveDownJB.addActionListener(new ActionListener() {
//
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                goDownAction();
//            }
//        });
//
//        moveTopJB.addActionListener(new ActionListener() {
//
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                goTopAction();
//            }
//        });
//
//        moveBottomJB.addActionListener(new ActionListener() {
//
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                goBottomAction();
//            }
//        });
//
//        deleteJB.addActionListener(new ActionListener() {
//
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                deleteAction();
//            }
//        });
    }
//    
//    private void goDownAction() {
//        int[] selRows = clipTable.getSelectedRows();
//        int lastRow = clipTable.getRowCount() - 1;
//
//        boolean lastRows = true;
//        for (int i = selRows.length - 1, j = lastRow; i >= 0; i--, j--) {
//            if (selRows[i] != j) {
//                lastRows = false;
//                break;
//            }
//        }
//        if (lastRows) {
//            return;
//        }
//
//        for (int i = selRows.length - 1; i >= 0; i--) {
//            int row = selRows[i];
//
//            if (row == lastRow) {
//                continue;
//            }
//
//            clipTableModel.swapRows(row, row + 1);
//            clipTable.removeRowSelectionInterval(row, row);
//            clipTable.addRowSelectionInterval(row + 1, row + 1);
//        }
//    }
//
//    private void goBottomAction() {
//        int[] selRows = clipTable.getSelectedRows();
//        int lastRow = clipTable.getRowCount() - 1;
//        int moveRow = lastRow;
//
//        for (int i = selRows.length - 1; i >= 0; i--) {
//            int row = selRows[i];
//
//            clipTableModel.move(row, moveRow);
//            clipTable.removeRowSelectionInterval(row, row);
//            clipTable.addRowSelectionInterval(moveRow, moveRow);
//
//            moveRow--;
//            if (moveRow < 0) {
//                moveRow = 0;
//            }
//        }
//        leftScrollPane.getVerticalScrollBar().setValue(Integer.MIN_VALUE);
//    }
//
//    private void goTopAction() {
//        int[] selRows = clipTable.getSelectedRows();
//        int moveRow = 0;
//        int lastRow = clipTable.getRowCount() - 1;
//
//        for (int i = 0; i < selRows.length; i++) {
//            int row = selRows[i];
//
//            clipTableModel.move(row, moveRow);
//            clipTable.removeRowSelectionInterval(row, row);
//            clipTable.addRowSelectionInterval(moveRow, moveRow);
//
//            moveRow++;
//            if (moveRow > lastRow) {
//                moveRow = lastRow;
//            }
//        }
//
//        leftScrollPane.getVerticalScrollBar().setValue(Integer.MAX_VALUE);
//    }
//
//    private void goUpAction() {
//        int[] selRows = clipTable.getSelectedRows();
//
//        boolean firstRows = true;
//        for (int i = 0; i < selRows.length; i++) {
//            if (selRows[i] != i) {
//                firstRows = false;
//                break;
//            }
//        }
//        if (firstRows) {
//            return;
//        }
//
//        for (int row : selRows) {
//            if (row == 0) {
//                continue;
//            }
//            clipTableModel.swapRows(row, row - 1);
//            clipTable.removeRowSelectionInterval(row, row);
//            clipTable.addRowSelectionInterval(row - 1, row - 1);
//        }
//
////        clipTable.printColumnSizes();
//    }
//
//    private void deleteAction() {
//        // if (!removeSelectedJB.isEnabled())
//        // return;
//        int[] selRows = clipTable.getSelectedRows();
//        if (selRows.length == 0) {
//            return;
//        }
//        clipTable.removeRowSelectionInterval(0, clipTable.getRowCount() - 1);
//
//        // for (int i : selRows) {
//        // File f = clipTable.getUploadTableModel().getUploadFile(i);
//        // totalSize -= f.length();
//        // }
//
//        clipTableModel.removeRows(selRows);
//
//        if (clipTable.getRowCount() > 0) {
//            clipTable.setRowSelectionInterval(0, 0);
//        }
//    }

    private void copyFilesAction() {
        int[] indices = clipTable.getSelectedRows();
        ArrayList<File> fileList = new ArrayList<File>();
        for (int i = 0; i < indices.length; i++) {
            ClipDataRow cd = clipTableModel.getDataRow(indices[i]);
            if (cd.getType() != ClipDataType.FILE_LIST) {
                JOptionPane.showMessageDialog(this, "Zaznaczony wiersz ("
                        + (indices[i] + 1) + ") nie zawiera plik�w!!",
                        "B��d kopiowania plik�w!!", JOptionPane.ERROR_MESSAGE);
                return;
            }

            String[] lines = cd.getFullText().split("\n");
            for (String line : lines) {
                File file = new File(line.trim());
                if (file.exists() == false) {
                    JOptionPane.showMessageDialog(this, "Plik " + line
                            + "\nz wiersza nr " + (indices[i] + 1)
                            + " nie istnieje!!", "B��d kopiowania plik�w!!",
                            JOptionPane.ERROR_MESSAGE);
                    continue;
                }
                fileList.add(file);
            }
        }

        if (fileList.isEmpty() == false) {
            ClipboardManager.getInstance().setClipboardFiles(fileList);
        }
    }

    @Override
    public void changeLanguage(Locale language) {
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        moveTopJB = new javax.swing.JButton();
        moveDownJB = new javax.swing.JButton();
        moveBottomJB = new javax.swing.JButton();
        deleteJB = new javax.swing.JButton();
        moveUpJB = new javax.swing.JButton();
        clipTableScroll = new javax.swing.JScrollPane();
        clipTable = new javax.swing.JTable();

        setPreferredSize(new java.awt.Dimension(250, 300));
        setLayout(new java.awt.GridBagLayout());

        moveTopJB.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/go-top.png"))); // NOI18N
        moveTopJB.setFocusable(false);
        moveTopJB.setMaximumSize(new java.awt.Dimension(49, 20));
        moveTopJB.setMinimumSize(new java.awt.Dimension(30, 20));
        moveTopJB.setPreferredSize(new java.awt.Dimension(30, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        add(moveTopJB, gridBagConstraints);

        moveDownJB.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/go-down.png"))); // NOI18N
        moveDownJB.setFocusable(false);
        moveDownJB.setMaximumSize(new java.awt.Dimension(49, 20));
        moveDownJB.setMinimumSize(new java.awt.Dimension(30, 20));
        moveDownJB.setPreferredSize(new java.awt.Dimension(30, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        add(moveDownJB, gridBagConstraints);

        moveBottomJB.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/go-bottom.png"))); // NOI18N
        moveBottomJB.setFocusable(false);
        moveBottomJB.setMaximumSize(new java.awt.Dimension(49, 20));
        moveBottomJB.setMinimumSize(new java.awt.Dimension(30, 20));
        moveBottomJB.setPreferredSize(new java.awt.Dimension(30, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        add(moveBottomJB, gridBagConstraints);

        deleteJB.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/delete.png"))); // NOI18N
        deleteJB.setFocusable(false);
        deleteJB.setMaximumSize(new java.awt.Dimension(49, 20));
        deleteJB.setMinimumSize(new java.awt.Dimension(30, 20));
        deleteJB.setPreferredSize(new java.awt.Dimension(30, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        add(deleteJB, gridBagConstraints);

        moveUpJB.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/go-up.png"))); // NOI18N
        moveUpJB.setFocusable(false);
        moveUpJB.setMaximumSize(new java.awt.Dimension(49, 20));
        moveUpJB.setMinimumSize(new java.awt.Dimension(30, 20));
        moveUpJB.setPreferredSize(new java.awt.Dimension(30, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        add(moveUpJB, gridBagConstraints);

        clipTableScroll.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        clipTableScroll.setFocusable(false);

        clipTable.setModel(clipTableModel);
        clipTable.setFillsViewportHeight(true);
        clipTable.setFocusable(false);
        initTable();
        clipTableScroll.setViewportView(clipTable);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 6;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(13, 0, 5, 0);
        add(clipTableScroll, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable clipTable;
    private javax.swing.JScrollPane clipTableScroll;
    private javax.swing.JButton deleteJB;
    private javax.swing.JButton moveBottomJB;
    private javax.swing.JButton moveDownJB;
    private javax.swing.JButton moveTopJB;
    private javax.swing.JButton moveUpJB;
    // End of variables declaration//GEN-END:variables
}
