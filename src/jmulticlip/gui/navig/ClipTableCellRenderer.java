package jmulticlip.gui.navig;
import java.awt.Color;
import java.awt.Component;
import java.awt.Insets;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableCellRenderer;

public class ClipTableCellRenderer extends DefaultTableCellRenderer {
	private static final Color COLOR = new Color(230, 248, 255);
	private static final EmptyBorder BORDER = new EmptyBorder(new Insets(10,
			10, 10, 10));

	public Component getTableCellRendererComponent(JTable table, Object value,
			boolean isSelected, boolean hasFocus, int row, int column) {

		Component c = super.getTableCellRendererComponent(table, value,
				isSelected, hasFocus, row, column);

		JLabel label = ((JLabel) c);
		label.setBorder(BORDER);

		label.setHorizontalAlignment(JLabel.CENTER);
//		if(row % 2 == 0)
//			label.setBackground(COLOR);
//		else 
//			label.setBackground(Color.white);

		return c;
	}
}
