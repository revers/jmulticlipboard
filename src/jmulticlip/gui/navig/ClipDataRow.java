package jmulticlip.gui.navig;

import jmulticlip.clipboard.ClipDataType;
import jmulticlip.gui.View;
import jmulticlip.gui.graph.CanvasPanel;
import pl.ugu.revers.swing.table.*;

/**
 *
 * @author Revers
 */
public class ClipDataRow implements DataRow {

    private static final int MAX_CHARS = 45;
    public static final int COLUMN_INDEX = 0;
    public static final int COLUMN_TYPE = 1;
    public static final int COLUMN_SHORT_TEXT = 2;
    public static final int COLUMN_KEEP_FIXED_POSITION = 3;
    private int index;
    private ClipDataType type;
    private String shortText;
    private boolean keepFixedPosition;
    private String fullText;
    private CanvasPanel canvas;
    private View defaultView;
    private View currentView;

    public ClipDataRow(View defaultView, String fullText) {
        this(defaultView, 0, fullText);
    }

    public ClipDataRow(View defaultView, String fullText, ClipDataType type) {
        this(defaultView, 0, fullText, type);
    }

    public ClipDataRow(View defaultView, int index, String fullText) {
        this(defaultView, index, fullText, ClipDataType.TEXT);
    }

    public ClipDataRow(View defaultView, int index, String fullText, ClipDataType type) {
        if (fullText.length() > MAX_CHARS) {
            shortText = fullText.substring(0, MAX_CHARS);
        } else {
            shortText = fullText;
        }
        this.defaultView = defaultView;
        this.currentView = defaultView;

//        if (type == ClipDataType.FILE_LIST) {
//            shortText = "{F} " + shortText;
//        } else if (type == ClipDataType.IMAGE) {
//            shortText = "{I} " + shortText;
//        }

        this.index = index;
        this.fullText = fullText;
        this.keepFixedPosition = false;
        this.type = type;
    }

    @Override
    public Object getValue(int column) {
        switch (column) {
            case COLUMN_INDEX:
                return getIndex();
            case COLUMN_TYPE:
                return getType();
            case COLUMN_SHORT_TEXT:
                return getShortText();
            case COLUMN_KEEP_FIXED_POSITION:
                return isKeepFixedPosition();
        }
        return null;
    }

    @Override
    public boolean setValue(int column, Object value) {
        switch (column) {
            case COLUMN_INDEX:
                setIndex((Integer) value);
                return true;
            case COLUMN_TYPE:
                setType((ClipDataType) value);
                return true;
            case COLUMN_SHORT_TEXT:
                setShortText((String) value);
                return true;
            case COLUMN_KEEP_FIXED_POSITION:
                setKeepFixedPosition((Boolean) value);
                return true;
        }
        return false;
    }

    public String getFullText() {
        return fullText;
    }

    public void setFullText(String fullText) {
        this.fullText = fullText;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public boolean isKeepFixedPosition() {
        return keepFixedPosition;
    }

    public void setKeepFixedPosition(boolean keepFixedPosition) {
        this.keepFixedPosition = keepFixedPosition;
    }

    public String getShortText() {
        return shortText;
    }

    public void setShortText(String shortText) {
        this.shortText = shortText;
    }

    public ClipDataType getType() {
        return type;
    }

    public void setType(ClipDataType type) {
        this.type = type;
    }

    public CanvasPanel getCanvas() {
        return canvas;
    }

    public void setCanvas(CanvasPanel canvas) {
        this.canvas = canvas;
    }

    public View getCurrentView() {
        return currentView;
    }

    public View getDefaultView() {
        return defaultView;
    }

    public void setCurrentView(View currentView) {
        this.currentView = currentView;
    }
}
