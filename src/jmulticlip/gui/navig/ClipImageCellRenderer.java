package jmulticlip.gui.navig;

import jmulticlip.clipboard.ClipDataType;
import java.awt.Color;
import java.awt.Component;
import java.awt.Insets;
import javax.swing.Icon;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableCellRenderer;

/**
 *
 * @author Revers
 */
public class ClipImageCellRenderer extends DefaultTableCellRenderer {

    private static final EmptyBorder BORDER = new EmptyBorder(new Insets(0,
            4, 0, 4));
    // JLabel lbl = new JLabel();

    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
            boolean hasFocus, int row, int column) {

        Component c = super.getTableCellRendererComponent(table, value,
                isSelected, hasFocus, row, column);

        if (c instanceof JLabel) {
            JLabel label = ((JLabel) c);
            label.setBorder(BORDER);

            label.setText("");
            label.setIcon(((ClipDataType) value).getIcon());
            // label.setHorizontalAlignment(alignment);
        }

        if (isSelected) {
            //  setBackground(table.getSelectionBackground());
            c.setBackground(table.getSelectionBackground());
        } else {
            //setBackground(table.getBackground());
            c.setBackground(table.getBackground());
        }

        c.setEnabled(table.isEnabled());
//    lbl.setText("");
//    lbl.setIcon(((ClipDataType)value).getIcon());
//    return lbl;
        return c;
    }
}
