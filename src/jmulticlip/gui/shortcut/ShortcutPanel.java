package jmulticlip.gui.shortcut;

import java.awt.*;

import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import javax.swing.*;
import javax.swing.border.*;

/**
 *
 * @author Revers
 */
public class ShortcutPanel extends JPanel {

    private static final int DEFAULT_INSETS = 5;
    private ArrayList<ShortcutLabel> shortcuts = new ArrayList<ShortcutLabel>();
    private Color color = new Color(139, 130, 131);
    private Font font = new Font("Arial", Font.PLAIN, 10);

    public ShortcutPanel() {
        setBorder(new EmptyBorder(DEFAULT_INSETS, DEFAULT_INSETS, DEFAULT_INSETS, DEFAULT_INSETS));
    }

    public void addShortcutLabel(ShortcutLabel sc) {
//        for (ShortcutLabel s : shortcuts) {
//            if (s.getS.equals(sc.getId())) {
//                throw new Error("APPLICATION DESIGN ERROR: ShortcutComponent with id='"
//                        + s.getId() + "' already exists!");
//            }
//        }
        shortcuts.add(sc);
    }

    public boolean removeShortcut(ShortcutLabel sc) {
        return shortcuts.remove(sc);
    }

    public ArrayList<ShortcutLabel> getShortcuts() {
        return shortcuts;
    }

    public void setBorderInsets(int insets) {
        setBorder(new EmptyBorder(insets, insets, insets, insets));
    }

    public void setBorderInsets(Insets insets) {
        setBorder(new EmptyBorder(insets));
    }

    @Override
    public void paint(Graphics g) {
//        paintComponent(g);
//        paintBorder(g);
//        paintChildren(g);
        super.paint(g);

        g.setFont(font);
        g.setColor(color);

        Graphics2D g2 = (Graphics2D) g;
        Object oldValue = g2.getRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING);
        g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
                RenderingHints.VALUE_TEXT_ANTIALIAS_ON);

        for (ShortcutLabel sc : shortcuts) {
            if (sc.isShowShortcut() == false) {
                continue;
            }
            if (sc.isDrawFromRight()) {
                FontMetrics fm = g2.getFontMetrics();
                Rectangle2D rect = fm.getStringBounds(sc.getKeyString(), g2);
                JComponent comp = sc.getComponent();
                g2.drawString(sc.getKeyString(),
                        comp.getX() + comp.getWidth() - sc.getTextOffsetX() - (int) rect.getWidth(),
                        comp.getY() - sc.getTextOffsetY());
            } else {
                g2.drawString(sc.getKeyString(), sc.getComponent().getX() + sc.getTextOffsetX(),
                        sc.getComponent().getY() - sc.getTextOffsetY());
            }
        }
        g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, oldValue);
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    @Override
    public Font getFont() {
        return font;
    }

    @Override
    public void setFont(Font font) {
        this.font = font;
    }

    private static void launchTestFrame() {
//        JFrame frame = new JFrame();
//        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//        //    GUIUtils.useApplicationLAF(frame);
//        try {
//            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
//        } catch (Exception ex) {
//            Logger.getLogger(ShortcutBorder.class.getName()).log(Level.SEVERE, null, ex);
//        }
//
//        frame.setLayout(new BorderLayout());
//        // frame.setLayout(new BoxLayout(frame.getContentPane(), BoxLayout.LINE_AXIS));
//
//
//        KeyStroke ks1 = KeyStroke.getKeyStroke(KeyEvent.VK_1, KeyEvent.ALT_MASK, true);
//
//        ShortcutPanel panel = new ShortcutPanel();
//        panel.setLayout(new FlowLayout());
//        panel.add(Box.createVerticalStrut(150));
//
//
//        JButton comp1 = new JButton("dupa");
//        ShortcutLabel sc1 = new ShortcutLabel("sc1", comp1, panel,
//                JComponent.WHEN_IN_FOCUSED_WINDOW, ShortcutScopeEnum.NO_BRACKETS, null, ks1);
//        panel.addShortcut(sc1);
//        panel.add(comp1);
//
//        panel.add(Box.createHorizontalStrut(20));
//
//        KeyStroke ks2a = KeyStroke.getKeyStroke(KeyEvent.VK_UP, KeyEvent.CTRL_MASK, true);
//        KeyStroke ks2b = KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, KeyEvent.CTRL_MASK, true);
//        JCheckBox comp2 = new JCheckBox("some check box");
//
//        ShortcutLabel sc2 = new ShortcutLabel("sc2", comp2, panel,
//                JComponent.WHEN_IN_FOCUSED_WINDOW, ShortcutScopeEnum.PARENT_COMPONENT_SCOPE, null, ks2a);
//        sc2.addKeyStroke(ks2b, null);
//        panel.addShortcut(sc2);
//        panel.add(comp2);
//
//        panel.add(Box.createHorizontalStrut(20));
//
//        KeyStroke ks3 = KeyStroke.getKeyStroke(KeyEvent.VK_K, KeyEvent.SHIFT_MASK, true);
//        JLabel comp3 = new JLabel("label label label");
//        ShortcutLabel sc3 = new ShortcutLabel("sc3", comp3, panel,
//                JComponent.WHEN_IN_FOCUSED_WINDOW, ShortcutScopeEnum.WINDOW_SCOPE, null, ks3);
//        panel.addShortcut(sc3);
//        panel.add(comp3);
//
//        panel.add(Box.createHorizontalStrut(20));
//
//        KeyStroke ks4 = KeyStroke.getKeyStroke(KeyEvent.VK_Z, KeyEvent.SHIFT_MASK | KeyEvent.CTRL_MASK, true);
//        JRadioButton comp4 = new JRadioButton("Radio Button");
//        ShortcutLabel sc4 = new ShortcutLabel("sc4",
//                comp4, panel, JComponent.WHEN_IN_FOCUSED_WINDOW, ShortcutScopeEnum.SYSTEM_SCOPE, null, ks4);
//        panel.addShortcut(sc4);
//        panel.add(comp4);
//
//        panel.add(Box.createHorizontalStrut(20));
//
//        JComboBox comp5 = new JComboBox(new String[]{"czerwony", "zielony", "niebieski"});
//        ShortcutLabel sc5 = new ShortcutLabel("sc5", comp5, panel,
//                JComponent.WHEN_IN_FOCUSED_WINDOW, ShortcutScopeEnum.NO_BRACKETS, null, ks1);
//        panel.addShortcut(sc5);
//        panel.add(comp5);
//
//        frame.add(panel);
//
//        frame.setBounds(200, 200, 800, 600);
//        frame.setVisible(true);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                launchTestFrame();
            }
        });
    }
}
