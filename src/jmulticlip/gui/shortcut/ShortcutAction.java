package jmulticlip.gui.shortcut;

import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.KeyStroke;

/**
 *
 * @author Revers
 */
public interface ShortcutAction extends Action {

    public static final String KEY_STROKE_PROPERTY = "key_stroke";

    public String getActionName();

    public Location getLocation();

    public JComponent getParent();

    public ShortcutScopeEnum getShortcutScope();

    public String getDescription();

    public void setDescription(String description);

    public KeyStroke getKeyStroke();

    public void setKeyStroke(KeyStroke keyStroke);

    public boolean isEditable();

    public void setEditable(boolean editable);
}
