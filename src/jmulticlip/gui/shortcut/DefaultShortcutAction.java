package jmulticlip.gui.shortcut;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.KeyStroke;

/**
 *
 * @author Revers
 */
public class DefaultShortcutAction extends AbstractShortcutAction {

    private ActionListener action;
    private Object source;

    public DefaultShortcutAction(Location location, JComponent parent,
            ShortcutScopeEnum shortcutScope, String actionName,
            KeyStroke keyStroke, ActionListener action) {

        super(location, parent, shortcutScope, actionName, keyStroke);
        this.action = action;
    }

    public Object getSource() {
        return source;
    }

    public void setSource(Object source) {
        this.source = source;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (source == null) {
            action.actionPerformed(e);
        } else {
            ActionEvent evt = new ActionEvent(source, e.getID(),
                    e.getActionCommand(), e.getWhen(), e.getModifiers());
            action.actionPerformed(evt);
        }
    }

    public ActionListener getAction() {
        return action;
    }

    public void setAction(ActionListener action) {
        this.action = action;
    }
}
