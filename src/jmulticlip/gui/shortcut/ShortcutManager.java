package jmulticlip.gui.shortcut;

import java.util.*;
import java.awt.event.*;
import javax.swing.*;

/**
 *
 * @author Revers
 */
public class ShortcutManager {

    private Map<Location, List<ShortcutAction>> shortcutMap =
            new HashMap<Location, List<ShortcutAction>>();
    private Map<Location, List<JComponent>> locationMap =
            new TreeMap<Location, List<JComponent>>();

    private ShortcutManager() {
    }

    public static ShortcutManager getInstance() {
        return ShortcutManagerHolder.INSTANCE;
    }

    private static class ShortcutManagerHolder {

        private static final ShortcutManager INSTANCE = new ShortcutManager();
    }

    public void registerComponentLocation(Location location, JComponent comp) {

        if (locationMap.containsKey(location) == false) {
            ArrayList<JComponent> list = new ArrayList<JComponent>();
            list.add(comp);
            locationMap.put(location, list);
        } else {
            List<JComponent> list = locationMap.get(location);
            list.add(comp);
        }
    }

    public void unregisterWholeComponentLocation(Location location) {
        System.out.println("uregistering " + location);
        locationMap.remove(location);
    }

    public boolean unregisterComponentLocation(Location location, JComponent comp) {
//        System.out.print("uregistering " + location + ": " + comp);
        if (locationMap.containsKey(location) == false) {
//            System.out.println("\tfalse");
            return false;
        } else {
            List<JComponent> list = locationMap.get(location);
            boolean result = list.remove(comp);
//            System.out.println("\t" + result);
            return result;
        }
    }

    public void registerShortcutAction(ShortcutAction action) {

        Location key = action.getLocation();

        if (shortcutMap.containsKey(key) == false) {
            ArrayList<ShortcutAction> list = new ArrayList<ShortcutAction>();
            list.add(action);
            shortcutMap.put(key, list);
        } else {
            shortcutMap.get(key).add(action);
        }

        registerShortcut(action);
    }

    private void registerShortcut(ShortcutAction sa) {
        if (sa.getShortcutScope() == ShortcutScopeEnum.LOCATION_SCOPE) {
            registerShortcut(sa.getParent(), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT, sa);
            List<JComponent> compList = locationMap.get(sa.getLocation());

            if (compList != null) {
                for (JComponent comp : compList) {
                    registerShortcut(comp, JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT, sa);
                    registerShortcut(comp, JComponent.WHEN_FOCUSED, sa);
                }
            }

        } else if (sa.getShortcutScope() == ShortcutScopeEnum.WINDOW_SCOPE) {
            registerShortcut(sa.getParent(), JComponent.WHEN_IN_FOCUSED_WINDOW, sa);
        } else if (sa.getShortcutScope() == ShortcutScopeEnum.SYSTEM_SCOPE) {
        }
    }

    private void registerShortcut(JComponent comp, int condition, ShortcutAction sa) {
        InputMap inputMap = comp.getInputMap(condition);
        inputMap.remove(sa.getKeyStroke());

        inputMap.put(sa.getKeyStroke(), sa.getActionName());

        ActionMap actionMap = comp.getActionMap();
        actionMap.put(sa.getActionName(), sa);

    }

    public void changeKeyStroke(ShortcutAction action, KeyStroke newKeyStroke) {
    }

    public void printAllShortcuts() {
        for (Map.Entry<Location, List<ShortcutAction>> entry : shortcutMap.entrySet()) {
            System.out.println(entry.getKey().getLocation() + ": ");
            for (ShortcutAction sa : entry.getValue()) {
                System.out.println("\t" + sa);
            }
        }
    }

    public static void main(String[] args) {
        int key = KeyEvent.VK_TAB;
        int mod = KeyEvent.CTRL_MASK | KeyEvent.ALT_MASK;

        KeyStroke ks = KeyStroke.getKeyStroke(key, mod, true);
        KeyStroke k2 = KeyStroke.getKeyStroke(key, mod, false);
        System.out.println("ks = " + ks + "; ks2 = " + k2);
        System.out.println("ks.equals(ks2) " + (ks.equals(k2)));

        String name = ShortcutHelper.getKeyStrokeName(key, mod);
        System.out.println("name = " + name);

    }
}
