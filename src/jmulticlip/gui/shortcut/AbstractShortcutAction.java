package jmulticlip.gui.shortcut;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeListener;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.KeyStroke;

/**
 *
 * @author Revers
 */
public abstract class AbstractShortcutAction extends AbstractAction implements ShortcutAction {

    private Location location;
    private JComponent parent;
    private ShortcutScopeEnum shortcutScope;
    private String actionName;
    private KeyStroke keyStroke;
    private String description;
    private boolean editable = true;

    public AbstractShortcutAction(Location location, JComponent parent,
            ShortcutScopeEnum shortcutScope, String actionName, KeyStroke keyStroke) {
        this.location = location;
        this.parent = parent;
        this.shortcutScope = shortcutScope;
        this.actionName = actionName;
        this.keyStroke = keyStroke;
        enabled = true;
    }

    @Override
    public boolean isEditable() {
        return editable;
    }

    @Override
    public void setEditable(boolean editable) {
        this.editable = editable;
    }
    
    @Override
    public String getActionName() {
        return actionName;
    }

    @Override
    public Location getLocation() {
        return location;
    }

    @Override
    public JComponent getParent() {
        return parent;
    }

    @Override
    public ShortcutScopeEnum getShortcutScope() {
        return shortcutScope;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public KeyStroke getKeyStroke() {
        return keyStroke;
    }

    @Override
    public void setKeyStroke(KeyStroke keyStroke) {
        KeyStroke old = this.keyStroke;
        this.keyStroke = keyStroke;

        firePropertyChange(KEY_STROKE_PROPERTY, old, keyStroke);
    }

    @Override
    public String toString() {
        return getClass().getName() + "[location=" + location.getLocation() + ", actionName="
                + actionName + ", shortcutScope=" + shortcutScope + ", keyStroke="
                + keyStroke + ", enabled=" + enabled + ", parent="
                + (parent == null ? null : parent.getClass().getName())
                + ", description=" + description + "]";
    }

    public static void main(String[] args) {
        Action a = new AbstractShortcutAction(Location.MAIN_WINDOW, null,
                ShortcutScopeEnum.WINDOW_SCOPE, "copy",
                KeyStroke.getKeyStroke(KeyEvent.VK_C, KeyEvent.CTRL_MASK, true)) {

            public void actionPerformed(ActionEvent e) {
                throw new UnsupportedOperationException("Not supported yet.");
            }
        };
        System.out.println(a);
    }
}
