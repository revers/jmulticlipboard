package jmulticlip.gui.shortcut;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.KeyEvent;
import javax.swing.KeyStroke;

/**
 *
 * @author Revers
 */
public class ShortcutHelper {

    public static final Font SHORTCUT_BORDER_FONT = new Font("Arial", Font.PLAIN, 11);
    public static final Color SHORTCUT_BORDER_COLOR = new Color(99, 130, 191);
    public static final char KEY_SEPARATOR = '-';
    public static final String BRACKET_SEPARATOR = " ";
    public static final String SHORTCUT_SEPARATOR = ", ";

    public static String getKeyStrokeName(KeyStroke ks) {
        if (ks == null) {
            return "";
        }
        return getKeyStrokeName(ks.getKeyCode(), ks.getModifiers());
    }

    public static String getKeyStrokeName(int key, int mod) {
        String keyStr = KeyEvent.getKeyText(key);
        String modStr = KeyEvent.getKeyModifiersText(mod);

        modStr = modStr.replace('+', ShortcutHelper.KEY_SEPARATOR);

        if (key == KeyEvent.VK_UP) {
            keyStr = "\u2191";
        } else if (key == KeyEvent.VK_LEFT) {
            keyStr = "\u2190";
        } else if (key == KeyEvent.VK_DOWN) {
            keyStr = "\u2193";
        } else if (key == KeyEvent.VK_RIGHT) {
            keyStr = "\u2192";
        }

        String[] keys = modStr.split("\\+");
        for (String s : keys) {
            if (s.equals(keyStr)) {
                keyStr = "";
                break;
            }
        }

        if (!modStr.equals("") && keyStr.equals("")) {
            keyStr = modStr;
        } else if (!modStr.equals("")) {
            keyStr = modStr + ShortcutHelper.KEY_SEPARATOR + keyStr;
        }
        return keyStr;
    }
}
