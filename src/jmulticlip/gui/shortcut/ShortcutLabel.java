package jmulticlip.gui.shortcut;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.*;
import javax.swing.*;

/**
 *
 * @author Revers
 */
public class ShortcutLabel implements PropertyChangeListener {

    private static final int DEFAULT_OFFSET_X = 0;
    private static final int DEFAULT_OFFSET_Y = 2;
    private ArrayList<ShortcutAction> shortcutActions = new ArrayList<ShortcutAction>(1);
    private boolean showShortcut = true;
    private String keyString;
    private int textOffsetX;
    private int textOffsetY;
    private boolean drawFromRight = false;
    private JComponent component;

    public ShortcutLabel(ShortcutAction shortcutAction, JComponent component) {
        this(shortcutAction, DEFAULT_OFFSET_X, DEFAULT_OFFSET_Y, component);
    }

    public ShortcutLabel(ShortcutAction shortcutAction, int textOffsetX, int textOffsetY, JComponent component) {
        this.component = component;
        this.textOffsetX = textOffsetX;
        this.textOffsetY = textOffsetY;

        shortcutActions.add(shortcutAction);

        keyString = createKeyString();
    }

    public void addShortcut(ShortcutAction action) {
        shortcutActions.add(action);
        keyString = createKeyString();
    }

    public void removeShortcut(ShortcutAction action) {
        shortcutActions.remove(action);
        keyString = createKeyString();
    }

    @Override
    public String toString() {
        return getClass().getName() + "[keyString=" + keyString + ", showShortcut="
                + showShortcut + ", drawFromRight=" + drawFromRight + ", textOffsetX=" + textOffsetX
                + ", textOffsetY=" + textOffsetY + ",component=" + component + "]";
    }

    public JComponent getComponent() {
        return component;
    }

    public boolean isDrawFromRight() {
        return drawFromRight;
    }

    public void setDrawFromRight(boolean drawFromRight) {
        this.drawFromRight = drawFromRight;
    }

    public boolean isShowShortcut() {
        return showShortcut;
    }

    public void setShowShortcut(boolean showShortcut) {
        this.showShortcut = showShortcut;
    }

    public int getTextOffsetX() {
        return textOffsetX;
    }

    public void setTextOffsetX(int textOffsetX) {
        this.textOffsetX = textOffsetX;
    }

    public int getTextOffsetY() {
        return textOffsetY;
    }

    public void setTextOffsetY(int textOffsetY) {
        this.textOffsetY = textOffsetY;
    }

    private String createKeyString() {
        String openingBracket = "";
        ShortcutScopeEnum shortcutType = shortcutActions.get(0).getShortcutScope();

        if (shortcutType != ShortcutScopeEnum.NO_BRACKETS) {
            openingBracket += shortcutType.getOpeningBracket();
        }

        String closingBracket = "";
        if (shortcutType != ShortcutScopeEnum.NO_BRACKETS) {
            closingBracket += shortcutType.getClosingBracket();
        }

        String result = openingBracket + ShortcutHelper.BRACKET_SEPARATOR + ShortcutHelper.getKeyStrokeName(shortcutActions.get(0).getKeyStroke());
        for (int i = 1; i < shortcutActions.size(); i++) {
            result += ShortcutHelper.SHORTCUT_SEPARATOR + ShortcutHelper.getKeyStrokeName(shortcutActions.get(i).getKeyStroke());
        }

        result += ShortcutHelper.BRACKET_SEPARATOR + closingBracket;

        return result;
    }

    public String getKeyString() {
        return keyString;
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (evt.getPropertyName().equals(ShortcutAction.KEY_STROKE_PROPERTY)) {
            keyString = createKeyString();
        }
    }
}
