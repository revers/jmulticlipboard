package jmulticlip.gui.shortcut;

/**
 *
 * @author Revers
 */
public enum ShortcutScopeEnum {

    SYSTEM_SCOPE('<', '>'), WINDOW_SCOPE('(', ')'), LOCATION_SCOPE('[', ']'),
    COMPONENT_SCOPE('[', ']'), RESERVED('{', '}'), NO_BRACKETS('\0', '\0');
    private final char openingBracket;
    private final char closingBracket;

    private ShortcutScopeEnum(char openingBracket, char closingBracket) {
        this.openingBracket = openingBracket;
        this.closingBracket = closingBracket;
    }

    public char getClosingBracket() {
        return closingBracket;
    }

    public char getOpeningBracket() {
        return openingBracket;
    }
}
