package jmulticlip.gui.shortcut;

/**
 *
 * @author Revers
 */
public enum Location {

    MAIN_WINDOW("mainwindow"),
    MAIN_WINDOW_TOOLBAR("mainwindow.toolbar"),
    MAIN_WINDOW_NAVIGATOR("mainwindow.navigator"),
    MAIN_WINDOW_NAVIGATOR_SCRIPTS("mainwindow.navigator.scripts"),
    MAIN_WINDOW_EDITOR("mainwindow.editor"),
    MAIN_WINDOW_EDITOR_TEXT("mainwindow.editor.text"),
    MAIN_WINDOW_EDITOR_GRAPHICS("mainwindow.editor.graphics"),
    CHOOSEN_SCRIPTS_DIALOG("choosenscripts");
    private final String location;

    private Location(String location) {
        this.location = location;
    }

    public String getLocation() {
        return location;
    }
}
