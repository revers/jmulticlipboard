package jmulticlip.gui;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Rectangle2D;

import javax.swing.*;
import javax.swing.border.*;
import jmulticlip.util.GUIUtils;

public class FocusBorder implements Border {

    private static final int LEFT_MARGIN = 14;
    private static final int Y_TITLE_OFFSET = 5;
    private int tickness = 5;
    private int margin = 5;
    private int arcLenght = tickness * 2;
    private Color color = Color.red;
    // private Color backgroundColor = new Color(236, 233, 216);
    private Font font = new Font("Verdana", Font.BOLD, 12);
    private String title;

    public FocusBorder(String title) {
        this.title = title;
    }

    @Override
    public void paintBorder(Component c,
            Graphics g,
            int x, int y,
            int width, int height) {

        Graphics2D g2 = (Graphics2D) g;
        Object oldValue = g2.getRenderingHint(RenderingHints.KEY_ANTIALIASING);
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);

        //g2.drawRect(x, y, width - 1, height - 1);

        g2.setColor(color);
        g2.setFont(font);

        int lineWidth = 4;
        Stroke oldStroke = g2.getStroke();
        g2.setStroke(new BasicStroke(lineWidth));
        int yOffset = +lineWidth + Y_TITLE_OFFSET;
        g2.drawRoundRect(x + lineWidth, y + yOffset, width - 2 * lineWidth, height - yOffset - lineWidth, arcLenght, arcLenght);

        FontMetrics fm = g2.getFontMetrics();
        Rectangle2D rect = fm.getStringBounds(title, g2);

        g2.setColor(c.getBackground());

        int temp = 2;
        g2.fillRect(x + LEFT_MARGIN - temp, y, (int) rect.getWidth() + 2 * temp, (int) rect.getHeight());

        g2.setColor(color);
        g2.drawString(title, x + LEFT_MARGIN, y + Y_TITLE_OFFSET * 2 + 4);

        g2.setStroke(oldStroke);
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                oldValue);
    }

    @Override
    public Insets getBorderInsets(Component c) {
        return new Insets(margin + Y_TITLE_OFFSET + 7, margin + 2, margin, margin+2);
    }

//    public Color getBackgroundColor() {
//        return backgroundColor;
//    }
//
//    public void setBackgroundColor(Color backgroundColor) {
//        this.backgroundColor = backgroundColor;
//    }
    public int getArcLenght() {
        return arcLenght;
    }

    public void setArcLenght(int arcLenght) {
        this.arcLenght = arcLenght;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public int getMargin() {
        return margin;
    }

    public void setMargin(int margin) {
        this.margin = margin;
    }

    public int getTickness() {
        return tickness;
    }

    public void setTickness(int tickness) {
        this.tickness = tickness;
    }

    public Font getFont() {
        return font;
    }

    public void setFont(Font font) {
        this.font = font;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public boolean isBorderOpaque() {
        return true;
    }

    private static void launchTestFrame() {
        JFrame frame = new JFrame();

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        GUIUtils.useApplicationLAF(frame);
//        try {
//            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
//        } catch (Exception ex) {
//            Logger.getLogger(ShortcutBorder.class.getName()).log(Level.SEVERE, null, ex);
//        }

        JPanel rootPanel = new JPanel();
        int insets = 50;
        rootPanel.setBorder(new EmptyBorder(insets, insets, insets, insets));
        frame.setLayout(new BorderLayout());
        frame.add(rootPanel, BorderLayout.CENTER);

        rootPanel.setLayout(new FlowLayout());
        //rootPanel.add(Box.createVerticalStrut(150));


        //frame.add(Box.createHorizontalStrut(50));
        // frame.setLayout(new BoxLayout(frame.getContentPane(), BoxLayout.LINE_AXIS));


        final JPanel panel = new JPanel();
        final FocusBorder fb = new FocusBorder("Navigation [ Ctrl-Tab ]");
        panel.setBorder(fb);

        Dimension dim = new Dimension(200, 200);
        panel.setPreferredSize(dim);

        rootPanel.add(panel);

        JButton button = new JButton("Zmien kolor ramki");

        button.addActionListener(new ActionListener() {

            private boolean b = true;

            public void actionPerformed(ActionEvent e) {
                if (b) {
                    fb.setColor(Color.BLUE);
                } else {
                    fb.setColor(Color.RED);
                }

                b = !b;
                panel.repaint();
            }
        });

        frame.add(rootPanel);
        frame.add(button, BorderLayout.SOUTH);


        // frame.add(panel);
        frame.setBounds(200, 200, 800, 600);
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                launchTestFrame();
            }
        });
    }
}