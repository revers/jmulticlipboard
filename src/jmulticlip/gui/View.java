package jmulticlip.gui;

import javax.swing.JPanel;

/**
 *
 * @author Revers
 */
public abstract class View extends JPanel {
    
    public abstract ViewType getType();
}
