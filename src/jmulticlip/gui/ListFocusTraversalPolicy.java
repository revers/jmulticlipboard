package jmulticlip.gui;

import java.util.*;
import java.awt.*;

/**
 *
 * @author Revers
 */
public class ListFocusTraversalPolicy
        extends FocusTraversalPolicy {

    private ArrayList<Component> order;

    public ListFocusTraversalPolicy() {
        order = new ArrayList<Component>();
    }

    public void addComponent(Component comp) {
        order.add(comp);
    }

    public void removeComponent(Component comp) {
        order.remove(comp);
    }

    public void removeAllComponents() {
        order.clear();
    }

    public ArrayList<Component> getComponentList() {
        return order;
    }

    public ListFocusTraversalPolicy(ArrayList<Component> order) {
        this.order = new ArrayList<Component>(order.size());
        this.order.addAll(order);
    }

    @Override
    public Component getComponentAfter(Container focusCycleRoot,
            Component aComponent) {
        int idx = (order.indexOf(aComponent) + 1) % order.size();
        return order.get(idx);
    }

    @Override
    public Component getComponentBefore(Container focusCycleRoot,
            Component aComponent) {
        int idx = order.indexOf(aComponent) - 1;
        if (idx < 0) {
            idx = order.size() - 1;
        }
        return order.get(idx);
    }

    @Override
    public Component getDefaultComponent(Container focusCycleRoot) {
        return order.get(0);
    }

    @Override
    public Component getLastComponent(Container focusCycleRoot) {
        return order.get(order.size() - 1);
    }

    @Override
    public Component getFirstComponent(Container focusCycleRoot) {
        return order.get(0);
    }
}
