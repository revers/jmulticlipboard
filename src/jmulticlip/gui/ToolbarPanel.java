/*
 * ToolbarPanel.java
 *
 * Created on 2011-07-25, 18:25:36
 */
package jmulticlip.gui;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.*;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import jmulticlip.clipboard.ClipDataType;
import jmulticlip.core.InitializationManager;

import jmulticlip.gui.graph.GraphicsViewPanel;
import jmulticlip.gui.navig.ClipDataRow;
import jmulticlip.gui.navig.NavigTablePanel;
import jmulticlip.gui.shortcut.*;
import jmulticlip.i18n.*;
import jmulticlip.ifaces.Initializable;
import jmulticlip.screencapture.ScreenFragmentFrame;
import jmulticlip.util.GUIUtils;
import pl.ugu.revers.logging.i18n.*;
import pl.ugu.revers.swing.*;

/**
 *
 * @author Revers
 */
public class ToolbarPanel extends ShortcutPanel implements Internationalizable, Initializable, ChangeListener {

    private I18NManager i18n = I18NManager.getInstance();
    private boolean lastSelected;
    private JPopupMenu popup;

    /** Creates new form ToolbarPanel */
    public ToolbarPanel() {
        popup = new JPopupMenu();
        initComponents();
        initPopup();
        i18n.addInternationalizable(this);

        InitializationManager.getInstance().addInitializable(this);

        ignoreCopyCB.addChangeListener(this);

        GUIUtils.makeButtonFlatWithBoreder(colorJB);
        GUIUtils.makeButtonFlatWithBoreder(editScriptsJB);
        GUIUtils.makeButtonFlatWithBoreder(newJB);
        GUIUtils.makeButtonFlatWithBoreder(prtScrJB);
        GUIUtils.makeButtonFlatWithBoreder(saveJB);
        GUIUtils.makeButtonFlatWithBoreder(settingsJB);
    }

    private void initPopup() {
        JMenuItem newDocumentItem = new JMenuItem("New document", GUIUtils.getIcon("src/img/document.png"));
        JMenuItem newImageItem = new JMenuItem("New image", GUIUtils.getIcon("src/img/image24.png"));

        popup.add(newDocumentItem);
        popup.add(newImageItem);

        newDocumentItem.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("mainwindow.toolbar.newDocument");
            }
        });

        newImageItem.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("mainwindow.toolbar.newImage");
            }
        });
    }

    @Override
    public void init() {
        addShortcuts();
    }

    @Override
    public void stateChanged(ChangeEvent e) {
        if (lastSelected == ignoreCopyCB.isSelected()) {
            return;
        }
        lastSelected = ignoreCopyCB.isSelected();

        if (lastSelected) {
            ignoreCopyCB.setFont(GUIUtils.SELECTED_CB_FONT);
        } else {
            ignoreCopyCB.setFont(GUIUtils.NORMAL_CB_FONT);
        }
        repaint();
    }

    private void addShortcuts() {
        //MainFrame mf = MainFrame.getInstance();
        ShortcutManager shortcutMgr = ShortcutManager.getInstance();
        JComponent parent = this;

        KeyStroke prtScrKS = KeyStroke.getKeyStroke(KeyEvent.VK_M, KeyEvent.SHIFT_MASK | KeyEvent.CTRL_MASK, true);
        AbstractShortcutAction prtScr = new AbstractShortcutAction(Location.MAIN_WINDOW_TOOLBAR,
                parent, ShortcutScopeEnum.SYSTEM_SCOPE, "prtScr", prtScrKS) {

            public void actionPerformed(ActionEvent e) {
                prtScrJBActionPerformed(e);
            }
        };

        shortcutMgr.registerShortcutAction(prtScr);
        //------------------------------------------------------
        KeyStroke settingsKS = KeyStroke.getKeyStroke(KeyEvent.VK_1, KeyEvent.CTRL_MASK, true);

        AbstractShortcutAction settings = new AbstractShortcutAction(Location.MAIN_WINDOW_TOOLBAR,
                parent, ShortcutScopeEnum.WINDOW_SCOPE, "settings", settingsKS) {

            public void actionPerformed(ActionEvent e) {
                System.out.println("mainwindow.toolbar.settings");
            }
        };

        shortcutMgr.registerShortcutAction(settings);
        //------------------------------------------------------
        KeyStroke newKS = KeyStroke.getKeyStroke(KeyEvent.VK_N, KeyEvent.CTRL_MASK, true);

        AbstractShortcutAction newAction = new AbstractShortcutAction(Location.MAIN_WINDOW_TOOLBAR,
                parent, ShortcutScopeEnum.WINDOW_SCOPE, "new", newKS) {

            public void actionPerformed(ActionEvent e) {
                System.out.println("mainwindow.toolbar.new");
            }
        };

        shortcutMgr.registerShortcutAction(newAction);
        //------------------------------------------------------
        KeyStroke saveKS = KeyStroke.getKeyStroke(KeyEvent.VK_S, KeyEvent.CTRL_MASK, true);

        AbstractShortcutAction save = new AbstractShortcutAction(Location.MAIN_WINDOW_TOOLBAR,
                parent, ShortcutScopeEnum.WINDOW_SCOPE, "save", saveKS) {

            public void actionPerformed(ActionEvent e) {
                System.out.println("mainwindow.toolbar.save");
            }
        };

        shortcutMgr.registerShortcutAction(save);
        //------------------------------------------------------
        KeyStroke editScriptsKS = KeyStroke.getKeyStroke(KeyEvent.VK_E, KeyEvent.CTRL_MASK, true);

        AbstractShortcutAction editScripts = new AbstractShortcutAction(Location.MAIN_WINDOW_TOOLBAR,
                parent, ShortcutScopeEnum.WINDOW_SCOPE, "editScripts", editScriptsKS) {

            public void actionPerformed(ActionEvent e) {
                System.out.println("mainwindow.toolbar.editScripts");
            }
        };

        shortcutMgr.registerShortcutAction(editScripts);
        //------------------------------------------------------

        KeyStroke ignoreCopyKS = KeyStroke.getKeyStroke(KeyEvent.VK_I, KeyEvent.CTRL_MASK | KeyEvent.SHIFT_MASK, true);

        AbstractShortcutAction ignoreCopy = new AbstractShortcutAction(Location.MAIN_WINDOW_TOOLBAR,
                parent, ShortcutScopeEnum.WINDOW_SCOPE, "ignoreCopy", ignoreCopyKS) {

            public void actionPerformed(ActionEvent e) {
                ignoreCopyCB.setSelected(!ignoreCopyCB.isSelected());
            }
        };

        shortcutMgr.registerShortcutAction(ignoreCopy);
        //------------------------------------------------------
        KeyStroke colorKS = KeyStroke.getKeyStroke(KeyEvent.VK_L, KeyEvent.CTRL_MASK | KeyEvent.SHIFT_MASK, true);

        AbstractShortcutAction color = new AbstractShortcutAction(Location.MAIN_WINDOW_TOOLBAR,
                parent, ShortcutScopeEnum.SYSTEM_SCOPE, "colorPicker", colorKS) {

            public void actionPerformed(ActionEvent e) {
                colorJBActionPerformed(e);
            }
        };

        shortcutMgr.registerShortcutAction(color);
        //------------------------------------------------------

        ShortcutLabel prtScrLabel = new ShortcutLabel(prtScr, prtScrJB);
        prtScrLabel.setTextOffsetY(GUIUtils.SHORTCUT_Y_OFFSET);
        addShortcutLabel(prtScrLabel);

        ShortcutLabel settingsLabel = new ShortcutLabel(settings, settingsJB);
        settingsLabel.setTextOffsetY(GUIUtils.SHORTCUT_Y_OFFSET);
        addShortcutLabel(settingsLabel);

        ShortcutLabel editScriptsLabel = new ShortcutLabel(editScripts, editScriptsJB);
        editScriptsLabel.setTextOffsetY(GUIUtils.SHORTCUT_Y_OFFSET);
        addShortcutLabel(editScriptsLabel);

        ShortcutLabel colorLabel = new ShortcutLabel(color, colorJB);
        colorLabel.setTextOffsetY(GUIUtils.SHORTCUT_Y_OFFSET);
        addShortcutLabel(colorLabel);

        ShortcutLabel newLabel = new ShortcutLabel(newAction, newJB);
        newLabel.setTextOffsetY(GUIUtils.SHORTCUT_Y_OFFSET);
        addShortcutLabel(newLabel);

        ShortcutLabel saveLabel = new ShortcutLabel(save, saveJB);
        saveLabel.setTextOffsetY(GUIUtils.SHORTCUT_Y_OFFSET);
        addShortcutLabel(saveLabel);

        ShortcutLabel ignoreCopyLabel = new ShortcutLabel(ignoreCopy, ignoreCopyCB);
        ignoreCopyLabel.setTextOffsetY(9);
        addShortcutLabel(ignoreCopyLabel);



    }

    @Override
    public void changeLanguage(Locale language) {
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        settingsJB = new javax.swing.JButton();
        editScriptsJB = new javax.swing.JButton();
        ignoreCopyCB = new javax.swing.JCheckBox();
        separator1 = new javax.swing.JSeparator();
        separator2 = new javax.swing.JSeparator();
        prtScrJB = new javax.swing.JButton();
        colorJB = new javax.swing.JButton();
        separator3 = new javax.swing.JSeparator();
        newJB = new javax.swing.JButton();
        saveJB = new javax.swing.JButton();
        separator4 = new javax.swing.JSeparator();

        setLayout(new java.awt.GridBagLayout());

        settingsJB.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/setting.png"))); // NOI18N
        settingsJB.setFocusable(false);
        settingsJB.setMargin(new java.awt.Insets(2, 10, 2, 10));
        settingsJB.setMinimumSize(new java.awt.Dimension(36, 36));
        settingsJB.setPreferredSize(new java.awt.Dimension(36, 36));
        settingsJB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                settingsJBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.SOUTH;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(10, 8, 0, 0);
        add(settingsJB, gridBagConstraints);

        editScriptsJB.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/note.png"))); // NOI18N
        editScriptsJB.setFocusable(false);
        editScriptsJB.setMargin(new java.awt.Insets(2, 2, 2, 2));
        editScriptsJB.setMinimumSize(new java.awt.Dimension(36, 36));
        editScriptsJB.setPreferredSize(new java.awt.Dimension(36, 36));
        editScriptsJB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editScriptsJBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.SOUTH;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(10, 8, 0, 0);
        add(editScriptsJB, gridBagConstraints);

        ignoreCopyCB.setText("Ignore Copy Inside The Program");
        ignoreCopyCB.setFocusable(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 9;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(10, 10, 0, 5);
        add(ignoreCopyCB, gridBagConstraints);

        separator1.setMinimumSize(new java.awt.Dimension(100, 3));
        separator1.setPreferredSize(new java.awt.Dimension(150, 3));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 10;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, -4, 0, -4);
        add(separator1, gridBagConstraints);

        separator2.setOrientation(javax.swing.SwingConstants.VERTICAL);
        separator2.setMinimumSize(new java.awt.Dimension(2, 5));
        separator2.setPreferredSize(new java.awt.Dimension(2, 5));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(10, 8, 0, 0);
        add(separator2, gridBagConstraints);

        prtScrJB.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/camera32.png"))); // NOI18N
        prtScrJB.setText("PrtScr");
        prtScrJB.setFocusable(false);
        prtScrJB.setMinimumSize(new java.awt.Dimension(97, 36));
        prtScrJB.setPreferredSize(new java.awt.Dimension(97, 36));
        prtScrJB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                prtScrJBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.SOUTH;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(10, 5, 0, 0);
        add(prtScrJB, gridBagConstraints);

        colorJB.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/color_picker2-32.png"))); // NOI18N
        colorJB.setText("Color");
        colorJB.setFocusable(false);
        colorJB.setMinimumSize(new java.awt.Dimension(80, 36));
        colorJB.setPreferredSize(new java.awt.Dimension(93, 36));
        colorJB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                colorJBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.SOUTH;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(10, 5, 0, 0);
        add(colorJB, gridBagConstraints);

        separator3.setOrientation(javax.swing.SwingConstants.VERTICAL);
        separator3.setMinimumSize(new java.awt.Dimension(2, 5));
        separator3.setPreferredSize(new java.awt.Dimension(2, 5));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 7;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(10, 8, 0, 0);
        add(separator3, gridBagConstraints);

        newJB.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/new32.png"))); // NOI18N
        newJB.setFocusable(false);
        newJB.setMinimumSize(new java.awt.Dimension(36, 36));
        newJB.setPreferredSize(new java.awt.Dimension(36, 36));
        newJB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                newJBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.SOUTH;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(10, 0, 0, 0);
        add(newJB, gridBagConstraints);

        saveJB.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/save32.png"))); // NOI18N
        saveJB.setFocusable(false);
        saveJB.setMinimumSize(new java.awt.Dimension(36, 36));
        saveJB.setPreferredSize(new java.awt.Dimension(36, 36));
        saveJB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveJBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.SOUTH;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(10, 8, 0, 0);
        add(saveJB, gridBagConstraints);

        separator4.setOrientation(javax.swing.SwingConstants.VERTICAL);
        separator4.setMinimumSize(new java.awt.Dimension(2, 5));
        separator4.setPreferredSize(new java.awt.Dimension(2, 5));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(10, 8, 0, 0);
        add(separator4, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void settingsJBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_settingsJBActionPerformed
        MainFrame mf = MainFrame.getInstance();

        Color color =
                JColorChooser.showDialog(this,
                "Choose a color", mf.getNavigationPanel().getBorderColor());

        if (color != null) {
            mf.getNavigationPanel().setBorderColor(color);
            mf.repaint();
        }
    }//GEN-LAST:event_settingsJBActionPerformed

    private void prtScrJBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_prtScrJBActionPerformed
        ScreenFragmentFrame.launchPrintScreen();
    }//GEN-LAST:event_prtScrJBActionPerformed

    private void editScriptsJBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editScriptsJBActionPerformed
        MainFrame mf = MainFrame.getInstance();
        Color color =
                JColorChooser.showDialog(this,
                "Choose a color", mf.getEditorPanel().getBorderColor());

        if (color != null) {
            mf.getEditorPanel().setBorderColor(color);
            mf.repaint();
        }
    }//GEN-LAST:event_editScriptsJBActionPerformed

    private void colorJBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_colorJBActionPerformed
        System.out.println("Color Picker action");
        MainFrame mf = MainFrame.getInstance();
        NavigTablePanel panel = mf.getNavigationPanel().getNavigTablePanel();
        for (int i = 0; i < 5; i++) {
            panel.addTestData();
        }
    }//GEN-LAST:event_colorJBActionPerformed

private void newJBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_newJBActionPerformed
    SwingUtilities.updateComponentTreeUI(popup);
    popup.show(this, newJB.getX(), newJB.getY() + 36);
}//GEN-LAST:event_newJBActionPerformed

private void saveJBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveJBActionPerformed
    MainFrame mf = MainFrame.getInstance();
    ClipDataRow clipDataRow = mf.getNavigationPanel().getNavigTablePanel().getCurrentClipDataRow();
    if (clipDataRow.getType() == ClipDataType.IMAGE) {
        View view = mf.getViewContainer().getView();
        if (view instanceof GraphicsViewPanel) {
            ((GraphicsViewPanel) view).saveImage();
        } else {
            System.out.println("DUPA, mial byc GraphicsViewPanel :(");
        }
    } else {
        System.out.println("Na razie zapisuje tylko obrazki :PP");
    }
}//GEN-LAST:event_saveJBActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton colorJB;
    private javax.swing.JButton editScriptsJB;
    private javax.swing.JCheckBox ignoreCopyCB;
    private javax.swing.JButton newJB;
    private javax.swing.JButton prtScrJB;
    private javax.swing.JButton saveJB;
    private javax.swing.JSeparator separator1;
    private javax.swing.JSeparator separator2;
    private javax.swing.JSeparator separator3;
    private javax.swing.JSeparator separator4;
    private javax.swing.JButton settingsJB;
    // End of variables declaration//GEN-END:variables
}
