package jmulticlip.screencapture;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.logging.*;
import javax.swing.*;
import javax.swing.table.*;
import java.util.regex.*;
import java.util.prefs.*;
import java.util.*;

import pl.ugu.revers.swing.*;
import pl.ugu.revers.net.*;
import pl.ugu.revers.event.*;

import pl.ugu.revers.swing.table.*;
/**
 *
 * @author Revers
 */
public class RedWindow extends JWindow {
    public RedWindow(int x, int y, int width, int height) {
        setLocation(x, y);
        setSize(width, height);
        JPanel panel = new JPanel();
        panel.setBackground(Color.red);
        setLayout(new BorderLayout());
        add(panel);
        setAlwaysOnTop(true);
    }
}
