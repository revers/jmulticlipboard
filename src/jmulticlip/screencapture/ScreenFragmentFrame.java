package jmulticlip.screencapture;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.logging.*;
import javax.swing.*;
import javax.swing.table.*;
import java.util.regex.*;
import java.util.prefs.*;
import java.util.*;
import jmulticlip.gui.MainFrame;
import jmulticlip.clipboard.ImageSelection;

import pl.ugu.revers.swing.*;
import pl.ugu.revers.net.*;
import pl.ugu.revers.event.*;

import pl.ugu.revers.swing.table.*;

/**
 *
 * @author Revers
 */
public class ScreenFragmentFrame extends javax.swing.JFrame implements KeyListener, Runnable, MouseListener {

    private static final long SLEEP_TIME = 50L;
    private DimensionPanel dimPanel;
    private Dimension screenDimension;
    private RedWindow horRedWindow1;
    private RedWindow vertRedWindow1;
    private RedWindow horRedWindow2;
    private RedWindow vertRedWindow2;
    private RedWindow currentHorWindow;
    private RedWindow currentVertWindow;
    private Point startPoint;
    private Point endPoint;
    private boolean keepAlive = true;
    private boolean firstPointChoosen;
    private JFrame parent;

    public static void launchPrintScreen() {

        MainFrame mf = MainFrame.getInstance();
        mf.setVisible(false);
        new ScreenFragmentFrame(mf).setVisible(true);
    }

    public ScreenFragmentFrame() {
        this(null);
    }

    /** Creates new form ScreenFragmentFrame */
    public ScreenFragmentFrame(JFrame frame) {
        this.parent = frame;

        setUndecorated(true);

        screenDimension = Toolkit.getDefaultToolkit().getScreenSize();
        int w = 140;
        int h = 40;
        setSize(w, h);
        setLocation(screenDimension.width - w, screenDimension.height - h);
        setAlwaysOnTop(true);


        // addMouseMotionListener(this);

        dimPanel = new DimensionPanel();
        // dimPanel.setRect(9999, 1800, 2048, 1024);//("x: 50    y = 34", "w: 800   h: 1024");
        //newJPanel1.addMouseListener(this);
        //newJPanel1.addMouseMotionListener(this);
        //centerPanel.setDoubleBuffered(false);
        //  centerPanel.setOpaque(false);
        setLayout(new BorderLayout());
        add(dimPanel, BorderLayout.CENTER);

        horRedWindow1 = new RedWindow(0, 50, screenDimension.width, 1);
        vertRedWindow1 = new RedWindow(50, 0, 1, screenDimension.height);

        horRedWindow2 = new RedWindow(0, 50, screenDimension.width, 1);
        vertRedWindow2 = new RedWindow(50, 0, 1, screenDimension.height);

        currentHorWindow = horRedWindow1;
        currentVertWindow = vertRedWindow1;

        horRedWindow1.addMouseListener(this);
        horRedWindow2.addMouseListener(this);
        vertRedWindow1.addMouseListener(this);
        vertRedWindow2.addMouseListener(this);

        addKeyListener(this);
        horRedWindow1.addKeyListener(this);
        vertRedWindow1.addKeyListener(this);
        horRedWindow2.addKeyListener(this);
        vertRedWindow2.addKeyListener(this);


      //  new Thread(this).start();
    }

    @Override
    public void run() {
        while (keepAlive) {
            Point p = MouseInfo.getPointerInfo().getLocation();

            currentHorWindow.setLocation(0, p.y);
            currentVertWindow.setLocation(p.x, 0);



            if (firstPointChoosen) {
                dimPanel.setRectWH(Math.abs(p.x - startPoint.x), Math.abs(p.y - startPoint.y));
            } else {
                dimPanel.setRectXY(p.x, p.y);
            }

            try {
                Thread.sleep(SLEEP_TIME);
            } catch (InterruptedException ex) {
                Logger.getLogger(ScreenFragmentFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public void setVisible(boolean b) {
        super.setVisible(b);
        if (!firstPointChoosen) {
            currentHorWindow.setVisible(b);
            currentVertWindow.setVisible(b);
            new Thread(this).start();
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
            horRedWindow1.setVisible(false);
            horRedWindow2.setVisible(false);
            vertRedWindow1.setVisible(false);
            vertRedWindow2.setVisible(false);
            setVisible(false);
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (firstPointChoosen == false) {
            startPoint = MouseInfo.getPointerInfo().getLocation();
            currentHorWindow = horRedWindow2;
            currentVertWindow = vertRedWindow2;
            currentHorWindow.setVisible(true);
            currentVertWindow.setVisible(true);
            firstPointChoosen = true;
        } else if (keepAlive) {
            endPoint = MouseInfo.getPointerInfo().getLocation();
            Rectangle rect = new Rectangle();
            if (startPoint.x < endPoint.x) {
                rect.x = startPoint.x;
            } else {
                rect.x = endPoint.x;
            }

            if (startPoint.y < endPoint.y) {
                rect.y = startPoint.y;
            } else {
                rect.y = endPoint.y;
            }

            rect.width = Math.abs(startPoint.x - endPoint.x);
            rect.height = Math.abs(startPoint.y - endPoint.y);

            horRedWindow1.setVisible(false);
            horRedWindow2.setVisible(false);
            vertRedWindow1.setVisible(false);
            vertRedWindow2.setVisible(false);
            setVisible(false);
            try {
                Robot robot = new Robot();
                Image img = robot.createScreenCapture(rect);

                ImageSelection imgSel = new ImageSelection(img);
                Toolkit.getDefaultToolkit().getSystemClipboard().setContents(imgSel, null);
                System.out.println("aaaa");
//                final JFrame frame = new LoadnigFrame();
//                new Thread() {
//
//                    @Override
//                    public void run() {
//                        frame.setVisible(true);
//                    }
//                    
//                }.start();

                //new MainFrame(img).setVisible(true);
                //System.out.println("bbbbYST");

                // frame.setVisible(false);
                parent.setVisible(true);

//            robot.keyPress(KeyEvent.VK_CONTROL);
//            robot.keyPress(KeyEvent.VK_SHIFT);
//            robot.keyPress(KeyEvent.VK_B);
//            robot.keyRelease(KeyEvent.VK_B);
//            robot.keyRelease(KeyEvent.VK_SHIFT);
//            robot.keyRelease(KeyEvent.VK_CONTROL);
            } catch (AWTException ex) {
                Logger.getLogger(ScreenFragmentFrame.class.getName()).log(Level.SEVERE, null, ex);
            }

            keepAlive = false;

            //System.exit(0);
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                ScreenFragmentFrame sff = new ScreenFragmentFrame();//.setVisible(true);
                //AWTUtilitiesWrapper.setWindowOpaque(sff, false);
                sff.setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
