package jmulticlip.screencapture;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
/**
 *
 * @author Revers
 */
public class DimensionPanel extends JPanel {

    //private final String TEST_STR = "test";
    private final Font SMALL_FONT = new Font("Tahoma", Font.BOLD, 14);
    private float transparency = 0.7f;
    private String textX = "";
    private String textY = "";
    private String textW = "";
    private String textH = "";
    private int textPosX = 5;
    private int textPosY = 15;
    private int rectX;
    private int rectY;
    private int rectW;
    private int rectH;

    public DimensionPanel() {
        setRectX(0);
        setRectY(0);
        setRectW(0);
        setRectH(0);
    }

    public void setRectXY(int x, int y) {
        setRectX(x);
        setRectY(y);
    }

    public void setRectWH(int w, int h) {
        setRectW(w);
        setRectH(h);
    }

    public void setRect(int x, int y, int w, int h) {
        setRectX(x);
        setRectY(y);
        setRectW(w);
        setRectH(h);
    }

    public int getRectH() {
        return rectH;
    }

    public void setRectH(int rectH) {
        this.rectH = rectH;
        textH = "h: " + rectH;
        repaint();
    }

    public int getRectW() {
        return rectW;
    }

    public void setRectW(int rectW) {
        this.rectW = rectW;
        textW = "w: " + rectW;
        repaint();
    }

    public int getRectX() {
        return rectX;
    }

    public void setRectX(int rectX) {
        this.rectX = rectX;
        textX = "x: " + rectX;
        repaint();
    }

    public int getRectY() {
        return rectY;
    }

    public void setRectY(int rectY) {
        this.rectY = rectY;
        textY = "y: " + rectY;
        repaint();
    }

    public void setTextPosition(int x, int y) {
        textPosX = x;
        textPosY = y;
    }

    public float getTransparency() {
        return transparency;
    }

    public void setTransparency(float transparency) {
        this.transparency = transparency;
        repaint();
    }

    @Override
    protected void paintComponent(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;

        g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER,
                transparency));
        g2.setColor(new Color(240, 240, 240));
        g2.fillRect(0, 0, getWidth(), getHeight());

        g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER,
                1.0f));

        drawString(g2, textX, textPosX, textPosY);
        drawString(g2, textY, textPosX + 75, textPosY);
        drawString(g2, textW, textPosX, textPosY + 20);
        drawString(g2, textH, textPosX + 75, textPosY + 20);


    }

    private void drawString(Graphics2D g2, String text, int x, int y) {

        g2.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);

        g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
                RenderingHints.VALUE_TEXT_ANTIALIAS_GASP);
        g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
                RenderingHints.VALUE_TEXT_ANTIALIAS_LCD_HRGB);

//            g2.setColor(Color.WHITE);
        g2.setFont(SMALL_FONT);
//
//            int shift = 2;
//            g2.drawString(text, x - shift, y - shift);
//            g2.drawString(text, x - shift, y + shift);
//            g2.drawString(text, x + shift, y - shift);
//            g2.drawString(text, x + shift, y + shift);

        g2.setColor(Color.BLACK);



        g2.drawString(text, x, y);
    }
}
