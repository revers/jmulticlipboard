package jmulticlip.clipboard;

import java.awt.Image;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.io.File;
import java.util.*;
import jmulticlip.core.ErrorManager;

public class ClipboardObserver extends Thread {

    private Clipboard clipboard;
    private String oldText;
    private List<File> oldFiles;
    private ClipboardListener clipListener;
    private Image oldImage;
    // TODO: max image list:
    private List<Image> imageList = new LinkedList<Image>();

    public ClipboardObserver(ClipboardListener clip) {
        clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        clipListener = clip;
        this.start();
    }

    @Override
    @SuppressWarnings("unchecked")
    public void run() {
        Transferable transferable = null;

        while (true) {
            try {
                transferable = clipboard.getContents(null);
              //  System.out.println(Arrays.toString(transferable.getTransferDataFlavors()));

                if (transferable != null) {
                    if (transferable.isDataFlavorSupported(DataFlavor.javaFileListFlavor)) {

                        List<File> files = (List<File>) transferable.getTransferData(DataFlavor.javaFileListFlavor);

                        boolean fileschanged = (oldFiles == null)
                                || oldFiles.size() != files.size();
                        if (!fileschanged) {

                            for (File file : oldFiles) {
                                boolean found = false;
                                for (File file2 : files) {
                                    if (file2.getAbsolutePath().equals(
                                            file.getAbsolutePath())) {
                                        found = true;
                                        break;
                                    }
                                }
                                if (!found) {
                                    fileschanged = true;
                                    break;
                                }
                            }
                        }
                        if (fileschanged) {

                            oldFiles = files;
                            //System.out.println("FILES CHANGED:");
                            System.out.println("FILES :P");
                            clipListener.cliboardChanged(files, ClipDataType.FILE_LIST);
                        }
                    } else if (transferable.isDataFlavorSupported(DataFlavor.stringFlavor)) {

                        String text = (String) transferable.getTransferData(DataFlavor.stringFlavor);
                        if (!text.equals(oldText)) {

                            oldText = text;
                            // System.out.println("TextChanged:");

                            // System.out.println(text);
                            clipListener.cliboardChanged(text, ClipDataType.TEXT);
                        }

                    } else if (transferable.isDataFlavorSupported(DataFlavor.imageFlavor)) {

                        Image img = (Image) transferable.getTransferData(DataFlavor.imageFlavor);
                        if (img != oldImage) {

                            oldImage = img;

                            ImageSelection imgSel = new ImageSelection(img);
                            Toolkit.getDefaultToolkit().getSystemClipboard().setContents(imgSel, null);

                            clipListener.cliboardChanged(img, ClipDataType.IMAGE);
                        }
                    }
                }

            } catch (Exception e2) {
                ErrorManager.errorSilent(ClipboardObserver.class, e2);
            }
            try {
                Thread.sleep(750L);
            } catch (InterruptedException e) {
                ErrorManager.errorSilent(ClipboardObserver.class, e);
            }

        }
    }

    public Image getOldImage() {
        return oldImage;
    }

    public void setOldImage(Image oldImage) {
        this.oldImage = oldImage;
    }

    public String getOldText() {
        return oldText;
    }

    public void setOldText(String oldText) {
        this.oldText = oldText;
    }
//
//	/**
//	 * @param args
//	 */
//	public static void main(String[] args) {
//		System.out.println("Testuje monitor schowka");
//
//		new ClipboardObserver();
//		System.out.println("END");
//	}
}
