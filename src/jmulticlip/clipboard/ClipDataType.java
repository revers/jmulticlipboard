package jmulticlip.clipboard;

import java.awt.Toolkit;
import javax.swing.Icon;
import javax.swing.ImageIcon;

public enum ClipDataType {

    TEXT(createIcon("img/text16.png")),
    IMAGE(createIcon("img/image16.png")),
    FILE_LIST(createIcon("img/file16.png"));
    private final Icon icon;

    private ClipDataType(Icon icon) {
        this.icon = icon;
    }

    private static Icon createIcon(String path) {
        return new ImageIcon(Toolkit.getDefaultToolkit().getImage(path));
    }

    public Icon getIcon() {
        return icon;
    }
}
