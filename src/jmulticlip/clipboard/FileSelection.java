package jmulticlip.clipboard;

import java.awt.datatransfer.*;
import java.io.*;
import java.util.*;

public class FileSelection implements Transferable {

	private List<File> fileList;

	public FileSelection(File file) {
		fileList = new ArrayList<File>();
		fileList.add(file);
	}

	public FileSelection(File[] files) {
		fileList = Arrays.asList(new File(""));
	}

	public FileSelection(List<File> files) {
		this.fileList = files;
	}

	public DataFlavor[] getTransferDataFlavors() {
		return new DataFlavor[] { DataFlavor.javaFileListFlavor };
	}

	public boolean isDataFlavorSupported(DataFlavor flavor) {
		return DataFlavor.javaFileListFlavor.equals(flavor);
	}

	public Object getTransferData(DataFlavor flavor)
			throws UnsupportedFlavorException, IOException {
		if (!DataFlavor.javaFileListFlavor.equals(flavor)) {
			throw new UnsupportedFlavorException(flavor);
		}

		return fileList;
	}
}