package jmulticlip.clipboard;

import com.sun.jna.Platform;
import java.io.*;
import java.util.*;
import java.awt.*;
import java.awt.datatransfer.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import jmulticlip.gui.MainFrame;
import jmulticlip.gui.graph.CanvasPanel;
import jmulticlip.gui.graph.GraphicsViewPanel;
import jmulticlip.gui.navig.ClipDataRow;
import jmulticlip.core.ErrorManager;

/**
 *
 * @author Revers
 */
public class ClipboardManager implements ClipboardListener, ClipboardOwner {

    private int imageCounter = 0;
    private ClipboardObserver clipboardObserver;

    private ClipboardManager() {
        clipboardObserver = new ClipboardObserver(this);
    }

    public static ClipboardManager getInstance() {
        return ClipboardManagerHolder.INSTANCE;
    }

    public ClipboardObserver getClipboardObserver() {
        return clipboardObserver;
    }

    public void setClipboardImage(Image img) {
        ImageSelection imgSel = new ImageSelection(img);
        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(imgSel, this);
    }

    public void setClipbardText(String text) {
        StringSelection stringSelection = new StringSelection(text);
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        clipboard.setContents(stringSelection, this);
    }

    public void setClipboardFiles(File... files) {
        FileSelection fileSelection = new FileSelection(files);
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        clipboard.setContents(fileSelection, this);
    }

    public void setClipboardFiles(java.util.List<File> fileList) {
        FileSelection fileSelection = new FileSelection(fileList);
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        clipboard.setContents(fileSelection, this);
    }

    public void ctrlC() {
        try {
            Robot r = new Robot();
            int ctrlKey = Platform.isMac() ? KeyEvent.VK_META : KeyEvent.VK_CONTROL;
            
            r.keyPress(ctrlKey);
            r.keyPress(KeyEvent.VK_C);
            r.keyRelease(KeyEvent.VK_C);
            r.keyRelease(ctrlKey);
            
        } catch (AWTException e) {
            ErrorManager.errorMinor(ClipboardManager.class, e,
                    "Cannot press key: Ctrl-C!!");
        }
    }

    public void ctrlV() {
        try {
            Robot r = new Robot();
            int ctrlKey = Platform.isMac() ? KeyEvent.VK_META : KeyEvent.VK_CONTROL;
            
            r.keyPress(ctrlKey);
            r.keyPress(KeyEvent.VK_V);
            r.keyRelease(KeyEvent.VK_V);
            r.keyRelease(ctrlKey);
            
        } catch (AWTException e) {
            ErrorManager.errorMinor(ClipboardManager.class, e,
                    "Cannot press key: Ctrl-V!!");
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public void cliboardChanged(Object value, ClipDataType type) {
        MainFrame mf = MainFrame.getInstance();

        ClipDataRow cd = null;
        if (type == ClipDataType.TEXT) {

            cd = new ClipDataRow(mf.getDefaultTextView(), (String) value);

        } else if (type == ClipDataType.FILE_LIST) {

            StringBuilder sb = new StringBuilder();
            java.util.List<File> fileList = (java.util.List<File>) value;
            int i = 0;
            for (File file : fileList) {
                sb.append(file.getAbsolutePath());
                if (i++ < fileList.size() - 1) {
                    sb.append('\n');
                }
            }
            String text = sb.toString();
            cd = new ClipDataRow(mf.getDefaultTextView(), text, type);

        } else if (type == ClipDataType.IMAGE) {

            Image img = (Image) value;
            String name = "Image #" + (++imageCounter);
            CanvasPanel canvas = new CanvasPanel(img);

            GraphicsViewPanel imageView = mf.getDefaultImageView();

            cd = new ClipDataRow(imageView, name, type);
            cd.setCanvas(canvas);

            System.out.println("imag = " + img + "; width = "
                    + img.getWidth(null) + "; height = " + img.getHeight(null) + "\n\t" + img.getSource());

            // imageView.setCanvas(canvas);

        }

        if (cd == null || cd.getShortText().equals("")) {
            return;
        }

        mf.getNavigationPanel().getNavigTablePanel().addClipDataRow(cd);
        mf.getNavigationPanel().requestFocusInWindow();
    }

    @Override
    public void lostOwnership(Clipboard e, Transferable t) {
        System.out.println("lostOwnership(): " + e + "; " + t);
    }

    private static class ClipboardManagerHolder {

        private static final ClipboardManager INSTANCE = new ClipboardManager();
    }
}
