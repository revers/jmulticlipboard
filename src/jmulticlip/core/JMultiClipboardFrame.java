package jmulticlip.core;

import jmulticlip.scripts.impl.cpp.CppHeaderGenerator;
import jmulticlip.gui.navig.ClipDataRow;
import jmulticlip.gui.navig.ClipImageCellRenderer;
import jmulticlip.clipboard.FileSelection;
import jmulticlip.clipboard.ClipboardObserver;
import jmulticlip.clipboard.ClipboardListener;
import jmulticlip.clipboard.ClipDataType;
import jmulticlip.gui.EditorPanel;
import jmulticlip.gui.View;
import jmulticlip.gui.ViewType;
import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.event.*;
import java.io.*;
import java.net.URI;
import java.net.URL;
import java.net.URLEncoder;
import java.util.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import jmulticlip.gui.graph.CanvasPanel;
import jmulticlip.gui.graph.GraphicsViewPanel;

import static java.awt.GridBagConstraints.*;
import jmulticlip.util.GUIUtils;

import pl.ugu.revers.swing.*;
import pl.ugu.revers.swing.table.*;

/*
 * TODO:
 * http://www.exampledepot.com/egs/java.awt.datatransfer/ToClipImg.html
 * dodac wrzucanie na wklej.org od razu
 * dodac zaznaczanie ekranu tak jak w aqq "zaznacz fragment"
 */
@SuppressWarnings("serial")
public class JMultiClipboardFrame extends JFrame implements ClipboardListener,
        ComponentListener, ClipboardOwner, Runnable {

    private static final int MARGIN = 10;
    private JMenu fileMenu = new JMenu("Plik");
    private JMenu helpMenu = new JMenu("Pomoc");
    private JMenuItem exitItem = new JMenuItem("Wyjście");
    private JMenuItem aboutItem = new JMenuItem("O programie...");
    private JButton deleteJB = new JButton(new ImageIcon("img/delete.png"));
    private JButton goUpJB = new JButton(new ImageIcon("img/go-up.png"));
    private JButton goDownJB = new JButton(new ImageIcon("img/go-down.png"));
    private JButton goTopJB = new JButton(new ImageIcon("img/go-top.png"));
    private JButton goBottomJB = new JButton(new ImageIcon("img/go-bottom.png"));
    private JToggleButton javaTB = new JToggleButton(new ImageIcon("img/j.png"));
    private JToggleButton javaArrayTB = new JToggleButton(new ImageIcon(
            "img/Capital-J.png"));
    private JToggleButton fileTB = new JToggleButton(new ImageIcon("img/f.png"));
    private JToggleButton youtubeTB = new JToggleButton(new ImageIcon(
            "img/y.png"));
    private JToggleButton imageTB = new JToggleButton(
            new ImageIcon("img/i.png"));
    private JToggleButton urlTB = new JToggleButton(new ImageIcon("img/u.png"));
    private JToggleButton regexpTB = new JToggleButton(new ImageIcon(
            "img/r.png"));
    private JToggleButton invertTB = new JToggleButton(new ImageIcon(
            "img/Capital-I.png"));
    private JToggleButton cppTB = new JToggleButton(new ImageIcon("img/c.png"));
    private JToggleButton cppPrototypeTB = new JToggleButton(new ImageIcon(
            "img/Capital-C.png"));
    private JToggleButton cppInlineTB = new JToggleButton(new ImageIcon(
            "img/d.png"));
    private JToggleButton[] toggleButtons = {javaTB, fileTB, youtubeTB,
        imageTB, urlTB, javaArrayTB, regexpTB, invertTB, cppTB,
        cppPrototypeTB, cppInlineTB};
    private ExtendedTextArea previewTA = new ExtendedTextArea();
    // private ClipTable clipTable = new ClipTable(clipTableModel);
    private JPanel mainPanel = new JPanel();
    private JSplitPane splitPane;
    private ClipboardObserver clipObserver;
    private boolean shiftPressed = false;
    private static final Column[] COLUMNS = {
        new Column(Integer.class, "#", false),
        new Column(String.class, "Typ", false),
        new Column(String.class, "Fragment", false),
        new Column(Boolean.class, "Utrzymaj pozycje", true),};
    private ExtendedTableModel<ClipDataRow> clipTableModel
            = new ExtendedTableModel<ClipDataRow>(COLUMNS);
    private ExtendedTableUtils tableUtils;
    private JTable clipTable;
    private static JMultiClipboardFrame jmultiClipFrame;
    private ClipDataRow currentClipDataRow;
    private JScrollPane leftScrollPane;
    private JScrollPane rightScrollPane;
    private View textView = new View() {

        @Override
        public ViewType getType() {
            return ViewType.TEXT_EDITOR;
        }
    };
    private GraphicsViewPanel imageView = new GraphicsViewPanel();
    private EditorPanel viewContainer;

    private JMultiClipboardFrame() {
        SwingUtils.setSizeAndCenter(this, 1000, 800);

        clipTable = new JTable(clipTableModel);
        initTable();

        initMenu();
        initComponents();

        GUIUtils.useApplicationLAF(this);
        addGlobalShortcuts();
        addTableShortcuts();

        clipObserver = new ClipboardObserver(this);
        addComponentListener(this);
    }

    public static JMultiClipboardFrame getInstance() {
        if (jmultiClipFrame == null) {
            return (jmultiClipFrame = new JMultiClipboardFrame());
        }

        return jmultiClipFrame;
    }

    public ClipDataRow getCurrentDataRow() {
        return currentClipDataRow;
    }

    public void refreshCurrentView() {
        viewContainer.setView(currentClipDataRow.getCurrentView());
    }

//    public LoadingPanel
    //public void set
//     private void setImageViewAsCurrent() {
//        ((CardLayout) viewContainer.getLayout()).show(viewContainer, "canvasPanel");
//        currentClipDataRow.setCurrentView(imageView);
//    }
//
//    private void setTextViewAsCurrent() {
//        ((CardLayout) viewContainer.getLayout()).show(viewContainer, "textPanel");
//         currentClipDataRow.setCurrentView(textView);
//    }
    private void initTable() {
        tableUtils = new ExtendedTableUtils(clipTable, clipTableModel);
        tableUtils.addShortcuts();
        //tableUtils.setHeaderSelectableColumns(TestDataRow.COLUMN_INCLUDE);
        tableUtils.setBoldHeader();
        tableUtils.setReorderingAllowed(false);
        tableUtils.setHeaderSelectableColumns(ClipDataRow.COLUMN_KEEP_FIXED_POSITION);
        tableUtils.addDoubleClickResizeing();
        clipTable.setRowHeight(clipTable.getRowHeight() + 2);
        clipTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        LabelCellRenderer labelCellRenderer = new LabelCellRenderer();
        tableUtils.setColumnCellRenderer(ClipDataRow.COLUMN_INDEX, labelCellRenderer);
        tableUtils.setColumnCellRenderer(ClipDataRow.COLUMN_SHORT_TEXT, labelCellRenderer);
        tableUtils.setColumnCellRenderer(ClipDataRow.COLUMN_KEEP_FIXED_POSITION, new CheckBoxCellRenderer());

        tableUtils.setColumnCellRenderer(ClipDataRow.COLUMN_TYPE, new ClipImageCellRenderer());

        //0 - NOT_PARSED
        //1 - PARSED_OK
        //2 - PARSED_NO_LINKS
        //3 - ERROR
//        String[] values = {"Nieprzeskanowany", "Przeskanowany", "Bez linków", "Błędny"};
//        MyComboBoxRenderer comboRenderer = new MyComboBoxRenderer(values);
//        tableUtils.setColumnCellRenderer(PostEntry.COLUMN_STATUS, comboRenderer);
//        MyComboBoxEditor comboEditor = new MyComboBoxEditor(values);
//        tableUtils.setColumnCellEditor(PostEntry.COLUMN_STATUS, comboEditor);
//        //tableUtils.setColumnCellRenderer(TestDataRow.COLUMN_INCLUDE, new CheckBoxCellRenderer());
//        URLCellRenderer urlRenderer = new URLCellRenderer(table, PostEntry.COLUMN_URL, URLCellRenderer.LOCALE_PL);
//        tableUtils.setColumnCellRenderer(PostEntry.COLUMN_URL, urlRenderer);
        // tableUtils.setOptimalWidthOnInit();
        tableUtils.setInitColumnSizePercent(0.1f, 0.1f, 0.65f, 0.15f);
        // tableUtils.setOptimalWidthOnInit();

//        tableModel.addTableModelListener(new TableModelListener() {
//
//            public void tableChanged(TableModelEvent e) {
//                rowCountJL.setText(Integer.toString(tableModel.getRowCount()));
//                if (canAddPostToList == false) {
//                    return;
//                }
//                if (e.getType() == TableModelEvent.DELETE) {
//                    System.out.println("table size = " + tableModel.getRowCount());
//                    System.out.println("e.getFirst() = " + e.getFirstRow());
//                    deletedPostList.add(tableModel.getDataRow(e.getFirstRow()));
//                } else if (e.getType() == TableModelEvent.UPDATE) {
//                    editedPostList.add(tableModel.getDataRow(e.getFirstRow()));
//                }
//            }
//        });
    }

    public void searchLastTextOnGoolge() {
        String text = null;
        for (int i = 0; i < clipTableModel.getRowCount(); i++) {
            ClipDataRow cd = clipTableModel.getDataRow(i);
            if (cd.getType() == ClipDataType.TEXT) {
                text = cd.getFullText();
                break;
            }
        }

        if (text == null) {
            return;
        }

        try {

            URL url = new URL(
                    "http://www.google.com/search?client=opera&rls=pl&q="
                    + URLEncoder.encode(text, "UTF8")
                    + "&sourceid=opera&ie=utf-8&oe=utf-8");
            Desktop.getDesktop().browse(url.toURI());
        } catch (Exception ex) {
            ErrorDialog.showErrorAndWait(ex);
        }
    }

    private void initComponents() {
        Image icon = Toolkit.getDefaultToolkit().getImage("./ico.png");
        // new ImageIcon(ClassLoader.getSystemResource("ico.png")).getImage();
        setIconImage(icon);

        leftScrollPane = new JScrollPane(clipTable,
                JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

        viewContainer = new EditorPanel();
        viewContainer.setView(textView);
//        
//        viewContainer.setLayout(new CardLayout());
        //editPanel 

        rightScrollPane = new JScrollPane(previewTA,
                JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

        textView.setLayout(new BorderLayout());
        textView.add(rightScrollPane, BorderLayout.CENTER);

        // viewContainer.add(textView, "textPanel");
        // viewContainer.add(imageView, "canvasPanel");
        splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, leftScrollPane,
                viewContainer);

        setDefaultCloseOperation(DISPOSE_ON_CLOSE);

        previewTA.setFont(new Font("Courier New", Font.PLAIN, 12));
        previewTA.setLocale(ExtendedTextArea.LOCALE_PL);

        setLayout(new BorderLayout());

        mainPanel.setBorder(new EmptyBorder(new Insets(MARGIN, MARGIN, MARGIN,
                MARGIN)));
        mainPanel.setLayout(new BorderLayout());

        JPanel buttonsPanel = new JPanel();
        // buttonsPanel.setLayout(new BoxLayout(buttonsPanel,
        // BoxLayout.X_AXIS));
        buttonsPanel.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.insets = new Insets(0, 0, 0, 5);
        c.fill = NONE;

        c.gridx = 0;
        buttonsPanel.add(goUpJB, c);
        c.gridx++;
        buttonsPanel.add(goDownJB, c);
        c.gridx++;
        buttonsPanel.add(goTopJB, c);
        c.gridx++;
        buttonsPanel.add(goBottomJB, c);
        c.gridx++;
        buttonsPanel.add(deleteJB, c);
        c.insets = new Insets(0, 10, 0, 5);
        c.gridx++;
        buttonsPanel.add(javaTB, c);
        c.insets = new Insets(0, 0, 0, 5);
        c.gridx++;
        buttonsPanel.add(fileTB, c);
        c.gridx++;
        buttonsPanel.add(urlTB, c);
        c.gridx++;
        buttonsPanel.add(imageTB, c);
        c.gridx++;
        buttonsPanel.add(regexpTB, c);
        c.gridx++;
        buttonsPanel.add(youtubeTB, c);
        c.gridx++;
        buttonsPanel.add(javaArrayTB, c);
        c.gridx++;
        buttonsPanel.add(invertTB, c);
        c.gridx++;
        buttonsPanel.add(cppPrototypeTB, c);
        c.gridx++;
        buttonsPanel.add(cppTB, c);

        c.gridx++;
        c.weightx = 1.0;
        c.anchor = WEST;
        buttonsPanel.add(cppInlineTB, c);

        splitPane.setBorder(new EmptyBorder(new Insets(MARGIN, 0, 0, 0)));
        splitPane.setOneTouchExpandable(true);
        splitPane.setDividerLocation(258);

        mainPanel.add(buttonsPanel, BorderLayout.NORTH);
        mainPanel.add(splitPane, BorderLayout.CENTER);

        add(mainPanel, BorderLayout.CENTER);

        // javaTB, fileTB, youtubeTB,
        // imageTB, urlTB };
        javaTB.setToolTipText("Kopiuj jako Java String");
        fileTB.setToolTipText("Kopiuj jako plik(i)");
        youtubeTB.setToolTipText("Kopiuj w tagach [youtube][/youtube]");
        imageTB.setToolTipText("Kopiuj w tagach [img][/img]");
        urlTB.setToolTipText("Kopiuj w tagach [url=...][/url]");
        goBottomJB.setToolTipText("Przesuń wiersze na koniec");
        goTopJB.setToolTipText("Przesuń wiersze na początek");
        goDownJB.setToolTipText("Przesuń wiersze do góry");
        goUpJB.setToolTipText("Przesuń wiersze do dołu");
        deleteJB.setToolTipText("Usuń zaznaczone wiersze");
        javaArrayTB.setToolTipText("Kopiuj jako tablica String[]");
        regexpTB.setToolTipText("Kopiuj jako wyrażenie regularne");
        invertTB.setToolTipText("Kopiuj wiersze w odwrotnej kolejności");
        cppTB.setToolTipText("Rozdziel dane naglowka C++ na deklaracje i implementacje");
        cppPrototypeTB.setToolTipText("Utworz definicje z prototypow C++");
        cppInlineTB.setToolTipText("Utwórz funkcje inline z funkcji w pliku naglowkowym C++");

        addListeners();
    }

    private void initMenu() {
        JMenuBar menuBar = new JMenuBar();

        fileMenu.add(exitItem);
        helpMenu.add(aboutItem);

        menuBar.add(fileMenu);
        menuBar.add(helpMenu);

        setJMenuBar(menuBar);

        addMenuListeners();
    }

    private String getJavaArrayString(String s) {
        s = s.replace("\\", "\\\\");
        s = s.replace("\"", "\\\"");

        StringBuilder result = new StringBuilder();
        result.append("{ ");
        String[] tokens = s.split("\n");
        for (int i = 0; i < tokens.length; i++) {
            result.append("\"" + tokens[i] + "\",\n");
        }
        result.append("};");

        return result.toString();
    }

    private String getRegexpString(String s) {
        s = s.replace("\\", "\\\\\\\\");
        s = s.replace("\"", "\\\"");
        s = s.replace("[", "\\\\[");
        s = s.replace("]", "\\\\]");
        s = s.replace("*", "\\\\*");
        s = s.replace("+", "\\\\+");
        s = s.replace("?", "\\\\?");
        s = s.replace(".", "\\\\.");
        s = s.replace("^", "\\\\^");
        s = s.replace("$", "\\\\$");
        s = s.replace("&", "\\\\&");
        s = s.replace("|", "\\\\|");
        s = s.replace("{", "\\\\{");
        s = s.replace("}", "\\\\}");
        s = s.replace(")", "\\\\)");
        s = s.replace("(", "\\\\(");

        return "\"" + s + "\"";
    }

    private String getJavaString(String s) {
        s = s.replace("\\", "\\\\");
        s = s.replace("\"", "\\\"");
        StringBuilder result = new StringBuilder();
        String[] tokens = s.split("\n");
        for (int i = 0; i < tokens.length; i++) {
            result.append("\"").append(tokens[i]);
            if (i < tokens.length - 1) {
                result.append("\\n\" +\n");
            } else {
                result.append("\"");
            }
        }
        return result.toString();
    }

    private String getImageString(String s) {
        StringBuilder builder = new StringBuilder();
        String[] lines = s.split("\\n");
        for (int i = 0; i < lines.length; i++) {
            if (lines[i].equals("")) {
                builder.append(lines[i]);
            } else {
                builder.append("[img]" + lines[i] + "[/img]");
            }
            if (i < lines.length - 1) {
                builder.append("\n\n");
            }
        }
        return builder.toString();
    }

    private String getYoutubeString(String s) {
        StringBuilder builder = new StringBuilder();
        String[] lines = s.split("\\n");
        for (int i = 0; i < lines.length; i++) {
            if (lines[i].equals("")) {
                builder.append(lines[i]);
            } else {
                builder.append("[youtube]" + lines[i] + "[/youtube]");
            }
            if (i < lines.length - 1) {
                builder.append("\n\n");
            }
        }
        return builder.toString();
    }

    private String getInvertString(String s) {
        StringBuilder builder = new StringBuilder();
        String[] lines = s.split("\n");
        for (int i = lines.length - 1; i >= 0; i--) {
            builder.append(lines[i]);
            if (i > 0) {
                builder.append("\n");
            }
        }

        return builder.toString();
    }

    private String getCppString(String s) {
        CppHeaderGenerator gen = new CppHeaderGenerator();
        String result = null;
        String className = JOptionPane.showInputDialog(null,
                "Podaj nazw� klasy:", "Nazwa Klasy",
                JOptionPane.QUESTION_MESSAGE);
        if (className == null) {
            return result;
        }

        try {
            result = gen.generate(className, s);
        } catch (Exception e) {
            ErrorDialog.showErrorAndWait(this, e);
        }

        return result;
    }

    private String getCppPrototypeString(String s) {
        CppHeaderGenerator gen = new CppHeaderGenerator();
        String result = null;
        String className = JOptionPane.showInputDialog(null,
                "Podaj nazw� klasy:", "Nazwa Klasy",
                JOptionPane.QUESTION_MESSAGE);
        if (className == null) {
            return result;
        }

        try {
            result = gen.generateByPrototypes(className, s);
        } catch (Exception e) {
            ErrorDialog.showErrorAndWait(this, e);
        }

        return result;
    }

    private String getCppInlineString(String s) {
        CppHeaderGenerator gen = new CppHeaderGenerator();
        String result = null;
        String className = JOptionPane.showInputDialog(null,
                "Podaj nazw� klasy:", "Nazwa Klasy",
                JOptionPane.QUESTION_MESSAGE);
        if (className == null) {
            return result;
        }

        try {
            result = gen.generateInline(className, s);
        } catch (Exception e) {
            ErrorDialog.showErrorAndWait(this, e);
        }

        return result;
    }

    private void enumarteRows() {
        ArrayList<ClipDataRow> rows = clipTableModel.getDataList();
        for (int i = 0; i < rows.size(); i++) {
            rows.get(i).setIndex(i + 1);
        }
    }

    private void copyAction() {
        if (fileTB.isSelected()) {
            copyFilesAction();
        } else {
            copyTextAction();
        }
    }

    private void copyFilesAction() {
        int[] indices = clipTable.getSelectedRows();
        ArrayList<File> fileList = new ArrayList<File>();
        for (int i = 0; i < indices.length; i++) {
            ClipDataRow cd = clipTableModel.getDataRow(indices[i]);
            if (cd.getType() != ClipDataType.FILE_LIST) {
                JOptionPane.showMessageDialog(this, "Zaznaczony wiersz ("
                        + (indices[i] + 1) + ") nie zawiera plik�w!!",
                        "B��d kopiowania plik�w!!", JOptionPane.ERROR_MESSAGE);
                return;
            }

            String[] lines = cd.getFullText().split("\n");
            for (String line : lines) {
                File file = new File(line);
                if (file.exists() == false) {
                    JOptionPane.showMessageDialog(this, "Plik " + line
                            + "\nz wiersza nr " + (indices[i] + 1)
                            + " nie istnieje!!", "B��d kopiowania plik�w!!",
                            JOptionPane.ERROR_MESSAGE);
                    continue;
                }
                fileList.add(file);
            }
        }

        FileSelection fileSelection = new FileSelection(fileList);
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        clipboard.setContents(fileSelection, this);
        setVisible(false);
        deselectToggleButtons();

        if (shiftPressed == false) {
            ctrlV();
        }
    }

    private void copyTextAction() {
        String text = previewTA.getText();

        String str = null;
        if (javaTB.isSelected()) {
            str = getJavaString(text);
        } else if (youtubeTB.isSelected()) {
            str = getYoutubeString(text);
        } else if (imageTB.isSelected()) {
            str = getImageString(text);
        } else if (urlTB.isSelected()) {
            str = "[url=" + text + "][/url]";
        } else if (javaArrayTB.isSelected()) {
            str = getJavaArrayString(text);
        } else if (regexpTB.isSelected()) {
            str = getRegexpString(text);
        } else if (invertTB.isSelected()) {
            str = getInvertString(text);
        } else if (cppTB.isSelected()) {
            str = getCppString(text);
        } else if (cppPrototypeTB.isSelected()) {
            str = getCppPrototypeString(text);
        } else if (cppInlineTB.isSelected()) {
            str = getCppInlineString(text);
        } else {
            str = text;
        }

        if (str == null) {
            return;
        }

        boolean urlTBSelected = urlTB.isSelected();

        clipObserver.setOldText(str);

        StringSelection stringSelection = new StringSelection(str);
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        clipboard.setContents(stringSelection, this);

        deselectToggleButtons();
        setVisible(false);

        if (shiftPressed) {
            return;
        }

        ctrlV();
        try {
            Robot r = new Robot();

            if (urlTBSelected) {
                for (int i = 0; i < 6; i++) {
                    r.keyPress(KeyEvent.VK_LEFT);
                    r.keyRelease(KeyEvent.VK_LEFT);
                }
            }

        } catch (AWTException e) {
            ErrorDialog.showErrorAndWait(this, e);
        }
    }

    private void ctrlV() {
        try {
            Robot r = new Robot();
            r.keyPress(KeyEvent.VK_CONTROL);
            r.keyPress(KeyEvent.VK_V);
            r.keyRelease(KeyEvent.VK_V);
            r.keyRelease(KeyEvent.VK_CONTROL);
        } catch (AWTException e) {
            ErrorDialog.showErrorAndWait(this, e);
        }
    }

    private void goDownAction() {
        int[] selRows = clipTable.getSelectedRows();
        int lastRow = clipTable.getRowCount() - 1;

        boolean lastRows = true;
        for (int i = selRows.length - 1, j = lastRow; i >= 0; i--, j--) {
            if (selRows[i] != j) {
                lastRows = false;
                break;
            }
        }
        if (lastRows) {
            return;
        }

        for (int i = selRows.length - 1; i >= 0; i--) {
            int row = selRows[i];

            if (row == lastRow) {
                continue;
            }

            clipTableModel.swapRows(row, row + 1);
            clipTable.removeRowSelectionInterval(row, row);
            clipTable.addRowSelectionInterval(row + 1, row + 1);
        }
    }

    private void goBottomAction() {
        int[] selRows = clipTable.getSelectedRows();
        int lastRow = clipTable.getRowCount() - 1;
        int moveRow = lastRow;

        for (int i = selRows.length - 1; i >= 0; i--) {
            int row = selRows[i];

            clipTableModel.move(row, moveRow);
            clipTable.removeRowSelectionInterval(row, row);
            clipTable.addRowSelectionInterval(moveRow, moveRow);

            moveRow--;
            if (moveRow < 0) {
                moveRow = 0;
            }
        }
        leftScrollPane.getVerticalScrollBar().setValue(Integer.MIN_VALUE);
    }

    private void goTopAction() {
        int[] selRows = clipTable.getSelectedRows();
        int moveRow = 0;
        int lastRow = clipTable.getRowCount() - 1;

        for (int i = 0; i < selRows.length; i++) {
            int row = selRows[i];

            clipTableModel.move(row, moveRow);
            clipTable.removeRowSelectionInterval(row, row);
            clipTable.addRowSelectionInterval(moveRow, moveRow);

            moveRow++;
            if (moveRow > lastRow) {
                moveRow = lastRow;
            }
        }

        leftScrollPane.getVerticalScrollBar().setValue(Integer.MAX_VALUE);
    }

    private void goUpAction() {
        int[] selRows = clipTable.getSelectedRows();

        boolean firstRows = true;
        for (int i = 0; i < selRows.length; i++) {
            if (selRows[i] != i) {
                firstRows = false;
                break;
            }
        }
        if (firstRows) {
            return;
        }

        for (int row : selRows) {
            if (row == 0) {
                continue;
            }
            clipTableModel.swapRows(row, row - 1);
            clipTable.removeRowSelectionInterval(row, row);
            clipTable.addRowSelectionInterval(row - 1, row - 1);
        }

//        clipTable.printColumnSizes();
    }

    private void deleteAction() {
        // if (!removeSelectedJB.isEnabled())
        // return;
        int[] selRows = clipTable.getSelectedRows();
        if (selRows.length == 0) {
            return;
        }
        clipTable.removeRowSelectionInterval(0, clipTable.getRowCount() - 1);

        // for (int i : selRows) {
        // File f = clipTable.getUploadTableModel().getUploadFile(i);
        // totalSize -= f.length();
        // }
        clipTableModel.removeRows(selRows);

        if (clipTable.getRowCount() > 0) {
            clipTable.setRowSelectionInterval(0, 0);
        }
    }

    private void javaTBAction() {
    }

    private void fileTBAction() {
    }

    //
    // private void removeAllJBActionPerformed(ActionEvent evt) {
    // clipTable.getUploadTableModel().removeAllRows();
    // totalSize = 0;
    // }
    private void deselectToggleButtons() {
        for (JToggleButton button : toggleButtons) {
            button.setSelected(false);
        }
    }

    public void setClipbardText(String text) {
        StringSelection stringSelection = new StringSelection(text);
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        clipboard.setContents(stringSelection, this);
    }

    public void addTextData(String text) {
        ClipDataRow cd = new ClipDataRow(textView, text);
        currentClipDataRow = cd;
        viewContainer.setView(cd.getCurrentView());
        //setTextViewAsCurrent();

        clipTableModel.addRowAtBeginning(cd);
        enumarteRows();
        clipTable.removeRowSelectionInterval(0, clipTable.getRowCount() - 1);
        clipTable.setRowSelectionInterval(0, 0);
        clipTable.repaint();
    }

    private void selectFirstRow() {
        if (clipTableModel.getRowCount() == 0) {
            return;
        }
        if (clipTable.getRowCount() > 1) {
            clipTable.removeRowSelectionInterval(1, clipTable.getRowCount() - 1);
        }
        clipTable.addRowSelectionInterval(0, 0);

        previewTA.setText(clipTableModel.getDataRow(0).getFullText());
        clipTable.requestFocusInWindow();
        EventQueue.invokeLater(this);
    }

    @Override
    public void run() {
        rightScrollPane.getHorizontalScrollBar().setValue(Integer.MIN_VALUE);
        // rightScrollPane.repaint();
    }
    private static int imageCounter = 0;

    @SuppressWarnings("unchecked")
    @Override
    public void cliboardChanged(Object value, ClipDataType type) {

        ClipDataRow cd = null;
        if (type == ClipDataType.TEXT) {

            cd = new ClipDataRow(textView, (String) value);

        } else if (type == ClipDataType.FILE_LIST) {

            StringBuilder sb = new StringBuilder();
            java.util.List<File> fileList = (java.util.List<File>) value;
            int i = 0;
            for (File file : fileList) {
                sb.append(file.getAbsolutePath());
                if (i++ < fileList.size() - 1) {
                    sb.append("\n");
                }
            }
            String text = sb.toString();
            cd = new ClipDataRow(textView, text, type);

        } else if (type == ClipDataType.IMAGE) {

            Image img = (Image) value;
            String name = "Image #" + (++imageCounter);
            CanvasPanel canvas = new CanvasPanel(img);
            cd = new ClipDataRow(imageView, name, type);
            cd.setCanvas(canvas);

            System.out.println("imag = " + img + "; width = "
                    + img.getWidth(null) + "; height = " + img.getHeight(null) + "\n\t" + img.getSource());

            imageView.setCanvas(canvas);

        }

        if (cd == null || cd.getShortText().equals("")) {
            return;
        }

        currentClipDataRow = cd;
        viewContainer.setView(cd.getCurrentView());

        clipTableModel.addRowAtBeginning(cd);
        enumarteRows();
        clipTable.removeRowSelectionInterval(0, clipTable.getRowCount() - 1);
        clipTable.setRowSelectionInterval(0, 0);
        clipTable.repaint();

    }

    @Override
    public void componentHidden(ComponentEvent e) {
    }

    @Override
    public void componentMoved(ComponentEvent e) {
    }

    @Override
    public void componentResized(ComponentEvent e) {
        selectFirstRow();
    }

    @Override
    public void componentShown(ComponentEvent e) {
        selectFirstRow();
    }

    @Override
    public void lostOwnership(Clipboard e, Transferable t) {
        System.out.println("lostOwnership(): " + e + "; " + t);
    }

    //
    // private void removeAllJBActionPerformed(ActionEvent evt) {
    // clipTable.getUploadTableModel().removeAllRows();
    // totalSize = 0;
    // }
    private void addMenuListeners() {
        exitItem.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                InitializationManager.getInstance().closeAll();
                System.exit(0);
            }
        });

        aboutItem.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(JMultiClipboardFrame.this,
                        "Author:  Kamil Kolaczynski\n"
                        + "Contact: kamil.kolaczynski@gmail.com",
                        "JMulitClipboard " + TitleAndStatusManager.APPLICATION_VERSION,
                        JOptionPane.INFORMATION_MESSAGE);
            }
        });
    }

    private void addListeners() {
        ChangeListener toggleListener = new ChangeListener() {

            @Override
            public void stateChanged(ChangeEvent e) {
                JToggleButton toggleButton = (JToggleButton) e.getSource();
                if (toggleButton.isSelected() == false) {
                    return;
                }

                for (JToggleButton button : toggleButtons) {
                    if (button == toggleButton) {
                        continue;
                    }

                    button.setSelected(false);
                }
            }
        };
        for (JToggleButton button : toggleButtons) {
            button.addChangeListener(toggleListener);
        }

        clipTable.addKeyListener(new KeyListener() {

            @Override
            public void keyPressed(KeyEvent e) {

                if (e.getKeyCode() == KeyEvent.VK_SHIFT) {
                    shiftPressed = true;
                } else if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    e.consume();
                    copyAction();
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_SHIFT) {
                    shiftPressed = false;
                }
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    e.consume();
                }
            }

            @Override
            public void keyTyped(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    e.consume();
                }
            }
        });

        clipTable.getSelectionModel().addListSelectionListener(
                new ListSelectionListener() {

                    @Override
                    public void valueChanged(ListSelectionEvent e) {

                        if (e.getValueIsAdjusting() == false) {

                            if (clipTable.getSelectedRow() != -1) {

                                int[] rows = clipTable.getSelectedRows();

                                ClipDataRow row = clipTableModel.getDataRow(
                                        rows[0]);

                                currentClipDataRow = row;

                                if (row.getType() == ClipDataType.IMAGE) {
                                    CanvasPanel canvas = row.getCanvas();
                                    imageView.setCanvas(canvas);
                                    viewContainer.setView(row.getCurrentView());
                                    clipTable.requestFocusInWindow();
                                } else {
                                    viewContainer.setView(row.getCurrentView());
                                    previewTA.setText(row.getFullText());

                                    for (int i = 1; i < rows.length; i++) {
                                        previewTA.append("\n"
                                                + clipTableModel.getDataRow(
                                                        rows[i]).getFullText());
                                    }
                                    EventQueue.invokeLater(JMultiClipboardFrame.this);
                                    clipTable.requestFocusInWindow();
                                }

                            }
                        }
                    }
                });

        goUpJB.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
//                clipTable.printColumnSizes();
                System.out.println(getSize());
                System.out.println("dividerLocation = "
                        + splitPane.getDividerLocation());
                rightScrollPane.getHorizontalScrollBar().setValue(
                        rightScrollPane.getHorizontalScrollBar().getMinimum());
                goUpAction();
            }
        });

        goDownJB.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                goDownAction();
            }
        });

        goTopJB.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                goTopAction();
            }
        });

        goBottomJB.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                goBottomAction();
            }
        });

        deleteJB.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                deleteAction();
            }
        });

        javaTB.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                javaTBAction();
            }
        });

        fileTB.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                fileTBAction();
            }
        });
    }

    private void addTableShortcuts() {
        Action deleteShortcutAction = new AbstractAction() {

            @Override
            public void actionPerformed(ActionEvent e) {
                deleteAction();
            }
        };

        Action goUpShortcutAction = new AbstractAction() {

            @Override
            public void actionPerformed(ActionEvent e) {
                goUpAction();
            }
        };

        Action goDownShortcutAction = new AbstractAction() {

            @Override
            public void actionPerformed(ActionEvent e) {
                goDownAction();
            }
        };

        Action goTopShortcutAction = new AbstractAction() {

            @Override
            public void actionPerformed(ActionEvent e) {
                goTopAction();
            }
        };

        Action goBottomShortcutAction = new AbstractAction() {

            @Override
            public void actionPerformed(ActionEvent e) {
                goBottomAction();
            }
        };

        KeyStroke deleteKey = KeyStroke.getKeyStroke(KeyEvent.VK_D, 0, false);
        KeyStroke goUpKey = KeyStroke.getKeyStroke(KeyEvent.VK_UP,
                InputEvent.CTRL_MASK, true);
        KeyStroke goDownKey = KeyStroke.getKeyStroke(KeyEvent.VK_DOWN,
                InputEvent.CTRL_MASK, true);
        KeyStroke goTopKey = KeyStroke.getKeyStroke(KeyEvent.VK_UP,
                InputEvent.CTRL_MASK | InputEvent.ALT_MASK, true);
        KeyStroke goBottomKey = KeyStroke.getKeyStroke(KeyEvent.VK_DOWN,
                InputEvent.CTRL_MASK | InputEvent.ALT_MASK, true);

        leftScrollPane.registerKeyboardAction(deleteShortcutAction, "delete",
                deleteKey, JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

        leftScrollPane.registerKeyboardAction(goDownShortcutAction, "goDown",
                goDownKey, JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

        leftScrollPane.registerKeyboardAction(goUpShortcutAction, "goUp",
                goUpKey, JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

        leftScrollPane.registerKeyboardAction(goBottomShortcutAction,
                "goBottom", goBottomKey,
                JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

        leftScrollPane.registerKeyboardAction(goTopShortcutAction, "goTop",
                goTopKey, JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

    }

    private void addGlobalShortcuts() {
        Action javaAction = new AbstractAction() {

            @Override
            public void actionPerformed(ActionEvent e) {
                javaTB.setSelected(!javaTB.isSelected());
            }
        };
        Action fileAction = new AbstractAction() {

            @Override
            public void actionPerformed(ActionEvent e) {
                fileTB.setSelected(!fileTB.isSelected());
            }
        };

        Action youtubeAction = new AbstractAction() {

            @Override
            public void actionPerformed(ActionEvent e) {
                youtubeTB.setSelected(!youtubeTB.isSelected());
            }
        };
        Action imageAction = new AbstractAction() {

            @Override
            public void actionPerformed(ActionEvent e) {
                imageTB.setSelected(!imageTB.isSelected());
            }
        };

        Action javaArrayAction = new AbstractAction() {

            @Override
            public void actionPerformed(ActionEvent e) {
                javaArrayTB.setSelected(!javaArrayTB.isSelected());
            }
        };

        Action invertAction = new AbstractAction() {

            @Override
            public void actionPerformed(ActionEvent e) {
                invertTB.setSelected(!invertTB.isSelected());
            }
        };

        Action regexpAction = new AbstractAction() {

            @Override
            public void actionPerformed(ActionEvent e) {
                regexpTB.setSelected(!regexpTB.isSelected());
            }
        };

        Action cppAction = new AbstractAction() {

            @Override
            public void actionPerformed(ActionEvent e) {
                cppTB.setSelected(!cppTB.isSelected());
            }
        };

        Action cppPrototypeAction = new AbstractAction() {

            @Override
            public void actionPerformed(ActionEvent e) {
                cppPrototypeTB.setSelected(!cppPrototypeTB.isSelected());
            }
        };

        Action cppInlineAction = new AbstractAction() {

            @Override
            public void actionPerformed(ActionEvent e) {
                cppInlineTB.setSelected(!cppInlineTB.isSelected());
            }
        };

        // Action enterAction = new AbstractAction() {
        // @Override
        // public void actionPerformed(ActionEvent e) {
        // copySelectedAction();
        // }
        // };
        Action escapeAction = new AbstractAction() {

            @Override
            public void actionPerformed(ActionEvent e) {
                // javaTB.setSelected(false);
                deselectToggleButtons();
                setVisible(false);
            }
        };

        Action urlAction = new AbstractAction() {

            @Override
            public void actionPerformed(ActionEvent e) {
                urlTB.setSelected(!urlTB.isSelected());
            }
        };

        KeyStroke urlKey = KeyStroke.getKeyStroke(KeyEvent.VK_U, 0, true);
        KeyStroke javaKey = KeyStroke.getKeyStroke(KeyEvent.VK_J, 0, true);
        KeyStroke fileKey = KeyStroke.getKeyStroke(KeyEvent.VK_F, 0, true);
        KeyStroke youtubeKey = KeyStroke.getKeyStroke(KeyEvent.VK_Y, 0, true);
        KeyStroke imageKey = KeyStroke.getKeyStroke(KeyEvent.VK_I, 0, true);
        KeyStroke javaArrayKey = KeyStroke.getKeyStroke(KeyEvent.VK_J,
                InputEvent.SHIFT_MASK, true);
        KeyStroke invertKey = KeyStroke.getKeyStroke(KeyEvent.VK_I,
                InputEvent.SHIFT_MASK, true);
        KeyStroke regexpKey = KeyStroke.getKeyStroke(KeyEvent.VK_R, 0, true);
        // KeyStroke enterKey = KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0,
        // true);
        KeyStroke escapeKey = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0,
                true);

        KeyStroke cppKey = KeyStroke.getKeyStroke(KeyEvent.VK_C, 0, true);
        KeyStroke cppInlineKey = KeyStroke.getKeyStroke(KeyEvent.VK_D, 0, true);
        KeyStroke cppPrototypeKey = KeyStroke.getKeyStroke(KeyEvent.VK_C,
                InputEvent.SHIFT_MASK, true);

        mainPanel.registerKeyboardAction(cppAction, "cpp", cppKey,
                JComponent.WHEN_IN_FOCUSED_WINDOW);

        mainPanel.registerKeyboardAction(cppInlineAction, "cppInline",
                cppInlineKey, JComponent.WHEN_IN_FOCUSED_WINDOW);

        mainPanel.registerKeyboardAction(cppPrototypeAction, "cppPrototype",
                cppPrototypeKey, JComponent.WHEN_IN_FOCUSED_WINDOW);

        mainPanel.registerKeyboardAction(urlAction, "urlMode", urlKey,
                JComponent.WHEN_IN_FOCUSED_WINDOW);
        mainPanel.registerKeyboardAction(javaAction, "javaMode", javaKey,
                JComponent.WHEN_IN_FOCUSED_WINDOW);
        mainPanel.registerKeyboardAction(fileAction, "fileMode", fileKey,
                JComponent.WHEN_IN_FOCUSED_WINDOW);
        mainPanel.registerKeyboardAction(youtubeAction, "youtubeMode",
                youtubeKey, JComponent.WHEN_IN_FOCUSED_WINDOW);
        mainPanel.registerKeyboardAction(imageAction, "imageMode", imageKey,
                JComponent.WHEN_IN_FOCUSED_WINDOW);

        mainPanel.registerKeyboardAction(escapeAction, "escape", escapeKey,
                JComponent.WHEN_IN_FOCUSED_WINDOW);

        mainPanel.registerKeyboardAction(javaArrayAction, "javaArray",
                javaArrayKey, JComponent.WHEN_IN_FOCUSED_WINDOW);

        mainPanel.registerKeyboardAction(invertAction, "invertArray",
                invertKey, JComponent.WHEN_IN_FOCUSED_WINDOW);

        mainPanel.registerKeyboardAction(regexpAction, "regexp", regexpKey,
                JComponent.WHEN_IN_FOCUSED_WINDOW);

        // mainPanel.registerKeyboardAction(enterAction, "enter", enterKey,
        // JComponent.WHEN_IN_FOCUSED_WINDOW);
    }

    public static void main(String[] args) {
        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {

            @Override
            public void uncaughtException(Thread t, Throwable e) {
                ErrorDialog.showErrorAndWait(new Exception(e));
            }
        });

        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                JMultiClipboardFrame frame = JMultiClipboardFrame.getInstance();
                frame.setVisible(true);
            }
        });
    }
}
