package jmulticlip.core;

import java.util.*;
import jmulticlip.clipboard.ClipboardManager;
import jmulticlip.config.GUIProperties;
import jmulticlip.config.GlobalProperties;
import jmulticlip.config.Options;
import jmulticlip.gui.MainFrame;
import jmulticlip.gui.shortcut.ShortcutManager;
import jmulticlip.hotkey.HotkeyManager;
import jmulticlip.i18n.I18NManager;
import jmulticlip.ifaces.Initializable;
import jmulticlip.ifaces.Resetable;
import jmulticlip.scripts.ScriptHelper;
import jmulticlip.scripts.ScriptRunner;
import jmulticlip.util.GUIUtils;

/**
 *
 * @author Revers
 */
public class InitializationManager {

    private static ArrayList<Initializable> initializableComponents = new ArrayList<Initializable>();
    private ArrayList<Resetable> resetableComponents = new ArrayList<Resetable>();

    public void addResetable(Resetable resetable) {
        resetableComponents.add(resetable);
    }

    public void removeResetable(Resetable resetable) {
        resetableComponents.add(resetable);
    }

    public ArrayList<Resetable> getResetables() {
        return resetableComponents;
    }

    public void addInitializable(Initializable initializable) {
        initializableComponents.add(initializable);
    }

    public void removeInitializable(Initializable initializable) {
        initializableComponents.add(initializable);
    }

    public ArrayList<Initializable> getInitializables() {
        return initializableComponents;
    }

    public void resetAll() {
        for (Resetable r : resetableComponents) {
            r.reset();
        }
    }

    private InitializationManager() {
    }

    public void init() {
        HotkeyManager.getInstance();
        Options.getInstance();
        GlobalProperties.getInstance();
        GUIProperties.getInstance();

        I18NManager.getInstance();

        GUIUtils.getInstance();

        ShortcutManager.getInstance();

        ScriptHelper.getInstance();
        ScriptRunner.getInstance();

        MainFrame.getInstance();

        TitleAndStatusManager.getInstance();

        ClipboardManager.getInstance();

        for (Initializable ini : initializableComponents) {
            ini.init();
        }
    }
    
    public void closeAll() {
        HotkeyManager.getInstance().cleanUp();
    }

    public static InitializationManager getInstance() {
        return InitializationManagerHolder.INSTANCE;
    }

    private static class InitializationManagerHolder {

        private static final InitializationManager INSTANCE = new InitializationManager();
    }
}
