package jmulticlip.core;

import jmulticlip.gui.MainFrame;
import jmulticlip.util.GUIUtils;
import org.apache.log4j.*;
import pl.ugu.revers.swing.ErrorDialog;

/**
 *
 * @author Revers
 */
public class ErrorManager {

    /**
     * SILENT - ignore, log only.
     * MINOR - you can ignore without bigger loss
     * NORMAL - can cause serious problems but not always
     * MAJOR - for sure will cause serious problems
     * FATAL - whole application will crash
     */
    public static enum Type {

        SILENT, MINOR, NORMAL, MAJOR, FATAL
    }
    private static final Logger log = Logger.getLogger(ErrorManager.class.getName());

    private ErrorManager() {
    }

    public static void error(Throwable t, Type type) {
        error(null, t, type, null);
    }

    public static void error(Throwable t, Type type, String msg) {
        error(null, t, type, msg);
    }

    public static void error(Class<?> cls, Throwable t, Type type, String msg) {
        switch (type) {
            case SILENT:
                errorSilent(cls, t, msg);
                break;
            case MINOR:
                errorMinor(cls, t, msg);
                break;
            case NORMAL:
                errorNormal(cls, t, msg);
                break;
            case MAJOR:
                errorMajor(cls, t, msg);
                break;
            case FATAL:
                errorFatal(cls, t, msg);
                break;
        }
    }

    private static String getMsg(Type type, Class<?> cls, Throwable t, String msg) {
        String result = type.toString() + ": ";
        if (cls != null && msg != null) {
            result += cls.getCanonicalName() + ": " + msg;
        } else if (cls != null && msg == null) {
            result += cls.getCanonicalName();
        } else if (cls == null && msg != null) {
            result += msg;
        }

        return result;
    }

    public static void errorSilent(Throwable t) {
        errorSilent(null, t, null);
    }

    public static void errorSilent(Throwable t, String msg) {
        errorSilent(null, t, null);
    }

    public static void errorSilent(Class<?> cls, Throwable t) {
        errorSilent(cls, t, null);
    }

    public static void errorSilent(Class<?> cls, Throwable t, String msg) {
        msg = getMsg(Type.SILENT, cls, t, msg);
        log.error(msg, t);

        // TODO: remove later:
      //  ErrorDialog.showError(t, msg);
        

    }

    public static void errorMinor(Throwable t) {
        errorMinor(null, t, null);
    }

    public static void errorMinor(Throwable t, String msg) {
        errorMinor(null, t, null);
    }

    public static void errorMinor(Class<?> cls, Throwable t) {
        errorMinor(cls, t, null);
    }

    public static void errorMinor(Class<?> cls, Throwable t, String msg) {
        msg = getMsg(Type.MINOR, cls, t, msg);
        log.error(msg, t);

        ErrorDialog.showError(t, msg);
        

    }

    public static void errorNormal(Throwable t) {
        errorNormal(null, t, null);
    }

    public static void errorNormal(Throwable t, String msg) {
        errorNormal(null, t, null);
    }

    public static void errorNormal(Class<?> cls, Throwable t) {
        errorNormal(cls, t, null);
    }

    public static void errorNormal(Class<?> cls, Throwable t, String msg) {
        msg = getMsg(Type.NORMAL, cls, t, msg);
        log.error(msg, t);

        ErrorDialog.showError(t, msg);
        

    }

    public static void errorMajor(Throwable t) {
        errorMajor(null, t, null);
    }

    public static void errorMajor(Throwable t, String msg) {
        errorMajor(null, t, null);
    }

    public static void errorMajor(Class<?> cls, Throwable t) {
        errorMajor(cls, t, null);
    }

    public static void errorMajor(Class<?> cls, Throwable t, String msg) {
        msg = getMsg(Type.MAJOR, cls, t, msg);
        log.error(msg, t);

        ErrorDialog.showError(t, msg);
        

    }

    public static void errorFatal(Throwable t) {
        errorFatal(null, t, null);
    }

    public static void errorFatal(Throwable t, String msg) {
        errorFatal(null, t, null);
    }

    public static void errorFatal(Class<?> cls, Throwable t) {
        errorFatal(cls, t, null);
    }

    public static void errorFatal(Class<?> cls, Throwable t, String msg) {
        msg = getMsg(Type.FATAL, cls, t, msg);
        log.error(msg, t);

        ErrorDialog.showError(t, msg);
        

    }

    public static void main(String[] args) {
        ErrorDialog.setShowStackTrace(false);

        try {
            throw new NullPointerException("bleble");
        } catch (Exception e) {
            ErrorManager.errorNormal(ErrorManager.class, e, "Jakis wyjatek :P");
        }
    }
}
