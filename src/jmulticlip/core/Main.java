package jmulticlip.core;

import com.tulskiy.keymaster.common.HotKey;
import com.tulskiy.keymaster.common.HotKeyListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.swing.KeyStroke;

import pl.ugu.revers.swing.ErrorDialog;
import jmulticlip.clipboard.ClipboardManager;
import jmulticlip.gui.MainFrame;
import jmulticlip.hotkey.HotkeyManager;
import jmulticlip.screencapture.ScreenFragmentFrame;
import java.util.*;

import com.sun.jna.Platform;
import java.io.IOException;
import javax.script.ScriptEngineFactory;

public class Main implements HotKeyListener {

    private static final KeyStroke CTRL_SHIFT_V = KeyStroke.getKeyStroke((int) 'V', Event.CTRL_MASK | Event.SHIFT_MASK);
    private static final KeyStroke CTRL_SHIFT_M = KeyStroke.getKeyStroke((int) 'M', Event.CTRL_MASK | Event.SHIFT_MASK);
    private static final KeyStroke CTRL_SHIFT_C = KeyStroke.getKeyStroke((int) 'C', Event.CTRL_MASK | Event.SHIFT_MASK);

    private MainFrame mainFrame = MainFrame.getInstance();

    public Main() {
        HotkeyManager hotkeyManager = HotkeyManager.getInstance();

        try {
            hotkeyManager.register(CTRL_SHIFT_V, this);
            hotkeyManager.register(CTRL_SHIFT_M, this);
            hotkeyManager.register(CTRL_SHIFT_C, this);
        } catch (Exception ex) {
            ErrorDialog.showErrorAndWait(ex);
        }
        setupSysTray();
    }

    private void setupSysTray() {

        if (SystemTray.isSupported()) {

            SystemTray tray = SystemTray.getSystemTray();
            Image image = Toolkit.getDefaultToolkit().getImage("./ico.png");
            //  new ImageIcon(ClassLoader.getSystemResource("ico.png")).getImage();

            ActionListener exitListener = new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    InitializationManager.getInstance().closeAll();
                    System.exit(0);
                }
            };

            PopupMenu popup = new PopupMenu();
            MenuItem defaultItem = new MenuItem("Exit");
            defaultItem.addActionListener(exitListener);
            MenuItem showItem = new MenuItem("Show JMultiClipboard");
            popup.add(showItem);
            popup.addSeparator();
            popup.add(defaultItem);

            showItem.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    mainFrame.setVisible(true);
                }
            });

            TrayIcon trayIcon = new TrayIcon(image, "JMultiClipboard", popup);

            trayIcon.setImageAutoSize(true);
            trayIcon.addMouseListener(new MouseListener() {

                @Override
                public void mouseClicked(MouseEvent e) {
                    if (e.getClickCount() == 2) {
                        mainFrame.setVisible(true);
                    }
                }

                @Override
                public void mouseEntered(MouseEvent e) {
                }

                @Override
                public void mouseExited(MouseEvent e) {
                }

                @Override
                public void mousePressed(MouseEvent e) {
                }

                @Override
                public void mouseReleased(MouseEvent e) {
                }
            });
            try {
                tray.add(trayIcon);
            } catch (AWTException e) {
                System.err.println("TrayIcon could not be added.");
            }
        }
    }

    private void launchPrintScreenTool() {
        System.out.println("CTRL + SHIFT + M");
        new ScreenFragmentFrame(mainFrame).setVisible(true);
    }

    public static void main(String[] args) {
        System.out.println("START");
        InitializationManager.getInstance().init();
        Main m = new Main();

        System.out.println("END");
    }

    @Override
    public void onHotKey(HotKey hotkey) {
        if (CTRL_SHIFT_V.equals(hotkey.keyStroke)) {

            mainFrame.showFrame();

        } else if (CTRL_SHIFT_C.equals(hotkey.keyStroke)) {
            try {
                Thread.sleep(500L);
            } catch (InterruptedException ex) {
                ErrorManager.errorSilent(Main.class, ex);
            }
            ClipboardManager.getInstance().ctrlC();
            mainFrame.setVisible(true);
        } else if (CTRL_SHIFT_M.equals(hotkey.keyStroke)) {
            launchPrintScreenTool();
        }
    }
}
