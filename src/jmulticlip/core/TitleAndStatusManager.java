package jmulticlip.core;

import jmulticlip.gui.*;
/**
 *
 * @author Revers
 */
public class TitleAndStatusManager {

    private static final String TITLE_SEPARATOR = " - ";
    public static final String APPLICATION_NAME = "JMultiClipboard";
    public static final String APPLICATION_VERSION = "0.4.7";
    public static final String APPLICATION_TITLE = APPLICATION_NAME
            + ' ' + APPLICATION_VERSION;
    
    private MainFrame mainFrame;

    private TitleAndStatusManager() {
        mainFrame = MainFrame.getInstance();
    }

    public static TitleAndStatusManager getInstance() {
        return TitleAndStatusManagerHolder.INSTANCE;
    }

    private static class TitleAndStatusManagerHolder {

        private static final TitleAndStatusManager INSTANCE = new TitleAndStatusManager();
    }
    
    public void restoreDefaultTitleStatus() {
        mainFrame.setTitle(APPLICATION_TITLE);
    }
    
    public void setTitleStatus(String status) {
        mainFrame.setTitle(APPLICATION_TITLE + TITLE_SEPARATOR + status);
    }
    
    public void restoreDefaultStatusbarStatus() {
        
    }
    
    public void setStatusbarStatus(String status) {
        
    }
}
