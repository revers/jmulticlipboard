package jmulticlip.config;

import java.util.*;

/**
 *
 * @author Revers
 */
public class GlobalProperties {

    protected HashMap<String, Object> propertiesMap = new HashMap<String, Object>();

    protected GlobalProperties() {
    }

    public static GlobalProperties getInstance() {
        return GlobalPropertiesHolder.INSTANCE;
    }

    public void addProperty(String name, Object value) {
        propertiesMap.put(name, value);
    }

    public Object getProperty(String name) {
        return propertiesMap.get(name);
    }

    public <T> T getProperty(String name, Class<T> valueClass) {
        return (T) propertiesMap.get(name);
    }

    public Set<String> getPropertyNames() {
        return propertiesMap.keySet();
    }

    /**
     * 
     * @param name
     * @return true if there was property named by "name" parameter, false otherwise.
     */
    public boolean removeProperty(String name) {
        if (propertiesMap.remove(name) == null) {
            return false;
        }
        return true;
    }

    /**
     * Sets property "name" with "value". Property is always set even if before there was
     * no such property. 
     * @param name
     * @return true if before there was property named by parameter "name", false otherwise.
     */
    public boolean setProperty(String name, Object value) {

        if (propertiesMap.containsKey(name) == false) {
            propertiesMap.put(name, value);
            return false;
        }

        propertiesMap.put(name, value);
        return true;
    }

    public boolean containsProperty(String name) {
        return propertiesMap.containsKey(name);
    }

    private static class GlobalPropertiesHolder {

        private static final GlobalProperties INSTANCE = new GlobalProperties();
    }
}
