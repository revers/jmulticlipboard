package jmulticlip.config;

import java.util.Locale;
import java.util.prefs.Preferences;

/**
 *
 * @author Revers
 */
public class Options {

    private static final String PREF_MAX_IMAGES = "maxImages";
    private static final String PREF_JPEG_QUALITY = "jpegQuality";
    private static final String PREF_SOUND_AFTER_UPLOAD = "soundAfterUpload";
    private static final String PREF_LANGUAGE = "language";
    private static final String PREF_MOVE_PASTED_ON_TOP = "movePastedOnTop";
    private int maxImages = 5;
    private int jpegQuality = 10;
    private boolean soundAfterUpload = true;
    private Locale language = Locale.ENGLISH;
    private boolean movePastedOnTop = true;
    private Preferences prefs = Preferences.userNodeForPackage(getClass());

    private Options() {
        String maxImagesStr = prefs.get(PREF_MAX_IMAGES, Integer.toString(maxImages));
        maxImages = Integer.parseInt(maxImagesStr);

        String jpegQualityStr = prefs.get(PREF_JPEG_QUALITY, Integer.toString(jpegQuality));
        jpegQuality = Integer.parseInt(jpegQualityStr);

        String soundAfterUploadStr = prefs.get(PREF_SOUND_AFTER_UPLOAD, Boolean.toString(soundAfterUpload));
        soundAfterUpload = Boolean.parseBoolean(soundAfterUploadStr);

        language = new Locale(prefs.get(PREF_LANGUAGE, language.getLanguage()));

        String movePastedOnTopStr = prefs.get(PREF_MOVE_PASTED_ON_TOP, Boolean.toString(movePastedOnTop));
        movePastedOnTop = Boolean.parseBoolean(movePastedOnTopStr);

    }

    public static Options getInstance() {
        return OptionsHolder.INSTANCE;
    }

    private static class OptionsHolder {

        private static final Options INSTANCE = new Options();
    }

    public int getMaxImages() {
        return maxImages;
    }

    public void setMaxImages(int maxImages) {
        this.maxImages = maxImages;
        prefs.put(PREF_MAX_IMAGES, Integer.toString(maxImages));
    }

    public int getJpegQuality() {
        return jpegQuality;
    }

    public void setJpegQuality(int jpegQuality) {
        this.jpegQuality = jpegQuality;
        prefs.put(PREF_JPEG_QUALITY, Integer.toString(jpegQuality));
    }

    public boolean getSoundAfterUpload() {
        return soundAfterUpload;
    }

    public void setSoundAfterUpload(boolean soundAfterUpload) {
        this.soundAfterUpload = soundAfterUpload;
        prefs.put(PREF_SOUND_AFTER_UPLOAD, Boolean.toString(soundAfterUpload));
    }

    public Locale getLanguage() {
        return language;
    }

    public void setLanguage(Locale language) {
        this.language = language;
        prefs.put(PREF_LANGUAGE, language.getLanguage());
    }

    public boolean getMovePastedOnTop() {
        return movePastedOnTop;
    }

    public void setMovePastedOnTop(boolean movePastedOnTop) {
        this.movePastedOnTop = movePastedOnTop;
        prefs.put(PREF_MOVE_PASTED_ON_TOP, Boolean.toString(movePastedOnTop));
    }
}