package jmulticlip.config;

import java.awt.*;
import java.io.*;
import java.util.*;
import jmulticlip.core.ErrorManager;

/**
 *
 * @author Revers
 */
public class GUIProperties extends GlobalProperties {
    public static final String FOCUSBORDER_COLOR_FOCUSED = "focusborder.color.focused";
    public static final String FOCUSBORDER_COLOR_NOTFOCUSED = "focusborder.color.notfocused";
    public static final String FOCUSBORDER_TICKNESS = "focusborder.tickness";
    public static final String FOCUSBORDER_FONT = "focusborder.font";
    public static final String SHORTCUTBORDER_COLOR = "shortcutborder.color";
    public static final String SHORTCUTBORDER_FONT = "shortcutborder.font";

    private Properties properties = new Properties();

    private GUIProperties() {
        try {
            properties.load(new FileInputStream("gui.properties"));

            addProperty(FOCUSBORDER_COLOR_FOCUSED, parseColor(properties.getProperty(FOCUSBORDER_COLOR_FOCUSED)));
            addProperty(FOCUSBORDER_COLOR_NOTFOCUSED, parseColor(properties.getProperty(FOCUSBORDER_COLOR_NOTFOCUSED)));
            addProperty(FOCUSBORDER_TICKNESS, parseInt(properties.getProperty(FOCUSBORDER_TICKNESS)));
            addProperty(FOCUSBORDER_FONT, parseFont(properties.getProperty(FOCUSBORDER_FONT)));
            addProperty(SHORTCUTBORDER_COLOR, parseColor(properties.getProperty(SHORTCUTBORDER_COLOR)));
            addProperty(SHORTCUTBORDER_FONT, parseFont(properties.getProperty(SHORTCUTBORDER_FONT)));

        } catch (IOException e) {
            addProperty(FOCUSBORDER_COLOR_FOCUSED, Color.green);
            addProperty(FOCUSBORDER_COLOR_NOTFOCUSED, Color.red);
            addProperty(FOCUSBORDER_TICKNESS, 5);
            addProperty(FOCUSBORDER_FONT, new Font("Tahoma", Font.BOLD, 12));
            addProperty(SHORTCUTBORDER_COLOR, Color.blue);
            addProperty(SHORTCUTBORDER_FONT, new Font("Tahoma", Font.BOLD, 8));
            
            ErrorManager.errorMinor(GUIProperties.class, e);
        }
    }

    public static GUIProperties getInstance() {
        return GUIPropertiesHolder.INSTANCE;
    }

    private static class GUIPropertiesHolder {

        private static final GUIProperties INSTANCE = new GUIProperties();
    }
    private Color parseColor(String s) {
        String[] parts = s.replaceAll("Color\\(([^,]+),([^,]+),([^,]+)\\)",
                "$1;$2;$3").split(";");
        int r = Integer.parseInt(parts[0]);
        int g = Integer.parseInt(parts[1]);
        int b = Integer.parseInt(parts[2]);

        return new Color(r, g, b);
    }

    private Font parseFont(String s) {
        String[] parts = s.replaceAll("Font\\(([^,]+),([^,]+),([^,]+)\\)",
                "$1;$2;$3").split(";");

        String name = parts[0];

        int style = Font.PLAIN;
        if (parts[1].equals("BOLD")) {
            style = Font.BOLD;
        } else if (parts[1].equals("ITALIC")) {
            style = Font.ITALIC;
        }
        int size = Integer.parseInt(parts[2]);

        return new Font(name, style, size);
    }

    private int parseInt(String s) {
        return Integer.parseInt(s.replaceAll("int\\((.*)\\)", "$1"));
    }

    private float parseFloat(String s) {
        return Float.parseFloat(s.replaceAll("float\\((.*)\\)", "$1"));
    }

    private boolean parseBoolean(String s) {
        return Boolean.parseBoolean(s.replaceAll("boolean\\((.*)\\)", "$1"));
    }

    private String parseString(String s) {
        return s.replaceAll("String\\((.*)\\)", "$1");
    }
    
    public static void main(String[] args) {
        GUIProperties guiProp = GUIProperties.getInstance();
        Color c = guiProp.getProperty(SHORTCUTBORDER_COLOR, Color.class);
        System.out.println(c.toString());
        
        Set<String> names = guiProp.getPropertyNames();
        for(String name : names) {
            System.out.println(name);
        }
    }
}
