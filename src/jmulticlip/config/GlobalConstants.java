package jmulticlip.config;

import java.util.Locale;

/**
 *
 * @author Revers
 */
public interface GlobalConstants {
    public static final Locale LOCALE_PL = new Locale("pl", "PL");
}
