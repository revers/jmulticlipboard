/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jmulticlip.hotkey;

import javax.swing.*;
import com.tulskiy.keymaster.common.*;

/**
 *
 * @author Revers
 */
public class HotkeyManager {
    
    private Provider provider;
    
    private HotkeyManager() {
        provider = Provider.getCurrentProvider(true);
    }
    
    public static HotkeyManager getInstance() {
        return HotkeyManagerHolder.INSTANCE;
    }
    
    public void cleanUp() {
        if (provider != null) {
            provider.reset();
            provider.stop();
            provider = null;
        }
    }
    
    public void register(KeyStroke ks, HotKeyListener hl) {
        provider.register(ks, hl);
    }
    
    private static class HotkeyManagerHolder {

        private static final HotkeyManager INSTANCE = new HotkeyManager();
    }
}
