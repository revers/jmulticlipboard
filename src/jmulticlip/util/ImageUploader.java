package jmulticlip.util;

import java.awt.*;
import java.awt.image.*;
import java.io.*;
import java.util.Iterator;
import javax.imageio.*;
import javax.imageio.stream.*;


import java.io.*;
import java.net.*;
import java.util.Locale;
import java.util.logging.*;
import java.util.regex.*;

import jmulticlip.ifaces.Cancelable;
import jmulticlip.ifaces.NotificationListener;
import pl.ugu.revers.misc.FileUtil;
import pl.ugu.revers.net.*;

/**
 *
 * @author Revers
 */
public class ImageUploader implements Cancelable {

    public class ImageLinks {

        private String link;
        private String forumLink;
        private String htmlLink;

        public ImageLinks(String link, String forumLink, String htmlLink) {
            this.link = link;
            this.forumLink = forumLink;
            this.htmlLink = htmlLink;
        }

        public String getForumLink() {
            return forumLink;
        }

        public String getHtmlLink() {
            return htmlLink;
        }

        public String getLink() {
            return link;
        }

        @Override
        public String toString() {
            return link + "\n\n" + forumLink + "\n\n" + htmlLink;
        }
    }
    private static final long TWO_MB = 2L * 1024L * 1024L;
    private static Logger logger = Logger.getLogger(ImageUploader.class.getName());
    //   private static final File HTML_TEMP_FILE = new File("bankfotek.temp.html");
    private static final String[] EXTENSIONS = {".jpg", ".jpeg", ".png",
        ".gif"};
    private boolean canceled = false;
    private ClientHttpRequest clientHttp;
    private static boolean loggerAdded = false;

    public ImageUploader() {
        if (loggerAdded == false) {
            logger.setLevel(Level.ALL);

            ConsoleHandler cs = new ConsoleHandler();
            SimpleFormatter formatter = new SimpleFormatter();
            cs.setFormatter(formatter);
            cs.setLevel(Level.ALL);
            logger.addHandler(cs);
            loggerAdded = true;
        }
    }

//    public void upload(File[] files) throws IOException {
//        int i = 0;
//        logger.fine("[" + (i + 1) + "]Zaczynam uploadowac obrazek:\n" + files[i].getAbsolutePath());
//        ImageLinks urlStr = upload(files[i]);
//        System.out.println("urlStr = " + urlStr);
//    }
    private File getImageFile(File directory) {
        int i = 0;
        File file = null;
        do {
            file = FileUtil.getFile(directory, Integer.toString(i) + ".jpeg");
            i++;
        } while (file.exists());

        return file;
    }

    public ImageLinks upload(BufferedImage image) throws IOException {
        return upload(image, null);
    }

    public ImageLinks upload(BufferedImage image, NotificationListener notificationListener) throws IOException {
        File tempDir = new File("temp");
        tempDir.mkdir();

        File imgFile = getImageFile(tempDir);
        boolean success = saveImageWithLimitedSize(image, imgFile, TWO_MB);

        if (success == false) {
            throw new IOException("CAN'T CHANGE IMAGE SIZE TO LESS OR EQUALL 2MB!!");
        }

        if (notificationListener != null) {
            notificationListener.notify(String.format("%.2f MB", ((float) imgFile.length() / (1024.0f * 1024f))));
        }
        ImageLinks imageLinks = upload(imgFile);

        imgFile.delete();

        if (canceled) {
            return null;
        }
        
        return imageLinks;
    }

    private ImageLinks upload(File f) throws IOException {
        String link = null;
        NetworkUtils netUtils = new NetworkUtils();
        netUtils.setBrowser(NetworkUtils.Browser.FIREFOX);

        logger.fine("Wczytuje strone http://bankfotek.pl/");
        netUtils.readPage("http://bankfotek.pl/");
        Pattern linkPattern = Pattern.compile("http://www.bankfotek.pl/view/\\d+");

        URL url = new URL("http://bankfotek.pl/process.php");

        URLConnection urlConn = url.openConnection();
        netUtils.setProperties(urlConn, NetworkUtils.Browser.FIREFOX);

        clientHttp = new ClientHttpRequest(urlConn);

        clientHttp.setParameter("imagefile", f);
        clientHttp.setParameter("comments", "");
        clientHttp.setParameter("MAX_FILE_SIZE", "");
        clientHttp.setParameter("upload", "");

        logger.fine(String.format(Locale.US, "ROZMIAR = %.2f MB",
                ((float) f.length() / (1024.0f * 1024.0f))));

        InputStream input = clientHttp.post();

        BufferedReader in = new BufferedReader(new InputStreamReader(input,
                "UTF-8"));

        Pattern miniPattern = Pattern.compile("<input type=\"text\" name=\"thetext\" size=\"\\d+\" value=\"([^\"]+)\">");

        String forumLink = null;
        String htmlLink = null;

        String line;
        while ((line = in.readLine()) != null) {
            // System.out.println(line);

            Matcher miniMatcher = miniPattern.matcher(line);
            Matcher linkMatcher = linkPattern.matcher(line);
            if (miniMatcher.find()) {
                if (htmlLink == null) {
                    htmlLink = miniMatcher.group(1);
                } else {
                    forumLink = miniMatcher.group(1);
                }
                logger.fine("MINI LINK: " + miniMatcher.group(1));
            } else if (linkMatcher.find()) {

                logger.fine("LINK: " + linkMatcher.group());
                link = linkMatcher.group();

                String[] tokens = link.split("/");
                String number = tokens[tokens.length - 1];
                link = "http://www.bankfotek.pl/image/" + number + ".jpeg";

                // System.out.println(linkMatcher.group());
                //  break;

            } else if (canceled) {
                logger.fine("Anulowano");
                break;
            }

            //  out.println(line);
        }

        in.close();
        // out.close();
        input.close();
        logger.fine("Uploadowanie zakonczone.");
        logger.fine("--------------------------------------------------");
        // http://www.bankfotek.pl/image/672070.jpeg
        // http://www.bankfotek.pl/image/672062.jpeg

        return new ImageLinks(link, forumLink, htmlLink);
    }

    public void cancel() {
        logger.fine("Trwa anulowanie...");
        canceled = true;
    }

    public boolean isCanceled() {
        return canceled;
    }

    public void addLoggerHandler(Handler handler) {
        logger.addHandler(handler);
    }
//
//    public void upload() throws IOException {
//        File[] files = new File("C:\\Users\\Revers\\Desktop\\bankfotek").listFiles(new FileFilter() {
//
//            @Override
//            public boolean accept(File f) {
//                if (f.isDirectory()) {
//                    return false;
//                }
//                if (f.length() > 1024L * 1024L * 2L) {
//                    return false;
//                }
//
//                String s = f.getName().toLowerCase();
//                for (String ext : EXTENSIONS) {
//                    if (s.endsWith(ext)) {
//                        return true;
//                    }
//                }
//                return false;
//            }
//        });
//
//        upload(files);
//
//    }

    private void saveImage(BufferedImage img, File file, float quality) throws IOException {
        Iterator iter = ImageIO.getImageWritersByFormatName("jpeg");

        ImageWriter writer = (ImageWriter) iter.next();
        ImageWriteParam iwp = writer.getDefaultWriteParam();

        iwp.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
        iwp.setCompressionQuality(quality);

        FileImageOutputStream output = new FileImageOutputStream(file);
        writer.setOutput(output);
        IIOImage image = new IIOImage(img, null, null);
        writer.write(null, image, iwp);
        writer.dispose();
    }

    private boolean saveImageWithLimitedSize(BufferedImage image, File file, long maxSize) throws IOException {
        float quality = 1.0f;
        long size = 0;

        do {
            if (file.exists() && file.delete() == false) {
                throw new IOException("CANNOT REMOVE FILE '" + file.getAbsolutePath() + "'!!");
            }

            saveImage(image, file, quality);
            size = file.length();

            quality -= 0.1f;
        } while (size > maxSize && quality >= 0.0f);

        if (size > maxSize) {
            return false;
        }

        return true;
    }

    public static void main(String[] args) throws IOException {
        LogManager.getLogManager().reset();

        // logger.setLevel(Level.ALL);

        // ConsoleHandler cs = new ConsoleHandler();
        // SimpleFormatter formatter = new SimpleFormatter();
        // cs.setFormatter(formatter);
        // cs.setLevel(Level.ALL);
        // logger.addHandler(cs);
        System.out.println("START");
        ImageUploader bank = new ImageUploader();
        ImageLinks s = bank.upload(new File("test.jpeg"));
        System.out.println("s = " + s);
        System.out.println("END");

//        System.out.println("Start");
//        File sourceimage = new File("test.bmp");
//        BufferedImage img = ImageIO.read(sourceimage);
//
//        ImageUploader iu = new ImageUploader();
//
//        iu.saveImageWithLimitedSize(img, new File("test.jpg"), TWO_MB);
//
//        System.out.println("End");
    }
}
