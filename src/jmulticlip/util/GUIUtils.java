package jmulticlip.util;

import java.beans.PropertyChangeEvent;
import javax.swing.event.ChangeEvent;
import jmulticlip.core.ErrorManager;

import de.muntjak.tinylookandfeel.TinyLookAndFeel;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeListener;
import javax.swing.*;
import javax.swing.event.ChangeListener;
import jmulticlip.gui.RectBorder;

/**
 *
 * @author Revers
 */
public class GUIUtils {

    private Icon helpIcon;
    public static final int SHORTCUT_Y_OFFSET = 5;
    public static final Color MENU_BACKGROUND_COLOR = new Color(255, 255, 255);
    public static final Color MENU_SELECTION_COLOR = new Color(189, 208, 234);
    public static final Color SELECTED_BORDER_COLOR = new Color(50, 121, 217);
    public static final Color UNSELECTED_BORDER_COLOR = new Color(133, 133, 140);
    public static final Color PANEL_COLOR = new Color(236, 233, 216);
    public static final Font NORMAL_CB_FONT = new Font("Tahoma", Font.PLAIN, 11);
    public static final Font SELECTED_CB_FONT = new Font("Tahoma", Font.BOLD, 11);
    private static MouseListener rolloverButtonListener;
    private static final RectBorder RECT_BORDER = new RectBorder();

    private GUIUtils() {
        helpIcon = new ImageIcon("src/img/question1.png");
    }

    public Icon getHelpIcon() {
        return helpIcon;
    }

    public static Icon getIcon(String path) {
        // TODO: getClass().getResource("/img/setting.png")
        return new ImageIcon(path);
    }

    public static Icon getColorIcon(Color color, int width, int height) {
        return getColorIcon(color, width, height, false);
    }

    public static Icon getColorIcon(Color color, int width, int height, boolean paintBorder) {
        BufferedImage bi = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        Graphics2D g2 = bi.createGraphics();
        g2.setColor(color);
        g2.fillRect(0, 0, width, height);
        if (paintBorder) {
            g2.setColor(Color.black);
            g2.drawRect(0, 0, width - 1, height - 1);
        }
        g2.dispose();
        return new ImageIcon(bi);
    }

    public static Image getColorImage(Color color, int width, int height) {
        return getColorImage(color, width, height, false);
    }

    public static Image getColorImage(Color color, int width, int height, boolean paintBorder) {
        BufferedImage bi = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        Graphics2D g2 = bi.createGraphics();
        g2.setColor(color);
        g2.fillRect(0, 0, width, height);
        if (paintBorder) {
            g2.setColor(Color.black);
            g2.drawRect(0, 0, width - 1, height - 1);
        }
        g2.dispose();
        return bi;
    }

    public static GUIUtils getInstance() {
        return GUIUtilsHolder.INSTANCE;
    }

    private static class GUIUtilsHolder {

        private static final GUIUtils INSTANCE = new GUIUtils();
    }

    public static void useApplicationLAF(Component component) {
        try {
            UIManager.setLookAndFeel(new TinyLookAndFeel());
            SwingUtilities.updateComponentTreeUI(component);
        } catch (UnsupportedLookAndFeelException ex) {
            ErrorManager.errorSilent(GUIUtils.class, ex, "Cannot set up Look And Feel!!");
        }
    }

    public static void setBoldFont(Component comp) {
        comp.setFont(getBoldFont(comp));
    }

    public static Font getBoldFont(Component comp) {
        return comp.getFont().deriveFont(Font.BOLD);
    }

    public static void makeButtonFlatWithBoreder(AbstractButton button, Color borderColor) {
        makeButtonFlat(button);
        button.setBorder(new RectBorder(borderColor));
        button.setBorderPainted(true);
    }

    public static void makeButtonFlatWithBoreder(AbstractButton button) {
        makeButtonFlat(button);
        button.setBorder(RECT_BORDER);
        button.setBorderPainted(true);
    }

    public static void makeButtonFlat(AbstractButton button) {
        button.setFocusPainted(false);
        button.setBorderPainted(false);

        button.addMouseListener(getRolloverButtonListener());

        if (button instanceof JToggleButton) {

            JToggleButton toggleButton = ((JToggleButton) button);
            toggleButton.addChangeListener(getToggleButtonChangeListener());

            if (toggleButton.isSelected()) {
                button.setContentAreaFilled(true);
            } else {
                button.setContentAreaFilled(false);
            }
        } else {
            button.setContentAreaFilled(false);
        }
    }
    private static ChangeListener toggleButtonChangeListener;

    private static ChangeListener getToggleButtonChangeListener() {
        if (toggleButtonChangeListener == null) {
            toggleButtonChangeListener = new ChangeListener() {

                @Override
                public void stateChanged(ChangeEvent e) {

                    @SuppressWarnings("unchecked")
                    JToggleButton button = (JToggleButton) e.getSource();

                    if (button.isSelected()) {
                        button.setContentAreaFilled(true);
                    } else if (button.getMousePosition() == null) {
                        button.setContentAreaFilled(false);
                    }

                }
            };
        }

        return toggleButtonChangeListener;
    }

    private static MouseListener getRolloverButtonListener() {
        if (rolloverButtonListener == null) {
            rolloverButtonListener = new MouseAdapter() {

                @Override
                public void mouseEntered(MouseEvent e) {
                    Object source = e.getSource();
                    if (source instanceof AbstractButton) {
                        AbstractButton button = (AbstractButton) source;
                        button.setContentAreaFilled(true);
                    }
                }

                @Override
                public void mouseExited(MouseEvent e) {
                    Object source = e.getSource();
                    if (source instanceof AbstractButton) {
                        AbstractButton button = (AbstractButton) source;
                        if (button instanceof JToggleButton && ((JToggleButton) button).isSelected()) {
                            button.setContentAreaFilled(true);
                        } else {
                            button.setContentAreaFilled(false);
                        }
                    }
                }
            };
        }
        return rolloverButtonListener;
    }
}
