package jmulticlip.util;

import java.net.*;
import java.io.*;
import java.util.ArrayList;
import jmulticlip.ifaces.Cancelable;

import pl.ugu.revers.net.*;

/**
 *
 * @author Revers
 */
public class WklejOrgUploader implements Cancelable {

    public static enum Language {

        ACTION_SCRIPT_3("as3"),
        BASH("bash"),
        BATCHFILE("bat"),
        C("c"),
        CPP("cpp"),
        CSHARP("csharp"),
        CSS("css"),
        GLSL("glsl"),
        HTML("html"),
        JAVA("java"),
        JAVA_SCRIPT("js"),
        JAVA_SERVER_PAGE("jsp"),
        MY_SQL("mysql"),
        PHP("php"),
        PYTHON("python"),
        PYTHO_3("python3"),
        SQL("sql"),
        TEX("tex"),
        TEXT("text"),
        XML("xml"),
        XSLT("xslt");
        private final String name;

        private Language(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }
    private URLConnection urlConn;

    public WklejOrgUploader() {
    }

    private static void setOperaProperties(URLConnection conn) {
        conn.setRequestProperty("User-Agent",
                "Opera/9.80 (Windows NT 5.1; U; pl) Presto/2.5.24 Version/10.53");
        // conn.setRequestProperty("Host", "www.vclub.pl");
        conn.setRequestProperty(
                "Accept",
                "text/html, application/xml;q=0.9, application/xhtml+xml, image/png, image/jpeg, image/gif, image/x-xbitmap, */*;q=0.1");
        conn.setRequestProperty("Accept-Language", "pl-PL,pl;q=0.9,en;q=0.8");
        conn.setRequestProperty("Accept-Charset",
                "iso-8859-1, utf-8, utf-16, *;q=0.1");
        conn.setRequestProperty("Accept-Encoding", "deflate, identity, *;q=0");
        conn.setRequestProperty("Connection", "Keep-Alive");
        // conn.setRequestProperty("TE",
        // "deflate, gzip, chunked, identity, trailers");
    }

    public URL upload(String text, Language lang) throws IOException {
        String url = post("http://wklej.org/", "nickname", "", "syntax", lang.getName(),
                "is_private", "on", "body", text, "comment", "", "tags", "", "x", "93", "y", "7");
        System.out.println("url = " + url);
        return new URL(url.trim());
    }

    @Override
    public void cancel() {
        // not yet implemented  
    }

    private String post(String urlStr, String... fields)
            throws IOException {
        urlConn = new URL(urlStr).openConnection();

        setOperaProperties(urlConn);

        urlConn.setDoOutput(true);
        OutputStreamWriter wr = new OutputStreamWriter(urlConn.getOutputStream());

        String data = URLEncoder.encode(fields[0], "UTF-8") + "="
                + URLEncoder.encode(fields[1], "UTF-8");
        for (int i = 2; i < fields.length; i += 2) {
            data += "&" + URLEncoder.encode(fields[i], "UTF-8") + "="
                    + URLEncoder.encode(fields[i + 1], "UTF-8");
        }

        wr.write(data);
        wr.flush();

        //urlConn.get
        BufferedReader rd = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));


        String line;
        while ((line = rd.readLine()) != null) {
            // System.out.println(line);
        }


        wr.close();
        rd.close();

        String location = urlConn.getHeaderField("Location");
        //System.out.println("\n\nLOCATION = " + location);

        return location;
    }

    public static void main(String[] args) throws IOException {

        HttpURLConnection.setFollowRedirects(false);

        System.out.println("START");
//        NetworkUtils nu = new NetworkUtils();
//        nu.setBrowser(NetworkUtils.Browser.OPERA);

        WklejOrgUploader wou = new WklejOrgUploader();
        String body = "WklejOrgUploader.Language";//"ble ble ble :) bla";
//        String url = 
//                wou.post("http://wklej.org/", "nickname", "", "syntax", Language.JAVA.getName(),
//                "is_private", "on", "body", body, "comment", "", "tags", "", "x", "93", "y", "7");
        URL url = wou.upload(body, Language.TEXT);
        System.out.println("WYNIK: " + url);


        System.out.println("END");
    }
}
