package jmulticlip.util;

/**
 *
 * @author Revers
 */
public class OSUtil {

    public static String getOS() {
        return System.getProperty("os.name").toLowerCase();
    }

    public static boolean isWindows() {
        return getOS().contains("win");
    }

    public static boolean isMac() {
        return getOS().contains("mac");
    }

    public static boolean isUnix() {
        String os = getOS();
        return os.contains("nix") || os.contains("nux") || os.contains("aix");
    }

    public static boolean isSolaris() {
        return getOS().contains("sunos");
    }

}
