package jmulticlip.scripts;

import java.util.Locale;
import javax.swing.KeyStroke;

/**
 *
 * @author Revers
 */
public interface Script {

    public String getName();

    public void setName(String name);

    public String getDescription();

    public void setDescription(String desc);

    public KeyStroke getKeyStroke();

    public void setKeyStroke(KeyStroke keyStroke);

    public String parse(String[] clipTextRows) throws ScriptException;

    public void afterPaste();

    public boolean isEnabled();

    public void setEnabled(boolean enabled);

    public int getPosition();

    public void setPosition(int position);
}
