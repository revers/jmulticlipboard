package jmulticlip.scripts;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.*;
import java.io.*;
import java.util.*;
import jmulticlip.core.ErrorManager;
import jmulticlip.scripts.config.ScriptElement;
import jmulticlip.scripts.config.ScriptXmlConfig;

/**
 *
 * @author Revers
 */
public class ScriptRunner {

    private static final String SCRIPTS_CONFIG_FILE = "scripts/scripts-config.xml";

    private ArrayList<Script> scriptList = new ArrayList<Script>();

    private ScriptRunner() {
        try {
            loadScriptConfig();
        } catch (FileNotFoundException ex) {
            ErrorManager.errorMajor(ScriptRunner.class, ex, "Cannon load '" + SCRIPTS_CONFIG_FILE + "' file!!");
        } catch (JAXBException ex) {
            ErrorManager.errorMajor(ScriptRunner.class, ex, "Cannon load '" + SCRIPTS_CONFIG_FILE + "' file!!");
        }
    }

    public Script[] getAllScripts() {
        return scriptList.toArray(new Script[]{});
    }

    public Script[] getEnabledScripts() {
        int enabedScripts = 0;
        for (Script s : scriptList) {
            if (s.isEnabled()) {
                enabedScripts++;
            }
        }

        Script[] scripts = new Script[enabedScripts];

        int index = 0;
        for (Script s : scriptList) {
            if (s.isEnabled()) {
                scripts[index++] = s;
            }
        }

        return scripts;
    }

    public static ScriptRunner getInstance() {
        return ScriptRunnerHolder.INSTANCE;
    }

    private static class ScriptRunnerHolder {

        private static final ScriptRunner INSTANCE = new ScriptRunner();

    }
    
    private String cleanString(String s) {
        return s.replace("\r", "");
    }

    public String runScirpts(Script[] scripts, String[] clipTextRows) throws ScriptException {
        if (scripts.length == 0) {
            StringBuilder sb = new StringBuilder();

            sb.append(clipTextRows[0]);
            
            for (int i = 1; i < clipTextRows.length; i++) {
                sb.append('\n').append(cleanString(clipTextRows[i]));
            }
            return sb.toString();
        }
        String result = scripts[0].parse(clipTextRows);

        for (int i = 1; i < scripts.length; i++) {
            result = scripts[i].parse(new String[]{cleanString(result)});
        }

        return result.replaceAll("^(.*)\n+$", "$1");
    }

    public void saveScripts() throws JAXBException, IOException {
        JAXBContext context = JAXBContext.newInstance(ScriptXmlConfig.class);
        PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(SCRIPTS_CONFIG_FILE)));
        Marshaller m =
                context.createMarshaller();
        m.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

        ScriptXmlConfig scriptXmlConfig = new ScriptXmlConfig();
        ArrayList<ScriptElement> elems = scriptXmlConfig.getScriptElements();

        for (Script scr : scriptList) {
            elems.add(new ScriptElement(scr));
        }

        m.marshal(scriptXmlConfig, out);
    }

    private void loadScriptConfig() throws FileNotFoundException, JAXBException {
        JAXBContext context = JAXBContext.newInstance(ScriptXmlConfig.class);
        BufferedReader in = new BufferedReader(new FileReader(SCRIPTS_CONFIG_FILE));
        ScriptXmlConfig scr = (ScriptXmlConfig) context.createUnmarshaller().unmarshal(in);
        //   System.out.println(scr.getScriptElements());
        Collections.sort(scr.getScriptElements());
        for (ScriptElement se : scr.getScriptElements()) {
            try {
                Script script = se.toScript();

                scriptList.add(script);
            } catch (ClassNotFoundException ex) {
                ErrorManager.errorNormal(ScriptRunner.class, ex, "Cannon load " + se.getClassName() + " script!!");
            } catch (InstantiationException ex) {
                ErrorManager.errorNormal(ScriptRunner.class, ex, "Cannon load " + se.getClassName() + " script!!");
            } catch (IllegalAccessException ex) {
                ErrorManager.errorNormal(ScriptRunner.class, ex, "Cannon load " + se.getClassName() + " script!!");
            }
        }
    }
}
