package jmulticlip.scripts.config;

import javax.swing.KeyStroke;
import javax.xml.bind.annotation.*;
import jmulticlip.scripts.Script;

/**
 *
 * @author Revers
 */
@XmlAccessorType(XmlAccessType.NONE)
public class ScriptElement implements Comparable<ScriptElement> {

    @XmlAttribute(name = "class")
    private String className;
    @XmlElement
    private boolean enabled;
    @XmlElement
    private int position;
    private KeyStroke keyStroke;

    public ScriptElement() {
    }

    public ScriptElement(Script script) {
        this.className = script.getClass().getName();
        this.enabled = script.isEnabled();
        this.position = script.getPosition();
        this.keyStroke = script.getKeyStroke();
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @XmlElement(name = "keystroke")
    public ScriptKeyStrokeElement getKeyStrokeElement() {
        return new ScriptKeyStrokeElement(keyStroke.getKeyCode(), keyStroke.getModifiers());
    }

    public void setKeyStrokeElement(ScriptKeyStrokeElement keyStrokeElement) {
        keyStroke = KeyStroke.getKeyStroke(keyStrokeElement.getKeyCode(), keyStrokeElement.getModifiers());
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public KeyStroke getKeyStroke() {
        return keyStroke;
    }

    @Override
    public String toString() {
        return getClass().getName() + "[className=" + className
                + ", enabled=" + enabled + ", position=" + position
                + ", keyStroke=" + keyStroke + "]";
    }

    @Override
    public int compareTo(ScriptElement o) {
        if (position > o.getPosition()) {
            return 1;
        } else if (position < o.getPosition()) {
            return -1;
        }
        return 0;
    }

    public Script toScript() throws ClassNotFoundException, InstantiationException, IllegalAccessException {
        Class<Script> cls = (Class<Script>) Class.forName(className);
        Script script = cls.newInstance();
        script.setKeyStroke(keyStroke);
        script.setEnabled(enabled);
        return script;
    }
}
