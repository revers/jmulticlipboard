package jmulticlip.scripts.config;

import java.util.*;
import javax.xml.bind.annotation.*;

/**
 *
 * @author Revers
 */
@XmlRootElement(name = "scripts")
@XmlAccessorType(XmlAccessType.NONE)
public class ScriptXmlConfig {

    @XmlElement(name = "script")
    private ArrayList<ScriptElement> scriptElements = new ArrayList<ScriptElement>();

    public ArrayList<ScriptElement> getScriptElements() {
        return scriptElements;
    }

    public void setScriptElements(ArrayList<ScriptElement> scriptElements) {
        this.scriptElements = scriptElements;
    }
}
