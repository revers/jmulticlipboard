package jmulticlip.scripts.config;

import javax.swing.KeyStroke;
import javax.xml.bind.annotation.*;

/**
 *
 * @author Revers
 */
@XmlAccessorType(XmlAccessType.NONE)
public class ScriptKeyStrokeElement {

    private int keyCode;
    private int modifiers;
    private KeyStroke keyStroke;

    public ScriptKeyStrokeElement() {
    }

    public ScriptKeyStrokeElement(KeyStroke ks) {
        this(ks.getKeyCode(), ks.getModifiers());
    }

    public ScriptKeyStrokeElement(int keyCode, int modifiers) {
        this.keyCode = keyCode;
        this.modifiers = modifiers;
    }

    @XmlAttribute
    public int getKeyCode() {
        return keyCode;
    }

    public void setKeyCode(int keyCode) {
        this.keyCode = keyCode;
    }

    @XmlAttribute
    public int getModifiers() {
        return modifiers;
    }

    public void setModifiers(int modifiers) {
        this.modifiers = modifiers;
    }

    public KeyStroke getKeyStroke() {
        return KeyStroke.getKeyStroke(keyCode, modifiers);
    }

    @Override
    public String toString() {
        return getKeyStroke().toString();
    }
}
