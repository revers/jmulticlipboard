package jmulticlip.scripts;

import java.util.Locale;
import javax.swing.KeyStroke;

/**
 *
 * @author Revers
 */
public abstract class AbstractScript implements Script {

    private String name;
    private String description;
    private KeyStroke keyStroke;
    private boolean enabled;
    protected ScriptHelper scriptHelper;
    private int position;

    public AbstractScript(String name, String description) {
        this.name = name;
        this.description = description;
        scriptHelper = ScriptHelper.getInstance();
        //keyStroke = scriptHelper.getKeyStroke(getClass());
    }

    @Override
    public String parse(String[] clipTextRows) throws ScriptException {
        if (clipTextRows.length == 1) {
            return parse(clipTextRows[0]);
        }

        StringBuilder sb = new StringBuilder();

        for (String s : clipTextRows) {
            sb.append(s).append('\n');
        }

        return parse(sb.toString());
    }

    public abstract String parse(String text) throws ScriptException;

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    @Override
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public KeyStroke getKeyStroke() {
        return keyStroke;
    }

    public void setKeyStroke(int keyCode) {
        setKeyStroke(keyCode, 0);
    }

    public void setKeyStroke(int keyCode, int modifiers) {
        setKeyStroke(KeyStroke.getKeyStroke(keyCode, modifiers, true));
    }

    @Override
    public void setKeyStroke(KeyStroke keyStroke) {
        this.keyStroke = keyStroke;
        //    ScriptHelper.getInstance().saveKeyStroke(getClass(), keyStroke);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void afterPaste() {
    }

    @Override
    public String toString() {
        return getClass().getName() + "[name=" + name + ", keyStroke=" + keyStroke + "]";
    }

    public void showError(String msg) {
        scriptHelper.showError(msg);
    }

    public void showInfo(String msg) {
        scriptHelper.showInfo(msg, getName());
    }

    public ScriptHelper.Result getYesNoCancel(String msg, String title) {
        return scriptHelper.getYesNoCancel(msg, getName());
    }

    public ScriptHelper.Result getYesNoCancel(String msg, String yesButtonText,
            String noButtonText, String cancelButtonText) {
        return scriptHelper.getYesNoCancel(msg, getName(), yesButtonText,
                noButtonText, cancelButtonText);

    }

    public boolean getYesNo(String msg) {
        return scriptHelper.getYesNo(msg, getName());
    }

    public boolean getYesNo(String msg, String yesButtonText, String noButtonText) {
        return scriptHelper.getYesNo(msg, getName(), yesButtonText, noButtonText);
    }

    public String getInput(String msg, Object[] possibleValues, Object selectedValue) {
        return scriptHelper.getInput(msg, getName(), possibleValues, selectedValue);
    }

    public String getInput(String msg) {
        return scriptHelper.getInput(msg, getName());
    }

    public String getInput(String msg, String initialValue) {
        return scriptHelper.getInput(msg, getName(), initialValue);
    }

    @Override
    public int getPosition() {
        return position;
    }

    @Override
    public void setPosition(int position) {
        this.position = position;
    }
}
