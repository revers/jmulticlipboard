package jmulticlip.scripts;

/**
 *
 * @author Revers
 */
public class ScriptException extends Exception {

    public ScriptException(Throwable cause) {
        super(cause);
    }

    public ScriptException(String message, Throwable cause) {
        super(message, cause);
    }

    public ScriptException(String message) {
        super(message);
    }

    public ScriptException() {
    }
}
