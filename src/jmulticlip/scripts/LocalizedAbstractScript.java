package jmulticlip.scripts;

/**
 *
 * @author Revers
 */
public abstract class LocalizedAbstractScript extends AbstractScript {

    public LocalizedAbstractScript(String name, String description) {
        super(name, description);
    }

    @Override
    public String getName() {
        return scriptHelper.getLocalizedString(super.getName());
    }

    @Override
    public String getDescription() {
        return scriptHelper.getLocalizedString(super.getDescription());
    }
}
