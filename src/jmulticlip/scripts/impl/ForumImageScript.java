package jmulticlip.scripts.impl;

import jmulticlip.scripts.LocalizedAbstractScript;
import jmulticlip.scripts.ScriptHelper;

/**
 *
 * @author Revers
 */
public class ForumImageScript extends LocalizedAbstractScript {

    public ForumImageScript() {
        super("forumImage", "forumImage.desc");
    }

    @Override
    public String parse(String text) {
        StringBuilder builder = new StringBuilder();
        String[] lines = text.split("\n");
        for (int i = 0; i < lines.length; i++) {
            if (lines[i].trim().equals("")) {
                builder.append(lines[i]);
            } else {
                builder.append("[img]").append(lines[i]).append("[/img]");
            }

            builder.append('\n');

        }
        return ScriptHelper.removeLastEmptyLine(builder);
    }
}
