package jmulticlip.scripts.impl;

import jmulticlip.scripts.*;
import jmulticlip.scripts.impl.cpp.CppHeaderGenerator;

/**
 *
 * @author Revers
 */
public class CppSeparateHeaderDefsScript extends LocalizedAbstractScript {

    public CppSeparateHeaderDefsScript() {
        super("cppSeparateHeaderDefs", "cppSeparateHeaderDefs.desc");
    }

    @Override
    public String parse(String text) throws ScriptException {
        CppHeaderGenerator gen = new CppHeaderGenerator();
        String result = null;
        String className = getInput(scriptHelper.getLocalizedString("cppSeparateHeaderDefs.className"));
        if (className == null) {
            return result;
        }

        try {
            result = gen.generate(className, text);
        } catch (Exception e) {
            throw new ScriptException(e);
        }

        return result;
    }
}
