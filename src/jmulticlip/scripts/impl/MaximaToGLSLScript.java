package jmulticlip.scripts.impl;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import jmulticlip.scripts.LocalizedAbstractScript;

/**
 *
 * @author Revers
 */
public class MaximaToGLSLScript extends LocalizedAbstractScript {

    private static final String NUMBER_PATTERN = "(-?\\d+(?:\\.?\\d*))";

    private static String getSimplePower(String base, String exponent) {
        if (exponent.contains(".")) {
            return "pow(" + base + ", " + exponent + ")";
        }
        try {
            int exp = Integer.parseInt(exponent);

            if (exp < 0) {
                return "pow(" + base + ", " + exponent + ")";
            } else if (exp == 0) {
                return "1";
            } else if (exp == 1) {
                return base;
            } else if (exp > 4) {
                return "pow(" + base + ", " + exponent + ")";
            }
            String result = "(" + base;
            for (int i = 1; i < exp; i++) {
                result += " * " + base;
            }
            result += ")";

            return result;
        } catch (NumberFormatException e) {
            return "pow(" + base + ", " + exponent + ")";
        }
    }

    private static String replaceSimplePowers(String input) {
        Pattern ptrn = Pattern.compile("(\\w+)\\^" + NUMBER_PATTERN);
        Matcher mchr = ptrn.matcher(input);
        String result = "";
        int index = 0;

        while (mchr.find()) {
            result += input.substring(index, mchr.start(1));
            result += getSimplePower(mchr.group(1), mchr.group(2));
            index = mchr.end(2);
        }
        result += input.substring(index);
        return result;
    }

    private static String replaceBracketPowers(String input) {
        Pattern ptrn = Pattern.compile("(\\))\\^" + NUMBER_PATTERN);
        Matcher mchr = ptrn.matcher(input);
        String result = "";
        int index = 0;

        while (mchr.find()) {
            int i = mchr.start(1) - 1;
            int nested = 0;

            for (; i >= 0; i--) {
                char c = input.charAt(i);

                if (c == ')') {
                    nested++;
                } else if (c == '(') {
                    if (nested > 0) {
                        nested--;
                    } else {
                        break;
                    }
                }
            }
            result += input.substring(index, i);
            result += "pow(" + input.substring(i + 1, mchr.start(1)) + ", " + mchr.group(2) + ")";
            index = mchr.end(2);
        }
        result += input.substring(index);
        return result;
    }

    public MaximaToGLSLScript() {
        super("maximaToGLSL", "maximaToGLSL.desc");
    }

    @Override
    public String parse(String input) {
        String[] lines = input.split("\n");
        
        if (lines.length > 1) {
            String lastLine = lines[lines.length - 1].trim();

            if (lastLine.startsWith("(")) {
                int idx = lastLine.indexOf(')');

                if (idx >= 0) {
                    input = lastLine.substring(idx + 1).trim();
                }
            }
        }

        if (input.endsWith("=0")) {
            input = input.substring(0, input.length() - 2);
        }
        System.out.println("Input: '" + input + "'");

        String result = replaceSimplePowers(input);

        result = replaceBracketPowers(result);
        result = result.replace('_', '.');
        result = result.replaceAll("(?<!\\.)(\\d+)(?!\\.)", "$1.0");

        return result;
    }
}
