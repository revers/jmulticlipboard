package jmulticlip.scripts.impl;

import jmulticlip.scripts.LocalizedAbstractScript;
import jmulticlip.scripts.ScriptHelper;

/**
 *
 * @author Revers
 */
public class RemoveEmptyLinesScript extends LocalizedAbstractScript {

    public RemoveEmptyLinesScript() {
        super("removeEmptyLines", "removeEmptyLines.desc");
    }

    @Override
    public String parse(String text) {

        String[] parts = text.split("\n");
        StringBuilder sb = new StringBuilder();

        for (String part : parts) {

            if (part.trim().equals("")) {
                continue;
            }

            sb.append(part).append('\n');
        }

        return ScriptHelper.removeLastEmptyLine(sb);
    }
}
