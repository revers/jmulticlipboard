package jmulticlip.scripts.impl;

import jmulticlip.scripts.LocalizedAbstractScript;

/**
 *
 * @author Revers
 */
public class BackslashReplacerScript extends LocalizedAbstractScript {

    public BackslashReplacerScript() {
        super("backslashReplacer", "backslashReplacer.desc");
    }

    @Override
    public String parse(String text) {
        return text.replace("\\", "/");
    }
    
}
