package jmulticlip.scripts.impl;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

import jmulticlip.scripts.LocalizedAbstractScript;
import jmulticlip.scripts.ScriptException;

/**
 *
 * @author Revers
 */
public class CalcEquationScript extends LocalizedAbstractScript {

    public CalcEquationScript() {
        super("calcEquation", "calcEquation.desc");
    }

    private String trimPointZero(String s) {
        if (s.endsWith(".0")) {
            return s.substring(0, s.length() - 2);
        }
        return s;
    }

    @Override
    public String parse(String text) throws ScriptException {
        try {
            ScriptEngineManager mgr = new ScriptEngineManager();
            ScriptEngine engine = mgr.getEngineByName("JavaScript");


            text = " " + text.replaceAll("\\s+", " ") + " ";
            text = text.replace(". ", " ");
            text = text.replace(" .", " ");
            text = text.replace(", ", " ");
            text = text.replace(" .", " ");

            String[] possibleValues = {".", ","};
            String delim = getInput(scriptHelper.getLocalizedString("calcEquation.delim"),
                    possibleValues, possibleValues[0]);

            if (delim == null) {
                delim = ".";
            }

            if (delim.equals(".")) {
                text = text.replace(",", " ");
            } else if (delim.equals(",")) {
                text = text.replace(",", ".");
            }

            String result = engine.eval(text.trim()).toString();

            String resultStr = null;
            boolean printEquation = getYesNo(scriptHelper.getLocalizedString("calcEquation.printEqu"));
            if (printEquation) {
                resultStr = text.trim();
                if (resultStr.endsWith("=") == false) {
                    resultStr += " =";
                }
                resultStr += " " + trimPointZero(result);
            } else {
                resultStr = result;
            }

            return resultStr;
        } catch (javax.script.ScriptException ex) {
            throw new ScriptException(ex);
        }
    }
    
    public static void main(String[] args) throws ScriptException {
       CalcEquationScript ces = new CalcEquationScript();
       String equ = "2 + 2 * 2";
       
       System.out.println("equ = " + ces.parse(equ));
    }
}
