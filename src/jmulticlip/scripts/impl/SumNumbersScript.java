package jmulticlip.scripts.impl;

import jmulticlip.scripts.LocalizedAbstractScript;

/**
 *
 * @author Revers
 */
public class SumNumbersScript extends LocalizedAbstractScript {

    public SumNumbersScript() {
        super("sumNumbers", "sumNumbers.desc");
    }

    private String trimPointZero(String s) {
        if (s.endsWith(".0")) {
            return s.substring(0, s.length() - 2);
        }
        return s;
    }

    @Override
    public String parse(String text) {
        text = " " + text.replaceAll("\\s+", " ") + " ";
        text = text.replace(". ", " ");
        text = text.replace(" .", " ");
        text = text.replace(", ", " ");
        text = text.replace(" .", " ");

        String[] possibleValues = {".", ","};
        String delim = getInput(scriptHelper.getLocalizedString("sumNumbers.delim"),
                possibleValues, possibleValues[0]);

        if (delim == null) {
            delim = ".";
        }

        if (delim.equals(".")) {
            text = text.replace(",", " ");
        } else if (delim.equals(",")) {
            text = text.replace(",", ".");
        }

        text = text.replaceAll("[^\\d \\.]+", "").trim();

        String[] numbers = text.split("\\s+");

        float result = 0;
        float[] floats = new float[numbers.length];
        for (int i = 0; i < numbers.length; i++) {
            float f = Float.parseFloat(numbers[i]);
            floats[i] = f;
            result += f;
        }

        String resultStr = null;

        boolean printEquation = getYesNo(scriptHelper.getLocalizedString("sumNumbers.printEqu"));
        if (printEquation) {
            resultStr = "";
            for (int i = 0; i < numbers.length; i++) {

                resultStr += trimPointZero(Float.toString(floats[i]));

                if (i < numbers.length - 1) {

                    resultStr += " + ";
                } else {
                    resultStr += " = ";
                }
            }
            resultStr += trimPointZero(Float.toString(result));
        } else {
            resultStr = trimPointZero(Float.toString(result));
        }

        return resultStr;
    }
}
