package jmulticlip.scripts.impl;

import jmulticlip.scripts.LocalizedAbstractScript;
import jmulticlip.scripts.ScriptHelper;

/**
 *
 * @author Revers
 */
public class ForumYoutubeScript extends LocalizedAbstractScript {

    public ForumYoutubeScript() {
        super("forumYoutube", "forumYoutube.desc");
    }

    @Override
    public String parse(String text) {
        StringBuilder builder = new StringBuilder();
        String[] lines = text.split("\\n");
        for (int i = 0; i < lines.length; i++) {
            if (lines[i].trim().equals("")) {
                builder.append(lines[i]);
            } else {
                builder.append("[youtube]").append(lines[i]).append("[/youtube]");
            }
            builder.append('\n');

        }

        return ScriptHelper.removeLastEmptyLine(builder);
    }
}
