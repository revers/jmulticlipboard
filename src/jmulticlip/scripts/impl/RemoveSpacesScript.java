package jmulticlip.scripts.impl;

import jmulticlip.scripts.LocalizedAbstractScript;

/**
 *
 * @author Revers
 */
public class RemoveSpacesScript extends LocalizedAbstractScript {

    public RemoveSpacesScript() {
        super("removeSpaces", "removeSpaces.desc");
    }

    @Override
    public String parse(String text) {

        text = text.replace('\n', ' ');
        text = text.replaceAll("\\s+", "");

        return text;
    }
}