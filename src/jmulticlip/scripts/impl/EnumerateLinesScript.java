package jmulticlip.scripts.impl;

import jmulticlip.scripts.LocalizedAbstractScript;
import jmulticlip.scripts.ScriptHelper;

/**
 *
 * @author Revers
 */
public class EnumerateLinesScript extends LocalizedAbstractScript {

    public EnumerateLinesScript() {
        super("enumerateLines", "enumerateLines.desc");
    }

    @Override
    public String parse(String text) {

        String[] parts = text.split("\n");
        StringBuilder sb = new StringBuilder();

        int counter = 1;
        for (String part : parts) {
            sb.append((counter++)).append(". ").append(part).append('\n');
        }

        return ScriptHelper.removeLastEmptyLine(sb);
    }
}
