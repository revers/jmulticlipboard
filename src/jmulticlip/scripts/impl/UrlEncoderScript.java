package jmulticlip.scripts.impl;

import java.io.UnsupportedEncodingException;
import java.net.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import jmulticlip.scripts.LocalizedAbstractScript;
import jmulticlip.scripts.ScriptException;

/**
 *
 * @author Revers
 */
public class UrlEncoderScript extends LocalizedAbstractScript {

    public UrlEncoderScript() {
        super("urlEncoder", "urlEncoder.desc");
    }

    @Override
    public String parse(String text) throws ScriptException {
        try {
            return URLEncoder.encode(text, "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            throw new ScriptException(ex);
        }
    }
}
