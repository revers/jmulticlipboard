package jmulticlip.scripts.impl;

import jmulticlip.scripts.*;
import jmulticlip.scripts.impl.cpp.CppHeaderGenerator;

/**
 *
 * @author Revers
 */
public class CppSepareteInlinesScript extends LocalizedAbstractScript {

    public CppSepareteInlinesScript() {
        super("cppSepareteInlines", "cppSepareteInlines.desc");
    }

    @Override
    public String parse(String text) throws ScriptException {
        CppHeaderGenerator gen = new CppHeaderGenerator();
        String result = null;
        String className = getInput(scriptHelper.getLocalizedString("cppSepareteInlines.className"));
        if (className == null) {
            return result;
        }

        try {
            result = gen.generateInline(className, text);
        } catch (Exception e) {
            throw new ScriptException(e);
        }

        return result;
    }
}
