package jmulticlip.scripts.impl;

import jmulticlip.scripts.LocalizedAbstractScript;
import jmulticlip.scripts.ScriptHelper;

/**
 *
 * @author Revers
 */
public class JavaArrayScript extends LocalizedAbstractScript {

    public JavaArrayScript() {
        super("javaArray", "javaArray.desc");
    }

    @Override
    public String parse(String text) {
        text = text.replace("\\", "\\\\");
        text = text.replace("\"", "\\\"");

        StringBuilder result = new StringBuilder();
        result.append("{ ");
        String[] tokens = text.split("\n");
        for (int i = 0; i < tokens.length; i++) {
            result.append("\"").append(tokens[i]).append("\",\n");
        }
        result.append("};");

        return ScriptHelper.removeLastEmptyLine(result);
    }
}
