package jmulticlip.scripts.impl;

import jmulticlip.scripts.LocalizedAbstractScript;
import jmulticlip.scripts.ScriptHelper;

/**
 *
 * @author Revers
 */
public class InterlineScript extends LocalizedAbstractScript {

    public InterlineScript() {
        super("interline", "interline.desc");
    }

    @Override
    public String parse(String text) {

        String[] parts = text.split("\n");
        StringBuilder sb = new StringBuilder();


        for (int i = 0; i < parts.length; i++) {
            sb.append(parts[i]).append('\n');

            if (i < parts.length - 1) {
                sb.append('\n');
            }
        }

        return ScriptHelper.removeLastEmptyLine(sb);
    }
}
