package jmulticlip.scripts.impl;

import jmulticlip.scripts.LocalizedAbstractScript;

/**
 *
 * @author Revers
 */
public class InvertLinesScript extends LocalizedAbstractScript {

    public InvertLinesScript() {
        super("invertLines", "invertLines.desc");
    }

    @Override
    public String parse(String[] clipTextRows) {

        if (clipTextRows.length == 1) {
            return parse(clipTextRows[0]);
        }

        StringBuilder sb = new StringBuilder();
        for (int i = clipTextRows.length - 1; i >= 0; i--) {
            sb.append(parse(clipTextRows[i])).append('\n');
        }

        return sb.toString();
    }

    @Override
    public String parse(String text) {
        String[] parts = text.split("\n");

        StringBuilder sb = new StringBuilder();

        for (int i = parts.length - 1; i >= 0; i--) {
            sb.append(parts[i]).append('\n');
        }

        return sb.toString();
    }
}
