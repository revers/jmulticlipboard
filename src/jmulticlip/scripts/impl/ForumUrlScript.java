package jmulticlip.scripts.impl;

import java.awt.event.KeyEvent;
import jmulticlip.scripts.LocalizedAbstractScript;

/**
 *
 * @author Revers
 */
public class ForumUrlScript extends LocalizedAbstractScript {

    public ForumUrlScript() {
        super("forumUrl", "forumUrl.desc");
    }

    @Override
    public String parse(String text) {
        return "[url=" + text + "][/url]";
    }

    @Override
    public void afterPaste() {
        scriptHelper.pressKeybordKey(KeyEvent.VK_LEFT, 6);
    }
}
