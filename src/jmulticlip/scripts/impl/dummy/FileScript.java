package jmulticlip.scripts.impl.dummy;

import jmulticlip.scripts.LocalizedAbstractScript;

/**
 *
 * @author Revers
 */
public class FileScript extends LocalizedAbstractScript {

    public static final FileScript INSTANCE = new FileScript();

    public FileScript() {
        super("file", "file.desc");
    }

    @Override
    public String parse(String text) {
        return null;
    }
}
