package jmulticlip.scripts.impl;

import jmulticlip.scripts.*;
import jmulticlip.scripts.impl.cpp.CppHeaderGenerator;

/**
 *
 * @author Revers
 */
public class CppDefsFromPrototypesScript extends LocalizedAbstractScript {

    public CppDefsFromPrototypesScript() {
        super("cppDefsFromPrototypes", "cppDefsFromPrototypes.desc");
    }

    @Override
    public String parse(String text) throws ScriptException {
        CppHeaderGenerator gen = new CppHeaderGenerator();
        String result = null;
        String className = getInput(scriptHelper.getLocalizedString("cppDefsFromPrototypes.className"));
        if (className == null) {
            return result;
        }

        try {
            result = gen.generateByPrototypes(className, text);
        } catch (Exception e) {
            throw new ScriptException(e);
        }

        return result;
    }
}
