package jmulticlip.scripts.impl;

import jmulticlip.scripts.LocalizedAbstractScript;

/**
 *
 * @author Revers
 */
public class OneLineScript extends LocalizedAbstractScript {

    public OneLineScript() {
        super("oneLine", "oneLine.desc");
    }

    @Override
    public String parse(String text) {
        String delim = getInput(scriptHelper.getLocalizedString("oneLine.delim"), ", ");
        if (delim == null) {
            delim = ", ";
        }

        String[] parts = text.split("\n");
        StringBuilder sb = new StringBuilder();

        for (String part : parts) {
            if (part.trim().equals("")) {
                continue;
            }

            sb.append(part.trim()).append(delim);

        }

        if (sb.length() >= delim.length()) {
            return sb.substring(0, sb.length() - delim.length());
        }
        return sb.toString();
    }
}
