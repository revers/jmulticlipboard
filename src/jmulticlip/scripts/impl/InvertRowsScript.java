package jmulticlip.scripts.impl;

import java.awt.event.KeyEvent;
import javax.swing.KeyStroke;
import jmulticlip.scripts.LocalizedAbstractScript;

/**
 *
 * @author Revers
 */
public class InvertRowsScript extends LocalizedAbstractScript {

    public InvertRowsScript() {
        super("invertRows", "invertRows.desc");
    }

    @Override
    public String parse(String[] clipTextRows) {

        if (clipTextRows.length == 1) {
            return clipTextRows[0];
        }

        StringBuilder sb = new StringBuilder();
        for (int i = clipTextRows.length - 1; i >= 0; i--) {
            sb.append(clipTextRows[i]).append('\n');
        }

        return sb.toString();
    }

    @Override
    public String parse(String text) {
        return text;
    }

    public static void main(String[] args) {
        InvertRowsScript is = new InvertRowsScript();
        System.out.println(is.getKeyStroke().toString());
        System.out.println("name = " + is.getName() + "; desc = " + is.getDescription());
        //  is.setKeyStroke(KeyStroke.getKeyStroke(KeyEvent.VK_I, 0));
    }
}
