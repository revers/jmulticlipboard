package jmulticlip.scripts.impl;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import jmulticlip.scripts.LocalizedAbstractScript;
import jmulticlip.scripts.ScriptException;

/**
 *
 * @author Revers
 */
public class UrlDecoderScript extends LocalizedAbstractScript {

    public UrlDecoderScript() {
        super("urlDecoder", "urlDecoder.desc");
    }

    @Override
    public String parse(String text) throws ScriptException {
        try {
            return URLDecoder.decode(text, "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            throw new ScriptException(ex);
        }
    }
}
