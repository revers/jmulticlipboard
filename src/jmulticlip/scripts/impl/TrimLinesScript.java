package jmulticlip.scripts.impl;

import jmulticlip.scripts.LocalizedAbstractScript;
import jmulticlip.scripts.ScriptHelper;

/**
 *
 * @author Revers
 */
public class TrimLinesScript extends LocalizedAbstractScript {

    public TrimLinesScript() {
        super("trimLines", "trimLines.desc");
    }

    @Override
    public String parse(String text) {

        String[] parts = text.split("\n");
        StringBuilder sb = new StringBuilder();

        for (String part : parts) {
            sb.append(part.trim()).append('\n');

        }

        return ScriptHelper.removeLastEmptyLine(sb);
    }
}
