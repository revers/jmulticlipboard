package jmulticlip.scripts.impl;

import jmulticlip.scripts.LocalizedAbstractScript;

/**
 *
 * @author Revers
 */
public class SpaceToPercentTwentyScript extends LocalizedAbstractScript {

    public SpaceToPercentTwentyScript() {
        super("spaceToPercentTwenty", "spaceToPercentTwenty.desc");
    }

    @Override
    public String parse(String text) {
        return text.replace(" ", "%20");
    }
}
