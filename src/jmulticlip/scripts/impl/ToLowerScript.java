package jmulticlip.scripts.impl;

import jmulticlip.scripts.LocalizedAbstractScript;

/**
 *
 * @author Revers
 */
public class ToLowerScript extends LocalizedAbstractScript {

    public ToLowerScript() {
        super("toLower", "toLower.desc");
    }

    @Override
    public String parse(String text) {
        return text.toLowerCase();
    }
}
