package jmulticlip.scripts.impl.cpp;
public class ParsingException extends Exception {
	public ParsingException(String msg) {
		super(msg);
	}
}