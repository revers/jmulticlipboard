package jmulticlip.scripts.impl.cpp;

import java.util.*;
import java.util.regex.*;

public class CppHeaderGenerator {

	private static final String[] LINES = {
			"Chunk(unsigned int id, const char* name, char* dataBegin, int dataLength) :",
			"    id(id), name(name), parent(NULL), subChunkList(NULL) {",
			"        data = new ChunkData(dataBegin, dataLength);",
			"    }",
			"",
			"    Chunk(Chunk* parent, unsigned int id, const char* name, char* dataBegin, int dataLength) :",
			"    id(id), name(name), parent(parent), subChunkList(NULL) {",
			"        data = new ChunkData(dataBegin, dataLength);", "    }",
			"", "    void addSubChunk(Chunk* chunk) {",
			"        if (subChunkList == NULL) {",
			"            subChunkList = new ChunkList();", "        }", "",
			"        subChunkList->push_back(chunk);", "    }", "",
			"    ChunkData* getChunkData() {", "        return data;", "    }",
			"", "    ChunkData* getData() const {", "        return data;",
			"    }", "", "    unsigned int getId() const {",
			"        return id;", "    }", "",
			"    const char* getName() const {", "        return name;",
			"    }", "", "    Chunk* getParent() const {",
			"        return parent;", "    }", "",
			"    ChunkList* getSubChunks() const {",
			"        return subChunkList;", "    }", "", "    ~Chunk() {",
			"        delete data;", "        if (subChunkList != NULL) {",
			"            delete subChunkList;", "        }", "    }",
			" std::string toString();", "    ",
			"    friend std::ostream & operator<<(std::ostream&, HexString&);", };

	private static final String PROTOTYPES_WHOLE = "static void printChunkDescTree(Array<ChunkDesc*> *tree, int level);\n"
			+ "\n"
			+ "    static Array<ChunkDesc*> * getChunkTree(std::vector<std::string> &data,\n"
			+ "            int level, int index) throw (std::runtime_error);\n"
			+ "\n"
			+ "    static int getElementCount(ChunkDesc &chunk);\n"
			+ "\n"
			+ " virtual ~ChunkDesc();\n"
			+ "\n"
			+ "    static Array<ChunkDesc*> * getChunkDescTree() throw (std::runtime_error);\n"
			+ "    static void printChunkDescTree(Array<ChunkDesc*> *tree);";

	private static final String LINES_WHOLE = "Chunk(unsigned int id, const char* name, char* dataBegin, int dataLength) :\n"
			+ "    id(id), name(name), parent(NULL), subChunkList(NULL) {\n"
			+ "        data = new ChunkData(dataBegin, dataLength);\n"
			+ "    }\n"
			+ "\n"
			+ "    Chunk(Chunk* parent, unsigned int id, const char* name, char* dataBegin, int dataLength) :\n"
			+ "    id(id), name(name), parent(parent), subChunkList(NULL) {\n"
			+ "        data = new ChunkData(dataBegin, dataLength);\n"
			+ "    }\n"
			+ "\n"
			+ "    void addSubChunk(Chunk* chunk) {\n"
			+ "        if (subChunkList == NULL) {\n"
			+ "            subChunkList = new ChunkList();\n"
			+ "        }\n"
			+ "\n"
			+ "        subChunkList->push_back(chunk);\n"
			+ "    }\n"
			+ "\n"
			+ "    ChunkData* getChunkData() {\n"
			+ "        return data;\n"
			+ "    }\n"
			+ "\n"
			+ "    ChunkData* getData() const {\n"
			+ "        return data;\n"
			+ "    }\n"
			+ "\n"
			+ "    unsigned int getId() const {\n"
			+ "        return id;\n"
			+ "    }\n"
			+ "\n"
			+ "    const char* getName() const {\n"
			+ "        return name;\n"
			+ "    }\n"
			+ "\n"
			+ "    Chunk* getParent() const {\n"
			+ "        return parent;\n"
			+ "    }\n"
			+ "\n"
			+ "    ChunkList* getSubChunks() const {\n"
			+ "        return subChunkList;\n"
			+ "    }\n"
			+ "\n"
			+ "    ~Chunk() {\n"
			+ "        delete data;\n"
			+ "        if (subChunkList != NULL) {\n"
			+ "            delete subChunkList;\n"
			+ "        }\n"
			+ "    }\n"
			+ "std::string toString() { aaa }\n"
			+ "    \n"
			+ "    friend std::ostream & operator<<(std::ostream&, HexString&) { bbb }\n"
			+ "    virtual ~ChunkDesc() {\n"
			+ "        \n"
			+ "    }\n"
			+ "\n"
			+ "    static Array<ChunkDesc*> * getChunkDescTree(); \n";

	private static final String INLINE_WHOLE = "friend Vector4D operator*(const Matrix44& m, const Vector4D& v) {\n"
			+ "                Vector4D result;\n"
			+ "                result.x = v.x * m[0][0] + v.y * m[1][0] + v.z * m[2][0] + v.w * m[3][0];\n"
			+ "                result.y = v.x * m[0][1] + v.y * m[1][1] + v.z * m[2][1] + v.w * m[3][1];\n"
			+ "                result.z = v.x * m[0][2] + v.y * m[1][2] + v.z * m[2][2] + v.w * m[3][2];\n"
			+ "                result.w = v.x * m[0][3] + v.y * m[1][3] + v.z * m[2][3] + v.w * m[3][3];\n"
			+ "                return result;\n"
			+ "            }\n"
			+ "\n"
			+ "            friend Vector4D operator *(const Vector4D &v, const Matrix44 &m) {\n"
			+ "                Vector4D result;\n"
			+ "                result.x = v.dotProduct(m[0]);\n"
			+ "                result.y = v.dotProduct(m[1]);\n"
			+ "                result.z = v.dotProduct(m[2]);\n"
			+ "                result.w = v.dotProduct(m[3]);\n"
			+ "\n"
			+ "                return result;\n"
			+ "            }\n"
			+ "\n"
			+ "            friend Matrix44 operator *(const Matrix44 &m, float f) {\n"
			+ "                Matrix44 result(m);\n"
			+ "                result *= f;\n"
			+ "                return result;\n"
			+ "            }\n"
			+ "\n"
			+ "            friend Matrix44 operator *(float f, const Matrix44 &m) {\n"
			+ "                Matrix44 result(m);\n"
			+ "                result *= f;\n"
			+ "                return result;\n" + "            }";

	private static final String NEW_LINE = "%%%NL%%%";
	private static final String[] IGNORED = { "private:", "public:",
			"protected:" };
	private static final String[] SOURCE_BANNED_KEYWORDS = { "virtual ",
			"static ", "inline " };

	private class Member {
		private String declaration;
		private String definition;
		private String className;
		private String memberData;

		private Member(String className, String memberData)
				throws ParsingException {
			this.memberData = memberData.replace(NEW_LINE, "\n");

			Pattern ptrn = Pattern.compile("^.*?\\s*?(\\S+?\\().*?\\{");
			Matcher mchr = ptrn.matcher(memberData);
			// System.out.println("member = " + memberData);

			if (mchr.find()) {
				declaration = mchr.group();
				if (declaration.trim().startsWith(className)) {
					int idx = findEndingBracket(declaration, mchr.end(1));
					if (idx == -1) {
						throw new ParsingException(
								"Nie moge znalezc znaku zamykajacy nawias listy argumentow! DLA:\n\t"
										+ mchr.group().replace(NEW_LINE, "\n"));
					}
					declaration = declaration.substring(0, idx + 1);
				} else {
					declaration = declaration.replaceAll("\\s*?\\{$", "");
				}

				declaration = declaration.replace(NEW_LINE, "\n").trim() + ";";
				// System.out.println("declaration = " + declaration);

				if (className.equals("") == false) {
					definition = memberData.substring(0, mchr.start(1))
							+ className + "::"
							+ memberData.substring(mchr.start(1));
				} else {
					definition = memberData;
				}
				definition = definition.replace(NEW_LINE, "\n").trim();
				for (String word : SOURCE_BANNED_KEYWORDS) {
					if (definition.startsWith(word)) {
						definition = definition.substring(word.length()).trim();
					}
				}
				// System.out.println("definition = " + definition);

			}
		}

		private int findEndingBracket(String s, int openBracketPos) {

			// System.out.println("s = " + s.substring(openBracketPos));
			char[] carr = s.toCharArray();
			int openBracketCount = 0;
			for (int i = openBracketPos; i < carr.length; i++) {
				if (carr[i] == '(') {
					openBracketCount++;
				} else if (carr[i] == ')') {
					if (openBracketCount == 0) {
						return i;
					} else {
						openBracketCount--;
					}
				}
			}
			return -1;
		}

		public String getDeclaration() {
			return declaration;
		}

		public String getDefinition() {
			return definition;
		}

		public String getClassName() {
			return className;
		}

		public String getMemberData() {
			return memberData;
		}

		@Override
		public String toString() {
			return "DECLARATION\n\t" + declaration + "\nDEFINITION:\n"
					+ definition;
		}
	}

	public String generate(String className, String text)
			throws ParsingException {

		ArrayList<Member> memberList = new ArrayList<Member>();

		boolean parsingMember = false;
		String memberData = "";
		int openBrackets = -1;
		int closeBrackets = 0;

		String[] lines = text.split("\n");
		for (String line : lines) {
			for (String ignored : IGNORED) {
				if (line.contains(ignored))
					continue;
			}

			// line = line.trim();
			// if (line.trim().equals("")) {
			// continue;
			// }

			if (!parsingMember && line.trim().equals("") == false) {
				memberData = "";// line + "\n";
				openBrackets = -1;
				closeBrackets = 0;
				parsingMember = true;
			}

			if (parsingMember == false)
				continue;

			int idx = line.indexOf('{');
			if (idx != -1 && openBrackets == -1) {
				openBrackets = 1;
			} else if (idx != -1) {
				openBrackets++;
			}

			idx = line.indexOf('}');
			if (idx != -1) {
				closeBrackets++;
			}
			memberData += line;
			if (openBrackets == closeBrackets) {
				parsingMember = false;
				Member mem = new Member(className, memberData);
				memberList.add(mem);
			} else {
				memberData += NEW_LINE;
			}
		}

		StringBuilder builder = new StringBuilder();
		for (Member mem : memberList) {
			builder.append(mem.getDeclaration()).append("\n\n");
		}

		builder
				.append("// ---------------------------------------------------------------------------------------------------------\n\n");
		for (Member mem : memberList) {
			builder.append(mem.getDefinition()).append("\n\n");
		}

		if (builder.length() == 0) {
			throw new ParsingException(
					"Nie znaleziono �adnej definicji funkcji!");
		}

		return builder.toString();
	}

	public String generateInline(String className, String text)
			throws ParsingException {

		ArrayList<Member> memberList = new ArrayList<Member>();

		boolean parsingMember = false;
		String memberData = "";
		int openBrackets = -1;
		int closeBrackets = 0;

		String[] lines = text.split("\n");
		for (String line : lines) {
			for (String ignored : IGNORED) {
				if (line.contains(ignored))
					continue;
			}

			// line = line.trim();
			// if (line.trim().equals("")) {
			// continue;
			// }

			if (!parsingMember && line.trim().equals("") == false) {
				memberData = "";// line + "\n";
				openBrackets = -1;
				closeBrackets = 0;
				parsingMember = true;
			}

			if (parsingMember == false)
				continue;

			int idx = line.indexOf('{');
			if (idx != -1 && openBrackets == -1) {
				openBrackets = 1;
			} else if (idx != -1) {
				openBrackets++;
			}

			idx = line.indexOf('}');
			if (idx != -1) {
				closeBrackets++;
			}
			memberData += line;
			if (openBrackets == closeBrackets) {
				parsingMember = false;
				Member mem = new Member(className, memberData);
				memberList.add(mem);
			} else {
				memberData += NEW_LINE;
			}
		}

		StringBuilder builder = new StringBuilder();
		for (Member mem : memberList) {
			builder.append("inline ").append(mem.getDeclaration()).append(
					"\n\n");
		}

		builder
				.append("// ---------------------------------------------------------------------------------------------------------\n\n");
		for (Member mem : memberList) {
			builder.append(mem.getDefinition()).append("\n\n");
		}

		if (builder.length() == 0) {
			throw new ParsingException(
					"Nie znaleziono �adnej definicji funkcji!");
		}

		return builder.toString();
	}

	public String generateByPrototypes(String className, String whole)
			throws ParsingException {
		StringBuilder builder = new StringBuilder();

		String lines = whole.replace("\n", NEW_LINE);

		Pattern ptrn = Pattern.compile(".*?\\s*?(\\S+?\\().*?\\;");

		Matcher mchr = ptrn.matcher(lines);
		while (mchr.find()) {
			String group = mchr.group();

			if (className.equals("") == false) {
				group = lines.substring(mchr.start(), mchr.start(1))
						+ className + "::"
						+ lines.substring(mchr.start(1), mchr.end());
			}

			group = group.replace(NEW_LINE, "\n").trim();
			group = group.replace(";", " {\n}");

			for (String word : SOURCE_BANNED_KEYWORDS) {
				if (group.startsWith(word)) {
					group = group.substring(word.length()).trim();
				}
			}
			builder.append(group).append("\n\n");
		}

		if (builder.length() == 0) {
			throw new ParsingException(
					"Nie znaleziono �adnego prototypu funkcji!");
		}

		return builder.toString();
	}

	/**
	 * @param args
	 * @throws ParsingException
	 */

	public static void main(String[] args) throws ParsingException {
		CppHeaderGenerator gen = new CppHeaderGenerator();
		String s = gen.generateInline("Matrix33", INLINE_WHOLE);

		// String s = gen.generate("Chunk", LINES_WHOLE);
		System.out.println(s);
	}

}
