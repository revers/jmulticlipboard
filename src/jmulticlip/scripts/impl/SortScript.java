package jmulticlip.scripts.impl;

import java.util.Arrays;
import java.util.Comparator;
import jmulticlip.scripts.LocalizedAbstractScript;

/**
 *
 * @author Revers
 */
public class SortScript extends LocalizedAbstractScript {

    public SortScript() {
        super("sort", "sort.desc");
    }

    @Override
    public String parse(String text) {

        String[] parts = text.trim().split("\n+");
        Arrays.sort(parts);

        StringBuilder sb = new StringBuilder();
        for (String part : parts) {
            sb.append(part).append('\n');
        }
        return sb.toString();
    }
}
