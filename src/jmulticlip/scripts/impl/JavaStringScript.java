package jmulticlip.scripts.impl;

import jmulticlip.scripts.LocalizedAbstractScript;

/**
 *
 * @author Revers
 */
public class JavaStringScript extends LocalizedAbstractScript {

    public JavaStringScript() {
        super("javaString", "javaString.desc");
    }

    @Override
    public String parse(String text) {
        text = text.replace("\\", "\\\\");
        text = text.replace("\"", "\\\"");

        StringBuilder result = new StringBuilder();
        String[] tokens = text.split("\n");

        for (int i = 0; i < tokens.length; i++) {
            result.append('"').append(tokens[i]);
            if (i < tokens.length - 1) {
                result.append("\\n\" +\n");
            } else {
                result.append('"');
            }
        }
        return result.toString();
    }
}
