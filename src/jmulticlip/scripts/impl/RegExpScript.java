package jmulticlip.scripts.impl;

import jmulticlip.scripts.LocalizedAbstractScript;

/**
 *
 * @author Revers
 */
public class RegExpScript extends LocalizedAbstractScript {

    public RegExpScript() {
        super("regExp", "regExp.desc");
    }

    @Override
    public String parse(String text) {
        text = text.replace("\\", "\\\\");
        text = text.replace("\"", "\\\"");
        text = text.replace("[", "\\[");
        text = text.replace("]", "\\]");
        text = text.replace("*", "\\*");
        text = text.replace("+", "\\+");
        text = text.replace("?", "\\?");
        text = text.replace(".", "\\.");
        text = text.replace("^", "\\^");
        text = text.replace("$", "\\$");
        text = text.replace("&", "\\&");
        text = text.replace("|", "\\|");
        text = text.replace("{", "\\{");
        text = text.replace("}", "\\}");
        text = text.replace(")", "\\)");
        text = text.replace("(", "\\(");
        return text;
    }
}
