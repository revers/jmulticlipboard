package jmulticlip.scripts.impl;

import jmulticlip.scripts.LocalizedAbstractScript;

/**
 *
 * @author Revers
 */
public class ToUpperScript extends LocalizedAbstractScript {

    public ToUpperScript() {
        super("toUpper", "toUpper.desc");
    }

    @Override
    public String parse(String text) {
        return text.toUpperCase();
    }
}
