package jmulticlip.i18n;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import pl.ugu.revers.logging.i18n.Internationalizable;
import java.util.*;
import javax.swing.UIManager;
import jmulticlip.config.Options;
import jmulticlip.core.ErrorManager;

/**
 *
 * @author Revers
 */
public class I18NManager {

    private static final String LANG_DIRECTORY = "config/";
    private ArrayList<Internationalizable> list = new ArrayList<Internationalizable>();
    private ResourceBundleClassLoader loader = new ResourceBundleClassLoader();
    private ResourceBundle resourceBundle;

    private I18NManager() {
        Options o = Options.getInstance();

        Locale lang = o.getLanguage();
        // changeLanguage(lang);
    }

    public static I18NManager getInstance() {
        return InternalizationManagerHolder.INSTANCE;
    }

    private static class InternalizationManagerHolder {

        private static final I18NManager INSTANCE = new I18NManager();
    }

    public void addInternationalizable(Internationalizable inter) {
        list.add(inter);
    }

    public void removeInternationalizable(Internationalizable inter) {
        list.remove(inter);
    }

    public void changeLanguage(Locale lang) {
        resourceBundle = ResourceBundle.getBundle(LANG_DIRECTORY + "lang", lang, loader);
        localizeSwingComponents();

        for (Internationalizable i : list) {
            i.changeLanguage(lang);
        }
    }

    public String getString(String key) {
        try {
            return resourceBundle.getString(key);
        } catch (MissingResourceException e) {
            ErrorManager.errorMinor(I18NManager.class, e, "Cannot find '"
                    + key + "' property in 'lang_"
                    + Options.getInstance().getLanguage().getLanguage() + "' file!");
            return '!' + key + '!';
        }
    }

    private void localizeSwingComponents() {
        UIManager.put("FileChooser.saveDialogTitleText", getString("saveDots"));
        UIManager.put("FileChooser.cancelButtonText", getString("cancel"));
        UIManager.put("FileChooser.saveButtonText", getString("save"));

        UIManager.put("FileChooser.openDialogTitleText", getString("openDots"));
        UIManager.put("FileChooser.cancelButtonText", getString("cancel"));
        UIManager.put("FileChooser.openButtonText", getString("open"));

        UIManager.put("OptionPane.cancelButtonText", getString("cancel"));
        UIManager.put("OptionPane.noButtonText", getString("no"));
        UIManager.put("OptionPane.okButtonText", getString("ok"));
        UIManager.put("OptionPane.yesButtonText", getString("yes"));

        UIManager.put("ColorChooser.cancelText", "Anuluj");
        UIManager.put("ColorChooser.okText", "OK");
        UIManager.put("ColorChooser.previewText", "Podgląd");
        UIManager.put("ColorChooser.resetText", "Reset");
        UIManager.put("ColorChooser.rgbBlueText", "Niebieski");
        UIManager.put("ColorChooser.rgbGreenText", "Zielony");
        UIManager.put("ColorChooser.rgbRedText", "Czerowny");
        UIManager.put("ColorChooser.sampleText", "Przykładowy tekst");
        UIManager.put("ColorChooser.swatchesNameText", "Próbki");
        UIManager.put("ColorChooser.swatchesRecentText", "Ostatnie:");
    }

    private class ResourceBundleClassLoader extends ClassLoader {

        @Override
        protected URL findResource(String name) {

            File f = new File(name);

            try {
                return f.toURI().toURL();
            } catch (MalformedURLException e) {
                ErrorManager.errorMinor(ResourceBundleClassLoader.class, e,
                        "Cannon transform File(\"" + f.getAbsolutePath() + "\") to URL!!");
            }
            return super.findResource(name);
        }
    }

    public static void main(String[] args) {
        I18NManager i18n = I18NManager.getInstance();
        System.out.println(i18n.getString("yes"));
        i18n.changeLanguage(new Locale("pl"));
        System.out.println(i18n.getString("yes"));
    }
}
