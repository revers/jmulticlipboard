package jmulticlip.ifaces;

/**
 *
 * @author Revers
 */
public interface NotificationListener {
    public void notify(String msg);
}
