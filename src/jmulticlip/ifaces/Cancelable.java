package jmulticlip.ifaces;

/**
 *
 * @author Revers
 */
public interface Cancelable {
    public void cancel();
}
