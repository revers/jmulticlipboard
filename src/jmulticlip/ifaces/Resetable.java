package jmulticlip.ifaces;

/**
 *
 * @author Revers
 */
public interface Resetable {

    public void reset();
}
