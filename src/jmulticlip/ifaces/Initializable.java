package jmulticlip.ifaces;

/**
 *
 * @author Revers
 */
public interface Initializable {

    public void init();
}
