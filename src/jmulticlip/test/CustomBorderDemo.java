package jmulticlip.test;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.border.*;

class ColorBorder implements Border {

    private int tickness = 5;
    private int margin = 5;
    private int arcLenght = tickness * 2;
    private int top = margin;
    private int left = margin;
    private int bottom = margin;
    private int right = margin;
    private Color color = Color.red;

    public int getArcLenght() {
        return arcLenght;
    }

    public void setArcLenght(int arcLenght) {
        this.arcLenght = arcLenght;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public int getMargin() {
        return margin;
    }

    public void setMargin(int margin) {
        this.margin = margin;
    }

    public int getTickness() {
        return tickness;
    }

    public void setTickness(int tickness) {
        this.tickness = tickness;

    }

    @Override
    public void paintBorder(Component c,
            Graphics g,
            int x, int y,
            int width, int height) {

        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);

        g.setColor(color);


        g.fillRoundRect(0, 0, tickness, height, arcLenght, arcLenght);
        g.fillRoundRect(width - tickness, 0, tickness, height, arcLenght, arcLenght);
        g.fillRoundRect(0, 0, width, tickness, arcLenght, arcLenght);
        g.fillRoundRect(0, height - tickness, width, tickness, arcLenght, arcLenght);

    }

    public Insets getBorderInsets(Component c) {
        return new Insets(top, left, bottom, right);
    }

    public boolean isBorderOpaque() {
        return true;
    }
}

public class CustomBorderDemo {

    public static void main(String[] a) {
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JButton button = new JButton("Aaaaaaaaaaa");
        // button.setBorder(new SimpleBorder());

        JPanel panel = new JPanel();
        panel.setLayout(new FlowLayout());
        final JPanel innerPanel = new JPanel();
        final ColorBorder border = new ColorBorder();
        innerPanel.setBorder(border);
        innerPanel.setLayout(new FlowLayout());
        //innerPanel.setP
        innerPanel.add(button);

        button.addActionListener(new ActionListener() {

            private boolean b = true;

            public void actionPerformed(ActionEvent e) {
                if (b) {
                    border.setColor(Color.green);
                } else {
                    border.setColor(Color.yellow);
                }
                b = !b;
                innerPanel.repaint();
            }
        });

        panel.setBorder(new EmptyBorder(50, 50, 50, 50));
        panel.add(innerPanel);
        frame.add(panel);
        frame.pack();
        frame.setVisible(true);
    }
}