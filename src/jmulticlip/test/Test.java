package jmulticlip.test;

import java.util.Map.Entry;
import java.util.Set;
import javax.swing.UIManager;

/**
 *
 * @author Revers
 */
public class Test {

    public static void main(String[] args) {
        Set<Entry<Object, Object>> uiProps = UIManager.getDefaults().entrySet();
        for (Entry<Object, Object> entry : uiProps) {
            System.out.format("%s = %s%n", entry.getKey(), entry.getValue());
        }
    }
}
