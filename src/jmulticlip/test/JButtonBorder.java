package jmulticlip.test;

import javax.swing.*;
import java.awt.event.*;
import jmulticlip.util.GUIUtils;

public class JButtonBorder extends JFrame {

    private JButton b1, b2;
    private JPanel p;

    public JButtonBorder() {
        super("JButtonBorder");
        p = new JPanel();
        b1 = new JButton("button with border");
        b2 = new JButton("button without border");

        b2.setBorderPainted(false);

        setContentPane(p);
        p.add(b1);
        p.add(b2);

        addWindowListener(new WindowAdapter() {

            public void windowClosing(WindowEvent event) {
                dispose();
                System.exit(0);
            }
        });

        pack();
        GUIUtils.useApplicationLAF(this);
        setVisible(true);
    }

    public static void main(String args[]) {
        JButtonBorder j = new JButtonBorder();
    }
}