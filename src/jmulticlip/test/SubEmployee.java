package jmulticlip.test;

import javax.xml.bind.annotation.*;

/**
 *
 * @author Revers
 */
@XmlRootElement(name = "subEmployee")
@XmlAccessorType(XmlAccessType.FIELD)
public class SubEmployee {

    @XmlAttribute
    private int ble;
    /**
     * Employee's first name
     */
    @XmlElement
    private String dupa;

    public SubEmployee() {
    }

    public SubEmployee(int ble, String dupa) {
        this.ble = ble;
        this.dupa = dupa;
    }

    public int getBle() {
        return ble;
    }

    public void setBle(int ble) {
        this.ble = ble;
    }

    public String getDupa() {
        return dupa;
    }

    public void setDupa(String dupa) {
        this.dupa = dupa;
    }
}
