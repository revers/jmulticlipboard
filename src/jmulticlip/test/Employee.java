package jmulticlip.test;

import java.io.*;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.annotation.*;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collections;
import javax.xml.bind.Marshaller;
import jmulticlip.scripts.config.ScriptElement;
import jmulticlip.scripts.config.ScriptXmlConfig;

@XmlRootElement(name = "employee")
@XmlAccessorType(XmlAccessType.FIELD)
public class Employee {

    public static void main(String args[]) throws JAXBException, IOException {
        final Employee john = new Employee();
        john.setId(1);
        john.setFirstName("John");
        john.setMiddleName("Robert");
        john.setLastName("Doe");
        
        john.getEmployeeList().add(new SubEmployee(0, "aaaa"));
        john.getEmployeeList().add(new SubEmployee(1, "bbbb"));
        john.getEmployeeList().add(new SubEmployee(2, "cccc"));
        john.getEmployeeList().add(new SubEmployee(3, "dddd"));
        

        // write it out as XML
        final JAXBContext jaxbContext = JAXBContext.newInstance(Employee.class);
        StringWriter writer = new StringWriter();
        PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("scripts/test.xml")));
        Marshaller m = 
        jaxbContext.createMarshaller();
          m.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
m.marshal(john, out);

        BufferedReader in = new BufferedReader(new FileReader("scripts/test.xml")) ;
        // read it from XML
        final Employee johnRead =
                (Employee) jaxbContext.createUnmarshaller().unmarshal(in);
        System.out.println(johnRead.employeeList.toString());
//        if (john.equals(johnRead)) {   // write the new object out as XML again.
//            writer = new StringWriter();
//            jaxbContext.createMarshaller().marshal(johnRead, writer);
//            System.out.println(
//                    "johnRead was identical to john: \n" + writer.toString());
//        } else {
//            System.out.println("john and johnRead are not equal");
//        }
        
        JAXBContext context = JAXBContext.newInstance(ScriptXmlConfig.class);
        BufferedReader in2 = new BufferedReader(new FileReader("scripts/scripts-config.xml")) ;
        ScriptXmlConfig scr = (ScriptXmlConfig)context.createUnmarshaller().unmarshal(in2);
     //   System.out.println(scr.getScriptElements());
        Collections.sort(scr.getScriptElements());
        for(ScriptElement se : scr.getScriptElements()) {
            System.out.println(se);
        }
    }
    @XmlAttribute
    private int id;
    /**
     * Employee's first name
     */
    @XmlElement
    private String firstName;
    /**
     * Employee's middle name
     */
    @XmlElement
    private String middleName;
    /**
     * Employee's last name
     */
    @XmlElement
    private String lastName;
    // XmLElementWrapper generates a wrapper element around XML representation
    //@XmlElementWrapper(name = "employeeList")
    // XmlElement sets the name of the entities
    @XmlElement(name = "subemployee")
    private ArrayList<SubEmployee> employeeList;

    public ArrayList<SubEmployee> getEmployeeList() {
        return employeeList;
    }

    public void setEmployeeList(ArrayList<SubEmployee> employeeList) {
        this.employeeList = employeeList;
    }

    public Employee() {
        employeeList = new ArrayList<SubEmployee>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Employee employee = (Employee) o;

        if (id != employee.id) {
            return false;
        }
        if (firstName != null ? !firstName.equals(employee.firstName)
                : employee.firstName != null) {
            return false;
        }
        if (lastName != null ? !lastName.equals(employee.lastName)
                : employee.lastName != null) {
            return false;
        }
        if (middleName != null ? !middleName.equals(employee.middleName)
                : employee.middleName != null) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        result = 31 * result + (middleName != null ? middleName.hashCode() : 0);
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Employee{"
                + "id=" + id
                + ", firstName='" + firstName + '\''
                + ", middleName='" + middleName + '\''
                + ", lastName='" + lastName + '\''
                + '}';
    }
}