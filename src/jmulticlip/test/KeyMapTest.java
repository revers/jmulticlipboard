package jmulticlip.test;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

/**
 *
 * @author Revers
 */
public class KeyMapTest {

    private static Action globalAction = new AbstractAction() {

        public void actionPerformed(ActionEvent e) {
            JOptionPane.showMessageDialog(null, "global action");
        }
    };
    private static Action textFieldAction = new AbstractAction() {

        public void actionPerformed(ActionEvent e) {
            JOptionPane.showMessageDialog(null, "textFieldMessage");
        }
    };

    public static void go() {

        JFrame frame = new JFrame();
        frame.setLayout(new FlowLayout(FlowLayout.LEFT, 50, 50));

        JTextField textField = new JTextField();
        textField.setPreferredSize(new Dimension(100, 20));
        frame.add(textField);
        
        KeyStroke ks = KeyStroke.getKeyStroke(KeyEvent.VK_K,KeyEvent.CTRL_MASK, true);
        String command = "dupa";
        textField.getInputMap().put(ks, "dupa");
         textField.getActionMap().put(command, textFieldAction);

        JButton button = new JButton("testButton");
        textField.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(ks, globalAction);

        frame.add(button);

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setBounds(200, 200, 400, 400);
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                go();
            }
        });
    }
}
