package jmulticlip.test;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.awt.geom.*;

import pl.ugu.revers.swing.*;

/**
 *
 * @author Revers
 */
public class BracketsDrawTest extends JPanel {

    private static final float DEFAULT_ARC_LENGTH = 20;
    private GeneralPath generalPath;
    private int xBracket = 30;
    private int yBracket = 30;
    private int widthBracket = 50;
    private int heightBracket = 200;

    public BracketsDrawTest() {
        generalPath = getRightCurlyBracket(xBracket, yBracket, widthBracket, heightBracket);
    }

    public GeneralPath getGeneralPath() {
        return generalPath;
    }

    public void setGeneralPath(GeneralPath generalPath) {
        this.generalPath = generalPath;
        Rectangle2D rect = generalPath.getBounds2D();
        xBracket = (int) rect.getX();
        yBracket = (int) rect.getY();
        widthBracket = (int) rect.getWidth();
        heightBracket = (int) rect.getHeight();

        //System.out.println(rect.toString());
        repaint();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;

        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);

        g2.setColor(Color.red);

        g2.drawRect(xBracket, yBracket, widthBracket, heightBracket);
        g2.setColor(Color.black);

        g2.draw(generalPath);
    }
    
    public GeneralPath getLowerCurlyBracket(float x, float y, float width, float height) {

        if (width <= 0.0f) {
            width = 1.0f;
        }
        if (height <= 0.0f) {
            height = 1.0f;
        }

        float yArcLength = DEFAULT_ARC_LENGTH;
        float xArcLength = DEFAULT_ARC_LENGTH;

        if (xArcLength * 4.0f > width) {
            xArcLength = width / 4.0f;
        }

        if (yArcLength * 2.0f > height) {
            yArcLength = height / 2.0f;
        }

        GeneralPath gp = new GeneralPath();

        float yCurrent = y;
        float xCurrent = x;
        gp.moveTo(xCurrent, yCurrent);

        yCurrent = y + height - yArcLength;
        xCurrent += xArcLength;

        float xControl = x;
        float yControl = yCurrent;

        gp.quadTo(xControl, yControl, xCurrent, yCurrent);

        float horizSegmentLength = (width - 4.0f * xArcLength) / 2.0f;

        if (horizSegmentLength > 0.0f) {
            xCurrent += horizSegmentLength;
            gp.lineTo(xCurrent, yCurrent);
        }

        yCurrent += yArcLength;
        xCurrent += xArcLength;

        xControl = xCurrent;
        gp.quadTo(xControl, yControl, xCurrent, yCurrent);

        xCurrent += xArcLength;
        yCurrent -= yArcLength;

        gp.quadTo(xControl, yControl, xCurrent, yCurrent);

        if (horizSegmentLength > 0.0f) {
            xCurrent += horizSegmentLength;
            gp.lineTo(xCurrent, yCurrent);
        }

        xCurrent += xArcLength;
        xControl = xCurrent;

        yCurrent = y;

        gp.quadTo(xControl, yControl, xCurrent, yCurrent);

        gp.moveTo(x, y);
        gp.closePath();
        return gp;
    }

    public GeneralPath getUpperCurlyBracket(float x, float y, float width, float height) {

        if (width <= 0.0f) {
            width = 1.0f;
        }
        if (height <= 0.0f) {
            height = 1.0f;
        }

        float yArcLength = DEFAULT_ARC_LENGTH;
        float xArcLength = DEFAULT_ARC_LENGTH;

        if (xArcLength * 4.0f > width) {
            xArcLength = width / 4.0f;
        }

        if (yArcLength * 2.0f > height) {
            yArcLength = height / 2.0f;
        }

        GeneralPath gp = new GeneralPath();

        float yCurrent = y + height;
        float xCurrent = x;
        gp.moveTo(xCurrent, yCurrent);

        yCurrent = y + yArcLength;
        xCurrent += xArcLength;

        float xControl = x;
        float yControl = yCurrent;

        gp.quadTo(xControl, yControl, xCurrent, yCurrent);

        float horizSegmentLength = (width - 4.0f * xArcLength) / 2.0f;

        if (horizSegmentLength > 0.0f) {
            xCurrent += horizSegmentLength;
            gp.lineTo(xCurrent, yCurrent);
        }

        yCurrent -= yArcLength;
        xCurrent += xArcLength;

        xControl = xCurrent;
        gp.quadTo(xControl, yControl, xCurrent, yCurrent);

        xCurrent += xArcLength;
        yCurrent += yArcLength;

        gp.quadTo(xControl, yControl, xCurrent, yCurrent);

        if (horizSegmentLength > 0.0f) {
            xCurrent += horizSegmentLength;
            gp.lineTo(xCurrent, yCurrent);
        }

        xCurrent += xArcLength;
        xControl = xCurrent;

        yCurrent = y + height;

        gp.quadTo(xControl, yControl, xCurrent, yCurrent);

        gp.moveTo(x, y + height);
        gp.closePath();
        return gp;
    }

    public GeneralPath getLeftCurlyBracket(float x, float y, float width, float height) {

        if (width == 0.0f) {
            width = 1.0f;
        }
        if (height == 0.0f) {
            height = 1.0f;
        }

        float yArcLength = DEFAULT_ARC_LENGTH;
        float xArcLength = DEFAULT_ARC_LENGTH;

        if (xArcLength * 2.0f > width) {
            xArcLength = width / 2.0f;
        }

        if (yArcLength * 4.0f > height) {
            yArcLength = height / 4.0f;
        }

        float xControl = x + xArcLength;
        float yControl = y;

        GeneralPath gp = new GeneralPath();

        float yCurrent = y;
        float xCurrent = x + width;
        gp.moveTo(xCurrent, yCurrent);

        yCurrent = y + yArcLength;
        xCurrent = xControl;

        gp.quadTo(xControl, yControl, xCurrent, yCurrent);

        float vertSegmentLength = (height - 4.0f * yArcLength) / 2.0f;

        if (vertSegmentLength > 0.0f) {
            yCurrent += vertSegmentLength;
            gp.lineTo(xCurrent, yCurrent);
        }

        yCurrent += yArcLength;
        xCurrent -= xArcLength;

        yControl = yCurrent;
        gp.quadTo(xControl, yControl, xCurrent, yCurrent);

        xCurrent += xArcLength;
        yCurrent += yArcLength;

        gp.quadTo(xControl, yControl, xCurrent, yCurrent);

        if (vertSegmentLength > 0.0f) {
            yCurrent += vertSegmentLength;
            gp.lineTo(xCurrent, yCurrent);
        }

        yCurrent += yArcLength;
        yControl = yCurrent;

        xCurrent = x + width;

        gp.quadTo(xControl, yControl, xCurrent, yCurrent);

        gp.moveTo(x, y);
        gp.closePath();
        return gp;
    }

    public GeneralPath getRightCurlyBracket(float x, float y, float width, float height) {

        if (width == 0.0f) {
            width = 1.0f;
        }
        if (height == 0.0f) {
            height = 1.0f;
        }

        float yArcLength = DEFAULT_ARC_LENGTH;
        float xArcLength = DEFAULT_ARC_LENGTH;

        if (xArcLength * 2.0f > width) {
            xArcLength = width / 2.0f;
        }

        if (yArcLength * 4.0f > height) {
            yArcLength = height / 4.0f;
        }

        float xControl = x + width - xArcLength;
        float yControl = y;

        GeneralPath gp = new GeneralPath();
        gp.moveTo(x, y);

        float yCurrent = y + yArcLength;
        float xCurrent = xControl;

        gp.quadTo(xControl, yControl, xCurrent, yCurrent);

        float vertSegmentLength = (height - 4.0f * yArcLength) / 2.0f;

        if (vertSegmentLength > 0.0f) {
            yCurrent += vertSegmentLength;
            gp.lineTo(xCurrent, yCurrent);
        }

        yCurrent += yArcLength;
        xCurrent += xArcLength;

        yControl = yCurrent;
        gp.quadTo(xControl, yControl, xCurrent, yCurrent);

        xCurrent -= xArcLength;
        yCurrent += yArcLength;

        gp.quadTo(xControl, yControl, xCurrent, yCurrent);

        if (vertSegmentLength > 0.0f) {
            yCurrent += vertSegmentLength;
            gp.lineTo(xCurrent, yCurrent);
        }

        yCurrent += yArcLength;
        yControl = yCurrent;

        xCurrent = x;

        gp.quadTo(xControl, yControl, xCurrent, yCurrent);

        gp.moveTo(x, y);
        gp.closePath();
        return gp;
    }

    public static void main(String[] args) {
        JFrame f = new JFrame();
        BracketsDrawTest bracketsPanel = new BracketsDrawTest();
        f.getContentPane().add(bracketsPanel);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        SwingUtils.setSizeAndCenter(f, 800, 800);

        f.setVisible(true);
    }
}
