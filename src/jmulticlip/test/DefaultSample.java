package jmulticlip.test;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.*;
import javax.swing.text.Keymap;

public class DefaultSample {

    private static void listKeys(Keymap map) {

        System.out.println("map = " + map);
        while (map != null) {
            KeyStroke[] keys = map.getBoundKeyStrokes();

            for (int i = 0; i < keys.length; i++) {
                // This method is defined in Converting a KeyStroke to a String
                String keystrokeStr = (keys[i]).toString();
                // Get the action name bound to this keystroke
                System.out.println(keystrokeStr);
                //Action action = (Action) map.getAction(keys[i]);
            }

            // The default action is invoked if a character is typed
            // and no key binding exists in the component's InputMap or Keymap.
            Action defAction = map.getDefaultAction();

            // Process all parent keymaps as well
            map = map.getResolveParent();
        }
    }

    private static void printInputMapFor(JComponent comp, int condition) {
        System.out.println("inputMap for " + comp.getClass().getName() + " with condition = " + condition);

        InputMap am = comp.getInputMap();
        if (am != null && am.allKeys() != null) {
            for (KeyStroke key : am.allKeys()) {
                System.out.printf("%s = %s%n", key, am.get(key));
            }
        } else {
            System.out.println("Zero elements :(");
        }
    }

    private static void printActionMap(JComponent comp) {
        System.out.println("actionMap for " + comp.getClass().getName());
        ActionMap am = comp.getActionMap();
        if (am != null && am.allKeys() != null) {
            for (Object key : am.allKeys()) {
                System.out.printf("%s = %s%n", key, am.get(key));
            }
        } else {
            System.out.println("Zero elements :(");
        }
    }

    public static void main(String args[]) {
        //listKeys();
        JFrame frame = new JFrame("Default Example");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Container content = frame.getContentPane();

        final JTextField textField = new JTextField();
        content.add(textField, BorderLayout.NORTH);
        
        final JButton defaultButton = new JButton("Default Button");

        final JTable table = new JTable(new Object[][]{{"aaa", "bbb", "ccc"}}, new Object[]{"co1", "col2", "col3"});
        content.add(table, BorderLayout.CENTER);

        ActionListener actionListener = new ActionListener() {

            public void actionPerformed(ActionEvent actionEvent) {
                System.out.println(actionEvent.getActionCommand() + " selected");
                Keymap map = textField.getKeymap();

                System.out.println("map = " + map);
                listKeys(map);

                JComponent comp = defaultButton;//textField;
                printInputMapFor(comp, JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
                printActionMap(comp);

            }
        };

        JPanel panel = new JPanel();
        
        defaultButton.addActionListener(actionListener);
        panel.add(defaultButton);

        JButton otherButton = new JButton("Other Button");
        otherButton.addActionListener(actionListener);
        panel.add(otherButton);


        content.add(panel, BorderLayout.SOUTH);

        textField.getInputMap().remove(null);
        Keymap keymap = textField.getKeymap();
        KeyStroke keystroke = KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0,
                false);
        // keymap.removeKeyStrokeBinding(keystroke);
//textField.getInp
        frame.getRootPane().setDefaultButton(defaultButton);

        frame.setSize(250, 150);
        frame.setVisible(true);


    }
}