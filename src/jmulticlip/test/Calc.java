package jmulticlip.test;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

public class Calc {

    public static void main(String[] args) throws Exception {
        if (args.length < 1) {
            System.err.println("Usage: java Calc <expression>");
            System.exit(1);
        }

        ScriptEngineManager mgr = new ScriptEngineManager();
        ScriptEngine engine = mgr.getEngineByName("JavaScript");
        System.out.println(engine.eval(args[0]));
    }
}
